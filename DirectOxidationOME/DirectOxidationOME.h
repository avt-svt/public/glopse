/**
 *
 * @brief File containing definition of flowsheet optimiziation problem for
 *        production of OME1 from H2 and CO2 via direct oxidation of methanol.
 *
 * ==============================================================================
 * © 2020, Process Systems Engineering (AVT.SVT), RWTH Aachen University
 * ==============================================================================
 *
 * used in:
 *    D. Bongartz: "Deterministic Global Flowsheet Optimization for the Design of Energy Conversion Processes",
 *     	            PhD Thesis, RWTH Aachen University, 2020.
 *
 */

#pragma once

#include "MAiNGOmodel.h"
#include "Thermo/SubcriticalComponent.h"
#include "Thermo/HenryComponent.h"
#include "Thermo/NRTL.h"


using Var = mc::FFVar;


//////////////////////////////////////////////////////////////////////////
// Model class to be passed to MAiNGO
class Model: public maingo::MAiNGOmodel {

	public:
		Model();
		maingo::EvaluationContainer evaluate(const std::vector<Var> &optVars);
		std::vector<maingo::OptimizationVariable> get_variables();


	private:
		// Constant model parameters:
			size_t ncompSubcritical, ncompHenry, ncomp, heavyKey, lightKey;
			double LHV_H2, LHV_MeOH, eta_s, eta_m, T_LP, T_MP, T_amb, e_H2, e_CO2, e_MeOH, e_OME1, kPurge;
			std::vector<double> distSplit;
		// Declaration of model variables:
			// Process
				std::vector<Var> H2in,H2LP,CO2in,CO2HP,Feed,Prod,Liq,Gas,Purge,Rec,TearGuess,TearCalc,Mix,ProdC,RecHP,Dist,Bot,Vap,alphaInv,xDist,xBot;
				Var H_H2in,H_H2LP,H_CO2in,H_CO2HP,H_Feed,H_Prod,H_Liq,H_Gas,H_Purge,H_Rec,H_TearGuess,H_TearCalc,H_Mix,H_ProdC,H_RecHP;
				Var p_H2in,p_H2LP,p_CO2in,p_CO2HP,p_Feed,p_Prod,p_Liq,p_Gas,p_Purge,p_Rec,p_TearGuess,p_TearCalc,p_Mix,p_ProdC,p_RecHP;
				Var T_H2in,T_H2LP,T_CO2in,T_CO2HP,T_Feed,T_Prod,T_Liq,T_Gas,T_Purge,T_Rec,T_TearGuess,T_TearCalc,T_Mix,T_ProdC,T_RecHP;
				Var KxRWGS,KxMeOH,rRWGS,rMeOH,sumProd,sumTearGuess,sumPurge,sumProdC,Q_Preheater,Q_Reactor,Tiso_CO2,S_CO2in,Siso_CO2,Hiso_CO2,Wc_CO2,Q_LP,Wc_Rec,Tiso_rec,Siso_Rec,Hiso_Rec,S_Rec;
				Var Ex_Reactor, Ex_LP, Ex_Flash1, Ex_Flash2, Ex_H2, Ex_CO2, Ex_MeOH;
				Var p_Dist, Rmin, R, Theta, underwoodRhs, Q_Cond, Q_Reb, sumDist, sumBot, nomBot, nomDist, rLHS, rRHS;
			// Flash
				std::vector<SubcriticalComponent <Var> > componentsSubcritical;
				std::vector<HenryComponent <Var> > componentsHenry;
				std::vector<Var> ps,x,y,henry;
				Var psi,sumGas,sumLiq,VbF,sumX,sumY,Q_Flash1;
			// Dummy Flash (LP steam exchanger)
				std::vector<Var> ps3,henry3;
				Var Q_Flash3,VbF3;



};


//////////////////////////////////////////////////////////////////////////
// function for providing optimization variable data to the Branch-and-Bound solver
std::vector<maingo::OptimizationVariable> Model::get_variables() {

	std::vector<maingo::OptimizationVariable> variables;
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,5),"Tear-0 [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,5),"Tear-1 [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,5),"Tear-2 [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,5),"Tear-3 [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,5),"Tear-4 [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1),"rRWGS [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1),"rMeOH [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,0.1),"xH2-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,0.1),"xCO2-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,0.1),"xCO-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"xMeOH-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"xH2O-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"yH2-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"yCO2-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"yCO-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"yMeOH-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"yH2O-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,5),"Gas-1 [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-6,1.75),"Liq-1 [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-1e6,0),"H_TearGuess [kW]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(25,150),"T-CO2-iso [C]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(25,150),"T-rec-iso [C]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(25,150),"T-mix [C]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(25,200),"T-prodC [C]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(60,70),"p-reac [bar]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(30,110),"T-1 [C]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1+1e-6,100),"Theta"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,100),"Rmin"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(20,100),"T-mix-MeOH/Air [C]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(393-273.15,413-273.15),"T-omeReac [C]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,7),"Gas-4 [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-6,7),"Liq-4 [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-100,25),"T-4 [C]"));
	for (size_t i=0; i<7; i++) {
		variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"x" + std::to_string(i) + "-4"));
	}
	for (size_t i=0; i<7; i++) {
		variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1),"y" + std::to_string(i) + "-4"));
	}
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1),"Azeo Rec. OME1 [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1),"Azeo Rec. MeOH [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1),"Crude MeOH Rec. H2O [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1),"Crude MeOH Rec. MeOH [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1+1e-6,100),"Theta2"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,100),"Rmin2"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1+1e-6,100),"Theta3"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,100),"Rmin3"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1+1e-6,100),"Theta4"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,100),"Rmin4"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1),"CO2in [kmol/s]"));

	return variables;
}



//////////////////////////////////////////////////////////////////////////
// constructor for the model
Model::Model() {

	// Parameters:
		// Misc
			e_H2 = 247.8 * 1e3;		// Chemical exergy in kJ/kmol approximated as delta G upon combustion + thermochemical exergy at 80 bar, 25C (amb: 1bar, 25C)
			e_MeOH = 702.6 * 1e3;	// Chemical exergy in kJ/kmol approximated as delta G upon combustion; neglecting thermochemical exergy
			e_CO2 = 10.2 * 1e3;		// Thermochemical exergy in kJ/kmol at 22.5bar, 25C (amb: 1bar, 25C)
			e_OME1 = 1903.3 * 1e3;	// Thermochemical exergy in kJ/kmol at 22.5bar, 25C (amb: 1bar, 25C)
			T_amb = 273.15 + 25;
			T_LP = 273.15 + 100;
			T_MP = 273.15 + 170;
		// Compressor
			eta_s = 0.8;
			eta_m = 0.9;
		// Splitter
			kPurge = 0.01;
		// Flash
			// Component models:
				componentsHenry.push_back(HenryComponent<Var>("H2",2.01588,273.15-252.76,13.13,64.147,0,0));
				componentsHenry.push_back(HenryComponent<Var>("CO2",44.0098,273.15+31.06,73.83,94,-393.510,0));
				componentsHenry.push_back(HenryComponent<Var>("CO",28.0104,273.15-140.23,34.99,94.4,-110.530,0));
				componentsSubcritical.push_back(SubcriticalComponent<Var>("MeOH",32.04,512.5,80.84,117,-200.940,0));
				componentsSubcritical.push_back(SubcriticalComponent<Var>("Water",18.0154,647.096,220.64,55.9472,-241.818,0));
				ncompHenry = componentsHenry.size();
				ncompSubcritical =componentsSubcritical.size();
				ncomp = ncompHenry + ncompSubcritical;
			// Henry component property parameters
				// H2
					componentsHenry[0].set_henry_model(HenryComponent<Var>::HENRY_ASPEN);
					componentsHenry[0].add_henry_parameters(std::vector<double>{3,-61.4347,1867.4,12.643,-0.027187,0});
					componentsHenry[0].add_henry_parameters(std::vector<double>{4,180.066,-6993.51,-26.3119,0.0150431,0});
					componentsHenry[0].set_solvent_mixing_rule(HenryComponent<Var>::MIX_ASPEN,std::vector<double>{117,55.9472});
					componentsHenry[0].set_heat_capacity_model(HenryComponent<Var>::CPIG_DIPPR107,std::vector<double>{27.617,9.56,2466,3.76,567.6});
				// CO2
					componentsHenry[1].set_henry_model(HenryComponent<Var>::HENRY_ASPEN);
					componentsHenry[1].add_henry_parameters(std::vector<double>{3,15.4699,-3426.7,1.5108,-0.025451,0});
					componentsHenry[1].add_henry_parameters(std::vector<double>{4,159.865,-8741.55,-21.669,0.00110259,0});
					componentsHenry[1].set_solvent_mixing_rule(HenryComponent<Var>::MIX_ASPEN,std::vector<double>{117,55.9472});
					componentsHenry[1].set_heat_capacity_model(HenryComponent<Var>::CPIG_DIPPR107,std::vector<double>{29.37,34.54,1428,26.4,588});
				// CO
					componentsHenry[2].set_henry_model(HenryComponent<Var>::HENRY_ASPEN);
					componentsHenry[2].add_henry_parameters(std::vector<double>{3,4.21187,1144.4,0,0,0});
					componentsHenry[2].add_henry_parameters(std::vector<double>{4,171.775,-8296.75,-23.3372,0,0});
					componentsHenry[2].set_solvent_mixing_rule(HenryComponent<Var>::MIX_ASPEN,std::vector<double>{117,55.9472});
					componentsHenry[2].set_heat_capacity_model(HenryComponent<Var>::CPIG_DIPPR107,std::vector<double>{29.108,8.773,3085.1,8.4553,1538.2});
			// Subcritical component property parameters
				// Extended Antoine
					componentsSubcritical[0].set_vapor_pressure_model(SubcriticalComponent<Var>::PVAP_XANTOINE,std::vector<double>{71.2051, -6904.5, 0.0, 0.0, -8.8622, 7.4664e-6, 2});
					componentsSubcritical[1].set_vapor_pressure_model(SubcriticalComponent<Var>::PVAP_XANTOINE,std::vector<double>{62.1361, -7258.2, 0.0, 0.0, -7.3037, 4.1653e-6, 2});
				// Enthalpy of vaporization
					componentsSubcritical[0].set_enthalpy_of_vaporization_model(SubcriticalComponent<Var>::DHVAP_DIPPR106,std::vector<double>{32615, -1.0407, 1.8695, -0.60801, 0});
					componentsSubcritical[1].set_enthalpy_of_vaporization_model(SubcriticalComponent<Var>::DHVAP_DIPPR106,std::vector<double>{56600, 0.61204, -0.6257, 0.3988, 0});
				// Heat capacity
					componentsSubcritical[0].set_heat_capacity_model(SubcriticalComponent<Var>::CPIG_DIPPR107,std::vector<double>{39.252,87.9,1916.5,53.654,896.7});
					componentsSubcritical[1].set_heat_capacity_model(SubcriticalComponent<Var>::CPIG_DIPPR107,std::vector<double>{33.363,26.79,2610.5,8.896,1169});


		// Resizing
			ps.resize(ncomp); x.resize(ncomp); y.resize(ncomp); henry.resize(ncomp); H2in.resize(ncomp); CO2in.resize(ncomp); Feed.resize(ncomp); Prod.resize(ncomp);
			Liq.resize(ncomp); Gas.resize(ncomp); Purge.resize(ncomp); Rec.resize(ncomp); TearGuess.resize(ncomp); TearCalc.resize(ncomp);
			Mix.resize(ncomp); ProdC.resize(ncomp); H2LP.resize(ncomp); CO2HP.resize(ncomp); RecHP.resize(ncomp);
			ps3.resize(ncomp); henry3.resize(ncomp);
			distSplit.resize(ncomp); Dist.resize(ncomp); Bot.resize(ncomp); Vap.resize(ncomp); alphaInv.resize(ncomp); xDist.resize(ncomp); xBot.resize(ncomp);

	// Input:
		// H2 input
			H2in[0] = 0.75; // H2
			H2in[1] = 0.0; // CO2
			H2in[2] = 0.0; // CO
			H2in[3] = 0.0; // MeOH
			H2in[4] = 0.0; // H2O
			Ex_H2 = H2in[0] * e_H2;
			T_H2in = T_amb;
			p_H2in = 70;
			H_H2in = 0.;
			for(size_t i=0; i<ncompHenry; ++i) {
				Var hig_i(componentsHenry[i].calculate_ideal_gas_enthalpy_conv(T_H2in));
				H_H2in += H2in[i]*hig_i;
			}
			for(size_t i=0; i<ncompSubcritical; ++i) {
				Var hig_i(componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(T_H2in));
				H_H2in += H2in[i+ncompHenry]*hig_i;
			}
		// CO2 input
			CO2in[0] = 0.0; // H2
			CO2in[1] = 0.0; // CO2 - chosen by optimizer!
			CO2in[2] = 0.0; // CO
			CO2in[3] = 0.0; // MeOH
			CO2in[4] = 0.0; // H2O
			T_CO2in = T_amb;
			p_CO2in = 22.5;

}


//////////////////////////////////////////////////////////////////////////
// Evaluate the model
maingo::EvaluationContainer Model::evaluate(const std::vector<Var> &optVars)
{

	// Rename  inputs
		sumTearGuess = 0.0;
		size_t iVar = 0;
		for (size_t i=0; i<ncomp; ++i) {
			TearGuess[i] = optVars[iVar++];
			sumTearGuess += TearGuess[i];
		}
		rRWGS = optVars[iVar++];
		rMeOH = optVars[iVar++];
		for (size_t i=0; i<ncomp; ++i) {
			x[i] = optVars[iVar++];
		}
		for (size_t i=0; i<ncomp; ++i) {
			y[i] = optVars[iVar++];
		}
		sumGas=optVars[iVar++];
		sumLiq=optVars[iVar++];
		H_TearGuess=optVars[iVar++];
		Tiso_CO2 = optVars[iVar++] + 273.15;
		Tiso_rec = optVars[iVar++] + 273.15;
		T_Mix = optVars[iVar++] + 273.15;
		T_ProdC = optVars[iVar++] + 273.15;
		p_Feed = optVars[iVar++];
		T_Prod = 200+273.15; // fixed
		T_Gas  = optVars[iVar++] + 273.15;
		Theta = optVars[iVar++];
		Rmin = optVars[iVar++];
		Var T_MeOHAir = optVars[iVar++] + 273.15;
		Var T_OmeReac = optVars[iVar++] + 273.15;
		Var sumGas4 = optVars[iVar++];
		Var sumLiq4 = optVars[iVar++];
		Var T4 = optVars[iVar++] + 273.15;
		std::vector<Var> x4(7), y4(7);
		for (size_t i=0; i<7; ++i) {
			x4[i] = optVars[iVar++];
		}
		for (size_t i=0; i<7; ++i) {
			y4[i] = optVars[iVar++];
		}
		Var azeoReOME1 = optVars[iVar++];
		Var azeoReMeOH = optVars[iVar++];
		Var crudeMeOHReH2O = optVars[iVar++];
		Var crudeMeOHReMeOH = optVars[iVar++];
		Var Theta2 = optVars[iVar++];
		Var Rmin2 = optVars[iVar++];
		Var Theta3 = optVars[iVar++];
		Var Rmin3 = optVars[iVar++];
		Var Theta4 = optVars[iVar++];
		Var Rmin4 = optVars[iVar++];
		CO2in[1] = optVars[iVar++];

	// Model
		maingo::EvaluationContainer result;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// MeOH process
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


		// H2 throttle
			H2LP = H2in;
			p_H2LP = p_Feed;
			H_H2LP = H_H2in;


		// CO2 input
			H_CO2in = 0.;
			for(size_t i=0; i<ncompHenry; ++i) {
				Var hig_i(componentsHenry[i].calculate_ideal_gas_enthalpy_conv(T_CO2in));
				H_CO2in += CO2in[i]*hig_i;
			}
			for(size_t i=0; i<ncompSubcritical; ++i) {
				Var hig_i(componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(T_CO2in));
				H_CO2in += CO2in[i+ncompHenry]*hig_i;
			}
			S_CO2in = 0.;
			for(size_t i=0; i<ncompHenry; ++i) {
				Var sig_i(componentsHenry[i].calculate_ideal_gas_entropy(T_CO2in,p_CO2in));
				S_CO2in += CO2in[i]*sig_i;
			}
			for(size_t i=0; i<ncompSubcritical; ++i) {
				Var sig_i(componentsSubcritical[i].calculate_ideal_gas_entropy(T_CO2in,p_CO2in));
				S_CO2in += CO2in[i+ncompHenry]*sig_i;
			}
			Ex_CO2 = CO2in[1] * e_CO2;


		// CO2 compressor
			CO2HP = CO2in;
			p_CO2HP = p_Feed;
			Siso_CO2 = 0.;
			for(size_t i=0; i<ncompHenry; ++i) {
				Var sig_i(componentsHenry[i].calculate_ideal_gas_entropy(Tiso_CO2,p_CO2HP));
				Siso_CO2 += CO2HP[i]*sig_i;
			}
			for(size_t i=0; i<ncompSubcritical; ++i) {
				Var sig_i(componentsSubcritical[i].calculate_ideal_gas_entropy(Tiso_CO2,p_CO2HP));
				Siso_CO2 += CO2HP[i+ncompHenry]*sig_i;
			}
			Hiso_CO2 = 0.;
			for(size_t i=0; i<ncompHenry; ++i) {
				Var hig_i(componentsHenry[i].calculate_ideal_gas_enthalpy_conv(Tiso_CO2));
				Hiso_CO2 += CO2HP[i]*hig_i;
			}
			for(size_t i=0; i<ncompSubcritical; ++i) {
				Var hig_i(componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(Tiso_CO2));
				Hiso_CO2 += CO2HP[i+ncompHenry]*hig_i;
			}
			H_CO2HP = H_CO2in*(1-1/eta_s) + Hiso_CO2/eta_s;
			Wc_CO2 = (Hiso_CO2-H_CO2in)/(eta_s*eta_m);
			result.eq.push_back( (S_CO2in - Siso_CO2) / 1e1 );


		// Tear "input":
			Gas = TearGuess;
			H_Gas = H_TearGuess;


		// Split:
			sumPurge = 0.0;
			for (size_t i=0; i<ncomp; ++i) {
				Purge[i] = Gas[i] * kPurge;
				sumPurge += Purge[i];
				Rec[i] = Gas[i] - Purge[i];
			}
			H_Purge = H_Gas * kPurge;
			H_Rec = H_Gas - H_Purge;
			T_Rec = T_Gas;
			p_Rec = 0.95*p_Feed;
			S_Rec = 0.;
			for(size_t i=0; i<ncompHenry; ++i) {
				Var sig_i(componentsHenry[i].calculate_ideal_gas_entropy(T_Rec,p_Rec));
				S_Rec += Rec[i]*sig_i;
			}
			for(size_t i=0; i<ncompSubcritical; ++i) {
				Var sig_i(componentsSubcritical[i].calculate_ideal_gas_entropy(T_Rec,p_Rec));
				S_Rec += Rec[i+ncompHenry]*sig_i;
			}


		// Recycle compressor
			RecHP = Rec;
			p_RecHP = p_Feed;
			Siso_Rec = 0.;
			for(size_t i=0; i<ncompHenry; ++i) {
				Var sig_i(componentsHenry[i].calculate_ideal_gas_entropy(Tiso_rec,p_RecHP));
				Siso_Rec += RecHP[i]*sig_i;
			}
			for(size_t i=0; i<ncompSubcritical; ++i) {
				Var sig_i(componentsSubcritical[i].calculate_ideal_gas_entropy(Tiso_rec,p_RecHP));
				Siso_Rec += RecHP[i+ncompHenry]*sig_i;
			}
			Hiso_Rec = 0.;
			for(size_t i=0; i<ncompHenry; ++i) {
				Var hig_i(componentsHenry[i].calculate_ideal_gas_enthalpy_conv(Tiso_rec));
				Hiso_Rec += RecHP[i]*hig_i;
			}
			for(size_t i=0; i<ncompSubcritical; ++i) {
				Var hig_i(componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(Tiso_rec));
				Hiso_Rec += RecHP[i+ncompHenry]*hig_i;
			}
			H_RecHP = H_Rec*(1-1/eta_s) + Hiso_Rec/eta_s;
			Wc_Rec = max(Var(0.),(Hiso_Rec-H_Rec)/(eta_s*eta_m));
			result.eq.push_back( (S_Rec - Siso_Rec) / 1e1 );


		// Mixer:
			for (size_t i=0; i<ncomp; ++i) {
				Mix[i] = H2LP[i] + CO2HP[i] + RecHP[i];
			}
			H_Mix = 0.;
			for(size_t i=0; i<ncompHenry; ++i) {
				Var hig_i(componentsHenry[i].calculate_ideal_gas_enthalpy_conv(T_Mix));
				H_Mix += Mix[i]*hig_i;
			}
			for(size_t i=0; i<ncompSubcritical; ++i) {
				Var hig_i(componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(T_Mix));
				H_Mix += Mix[i+ncompHenry]*hig_i;
			}
			result.eq.push_back((H_Mix - (H_H2LP+H_CO2HP+H_RecHP))/1e5);


		// Pre-heater - cold side
			Feed = Mix;
			T_Feed = 273.15 + 170;
			H_Feed = 0.;
			for(size_t i=0; i<ncompHenry; ++i) {
				Var hig_i(componentsHenry[i].calculate_ideal_gas_enthalpy_conv(T_Feed));
				H_Feed += Feed[i]*hig_i;
			}
			for(size_t i=0; i<ncompSubcritical; ++i) {
				Var hig_i(componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(T_Feed));
				H_Feed += Feed[i+ncompHenry]*hig_i;
			}
			Q_Preheater = H_Feed - H_Mix;
			Var Tm_Preheater = lmtd(T_Mix,T_Feed);
			Var Ex_Preheater = Q_Preheater*(1 - T_amb/Tm_Preheater);


		// Reactor:
			p_Prod = p_Feed * 0.95;
			KxRWGS = pow( 10.0 , -2073.0/T_Prod+2.029 );
			KxMeOH = pow( 10.0 , 5139.0/T_Prod-12.621 ) * sqr(p_Prod);
			Prod[0] = Feed[0] - 2*rMeOH - rRWGS;
			Prod[1] = Feed[1] - rRWGS;
			Prod[2] = Feed[2] - rMeOH + rRWGS;
			Prod[3] = Feed[3] + rMeOH;
			Prod[4] = Feed[4] + rRWGS;
			for (size_t i=0; i<ncomp; ++i) {
				result.ineq.push_back((1e-5 - Prod[i])/0.1,"1e-5<=Prod[i]");
				Prod[i] = max(1e-5,Prod[i]);
			}
			sumProd = 0.0;
			for (size_t i=0; i<ncomp; ++i) {
				sumProd += Prod[i];
			}
			H_Prod = 0.;
			for(size_t i=0; i<ncompHenry; ++i) {
				Var hig_i(componentsHenry[i].calculate_ideal_gas_enthalpy_conv(T_Prod));
				H_Prod += Prod[i]*hig_i;
			}
			for(size_t i=0; i<ncompSubcritical; ++i) {
				Var hig_i(componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(T_Prod));
				H_Prod += Prod[i+ncompHenry]*hig_i;
			}
			Q_Reactor = (H_Prod - H_Feed);
			Var Tm_Reactor = lmtd(T_Feed,T_Prod);
			Ex_Reactor = Q_Reactor * (1 - T_amb / Tm_Reactor);
			result.eq.push_back( (KxRWGS*Prod[0]*Prod[1] - Prod[2]*Prod[4]) / 1e-2 );
			result.eq.push_back( (KxMeOH*sqr(Prod[0])*Prod[2] - sqr(sumProd)*Prod[3]) / 1e-1 );


		// Add NRTL model
			NRTLpars myTmpNRTLpars;
			std::vector< std::vector<double> > myTmpMatrix(ncomp, std::vector<double>(ncomp, 0.0));
			myTmpNRTLpars.a = myTmpMatrix; myTmpNRTLpars.b = myTmpMatrix; myTmpNRTLpars.c = myTmpMatrix; myTmpNRTLpars.d = myTmpMatrix; myTmpNRTLpars.e = myTmpMatrix; myTmpNRTLpars.f = myTmpMatrix;
			// CO2 - Water: 1 - 4; from APV88 ENRTL-RK
				myTmpNRTLpars.a[1][4] = 10.064;
				myTmpNRTLpars.a[4][1] = 10.064;
				myTmpNRTLpars.b[1][4] = -3268.14;
				myTmpNRTLpars.b[4][1] = -3268.14;
				myTmpNRTLpars.c[1][4] = 0.3;
				myTmpNRTLpars.c[4][1] = 0.3;
			// Water - MeOH: 4 - 3; from APV88 VLE-IG
				myTmpNRTLpars.a[4][3] = 2.7322;
				myTmpNRTLpars.a[3][4] = -0.693;
				myTmpNRTLpars.b[4][3] = -617.269;
				myTmpNRTLpars.b[3][4] = 172.987;
				myTmpNRTLpars.c[4][3] = 0.3;
				myTmpNRTLpars.c[3][4] = 0.3;
			NRTL myNRTLmodel;
			myNRTLmodel.setPars<Var>(myTmpNRTLpars);


		// Cooler (LP steam)
			ProdC = Prod;
			sumProdC = sumProd;
			p_ProdC = p_Prod;
			// Calculate dew point
				H_ProdC = 0.;
				Var VsumX3 = 0.;
				for (size_t i=0; i<ncompHenry; ++i) {
					  // Phase equilibrium
					henry3[i] = componentsHenry[i].calculate_henry_single(T_ProdC,4);	// using water only, since it will condense first at the dew point
					Var Vxi = p_ProdC*ProdC[i]/henry3[i];
					VsumX3 += Vxi;
					  // Enthalpies
					Var hig_i(componentsHenry[i].calculate_ideal_gas_enthalpy_conv(T_ProdC));
					H_ProdC += ProdC[i]*hig_i;
				}
				for (unsigned i=0;i<ncompSubcritical;i++) {
					  // Phase equilibrium
					Var gammaInfDilLog = (i==1) ? 0 : myNRTLmodel.calculateInifiniteDilutionGammaLog(T_ProdC,i+ncompHenry,4);
					ps3[i+ncompHenry] = componentsSubcritical[i].calculate_vapor_pressure_conv(T_ProdC);
					Var Vxi = p_ProdC*ProdC[i+ncompHenry]*exp(-gammaInfDilLog)/ps3[i+ncompHenry];
					VsumX3 += Vxi;
					  // // Enthalpies
					Var hig_i(componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(T_ProdC));
					H_ProdC += ProdC[i+ncompHenry]*hig_i;
				}
			// Closure relation for few point
			result.eq.push_back((VsumX3-sumProdC)/1.0);
			// Energy balance:
			Q_LP = (H_ProdC - H_Prod);
			Var Tm_LP = lmtd(T_Prod,T_ProdC);
			Ex_LP = Q_LP * (1 - T_amb / Tm_LP);



		// Flash:
			// 1. Overall mass balance
				// in equality constraint
			// 2. Calculate phase equilibrium residual & enthalpies
				psi=0, sumX=0, sumY=0;
				H_Gas=0, H_Liq=0;
				p_Gas = p_ProdC;
				p_Liq = p_ProdC;
				std::vector<Var> gamma = myNRTLmodel.calculateGamma(T_Gas,x);
				for (size_t i=0; i<ncompHenry; ++i) {
					  // Phase equilibrium
					henry[i] = componentsHenry[i].calculate_henry_mixed(T_Gas,x);
					sumX += x[i];
					sumY += y[i];
					psi += x[i]-y[i];
					Liq[i] = sumLiq*x[i];
					Gas[i] = sumGas*y[i];
					  // Enthalpies
					Var hig_i(componentsHenry[i].calculate_ideal_gas_enthalpy_conv(T_Gas));
					Var deltahsol_i(componentsHenry[i].calculate_solution_enthalpy_mixed(T_Gas,x));
					H_Gas += Gas[i]*hig_i;
					H_Liq += Liq[i]*(hig_i - deltahsol_i);
					result.eq.push_back((p_Gas*y[i]-gamma[i]*henry[i]*x[i])/1.0);
				}
				for (unsigned i=0;i<ncompSubcritical;i++) {
					  // Phase equilibrium
					ps[i+ncompHenry] = componentsSubcritical[i].calculate_vapor_pressure_conv(T_Gas);
					sumX += x[i+ncompHenry];
					sumY += y[i+ncompHenry];
					psi += x[i+ncompHenry]-y[i+ncompHenry];
					Liq[i+ncompHenry] = sumLiq*x[i+ncompHenry];
					Gas[i+ncompHenry] = sumGas*y[i+ncompHenry];
					  // Enthalpies
					Var hig_i(componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(T_Gas));
					Var deltahv_i(componentsSubcritical[i].calculate_vaporization_enthalpy_conv(T_Gas));
					H_Gas += Gas[i+ncompHenry]*hig_i;
					H_Liq += Liq[i+ncompHenry]*(hig_i - deltahv_i);
					result.eq.push_back((p_Gas*y[i+ncompHenry]-gamma[i+ncompHenry]*ps[i+ncompHenry]*x[i+ncompHenry])/1.0);
				}
			// 3. Energy balance:
				Var hE = myNRTLmodel.calculateHE(T_Gas,x);
				H_Liq += sumLiq*hE;
				Q_Flash1 = H_Gas + H_Liq - H_ProdC;
				Var Tm_Flash1 = lmtd(T_Gas,T_ProdC);
				Ex_Flash1 = Q_Flash1*(1 - T_amb/Tm_Flash1);	// Cooling with cooling water
			// Overall constraints
				result.eq.push_back(((sumGas+sumLiq) - sumProd)/1.0);
				for (size_t i=0; i<ncomp; ++i) {
					result.eq.push_back((ProdC[i]-(TearGuess[i] + Liq[i]))/1.0);
				}
				result.eq.push_back((psi-0.0)/1.0);
				// r.o.
					result.eqRelaxationOnly.push_back( (sumX-1.0)/1.0 );
					result.eqRelaxationOnly.push_back( (sumY-1.0)/1.0 );


		// Tear "output"
			TearCalc = Gas;
			H_TearCalc = H_Gas;

			for (size_t i=0; i<ncomp; ++i) {
				result.eq.push_back((TearCalc[i] - TearGuess[i])/0.1);
			}
			result.eq.push_back((H_TearCalc - H_TearGuess)/1e5);


		// Equalities (=0):
			// Overall
					result.eqRelaxationOnly.push_back( (H_H2in+H_CO2in-H_Purge-H_Liq+Q_Preheater+Q_Reactor+Q_LP+Q_Flash1 + Wc_CO2*eta_m+Wc_Rec*eta_m)/1e5 );
					result.eqRelaxationOnly.push_back( ((H2in[0]+CO2in[1]-2*rMeOH)-(sumLiq+sumPurge))/1.0 );
					result.eqRelaxationOnly.push_back( ((H2in[0] + CO2in[0] - 2*rMeOH - rRWGS)-(Liq[0]+Purge[0]))/0.1 );
					result.eqRelaxationOnly.push_back( ((H2in[1] + CO2in[1]           - rRWGS)-(Liq[1]+Purge[1]))/0.1 );
					result.eqRelaxationOnly.push_back( ((H2in[2] + CO2in[2] -   rMeOH + rRWGS)-(Liq[2]+Purge[2]))/0.1 );
					result.eqRelaxationOnly.push_back( ((H2in[3] + CO2in[3] +   rMeOH        )-(Liq[3]+Purge[3]))/0.1 );
					result.eqRelaxationOnly.push_back( ((H2in[4] + CO2in[4]           + rRWGS)-(Liq[4]+Purge[4]))/0.1 );



		// Tear "input": Crude MeOH recycle from OME1 process
			std::vector<Var> CrudeMeOHRecGuess(ncomp,0.);
			CrudeMeOHRecGuess[3] = crudeMeOHReMeOH;
			CrudeMeOHRecGuess[4] = crudeMeOHReH2O;
			Var T_CrudeMeOHRecGuess = 331; // Taken from anzi01
			Var H_CrudeMeOHRecGuess = 0.;
			for (size_t i=0; i<ncompSubcritical; ++i) {
				H_CrudeMeOHRecGuess += CrudeMeOHRecGuess[i+ncompHenry]*(
														 componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(T_CrudeMeOHRecGuess)
														-componentsSubcritical[i].calculate_vaporization_enthalpy_conv(T_CrudeMeOHRecGuess));
			}


		// Mixer:
			Var p_mix2 = 1.;
			std::vector<Var> Mix2(ncomp);
			for (size_t i=0; i<ncomp; ++i) {
				Mix2[i] = Liq[i] + CrudeMeOHRecGuess[i];
			}
			Var H_Mix2 = H_Liq + H_CrudeMeOHRecGuess;


		// Distillation Column 1 - Cut: CO2, MeOH | H2O - Underwood shortcut
			Var p_col = p_mix2;
			size_t ncompDist = 3;	// Only considering CO2, MeOH, and H2O
			// 1. Mass balances
				sumDist = 0.;
				sumBot = 0.;
				nomDist = 0.;
				nomBot = 0.;
				std::vector<unsigned> imap(ncompDist);
				imap[0] = 1;	// CO2
				imap[1] = 3;	// MeOH
				imap[2] = 4;	// H2O
				heavyKey = 2;
				lightKey = 1;
				distSplit[0] = 1.; // H2
				distSplit[1] = 1.; // CO2
				distSplit[2] = 1.; // CO
				distSplit[3] = 1.; // MeOH
				distSplit[4] = 0.; // H2O
				for (size_t i=0; i<ncompDist; ++i) {
					Dist[i] = distSplit[imap[i]] * Mix2[imap[i]];
					Bot[i] = (1-distSplit[imap[i]]) * Mix2[imap[i]];
					sumDist += Dist[i];
					sumBot += Bot[i];

				}
				for (size_t i=0; i<ncompDist; ++i) {
					xDist[i] = Dist[i]/sumDist;
					xBot[i] = Bot[i]/sumBot;
				}
				Ex_MeOH = -Dist[1] * e_MeOH;
			// 2. Compute reflux ratio
				// Relative volatilities - here using fixed temperature estaimtes for top and bottom
					Var TestTop = 64+273.15;  // Mostly methanol (64.7C, but some CO2)
					Var TestBot = 100+273.15; // Water
					std::vector<Var> psPseudo(3);
					psPseudo[0] = sqrt(componentsHenry[1].calculate_henry_single(TestTop,3)*componentsHenry[1].calculate_henry_single(TestBot,4));
					psPseudo[1] = sqrt(componentsSubcritical[0].calculate_vapor_pressure_conv(TestTop)*componentsSubcritical[0].calculate_vapor_pressure_conv(TestBot));
					psPseudo[2] = sqrt(componentsSubcritical[1].calculate_vapor_pressure_conv(TestTop)*componentsSubcritical[1].calculate_vapor_pressure_conv(TestBot));
				// compute Underwood factor
					underwoodRhs = 0.;
					for (size_t i=0; i<ncompDist; ++i) {
						Var tmp = psPseudo[i]*Mix2[imap[i]];
						for (size_t j=0; j<ncompDist; ++j) {
							if (j!=i) {
								tmp = tmp*(psPseudo[j]-Theta*psPseudo[heavyKey]);
							}
						}
						underwoodRhs += tmp;
					}
					result.eq.push_back( (underwoodRhs) / 1e0, "Dist, Theta");
					result.ineq.push_back( (psPseudo[heavyKey]*Theta - psPseudo[lightKey]) / 1e1, "Theta<=alphaLS");
				// compute reflux ratio
					rRHS = 0.;
					rLHS = (Rmin+1)*sumDist;
					for (size_t i=0; i<ncompDist; ++i) {
						rLHS = rLHS * (psPseudo[i]-Theta*psPseudo[heavyKey]);
						Var tmp = Dist[i]*psPseudo[i];
						for (size_t j=0; j<ncompDist; ++j) {
							if (j!=i) {
								tmp = tmp*(psPseudo[j]-Theta*psPseudo[heavyKey]);
							}
						}
						rRHS += tmp;
					}
					result.eq.push_back( (rRHS - rLHS) / 1e0, "Dist, Rmin");
					R = Rmin * 1.2;
			// 3. Compute reboiler duty
				// Condenser:
					std::vector<Var> Ref (ncompDist);
					for (size_t i=0; i<ncompDist; ++i) {
						Ref[i] = R*Dist[i];
					}
					// energy balance: assume the temperature of Vap is approximately that in the condenser
					Q_Cond = 0.;
					Q_Cond -= Ref[0]*componentsHenry[1].calculate_solution_enthalpy_single(TestTop,3);
					for (size_t i=1; i<ncompDist; ++i) {
						Q_Cond -= Ref[i]*componentsSubcritical[imap[i]-ncompHenry].calculate_vaporization_enthalpy_conv(TestTop);
					}
					Var Ex_Cond = Q_Cond*(1-T_amb/TestTop);
					Var HDist = 0.;
					HDist += Dist[0]*componentsHenry[1].calculate_ideal_gas_enthalpy_conv(TestTop);
					for (size_t i=1; i<ncompDist; ++i) {
						HDist += Dist[i]*componentsSubcritical[imap[i]-ncompHenry].calculate_ideal_gas_enthalpy_conv(TestTop);
					}
				// Reboiler:
					Var HBot = 0.;
					HBot += Bot[0]*( componentsHenry[1].calculate_ideal_gas_enthalpy_conv(TestBot)
									-componentsHenry[1].calculate_solution_enthalpy_single(TestBot,3));
					for (size_t i=1; i<ncompDist; ++i) {
						HBot += Bot[i]*( componentsSubcritical[imap[i]-ncompHenry].calculate_ideal_gas_enthalpy_conv(TestBot)
									    -componentsSubcritical[imap[i]-ncompHenry].calculate_vaporization_enthalpy_conv(TestBot));
					}
					Q_Reb = HDist + HBot - Q_Cond - H_Mix2;
					Var Ex_Reb = Q_Reb*(1-T_amb/TestBot);


		// H2O cooler
			std::vector<Var> H2Oout = Bot;
			Var T_H2Oout = 35+273.15;
			Var p_H2Oout = p_col;
			Var H_H2Oout = 0.;
			H_H2Oout += H2Oout[0]*( componentsHenry[1].calculate_ideal_gas_enthalpy_conv(T_H2Oout)
								   -componentsHenry[1].calculate_solution_enthalpy_single(T_H2Oout,3));
			for (size_t i=1; i<ncompDist; i++) {
				H_H2Oout += H2Oout[i]*( componentsSubcritical[imap[i]-ncompHenry].calculate_ideal_gas_enthalpy_conv(T_H2Oout)
									   -componentsSubcritical[imap[i]-ncompHenry].calculate_vaporization_enthalpy_conv(T_H2Oout));
			}
			Var Q_H2O = H_H2Oout - HBot;
			Var Tm_H2O = lmtd(TestBot,T_H2Oout);
			Var Ex_H2O = Q_H2O*(1-T_amb/Tm_H2O);


		// Purge combustor
			componentsHenry.push_back(HenryComponent<Var>("O2", 31.9988, 154.58, 50.43, 73.4, 0, 0));
			componentsHenry[ncompHenry].set_heat_capacity_model(HenryComponent<Var>::CPIG_DIPPR107, std::vector<double>{29.103, 10.040, 2526.5, 9.356, 1153.8});
			componentsHenry.push_back(HenryComponent<Var>("N2", 28.01348, 126.2, 34, 89.21, 0, 0));
			componentsHenry[ncompHenry+1].set_heat_capacity_model(HenryComponent<Var>::CPIG_DIPPR107, std::vector<double>{29.105, 8.6149, 1701.6, 0.10347, 909.79});
			// CO2
			// Inlet
			Var O2demandPurge = 0.5*Purge[0] /*H2*/ + 0.5*Purge[2] /*CO*/ + 1.5*Purge[3] /*MeOH*/;
			double lambdaPurge = 5.5;
			Var O2InPurgeComb = lambdaPurge*O2demandPurge;
			Var N2InPurgeComb = lambdaPurge*O2demandPurge*0.79/0.21;
			Var H_CombustionAir = 0.;
			H_CombustionAir += O2InPurgeComb*componentsHenry[ncompHenry].calculate_ideal_gas_enthalpy_conv(T_amb);
			H_CombustionAir += N2InPurgeComb*componentsHenry[ncompHenry+1].calculate_ideal_gas_enthalpy_conv(T_amb);
			// Outlet - after HRSG
			Var O2OutPurgeComb = (lambdaPurge-1)*O2demandPurge;
			Var N2OutPurgeComb = N2InPurgeComb;
			Var CO2OutPurgeComb = Purge[1] /*CO2*/ + Purge[2] /*CO*/ + Purge[3] /*MeOH*/;
			Var H2OOutPurgeComb = Purge[0] /*H2*/ + 2.*Purge[3] /*MeOH*/ + Purge[4] /*H2O*/;
			Var T_HrsgIn = 600 + 273.15;
			Var T_HrsgOut = 80 + 273.15;
			Var H_HrsgOut = 0.;
			H_HrsgOut += O2OutPurgeComb*componentsHenry[ncompHenry].calculate_ideal_gas_enthalpy_conv(T_HrsgOut);
			H_HrsgOut += N2OutPurgeComb*componentsHenry[ncompHenry+1].calculate_ideal_gas_enthalpy_conv(T_HrsgOut);
			H_HrsgOut += CO2OutPurgeComb*componentsHenry[1].calculate_ideal_gas_enthalpy_conv(T_HrsgOut);
			H_HrsgOut += H2OOutPurgeComb*componentsSubcritical[1].calculate_ideal_gas_enthalpy_conv(T_HrsgOut);
			Var Q_Hrsg = H_HrsgOut - (H_Purge+H_CombustionAir);
			Var Tm_HRSG = lmtd(T_HrsgIn,T_HrsgOut);
			Var Ex_Hrsg = Q_Hrsg*(1-T_amb/Tm_HRSG);



	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// OME1 process
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Thermo models for this part
		componentsSubcritical.clear(); componentsHenry.clear();
		componentsSubcritical.push_back(SubcriticalComponent<Var>("OME1", 76.09532, 480.6, 39.5, 213, -348.2, -226.3));
		componentsSubcritical.push_back(SubcriticalComponent<Var>("MF", 60.05256, 487.2, 60, 172, -352.4, -295));
		componentsSubcritical.push_back(SubcriticalComponent<Var>("Water", 18.0154, 647.096, 220.64, 55.9472, -241.818, -228.572));
		componentsSubcritical.push_back(SubcriticalComponent<Var>("MeOH", 32.04, 512.5, 80.84, 117, -200.940, -162.32));
		componentsHenry.push_back(HenryComponent<Var>("O2", 31.9988, 154.58, 50.43, 73.4, 0, 0));
		componentsHenry.push_back(HenryComponent<Var>("N2", 28.01348, 126.2, 34, 89.21, 0, 0));
		componentsHenry.push_back(HenryComponent<Var>("CO2",44.0098,273.15+31.06,73.83,94,-393.510,0));
		ncompSubcritical = componentsSubcritical.size();
		ncompHenry = componentsHenry.size();
		ncomp = ncompSubcritical + ncompHenry;
		//Subcritical component property parameters
			//OME1
				componentsSubcritical[0].set_vapor_pressure_model(SubcriticalComponent<Var>::PVAP_XANTOINE, std::vector<double>{80.1271, -6279.1, 0.0, 0.0, -10.631, 9.7948e-6, 2});
				componentsSubcritical[0].set_enthalpy_of_vaporization_model(SubcriticalComponent<Var>::DHVAP_DIPPR106, std::vector<double>{51245, 0.90984, -0.66423, 0.19938, 0});
				componentsSubcritical[0].set_heat_capacity_model(SubcriticalComponent<Var>::CPIG_DIPPR107, std::vector<double>{74.976, 161.660, 862.87, 789.640, 4671.8});
			//MF
				componentsSubcritical[1].set_vapor_pressure_model(SubcriticalComponent<Var>::PVAP_XANTOINE, std::vector<double>{65.6711, -5606.1, 0.0, 0.0, -8.392, 7.8468e-6, 2});
				componentsSubcritical[1].set_enthalpy_of_vaporization_model(SubcriticalComponent<Var>::DHVAP_DIPPR106, std::vector<double>{47691, 0.98928, -0.98574, 0.42695, 0});
				componentsSubcritical[1].set_heat_capacity_model(SubcriticalComponent<Var>::CPIG_DIPPR107, std::vector<double>{50.600, 121.900, 1637, 89.400, 743});
			//Water
				componentsSubcritical[2].set_vapor_pressure_model(SubcriticalComponent<Var>::PVAP_XANTOINE, std::vector<double>{62.1361, -7258.2, 0.0, 0.0, -7.3037, 4.1653e-6, 2});
				componentsSubcritical[2].set_enthalpy_of_vaporization_model(SubcriticalComponent<Var>::DHVAP_DIPPR106, std::vector<double>{56600, 0.61204, -0.6257, 0.3988, 0});
				componentsSubcritical[2].set_heat_capacity_model(SubcriticalComponent<Var>::CPIG_DIPPR107, std::vector<double>{33.363, 26.79, 2610.5, 8.896, 1169});
			//MeOH
				componentsSubcritical[3].set_vapor_pressure_model(SubcriticalComponent<Var>::PVAP_XANTOINE, std::vector<double>{71.2051, -6904.5, 0.0, 0.0, -8.8622, 7.4664e-6, 2});
				componentsSubcritical[3].set_enthalpy_of_vaporization_model(SubcriticalComponent<Var>::DHVAP_DIPPR106, std::vector<double>{32615, -1.0407, 1.8695, -0.60801, 0});
				componentsSubcritical[3].set_heat_capacity_model(SubcriticalComponent<Var>::CPIG_DIPPR107, std::vector<double>{39.252, 87.9, 1916.5, 53.654, 896.7});
		// Henry component properties
			//O2
				componentsHenry[0].set_henry_model(HenryComponent<Var>::HENRY_ASPEN);
				componentsHenry[0].add_henry_parameters(std::vector<double>{5, 144.408075, -7775.06, -18.3974, -0.00944354, 0});
				componentsHenry[0].add_henry_parameters(std::vector<double>{6, 26.6981765, -767.570007, -2.9774, 0.002192, 0});
				componentsHenry[0].set_solvent_mixing_rule(HenryComponent<Var>::MIX_ASPEN, std::vector<double>{55.9472, 117});
				componentsHenry[0].set_heat_capacity_model(HenryComponent<Var>::CPIG_DIPPR107, std::vector<double>{29.103, 10.040, 2526.5, 9.356, 1153.8});
			// N2
				componentsHenry[1].set_henry_model(HenryComponent<Var>::HENRY_ASPEN);
				componentsHenry[1].add_henry_parameters(std::vector<double>{5, 164.994075, -8432.77, -21.558, -0.00843624, 0});
				componentsHenry[1].add_henry_parameters(std::vector<double>{6, -0.34302547, -39.546001, 1.9517, -0.00808, 0});
				componentsHenry[1].set_solvent_mixing_rule(HenryComponent<Var>::MIX_ASPEN, std::vector<double>{55.9472, 117});
				componentsHenry[1].set_heat_capacity_model(HenryComponent<Var>::CPIG_DIPPR107, std::vector<double>{29.105, 8.6149, 1701.6, 0.10347, 909.79});
			// CO2
				componentsHenry[2].set_henry_model(HenryComponent<Var>::HENRY_ASPEN);
				componentsHenry[2].add_henry_parameters(std::vector<double>{5,159.865,-8741.55,-21.669,0.00110259,0});
				componentsHenry[2].add_henry_parameters(std::vector<double>{6,15.4699,-3426.7,1.5108,-0.025451,0});
				componentsHenry[2].set_solvent_mixing_rule(HenryComponent<Var>::MIX_ASPEN,std::vector<double>{117,55.9472});
				componentsHenry[2].set_heat_capacity_model(HenryComponent<Var>::CPIG_DIPPR107,std::vector<double>{29.37,34.54,1428,26.4,588});
		// Order: 0=O2, 1=N2, 2=CO2, 3=OME1, 4=MF, 5=H2O, 6=MeOH
		// NRTL model
			NRTLpars tmpNRTLpars;
			std::vector< std::vector<double> > tmpMatrix(ncomp, std::vector<double>(ncomp, 0.0));
			tmpNRTLpars.a = tmpMatrix; tmpNRTLpars.b = tmpMatrix; tmpNRTLpars.c = tmpMatrix; tmpNRTLpars.d = tmpMatrix; tmpNRTLpars.e = tmpMatrix; tmpNRTLpars.f = tmpMatrix;
			// Water - MeOH: 5 - 6; from APV88 VLE-IG
				tmpNRTLpars.a[5][6] = 2.7322;
				tmpNRTLpars.a[6][5] = -0.693;
				tmpNRTLpars.b[5][6] = -617.269;
				tmpNRTLpars.b[6][5] = 172.987;
				tmpNRTLpars.c[5][6] = 0.3;
				tmpNRTLpars.c[6][5] = 0.3;
			// Water - OME1: 5 - 3; from APV88 VLE-IG
				tmpNRTLpars.a[5][3] = 0;
				tmpNRTLpars.a[3][5] = 0;
				tmpNRTLpars.b[5][3] = 618.9311;
				tmpNRTLpars.b[3][5] = 491.2114;
				tmpNRTLpars.c[5][3] = 0.3;
				tmpNRTLpars.c[3][5] = 0.3;
			// MeOH - OME1: 6 - 3; from APV88 VLE-IG
				tmpNRTLpars.a[6][3] = 0;
				tmpNRTLpars.a[3][6] = 0;
				tmpNRTLpars.b[6][3] = 303.133;
				tmpNRTLpars.b[3][6] = 175.422;
				tmpNRTLpars.c[6][3] = 0.3;
				tmpNRTLpars.c[3][6] = 0.3;
			// MeOH - MF: 6 - 4; from APV88 VLE-IG
				tmpNRTLpars.a[6][4] = 0;
				tmpNRTLpars.a[4][6] = 0;
				tmpNRTLpars.b[6][4] = 199.0137;
				tmpNRTLpars.b[4][6] = 217.046;
				tmpNRTLpars.c[6][4] = 0.3;
				tmpNRTLpars.c[4][6] = 0.3;
			//Water - MF: 5 - 4; from R-PCES
				tmpNRTLpars.b[5][4] = 652.311583;
				tmpNRTLpars.b[4][5] = 317.398583;
				tmpNRTLpars.c[5][4] = 0.3;
				tmpNRTLpars.c[4][5] = 0.3;
			//MF - OME1: 4 - 3; from Lee et al. 2015 isobaric VLE
				tmpNRTLpars.b[4][3] = -13.502;
				tmpNRTLpars.b[3][4] = 76.25;
				tmpNRTLpars.c[4][3] = 0.3;
				tmpNRTLpars.c[3][4] = 0.3;
			NRTL NRTLmodel;
			NRTLmodel.setPars<Var>(tmpNRTLpars);



		// MeOH inlet
			Var p_MeOH = p_col;
			std::vector<Var> MeOHin(ncomp,0.);
			MeOHin[2] = Dist[0]; // CO2
			MeOHin[5] = Dist[2]; // H2O
			MeOHin[6] = Dist[1]; // MeOH
			Var HMeOHin = 0.;
			HMeOHin += MeOHin[2]*componentsHenry[2].calculate_ideal_gas_enthalpy_conv(TestTop);
			HMeOHin += MeOHin[5]*componentsSubcritical[2].calculate_ideal_gas_enthalpy_conv(TestTop);
			HMeOHin += MeOHin[6]*componentsSubcritical[3].calculate_ideal_gas_enthalpy_conv(TestTop);


		// Air inlet
			Var p_air = 1.;
			std::vector<Var> AirIn(ncomp,0.);
			AirIn[0] = 3.*MeOHin[6]; 	// Liu et al.: MeOH:O2 = 1:3
			AirIn[1] = AirIn[0]*0.79/0.21;
			Var T_AirIn = T_amb;
			Var H_AirIn = 0.;
			H_AirIn += AirIn[0]*componentsHenry[0].calculate_ideal_gas_enthalpy_conv(T_AirIn);
			H_AirIn += AirIn[1]*componentsHenry[1].calculate_ideal_gas_enthalpy_conv(T_AirIn);


		// Mixer
			Var p_MeOHAir = p_MeOH;
			std::vector<Var> MeOHAir(ncomp,0.);
			for (size_t i=0; i<ncomp; i++) {
				MeOHAir[i] = MeOHin[i] + AirIn[i];
			}
			Var Htmp = 0.;
			for (size_t i=0; i<ncompHenry; i++) {
				Htmp += MeOHAir[i]*componentsHenry[i].calculate_ideal_gas_enthalpy_conv(T_MeOHAir);
			}
			for (size_t i=0; i<ncompSubcritical; i++) {
				Htmp += MeOHAir[i+ncompHenry]*componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(T_MeOHAir);
			}
			Var HMeOHAir = HMeOHin + H_AirIn;
			result.eq.push_back((Htmp - HMeOHAir)/1e4,"EB mix");


		// Preheater
			Var p_MeOHAirHot = p_MeOHAir;
			std::vector<Var> MeOHAirHot = MeOHAir;
			Var TomePre = 110+273.15;
			Var HMeOHAirHot = 0.;
			for (size_t i=0; i<ncompHenry; i++) {
				HMeOHAirHot += MeOHAirHot[i]*componentsHenry[i].calculate_ideal_gas_enthalpy_conv(TomePre);
			}
			for (size_t i=0; i<ncompSubcritical; i++) {
				HMeOHAirHot += MeOHAirHot[i+ncompHenry]*componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(TomePre);
			}
			Var Q_OmePre = HMeOHAirHot - HMeOHAir;
			Var Tm_OmePre = lmtd(TomePre,T_MeOHAir);
			Var Ex_OmePre = Q_OmePre*(1-T_amb/Tm_OmePre);


		// Reactor
			Var p_omeProd = p_MeOHAirHot;
			// fitting parameters for selectivity; from Liu et al. 2008
				std::vector< std::vector<double> > k_yield;
				k_yield.resize(3);
				for (int i = 0; i < k_yield.size(); i++) {
						k_yield[i].resize(2, 0.0);
				}
				// 3 CH3OH + 0.5 O2 -> OME1(=C3H8O2) + 2 H2O       |   CH3OH + 0.5/3 O2 -> 1/3 OME1 + 2/3 H2O
					k_yield[0][0] = -0.00086;
					k_yield[1][0] = 408.2907;
					k_yield[2][0] = 0.60007;
				// 2 CH3OH + 1 O2 -> MF (=C2H4O2) + 2 H2O          |   CH3OH + 1/2 O2 -> 1/2 MF + H2O
					k_yield[0][1] = 0.000525;
					k_yield[1][1] = 394.514;
					k_yield[2][1] = 0.0198;
				std::vector<Var> Yield(2);
				Yield[0] = (k_yield[0][0] * pow((T_OmeReac - k_yield[1][0]), 2) + k_yield[2][0]);
				Yield[1] = (k_yield[0][1] * pow((T_OmeReac - k_yield[1][1]), 2) + k_yield[2][1]);
			// Stoichiometry
				std::vector<Var> OmeProd(ncomp);
				OmeProd[0] = MeOHAirHot[0] - MeOHAirHot[6]*(Yield[0]*0.5/3.+Yield[1]*1./2.);	// O2
				OmeProd[1] = MeOHAirHot[1];	// N2
				OmeProd[2] = MeOHAirHot[2];	// CO2
				OmeProd[3] = MeOHAirHot[3] + MeOHAirHot[6]*Yield[0]/3.;	// OME1
				OmeProd[4] = MeOHAirHot[4] + MeOHAirHot[6]*Yield[1]/2.;	// MF
				OmeProd[5] = MeOHAirHot[5] + MeOHAirHot[6]*(2.*Yield[0]/3.+Yield[1]);	// H2O
				OmeProd[6] = MeOHAirHot[6] * (1.-Yield[0]-Yield[1]);	// MeOH
				Var sumOmeProd = MeOHAirHot[0] + MeOHAirHot[1] + MeOHAirHot[2] + MeOHAirHot[3] + MeOHAirHot[4] + MeOHAirHot[5] + MeOHAirHot[6]*(1. - (0.5/3.)*Yield[0]);
				for (size_t i=0; i<ncomp; i++) {
					result.ineq.push_back((0. - OmeProd[i])/0.1,"0<=OmeProd[i]");
				}
			// Heat flow rate
				Var HomeProd = 0.;
				for (size_t i=0; i<ncompHenry; i++) {
					HomeProd += OmeProd[i]*componentsHenry[i].calculate_ideal_gas_enthalpy_conv(T_OmeReac);
				}
				for (size_t i=0; i<ncompSubcritical; i++) {
					HomeProd += OmeProd[i+ncompHenry]*componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(T_OmeReac);
				}
				Var Q_OmeReac = HomeProd - HMeOHAirHot;
				Var Tm_OmeReac = lmtd(TomePre,T_OmeReac);
				Var Ex_OmeReac = Q_OmeReac * (1 - T_amb / Tm_OmeReac);


		// Cooler (Cooling Water)
			std::vector<Var> OmeProdC = OmeProd;
			Var sumOmeProdC = sumOmeProd;
			Var p_omeProdC = p_omeProd;
			Var T_omeProdC = 273.15 + 35;
			Var H_omeProdC = 0.;
			for (size_t i=0; i<ncompHenry; i++) {
				H_omeProdC += OmeProdC[i]*componentsHenry[i].calculate_ideal_gas_enthalpy_conv(T_omeProdC);
			}
			for (size_t i=0; i<ncompSubcritical; i++) {
				H_omeProdC += OmeProdC[i+ncompHenry]*componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(T_omeProdC);
			}
			Var Q_OmeCooler = (H_omeProdC - HomeProd);
			Var Tm_OmeCooler = lmtd(T_OmeReac,T_omeProdC);
			Var Ex_OmeCooler = Q_OmeCooler*(1-T_amb/Tm_OmeCooler);


		// Cryogenic flash:
			Var p4 = p_omeProdC;
			// 1. Overall mass balance
				result.eq.push_back(((sumGas4+sumLiq4) - sumOmeProdC)/1.0);
			// 2. Calculate phase equilibrium residual & enthalpies
				Var psi4=0, sumX4=0, sumY4=0;
				Var Hliq4 = 0., Hvap4 = 0.;
				std::vector<Var> Liq4(ncomp), Gas4(ncomp);
				std::vector<Var> gamma4 = NRTLmodel.calculateGamma<Var>(T4, x4);
				for (size_t i=0; i<ncompHenry; ++i) {
					  // Phase equilibrium
					Var henry_i = componentsHenry[i].calculate_henry_mixed(T4,x4);
					sumX4 += x4[i];
					sumY4 += y4[i];
					psi4 += x4[i]-y4[i];
					Liq4[i] = sumLiq4*x4[i];
					Gas4[i] = sumGas4*y4[i];
					  // Enthalpies
					Var hig_i(componentsHenry[i].calculate_ideal_gas_enthalpy_conv(T4));
					Var deltahsol_i(componentsHenry[i].calculate_solution_enthalpy_mixed(T4,x4));
					Hvap4 += Gas4[i]*hig_i;
					Hliq4 += Liq4[i]*(hig_i-deltahsol_i);
					result.eq.push_back((p4*y4[i]-henry_i*x4[i])/1.0);
				}
				for (unsigned i=0;i<ncompSubcritical;i++) {
					  // Phase equilibrium
					Var ps_i = componentsSubcritical[i].calculate_vapor_pressure_conv(T4);
					sumX4 += x4[i+ncompHenry];
					sumY4 += y4[i+ncompHenry];
					psi4 += x4[i+ncompHenry]-y4[i+ncompHenry];
					Liq4[i+ncompHenry] = sumLiq4*x4[i+ncompHenry];
					Gas4[i+ncompHenry] = sumGas4*y4[i+ncompHenry];
					  // Enthalpies
					Var hig_i(componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(T4));
					Var deltahv_i(componentsSubcritical[i].calculate_vaporization_enthalpy(T4));
					Hvap4 += Gas4[i+ncompHenry]*hig_i;
					Hliq4 += Liq4[i+ncompHenry]*(hig_i-deltahv_i);
					result.eq.push_back((p4*y4[i+ncompHenry]-gamma4[i+ncompHenry]*ps_i*x4[i+ncompHenry])/1.0);
				}
			// 3. Energy balance:
			Var Q_OmeCryo = (Hvap4+Hliq4-H_omeProdC);
			Var Tm_OmeCryo = lmtd(T_omeProdC,T4);
			Var Ex_OmeCryo = Q_OmeCryo * (1 - T_amb / Tm_OmeCryo);
			// Overall constraints
				for (size_t i=0; i<ncomp; ++i) {
					result.eq.push_back((OmeProdC[i]-(Gas4[i] + Liq4[i]))/1.0);
				}
				result.eq.push_back((psi4-0.0)/1.0);
				// r.o.
					result.eqRelaxationOnly.push_back( (sumX4-1.0)/1.0 );
					result.eqRelaxationOnly.push_back( (sumY4-1.0)/1.0 );
			// Purity w.r.t. CO2:
				result.ineq.push_back((x4[2] - 0.001)/1e-3,"Purity, CO2");


		// Gas reheater
			std::vector<Var> GasW = Gas4;
			Var sumGasW = sumGas4;
			Var p_gasW = p4;
			Var T_gasW = 273.15 + 25;
			Var H_gasW = 0.;
			for (size_t i=0; i<ncompHenry; i++) {
				H_gasW += GasW[i]*componentsHenry[i].calculate_ideal_gas_enthalpy_conv(T_gasW);
			}
			for (size_t i=0; i<ncompSubcritical; i++) {
				H_gasW += GasW[i+ncompHenry]*componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(T_gasW);
			}
			Var Q_GasRe = (H_gasW - Hvap4);
			Var Tm_GasRe = lmtd(T_gasW,T4);
			Var Ex_GasRe = Q_GasRe*(1-T_amb/Tm_GasRe);


		// Liquid reheater - ASSUMPTION: NEGLECTING O2, N2, and CO2 from now on!
		// New order: 0=OME1, 1=MF, 2=H2O, 3=MeOH
			ncomp = ncompSubcritical;
			ncompHenry = 0;
			Var p5 = p4;
			Var T_LiqW = 273.15 + 25;
			std::vector<Var> LiqW(ncompSubcritical);
			Var sumLiqW = 0.;
			Var H_LiqW = 0.;
			for (size_t i=0; i<ncompSubcritical; i++) {
				LiqW[i] = Liq4[i+3];	// Shifting to account for old Henry components
				sumLiqW += LiqW[i];
				H_LiqW += LiqW[i]*(componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(T_LiqW) - componentsSubcritical[i].calculate_vaporization_enthalpy_conv(T_LiqW));
			}
			Var Q_LiqRe = (H_LiqW-Hliq4);
			Var Tm_LiqRe = lmtd(T_LiqW,T4);
			Var Ex_LiqRe = Q_LiqRe*(1-T_amb/Tm_LiqRe);



		// Distillation Column 2 - Cut: MF | OME1, MeOH, H2O - Underwood shortcut
			// Order in vectors: OME1, MF, H2O, MeOH
			Var p_col2 = 1.;
			// 1. Mass balances
				size_t heavyKey2 = 0;
				size_t lightKey2 = 1;
				std::vector<double> distSplit2(ncompSubcritical);
				distSplit2[0] = 0.;
				distSplit2[1] = 1.;
				distSplit2[2] = 0.;
				distSplit2[3] = 0.;
				std::vector<Var> Dist2(ncompSubcritical), Bot2(ncompSubcritical);
				Var sumDist2 = 0.;
				Var sumBot2 = 0.;
				for (size_t i=0; i<ncompSubcritical; ++i) {
					Dist2[i] = distSplit2[i] * LiqW[i];
					Bot2[i] = (1.-distSplit2[i]) * LiqW[i];
					sumDist2 += Dist2[i];
					sumBot2 += Bot2[i];

				}
				std::vector<Var> xDist2(ncompSubcritical), xBot2(ncompSubcritical);
				for (size_t i=0; i<ncompSubcritical; ++i) {
					xDist2[i] = Dist2[i]/sumDist2;
					xBot2[i] = Bot2[i]/sumBot2;
				}
			// 2. Compute reflux ratio
				// Relative volatilities - here using fixed temperature estaimtes for top and bottom
					Var TestTop2 = 31.5 + 273.15;  // boiling point of pure MF at 1 bar
					Var TestBot2 = 320;  // from anzi01
					std::vector<Var> psPseudo2(ncompSubcritical);
					for (size_t i=0; i<ncompSubcritical; ++i) {
						psPseudo2[i] = sqrt(componentsSubcritical[i].calculate_vapor_pressure_conv(TestBot2)*componentsSubcritical[i].calculate_vapor_pressure_conv(TestTop2));
					}
				// compute Underwood factor
					Var underwoodRhs2 = 0.;
					for (size_t i=0; i<ncompSubcritical; ++i) {
						Var tmp = psPseudo2[i]*LiqW[i];
						for (size_t j=0; j<ncompSubcritical; ++j) {
							if (j!=i) {
								tmp = tmp*(psPseudo2[j]-Theta2*psPseudo2[heavyKey2]);
							}
						}
						underwoodRhs2 += tmp;
					}
					result.eq.push_back( (underwoodRhs2) / 1e0, "Dist, Theta2");
					result.ineq.push_back( (psPseudo2[heavyKey2]*Theta2 - psPseudo2[lightKey2]) / 1e1, "Theta2<=alphaLS2");
				// compute reflux ratio - special case: distillate = sinlge substance (lightKey2 only)
					Var rRHS2 = psPseudo2[lightKey2];
					Var rLHS2 = (Rmin2+1.)*(psPseudo2[lightKey2]-Theta2*psPseudo2[heavyKey2]);
					result.eq.push_back( (rRHS2 - rLHS2) / 1e0, "Dist, Rmin2");
					Var R2 = Rmin2 * 1.2;
			// 3. Compute reboiler duty
				// Condenser:
					std::vector<Var> Vap2(ncompSubcritical);
					for (size_t i=0; i<ncompSubcritical; ++i) {
						Vap2[i] = (1.+R2)*Dist2[i];
					}
					// energy balance: assume the temperature of Vap is approximately that in the condenser
					Var Q_Cond2 = 0.;
					for (size_t i=0; i<ncompSubcritical; ++i) {
						Q_Cond2 -= Vap2[i]*componentsSubcritical[i].calculate_vaporization_enthalpy_conv(TestTop2);
					}
					Var Ex_Cond2 = Q_Cond2*(1-T_amb/TestTop2);
					Var HDist2 = 0.;
					for (size_t i=0; i<ncompSubcritical; ++i) {
						HDist2 += Dist2[i]*( componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(TestTop2)
									    -componentsSubcritical[i].calculate_vaporization_enthalpy_conv(TestTop2));
					}
				// Reboiler:
					Var HBot2 = 0.;
					for (size_t i=0; i<ncompSubcritical; ++i) {
						HBot2 += Bot2[i]*( componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(TestBot2)
									    -componentsSubcritical[i].calculate_vaporization_enthalpy_conv(TestBot2));
					}
					Var Q_Reb2 = HDist2 + HBot2 - Q_Cond2 - H_LiqW;
					Var Ex_Reb2 = Q_Reb2*(1-T_amb/TestBot2);


		// Tear "input": Azeotrope recycle
			std::vector<Var> AzeoReGuess(ncompSubcritical,0.);
			AzeoReGuess[0] = azeoReOME1;
			AzeoReGuess[3] = azeoReMeOH;
			Var H_AzeoReGuess = 0.;
			Var T_AzeoReGuess = 128.2+273.15;
			for (size_t i=0; i<ncompSubcritical; ++i) {
				H_AzeoReGuess += AzeoReGuess[i]*( componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(T_AzeoReGuess)
												-componentsSubcritical[i].calculate_vaporization_enthalpy_conv(T_AzeoReGuess));
			}



		// Mixer: mixing recycle of azeotrope from Column 4
			Var p_mix3 = p_col2;
			std::vector<Var> Mix3(ncompSubcritical);
			for (size_t i=0; i<ncompSubcritical; ++i) {
				Mix3[i] = Bot2[i] + AzeoReGuess[i];
			}
			Var H_Mix3 = HBot2 + H_AzeoReGuess;



		// Distillation Column 3 - Cut: OME1/MeOH Azeo1 | MeOH, H2O - Underwood shortcut
			// Order in vectors: OME1, MF, H2O, MeOH
			Var p_col3 = 0.64;
			// 1. Mass balances
				std::vector<double> distSplit3(ncompSubcritical);
				distSplit3[0] = 1.;
				distSplit3[1] = 1.;
				distSplit3[2] = 0.;
				std::vector<Var> Dist3(ncompSubcritical), Bot3(ncompSubcritical);
				Var sumDist3 = 0.;
				Var sumBot3 = 0.;
				for (size_t i=0; i<ncompSubcritical; ++i) {
					if (i!=3) {
						Dist3[i] = distSplit3[i] * Mix3[i];
						Bot3[i] = (1.-distSplit3[i]) * Mix3[i];
						sumDist3 += Dist3[i];
						sumBot3 += Bot3[i];
					} else {
						Dist3[3] = 0.09/0.91*Dist3[0];
						Bot3[3] = Mix3[3] - Dist3[3];
						result.ineq.push_back((1e-3-Bot3[3])/1e-3,"1e-3<=Bot3[3]");
						Bot3[3] = max(Bot3[3],1e-3);
						sumDist3 += Dist3[i];
						sumBot3 += Bot3[i];
					}

				}
				std::vector<Var> xDist3(ncompSubcritical), xBot3(ncompSubcritical);
				for (size_t i=0; i<ncompSubcritical; ++i) {
					xDist3[i] = Dist3[i]/sumDist3;
					xBot3[i] = Bot3[i]/sumBot3;
				}
			// 2. Compute reflux ratio
				// For the Underwood method, we work with the Pseudocomponent Azeo1:
				// Azeo1, MeOH, H2O
				size_t ncompDist3 = 3;
				std::vector<size_t> imap3(ncompDist3);
				imap3[0] = 0;  // This is the pseudocomponent! Mapping it to OME1 since this determines the molar flow rate of the Pseudocomponent!
				imap3[1] = 3;  // MeOH - CAREFUL in mole balances - only use the methanol that is left over from the azeotrope!
				imap3[2] = 2;  // H2O
				size_t lightKey3 = 0;
				size_t heavyKey3 = 1;
				// Relative volatilities - here using fixed temperature estaimtes for top and bottom
					Var TestTop3 = 303;  // from anzi01
					Var TestBot3 = 331;  // from anzi01
					std::vector<Var> psPseudo3(ncompDist3);
					psPseudo3[0] = sqrt(0.651*1.783);	// Vapor pressures of the azeotrope at 303 and 331 K, respectively; obtained from AspenPlus with the same property model (incl. NRTL)
					for (size_t i=1; i<ncompDist3; ++i) {
						psPseudo3[i] = sqrt(componentsSubcritical[imap3[i]].calculate_vapor_pressure_conv(TestBot3)*componentsSubcritical[imap3[i]].calculate_vapor_pressure_conv(TestTop3));
					}
				// compute Underwood factor
					Var underwoodRhs3 = 0.;
					for (size_t i=0; i<ncompDist3; ++i) {
						Var tmp;
						if (i!=1) {
							tmp = psPseudo3[i]*Mix3[imap3[i]];
						} else {
							tmp = psPseudo3[1]*(Mix3[imap3[1]]-0.09/0.91*Mix3[imap3[0]]);	// For MeOH, only using what is there in addition to the azeotrope
						}
						for (size_t j=0; j<ncompDist3; ++j) {
							if (j!=i) {
								tmp = tmp*(psPseudo3[j]-Theta3*psPseudo3[heavyKey3]);
							}
						}
						underwoodRhs3 += tmp;
					}
					result.eq.push_back( (underwoodRhs3) / 1e0, "Dist, Theta3");
					result.ineq.push_back( (psPseudo3[heavyKey3]*Theta3 - psPseudo3[lightKey3]) / 1e1, "Theta3<=alphaLS3");
				// compute reflux ratio - special case: distillate = sinlge substance (lightKey2 only)
					Var rRHS3 = psPseudo3[lightKey3];
					Var rLHS3 = (Rmin3+1.)*(psPseudo3[lightKey3]-Theta3*psPseudo3[heavyKey3]);
					result.eq.push_back( (rRHS3 - rLHS3) / 1e0, "Dist, Rmin3");
					Var R3 = Rmin3 * 1.2;
			// 3. Compute reboiler duty - now using actual components again
				// Condenser:
					std::vector<Var> Vap3(ncompSubcritical);
					for (size_t i=0; i<ncompSubcritical; ++i) {
						Vap3[i] = (1.+R3)*Dist3[i];
					}
					// energy balance: assume the temperature of Vap is approximately that in the condenser
					Var Q_Cond3 = 0.;
					for (size_t i=0; i<ncompSubcritical; ++i) {
						Q_Cond3 -= Vap3[i]*componentsSubcritical[i].calculate_vaporization_enthalpy_conv(TestTop3);
					}
					Var Ex_Cond3 = Q_Cond3*(1-T_amb/TestTop3);
					Var HDist3 = 0.;
					for (size_t i=0; i<ncompSubcritical; ++i) {
						HDist3 += Dist3[i]*( componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(TestTop3)
									    -componentsSubcritical[i].calculate_vaporization_enthalpy_conv(TestTop3));
					}
				// Reboiler:
					Var HBot3 = 0.;
					for (size_t i=0; i<ncompSubcritical; ++i) {
						HBot3 += Bot3[i]*( componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(TestBot3)
									    -componentsSubcritical[i].calculate_vaporization_enthalpy_conv(TestBot3));
					}
					Var Q_Reb3 = HDist3 + HBot3 - Q_Cond3 - H_Mix3;
					Var Ex_Reb3 = Q_Reb3*(1-T_amb/TestBot3);



		// Purge splitter crude MeOH recycle
			std::vector<Var> CrudeMeOHRecCalc(ncompSubcritical), Purge2(ncompSubcritical);
			double kPurge2 = 0.01;
			for (size_t i=0; i<ncompSubcritical; ++i) {
				CrudeMeOHRecCalc[i] = (1.-kPurge2)*Bot3[i];
				Purge2[i] = kPurge2*Bot3[i];
			}
			Var H_Purge2 = kPurge2*HBot3;



		// Tear "output": crude MeOH recýcle
			// Recall that the indices are different in the first part...!!
			result.eq.push_back((CrudeMeOHRecCalc[2] - CrudeMeOHRecGuess[4])/0.1);	// H2O
			result.eq.push_back((CrudeMeOHRecCalc[3] - CrudeMeOHRecGuess[3])/0.1);	// MeOH



		// Distillation Column 4 - Cut: OME1/MeOH Azeo2 | OME1 - Underwood shortcut
			// Order in vectors: OME1, MF, H2O, MeOH
			Var p_col4 = 12.;	// Neglecting pumping power
			// 1. Mass balances
				std::vector<double> distSplit4(ncompSubcritical);
				// distSplit4[0] = ?;	// OME1 determined by azeotrope
				distSplit4[1] = 1.;
				distSplit4[2] = 0.;
				distSplit4[3] = 1.;	// MeOH actually goes to the top b/c of the azeotrope!
				std::vector<Var> Dist4(ncompSubcritical), Bot4(ncompSubcritical);
				Var sumDist4 = 0.;
				Var sumBot4 = 0.;
				for (size_t i=0; i<ncompSubcritical; ++i) {
					if (i!=0) {
						Dist4[i] = distSplit4[i] * Dist3[i];
						Bot4[i] = (1.-distSplit4[i]) * Dist3[i];
						sumDist4 += Dist4[i];
						sumBot4 += Bot4[i];
					}
				}
				Dist4[0] = 0.61/0.39*Dist4[3];
				Bot4[0] = Dist3[0] - Dist4[0];
				sumDist4 += Dist4[0];
				sumBot4 += Bot4[0];
				std::vector<Var> xDist4(ncompSubcritical), xBot4(ncompSubcritical);
				for (size_t i=0; i<ncompSubcritical; ++i) {
					xDist4[i] = Dist4[i]/sumDist4;
					xBot4[i] = Bot4[i]/sumBot4;
				}
				Var Ex_OME1 = Bot4[0] * e_OME1;
			// 2. Compute reflux ratio
				// For the Underwood method, we work with the Pseudocomponent Azeo2:
					// Azeo2, OME1
					size_t ncompDist4 = 2;
					std::vector<size_t> imap4(ncompDist4);
					imap4[0] = 3;  // This is the pseudocomponent! Mapping it to MeOH since this determines the molar flow rate of the Pseudocomponent!
					imap4[1] = 0;  // OME1 - CAREFUL in mole balances - only use the OME1 that is left over from the azeotrope!
					size_t lightKey4 = 0;
					size_t heavyKey4 = 1;
				// Relative volatilities - here using fixed temperature estaimtes for top and bottom
					Var TestTop4 = 128.2+273.15;
					Var TestBot4 = 136.1+273.15;
					std::vector<Var> psPseudo4(ncompDist4);
					psPseudo4[0] = sqrt(12.*14.26);	// Vapor pressures of the azeotrope at TestTop4 and TestBot4, respectively; obtained from AspenPlus with the same property model (incl. NRTL)
					psPseudo4[1] = sqrt(componentsSubcritical[imap4[1]].calculate_vapor_pressure_conv(TestBot4)*componentsSubcritical[imap4[1]].calculate_vapor_pressure_conv(TestTop4));
				// compute Underwood factor
					Var underwoodRhs4 = 0.;
					for (size_t i=0; i<ncompDist4; ++i) {
						Var tmp;
						if (i!=1) {
							tmp = psPseudo4[i]*Dist3[imap4[i]];
						} else {
							tmp = psPseudo4[1]*(Dist3[imap4[1]]-0.61/0.39*Dist3[imap4[0]]);
						}
						for (size_t j=0; j<ncompDist4; ++j) {
							if (j!=i) {
								tmp = tmp*(psPseudo4[j]-Theta4*psPseudo4[heavyKey4]);
							}
						}
						underwoodRhs4 += tmp;
					}
					result.eq.push_back( (underwoodRhs4) / 1e0, "Dist, Theta4");
					result.ineq.push_back( (psPseudo4[heavyKey4]*Theta4 - psPseudo4[lightKey4]) / 1e1, "Theta4<=alphaLS4");
				// compute reflux ratio - special case: distillate = sinlge substance (lightKey2 only)
					Var rRHS4 = psPseudo4[lightKey4];
					Var rLHS4 = (Rmin4+1.)*(psPseudo4[lightKey4]-Theta4*psPseudo4[heavyKey4]);
					result.eq.push_back( (rRHS4 - rLHS4) / 1e0, "Dist, Rmin4");
					Var R4 = Rmin4 * 1.2;
			// 3. Compute reboiler duty
				// Condenser:
					std::vector<Var> Vap4(ncompSubcritical);
					for (size_t i=0; i<ncompSubcritical; ++i) {
						Vap4[i] = (1.+R4)*Dist4[i];
					}
					// energy balance: assume the temperature of Vap is approximately that in the condenser
					Var Q_Cond4 = 0.;
					for (size_t i=0; i<ncompSubcritical; ++i) {
						Q_Cond4 -= Vap4[i]*componentsSubcritical[i].calculate_vaporization_enthalpy_conv(TestTop4);
					}
					Var Ex_Cond4 = Q_Cond4*(1-T_amb/TestTop4);
					Var HDist4 = 0.;
					for (size_t i=0; i<ncompSubcritical; ++i) {
						HDist4 += Dist4[i]*( componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(TestTop4)
											-componentsSubcritical[i].calculate_vaporization_enthalpy_conv(TestTop4));
					}
				// Reboiler:
					Var HBot4 = 0.;
					for (size_t i=0; i<ncompSubcritical; ++i) {
						HBot4 += Bot4[i]*( componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(TestBot4)
									     -componentsSubcritical[i].calculate_vaporization_enthalpy_conv(TestBot4));
					}
					Var Q_Reb4 = HDist4 + HBot4 - Q_Cond4 - HDist3;
					Var Ex_Reb4 = Q_Reb4*(1-T_amb/TestBot4);



		// Purge splitter azeotrope recycle
			std::vector<Var> AzeoReCalc(ncompSubcritical), Purge3(ncompSubcritical);
			double kPurge3 = 0.01;
			for (size_t i=0; i<ncompSubcritical; ++i) {
				AzeoReCalc[i] = (1.-kPurge3)*Dist4[i];
				Purge3[i] = kPurge3*Dist4[i];
			}
			Var H_Purge3 = kPurge3*HDist4;



		// Tear "output": azeotrope recýcle
			result.eq.push_back((AzeoReCalc[0] - AzeoReGuess[0])/0.1);
			result.eq.push_back((AzeoReCalc[3] - AzeoReGuess[3])/0.1);




		// OME1 cooler
			std::vector<Var> OME1out = Bot4;
			Var T_OME1out = 35+273.15;
			Var p_OME1out = 1;
			Var H_OME1out = 0.;
			for (size_t i=0; i<ncompSubcritical; i++) {
				H_OME1out += OME1out[i]*( componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(T_OME1out)
									   -componentsSubcritical[i].calculate_vaporization_enthalpy_conv(T_OME1out));
			}
			Var Q_OME1 = H_OME1out - HBot4;
			Var Tm_OME1 = lmtd(TestBot4,T_OME1out);
			Var Ex_OME1HX = Q_OME1*(1-T_amb/Tm_OME1);


		// Purge combustor2
			// Inlet
			Var O2demandPurge2 = 4.*Purge3[0] /*OME1*/ + 2.*Dist2[1] /*MF*/ + 1.5*(Purge3[3]+Purge2[3]) /*MeOH*/;
			double lambdaPurge2 = 5;
			Var O2InPurgeComb2 = lambdaPurge2*O2demandPurge2;
			Var N2InPurgeComb2 = lambdaPurge2*O2demandPurge2*0.79/0.21;
			Var H_CombustionAir2 = 0.;
			H_CombustionAir2 += O2InPurgeComb2*componentsHenry[0].calculate_ideal_gas_enthalpy_conv(T_amb);
			H_CombustionAir2 += N2InPurgeComb2*componentsHenry[1].calculate_ideal_gas_enthalpy_conv(T_amb);
			// Outlet - after HRSG
			Var O2OutPurgeComb2 = (lambdaPurge2-1)*O2demandPurge2;
			Var N2OutPurgeComb2 = N2InPurgeComb2;
			Var CO2OutPurgeComb2 = 3.*Purge3[0] /*OME1*/ + 2.*Dist2[1] /*MF*/ + (Purge3[3]+Purge2[3]) /*MeOH*/;
			Var H2OOutPurgeComb2 = 4.*Purge3[0] /*OME1*/ + 2.*Dist2[1] /*MF*/ + 2.*(Purge3[3]+Purge2[3]) /*MeOH*/ + (Purge3[2]+Purge2[2]) /*H2O*/;
			Var T_HrsgIn2 = 600 + 273.15;
			Var T_HrsgOut2 = 80 + 273.15;
			Var H_HrsgOut2 = 0.;
			H_HrsgOut2 += O2OutPurgeComb2*componentsHenry[0].calculate_ideal_gas_enthalpy_conv(T_HrsgOut2);
			H_HrsgOut2 += N2OutPurgeComb2*componentsHenry[1].calculate_ideal_gas_enthalpy_conv(T_HrsgOut2);
			H_HrsgOut2 += CO2OutPurgeComb2*componentsHenry[2].calculate_ideal_gas_enthalpy_conv(T_HrsgOut2);
			H_HrsgOut2 += H2OOutPurgeComb2*componentsSubcritical[2].calculate_ideal_gas_enthalpy_conv(T_HrsgOut2);
			Var Q_Hrsg2 = H_HrsgOut2 - (HDist2+H_Purge2+H_Purge3+H_CombustionAir2);
			Var Tm_HRSG2 = lmtd(T_HrsgIn2,T_HrsgOut2);
			Var Ex_Hrsg2 = Q_Hrsg2*(1-T_amb/Tm_HRSG2);




	// Prepare output
		Var Ex_Qin = Ex_Preheater + Ex_Reb + Ex_OmePre + Ex_OmeCryo + Ex_Reb2 + Ex_Reb3 + Ex_Reb4;
		Var Ex_Qout = Ex_Reactor + Ex_LP + Ex_Flash1 + Ex_Cond + Ex_H2O + Ex_OmeReac + Ex_OmeCooler + Ex_GasRe + Ex_LiqRe + Ex_Cond2 + Ex_Cond3 + Ex_Cond4 + Ex_Hrsg + Ex_Hrsg2 + Ex_OME1HX;


		result.ineq.push_back((0. - ( (Wc_CO2+Wc_Rec) + Ex_Qin ))/1e6,"0<=numerator");

		// Objective:
			Var eta_exChem = ( Ex_OME1 - Ex_Qout ) / ( (Ex_H2+Ex_CO2) + max( (Wc_CO2+Wc_Rec) + Ex_Qin, 0. ) );
			result.objective = -eta_exChem;

		return result;
}