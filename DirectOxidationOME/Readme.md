# DirectOxidationOME

Flowsheet optimization problem for maximizing the exergy efficiency of a process for producing OME1 (also known as methylal or dimethoxymehtane) from hydrogen and carbon dioxide via direct oxidation of methanol.
Phase equilibrium is descibed using the extended Antoine equation for vapor pressures, the Henry's constant model and mixing rule from AspenPlus, and the NRTL model (for parts of the flowsheet).
Enthalpies are computed using the DIPPR107 equation for ideal gas heat capacity, the DIPPR 106 equation for enthalpy of vaporization, the Henry's constant model for enthalpy of solution, and the NRTL model for excess enthalpy in the respective parts of the flwosheet.
The distillation columns in the process are modeled via the Underwood shortcut.
The MAiNGO version of the problems (both for the C++ API and the text-based versions) use intrinsic functions and custom relaxations for the above-mentioned property models.

The problem was first implemented for the following publication:
 - Bongartz, D. (2020). [Deterministic global flowsheet optimization for the design of energy conversion processes](https://doi.org/10.18154/RWTH-2020-06052). Ph.D. Thesis, RWTH Aachen University.                        



| Instance        | Problem Type | Number of Continuous Variables | Number of Integer Variables | Number of Binary Variables | Number of Equalities | Number of Inequalities | Number of Relaxation-Only Constraints* | Best Known Objective Value | Proven Objective Bound | Description           |
| ------------- |-------------| ------------| ------------| ------------| ------------| ------------| ------------| :------------: | :-----: | ----- |
| DirectOxidationOME  | DNLP | 58 | 0 | 0 | 53 | 19 | 11 | -0.833 | -0.842 | The problem is described in Section 5.3 of Bongartz (2020) |

*The problems contain the redundant summation equations for the liquid and vapor phase mole fractions in the flash units as well as redundant overall balances around the flowsheet. In the MAiNGO version of the problems, these are implemented as relaxation-only constraints as proposed by [Sahinidis & Tawarmalani](https://link.springer.com/article/10.1007/s10898-004-2705-8). In the GAMS version of the problems, they are implemented as regular constraints because GAMS itself does not allow labelling constraints as relaxation-only. However, certain solvers may be able to label them as relaxation-only via their settings file (e.g., for BARON in baron.opt).
