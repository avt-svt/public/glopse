# TrainingArtificialNeuralNetworks

Parameter estimation problem for training an artificial neural network with one hidden layer and one output value with weight decay as a regularization.
In the C++ header files for MAiNGO, we provide the summed squared prediction error and the mean squared prediction error as the objective for the standard B&B algorithm as well as the squared prediction error for each data point as the objective per data used in the B&B algorithm with growing datasets, see [manual of MAiNGO](https://avt-svt.pages.rwth-aachen.de/public/maingo/special_uses.html#growing_datasets)  and [Sass et al. (2024a)](https://doi.org/10.1016/j.ejor.2024.02.020).
For changing the number of inputs and neurons, please adapt parameters \_noOfInputs and \_noOfHiddenNeurons, respectively, see Lines 109 and 110 of *TrainingArtificialNeuralNetworks.h*.
In the GAMS file, we formulate all model equations as equalities in dependence of the data points to increase the readability of the model.

We use the Bike Sharing Dataset provided in the following publications:
 - Fanaee-T, H. (2013). [Bike Sharing Dataset](https://www.doi.org/10.24432/C5W894), *UCI Machine Learning Repository*.
 - Fanaee-T, H., Gama, J. (2014). [Event labeling combining ensemble detectors and background knowledge](https://www.doi.org/10.1007/s13748-013-0040-3). *Progress in Artificial Intelligence*, 2(2-3), 113-127.

Features _cnt_, _mnth_, and _temp_ are saved in this order in csv-files, where the first feature is used as the output variable and the following features are used as the input variables.
Note that we randomly split the dataset into training and validation data.
Note further that the data is stored in rows to enable an efficient reading of the data into the MAiNGO model.

The problem was originally implemented for the following publication:
 - Sass, S., Mitsos, A., Nikolov, N. I., & Tsoukalas, A. (2024b).  Out-of-sample estimation for a branch-and-bound algorithm with growing datasets. Submitted.
 
| Instance        | Problem Type | Number of Continuous Variables | Number of Integer Variables | Number of Binary Variables | Number of Equalities | Number of Inequalities | Best Known Objective Value* | Proven Objective Bound* | Description           |
| ------------- |-------------| ------------| ------------| ------------| ------------| ------------| :------------: | :-----: | ----- |
| TrainingArtificialNeuralNetworks | NLP | 21** | 0 | 0 | 0 | 0 |  7.2233*** | 0.0072*** | Model trainANN of Sass et al. (2024b) |

*The values correspond to using the summed squared error as the objective function.

**In general, (No. of inputs + 1)*(No. of hidden neurons) weights and (No. of hidden neurons + 1) biases

***Calculated with the SSE heuristic of the B&B algorithm with growing datasets applying augmentation rule SCALING and resampling of the initial dataset within a CPU time limit of 23h, which gives a deterministic global optimization method in this case, see Sass et al. (2024b)