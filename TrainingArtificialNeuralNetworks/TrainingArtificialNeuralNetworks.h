/**
 *
 * @brief trainANN model using the Bike Sharing Dataset provided in
 *        H. Fanaee-T: "Bike Sharing Dataset", UCI Machine Learning Repository (2013).
 *              https://www.doi.org/10.24432/C5W894
 *        H. Fanaee-T, J. Gama: "Event labeling combining ensemble detectors and
 *              background knowledge", Progress in Artificial Intelligence 2(2-3),
 *              113-127 (2014).
 *              https://www.doi.org/10.1007/s13748-013-0040-3
 *        that was originally implemented for
 *        S. Sass, A. Mitsos, N. I. Nikolov, A. Tsoukalas: "Out-of-sample
 *              estimation for a branch-and-bound algorithm with growing datasets",
 *              Submitted 2024.
 *        E-mail: amitsos@alum.mit.edu
 *
 * ==============================================================================
 * © 2024, Process Systems Engineering (AVT.SVT), RWTH Aachen University
 * ==============================================================================
 */

#pragma once

#include "MAiNGOmodel.h"


namespace userInput {
    // Enter the path to the directory containing the csv-file with the data
    std::string pathToCSVfile = "../GloPSE/TrainingArtificialNeuralNetworks/data/";
}

///////////////
//// 1. Model-specific auxiliary functions etc.
///////////////

//////////////////////////////////////////////////////////////////////////
// struct for saving data points of full dataset
struct AnnData {
    std::vector<double> output;
    std::vector<std::vector<double>> input;
};

//////////////////////////////////////////////////////////////////////////
// regularization
namespace ANN {
    const double lambda = 0.01; // Regularization parameter
}


///////////////
//// 2. Data input
///////////////

//////////////////////////////////////////////////////////////////////////
// auxiliary function for reading data from csv-file
void read_csv_to_vector(const std::string name, std::vector<std::vector<double>> &data)
{
    // Open data file
    std::ifstream infile;
    infile.open(name);
    if (infile.fail()) {
        throw std::runtime_error("Error in model: Unable to open data file "+name);
    }
    else {
        std::cout << "Read data from " << name << std::endl;
    }

    std::string line, tmpCooStr;
    std::vector<double> tmpData;
    double tmpCoo;
    data.clear();
    // Read all lines
    while (std::getline(infile, line)) {
        // Split line into coordinates
        std::istringstream stream(line);
        tmpData.resize(0);
        // First entry
        stream >> tmpCoo;
        tmpData.push_back(tmpCoo);
        // Read all remaining entries
        while (std::getline(stream, tmpCooStr, ';')) {
            stream >> tmpCoo;
            tmpData.push_back(tmpCoo);
        }
        data.push_back(tmpData);
    }
}


///////////////
//// 3. MAiNGO model
///////////////

class Model: public maingo::MAiNGOmodel {

  public:

    Model();

    maingo::EvaluationContainer evaluate(const std::vector<Var> &optVars);
    std::vector<maingo::OptimizationVariable> get_variables();
    std::vector<double> get_initial_point();

  private:
    // Fixed parameters
    const unsigned int _noOfInputs        = 2;
    const unsigned int _noOfHiddenNeurons = 5;

    // Measurement data
    unsigned int _noOfDataPoints; // Number of data points in full dataset
    AnnData _annData;             // Full dataset
};


//////////////////////////////////////////////////////////////////////////
// function for providing optimization variable data to the Branch-and-Bound solver
std::vector<maingo::OptimizationVariable>
Model::get_variables()
{

    std::vector<maingo::OptimizationVariable> variables;

    // weights input layer -> hidden layer
    for (auto u = 0; u < _noOfInputs; u++) {
        for (auto l = 0; l < _noOfHiddenNeurons; l++) {
            variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0., 1.), maingo::VT_CONTINUOUS, "weight (in " + std::to_string(u) + " to hidden " + std::to_string(l) + ")"));
        }
    }
    // weights hidden layer -> output layer
    for (auto l = 0; l < _noOfHiddenNeurons; l++) {
        variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0., 1.), maingo::VT_CONTINUOUS, "weight (hidden " + std::to_string(l) + " to out)"));
    }
    // bias for hidden layer
    for (auto i = 0; i < _noOfHiddenNeurons; i++) {
        variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0., 1.), maingo::VT_CONTINUOUS, "biasHidden[" + std::to_string(i) + "]"));
    }
    // bias for output layer
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0., 1.), maingo::VT_CONTINUOUS, "biasOut"));

    return variables;
}


//////////////////////////////////////////////////////////////////////////
// function for providing initial point data to the Branch-and-Bound solver
std::vector<double>
Model::get_initial_point()
{

    // The initial point needs to be adapted when changing the number of
    // hidden neurons or inputs
    std::vector<double> initialPoint = {
        // random weights between 0 and 1
        0.3500,
        0.1966,
        0.2511,
        0.6160,
        0.4733,
        0.3517,
        0.8308,
        0.5853,
        0.5497,
        0.9172,
        0.2858,
        0.7572,
        0.0537,
        0.3804,
        0.5678,
        // biases
        0,
        0,
        0,
        0,
        0,
        0
    };
 
    return initialPoint;
}


//////////////////////////////////////////////////////////////////////////
// constructor for the model
Model::Model()
{
    // Catch measurement data from file
    std::string nameFile = userInput::pathToCSVfile + "data_FanaeeT2013_training.csv";
    std::vector<std::vector<double>> tmpData;
    tmpData.clear();
    read_csv_to_vector(nameFile, tmpData);

    // Plug data into struct
    _annData.output = tmpData[0];
    _annData.input.clear();
    for(auto i = 1; i < tmpData.size(); i++){
        _annData.input.push_back(tmpData[i]);
    }
    if (_annData.input.size() != _noOfInputs) {
        throw std::runtime_error("Error in model: Unexpected number of input variables in data file " + nameFile);
    }

    // Catch size of full dataset
    _noOfDataPoints = _annData.output.size();
}


//////////////////////////////////////////////////////////////////////////
// evaluate the model
maingo::EvaluationContainer
Model::evaluate(const std::vector<Var> &optVars)
{
    // Catch optimization variables
    // The vector optVars is of the same size and sorted in the same order as the user-defined variables vector in function get_variables()
    unsigned int iVar = 0;
    std::vector<std::vector<Var>> weightIn;
    std::vector<Var> weightHidden, biasHidden;

    // weights input layer -> hidden layer
    weightIn.resize(_noOfInputs);
    for (auto u = 0; u < _noOfInputs; u++) {
        weightIn[u].resize(_noOfHiddenNeurons);
        for (auto l = 0; l < _noOfHiddenNeurons; l++) {
            weightIn[u][l] = optVars.at(iVar);
            iVar++;
        }
    }
    // weights hidden layer -> output layer
    weightHidden.resize(_noOfHiddenNeurons);
    for (auto l = 0; l < _noOfHiddenNeurons; l++) {
        weightHidden[l] = optVars.at(iVar);
        iVar++;
    }
    // bias for hidden layer
    biasHidden.resize(_noOfHiddenNeurons);
    for (auto l = 0; l < _noOfHiddenNeurons; l++) {
        biasHidden[l] = optVars.at(iVar);
        iVar++;
    }
    // bias for output
    Var biasOut = optVars.at(iVar);
    iVar++;

    // Model
    maingo::EvaluationContainer result;

    Var hiddenIn, output;
    std::vector<Var> hiddenOut;
    hiddenOut.resize(_noOfHiddenNeurons);

    // Calculate squared Euclidean norm of weights
    Var regWeightIn = 0;
    for (auto i = 0; i < weightIn.size(); i++) {
        for (auto j = 0; j < weightIn[i].size(); j++) {
            regWeightIn = regWeightIn + sqr(weightIn[i][j]);
        }
    }

    Var regWeightHidden = 0;
    for (auto i = 0; i < weightHidden.size(); i++) {
        regWeightHidden = regWeightHidden + sqr(weightHidden[i]);
    }

    // Calculate constant regularization term for each data point
    Var regularization = ANN::lambda / _noOfDataPoints * (regWeightIn + regWeightHidden);

    Var se = 0; // Summed squared prediction error (SSE) + regularization
    for (auto i = 0; i < _noOfDataPoints; i++){
        // Hidden layer
        hiddenIn = 0;
        for (auto l = 0; l < _noOfHiddenNeurons; l++) {
            // Input layer
            for (auto u = 0; u < _noOfInputs; u++) {
                hiddenIn = hiddenIn + weightIn[u][l] * _annData.input[u][i];
            }
            hiddenIn = hiddenIn + biasHidden[l];
            hiddenOut[l] = tanh(hiddenIn);
        }

        // Output layer with 1 output
        output = 0;
        for (auto l = 0; l < _noOfHiddenNeurons; l++) {
            output = output + weightHidden[l] * hiddenOut[l];
        }
        output = output + biasOut;

        Var sePerData = sqr(output - _annData.output[i]) + regularization;
        se += sePerData;

        // Objective for B&B algorithm with growing datasets
        result.objective_per_data.push_back(sePerData);
    }
    // Objective for standard B&B algorithm
    result.objective = se; // SSE
//    result.objective = se / _noOfDataPoints; // Mean squared error (MSE)

    return result;
}