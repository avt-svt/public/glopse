* trainANN model using the Bike Sharing Dataset provided in
*     H. Fanaee-T: "Bike Sharing Dataset", UCI Machine Learning Repository (2013).
*          https://www.doi.org/10.24432/C5W894
*     H. Fanaee-T, J. Gama: "Event labeling combining ensemble detectors and
*          background knowledge", Progress in Artificial Intelligence 2(2-3),
*          113-127 (2014).
*          https://www.doi.org/10.1007/s13748-013-0040-3
* that was originally implemented for
*     S. Sass, A. Mitsos, N. I. Nikolov, A. Tsoukalas: "Out-of-sample
*          estimation for a branch-and-bound algorithm with growing datasets",
*          Submitted 2024.
* E-mail: amitsos@alum.mit.edu
*
* Copyright (c) 2024 Process Systems Engineering (AVT.SVT), RWTH Aachen University

*
* Optimization variables
*

*Continuous variables
Set
    idxVar    'index of variable (x1 = output)'   / x1*x3  /
    idxInput  'index of input variables'   / x2*x3  /
    idxHidden 'index of hidden neuron'   / h1*h5  /
    ;
Variables weightIn(idxInput,idxHidden), weightHidden(idxHidden), biasHidden(idxHidden), biasOut, objectiveVar;

*Continuous variable bounds
weightIn.LO(idxInput,idxHidden) = 0.;
weightIn.UP(idxInput,idxHidden) = 1.;
weightHidden.LO(idxHidden) = 0.;
weightHidden.UP(idxHidden) = 1.;
biasHidden.LO(idxHidden) = 0.;
biasHidden.UP(idxHidden) = 1.;
biasOut.LO = 0.;
biasOut.UP = 1.;

*Random initial point
weightIn.L('x2','h1') = 0.3500;
weightIn.L('x2','h2') = 0.1966;
weightIn.L('x2','h3') = 0.2511;
weightIn.L('x2','h4') = 0.6160;
weightIn.L('x2','h5') = 0.4733;
weightIn.L('x3','h1') = 0.3517;
weightIn.L('x3','h2') = 0.8308;
weightIn.L('x3','h3') = 0.5853;
weightIn.L('x3','h4') = 0.5497;
weightIn.L('x3','h5') = 0.9172;
weightHidden.L('h1') = 0.2858;
weightHidden.L('h2') = 0.7572;
weightHidden.L('h3') = 0.0537;
weightHidden.L('h4') = 0.3804;
weightHidden.L('h5') = 0.5678;
biasHidden.L(idxHidden) = 0.;
biasOut.L = 0.;


*
* Constants and data
*

*Constants
Scalar
    lambda 'Regularization parameter' / 0.01 /
    ;

*Dataset
Set
   idxData  'index of data point'
   ivar(idxVar) 'index of state variable';

*Pick the dataset you want to optimize for 
$call csv2gdx data/data_FanaeeT2013_training.csv id=meas autoCol=dataPoint autoRow=x colCount=220 values=1..lastCol fieldSep=SemiColon output=data.gdx

$gdxIn data.gdx
$load ivar = Dim1
$load idxData  = Dim2

Parameter meas(ivar,idxData) 'measurements';
$load meas
$gdxIn
display meas;

*
* Model equations
*

*Auxiliary variables
Variables
    regularization,
    hiddenIn(idxHidden,idxData), hiddenOut(idxHidden,idxData),
    output(idxData);

*Equation variables
Equations
    calculateRegularization,
    calculateHiddenIn(idxHidden,idxData),
    calculateHiddenOut(idxHidden,idxData),
    calculateOutput(idxData)
    objective;

*Auxiliary equations
calculateRegularization.. regularization =E= lambda * sum(idxHidden, sum(idxInput, sqr(weightIn(idxInput,idxHidden)) ) + sqr(weightHidden(idxHidden)) );

calculateHiddenIn(idxHidden,idxData).. hiddenIn(idxHidden,idxData) =E= weightIn('x2',idxHidden)*meas('x2',idxData) + weightIn('x3',idxHidden)*meas('x3',idxData) + biasHidden(idxHidden);
calculateHiddenOut(idxHidden,idxData).. hiddenOut(idxHidden,idxData) =E= tanh(hiddenIn(idxHidden,idxData));

calculateOutput(idxData).. output(idxData) =E= sum(idxHidden, weightHidden(idxHidden)*hiddenOut(idxHidden,idxData)) + biasOut;

*Objective function
*SSE:
objective.. objectiveVar =E= sum(idxData, sqr(meas('x1',idxData) - output(idxData))) + regularization;
*MSE:
*objective .. objectiveVar =E= (sum(idxData, sqr(meas('x1',idxData) - output(idxData))) + regularization)/card(idxData);


*
* Optimization problem
*

*Model information and options
Model trainAnn / all /;

*Optimality tolerances, time and solver
option OPTCA = 0.01;
option OPTCR = 0.01;
option RESLIM = 82800;
*option NLP = BARON;

*Solve statement
solve trainAnn using NLP minimizing objectiveVar;