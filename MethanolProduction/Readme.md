# MethanolProduction

Flowsheet optimization problems for the maximizing the exergy efficiency of a process for methanol production from hydrogen and carbon dioxide (except for the final distillation column).
Phase equilibrium is descibed using the extended Antoine equation for vapor pressures and the Henry's constant correlation and mixing rules from AspenPlus.
Enthalpies are computed using the DIPPR 107 equation for ideal gas heat capacity, the DIPPR 106 equation for enthalpy of vaporization, and the Henry's constant model for enthalpy of solution.
The MAiNGO versions of the problems (both for the C++ API and the text-based versions) use intrinsic functions and custom relaxations for the above-mentioned property models.

The problems and flash models were originally implemented for the following publication:
 - Bongartz, D., & Mitsos, A. (2019). [Deterministic global flowsheet optimization: Between equation-oriented and sequential-modular methods](https://aiche.onlinelibrary.wiley.com/doi/full/10.1002/aic.16507). *AIChE Journal*, 65(3), 1022-1034.

The custom relaxations for vapor pressure, ideal gas enthalpy, enthalpy of vaporization, and the Henry's constant model were originally implemented for the following publication:
 - Najman, J., Bongartz, D., & Mitsos, A. (2019). [Relaxations of thermodynamic property and costing models in process engineering](https://www.sciencedirect.com/science/article/abs/pii/S0098135419309494). *Computers & Chemical Engineering*, 130, 106571.                        



| Instance        | Problem Type | Number of Continuous Variables | Number of Integer Variables | Number of Binary Variables | Number of Equalities | Number of Inequalities | Number of Relaxation-Only Constraints* | Best Known Objective Value | Proven Objective Bound | Description           |
| ------------- |-------------| ------------| ------------| ------------| ------------| ------------| ------------| :------------: | :-----: | ----- |
| MethanolProduction_2DoF  | DNLP | 48 | 0 | 0 | 46 | 6 | 13 | -0.91635 | -0.91636 | Reduced-space formulation of Case `a' in Bongartz & Mitsos (2019) |
| MethanolProduction_4DoF  | DNLP | 50 | 0 | 0 | 46 | 6 | 13 | -0.91635 | -0.91636 | Reduced-space formulation of Case `b' in Bongartz & Mitsos (2019) |
| MethanolProduction_4DoF  | DNLP | 51 | 0 | 0 | 46 | 6 | 13 | -0.91643 | -0.91645 | Reduced-space formulation of Case `c' in Bongartz & Mitsos (2019) |

*The problems contain the redundant summation equations for the liquid and vapor phase mole fractions in the flash units as well as redundant overall balances around the flowsheet. In the MAiNGO version of the problems, these are implemented as relaxation-only constraints as proposed by [Sahinidis & Tawarmalani](https://link.springer.com/article/10.1007/s10898-004-2705-8). In the GAMS version of the problems, they are implemented as regular constraints because GAMS itself does not allow labelling constraints as relaxation-only. However, certain solvers may be able to label them as relaxation-only via their settings file (e.g., for BARON in baron.opt).
