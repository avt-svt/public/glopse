/**
 *
 * @brief File implementing the optimization problem for the case study of methanol production
 *        using custom relaxations of the temperature-dependent thermodynamic models.
 *
 *        The process model was originally published in:
 *        D. Bongartz, A. Mitsos: "Deterministic global flowsheet optimization:
 *             Between equation-oriented and sequential-modular methods",
 *             AIChE Journal 65 (2019), 1022-1034.
 *             https://aiche.onlinelibrary.wiley.com/doi/full/10.1002/aic.16507
 *        E-mail: amitsos@alum.mit.edu
 *
 *        The relaxations of the thermodynamic models were originally published in:
 *        J. Najman, D. Bongartz, and A. Mitsos: "Relaxations of thermodynamic property
 *             and costing models in process engineering",
 *             Computers & Chemical Engineering 130 (2019), 106571.
 *             https://www.sciencedirect.com/science/article/abs/pii/S0098135419309494
 *        E-mail: amitsos@alum.mit.edu
 *
 * ==============================================================================
 * © 2020, Process Systems Engineering (AVT.SVT), RWTH Aachen University
 * ==============================================================================
 *
 * used in:
 *    D. Bongartz: "Deterministic Global Flowsheet Optimization for the Design of Energy Conversion Processes",
 *     	            PhD Thesis, RWTH Aachen University, 2020.
 *
 */

#pragma once

#include "MAiNGOmodel.h"
#include "../Thermo/SubcriticalComponent.h"
#include "../Thermo/HenryComponent.h"


using Var = mc::FFVar;


//////////////////////////////////////////////////////////////////////////
// Model class to be passed to MAiNGO
class Model: public maingo::MAiNGOmodel {

	public:
		Model();
		maingo::EvaluationContainer evaluate(const std::vector<Var> &optVars);
		std::vector<maingo::OptimizationVariable> get_variables();


	private:
		// Constant model parameters:
			size_t ncompSubcritical, ncompHenry, ncomp;
			double LHV_H2, LHV_MeOH, eta_s, eta_m, T_LP, T_MP, T_amb, e_H2, e_CO2, e_MeOH, kPurge;
		// Declaration of model variables:
			// Process
				std::vector<Var> H2in,H2LP,CO2in,CO2HP,Feed,Prod,Liq,Gas,Purge,Rec,TearGuess,TearCalc,Liq2,Gas2,Mix,ProdC,ProdCC,RecHP;
				Var H_H2in,H_H2LP,H_CO2in,H_CO2HP,H_Feed,H_Prod,H_Liq,H_Gas,H_Purge,H_Rec,H_TearGuess,H_TearCalc,H_Liq2,H_Gas2,H_Mix,H_ProdC,H_ProdCC,H_RecHP;
				Var p_H2in,p_H2LP,p_CO2in,p_CO2HP,p_Feed,p_Prod,p_Liq,p_Gas,p_Purge,p_Rec,p_TearGuess,p_TearCalc,p_Liq2,p_Gas2,p_Mix,p_ProdC,p_ProdCC,p_RecHP;
				Var T_H2in,T_H2LP,T_CO2in,T_CO2HP,T_Feed,T_Prod,T_Liq,T_Gas,T_Purge,T_Rec,T_TearGuess,T_TearCalc,T_Liq2,T_Gas2,T_Mix,T_ProdC,T_ProdCC,T_RecHP;
				Var KxRWGS,KxMeOH,rRWGS,rMeOH,sumProd,sumTearGuess,sumPurge,sumProdC,sumProdCC,Q_Preheater,Q_Reactor,Tiso_CO2,S_CO2in,Siso_CO2,Hiso_CO2,Wc_CO2,Q_LP,Wc_Rec,Tiso_rec,Siso_Rec,Hiso_Rec,S_Rec;
				Var Ex_Reactor, Ex_LP, Ex_Flash1, Ex_Flash2, Ex_H2, Ex_CO2, Ex_MeOH;
			// Flash
				std::vector<SubcriticalComponent <Var> > componentsSubcritical;
				std::vector<HenryComponent <Var> > componentsHenry;
				std::vector<Var> ps,x,y,henry;
				Var psi,sumGas,sumLiq,VbF,sumX,sumY,Q_Flash1;
			// 2nd Flash
				std::vector<Var> ps2,x2,y2,henry2;
				Var psi2,sumGas2,sumLiq2,sumX2,sumY2,Q_Flash2;
			// Dummy Flash (LP steam exchanger)
				std::vector<Var> ps3,x3,y3,henry3;
				Var psi3,sumGas3,sumLiq3,sumX3,sumY3,Q_Flash3,VbF3;



};


//////////////////////////////////////////////////////////////////////////
// function for providing optimization variable data to the Branch-and-Bound solver
std::vector<maingo::OptimizationVariable> Model::get_variables() {

	std::vector<maingo::OptimizationVariable> variables;
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,5),"Tear-0 [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,5),"Tear-1 [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,5),"Tear-2 [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,5),"Tear-3 [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,5),"Tear-4 [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1),"rRWGS [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1),"rMeOH [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,0.1),"xH2-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,0.1),"xCO2-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,0.1),"xCO-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"xMeOH-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"xH2O-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"yH2-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"yCO2-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"yCO-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"yMeOH-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"yH2O-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,5),"Gas-1 [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1.75),"Liq-1 [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,0.0005),"xH2-2"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,0.0005),"xCO2-2"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,0.0005),"xCO-2"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.4,0.6),"xMeOH-2"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.4,0.6),"xH2O-2"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"yH2-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"yCO2-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"yCO-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"yMeOH-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"yH2O-1"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1.75),"Gas-2 [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1.75),"Liq-2 [kmol/s]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-1e6,0),"H_TearGuess [kW]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(25,150),"T-CO2-iso [C]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(25,150),"T-rec-iso [C]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,5),"Gas-3 [-]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1.75),"Liq-3 [-]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"xH2-3"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"xCO2-3"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"xCO-3"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"xMeOH-3"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"xH2O-3"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"yH2-3"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"yCO2-3"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"yCO-3"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"yMeOH-3"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-8,1),"yH2O-3"));
	// Degrees of freedom:
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(60,80),"p-reac [bar]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(200,300),"T-reac [C]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1,10),"p-2 [bar]"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(30,100),"T-2 [C]")); 

	return variables;
}



//////////////////////////////////////////////////////////////////////////
// constructor for the model
Model::Model() {

	// Parameters:
		// Misc
			e_H2 = (237.2 + 10.86) * 1e3;	// Chemical exergy in kJ/kmol approximated as delta G upon combustion + thermochemical exergy at 80 bar, 25C (amb: 1bar, 25C)
			e_MeOH = 702.6 * 1e3;			// Chemical exergy in kJ/kmol approximated as delta G upon combustion; neglecting thermochemical exergy
			e_CO2 = 7.718 * 1e3;			// Thermochemical exergy in kJ/kmol at 22.5bar, 25C (amb: 1bar, 25C)
			T_amb = 273.15 + 25;
			T_LP = 273.15 + 100;
			T_MP = 273.15 + 170;
		// Compressor
			eta_s = 0.8;
			eta_m = 0.9;
		// Splitter
			kPurge = 0.01;
		// Flash
			// Component models:
				componentsHenry.push_back(HenryComponent<Var>("H2",2.01588,273.15-252.76,13.13,64.147,0,0));
				componentsHenry.push_back(HenryComponent<Var>("CO2",44.0098,273.15+31.06,73.83,94,-393.510,0));
				componentsHenry.push_back(HenryComponent<Var>("CO",28.0104,273.15-140.23,34.99,94.4,-110.530,0));
				componentsSubcritical.push_back(SubcriticalComponent<Var>("MeOH",32.04,512.5,80.84,117,-200.940,0));
				componentsSubcritical.push_back(SubcriticalComponent<Var>("Water",18.0154,647.096,220.64,55.9472,-241.818,0));
				ncompHenry = componentsHenry.size();
				ncompSubcritical =componentsSubcritical.size();
				ncomp = ncompHenry + ncompSubcritical;
			// Henry component property parameters
				// H2
					componentsHenry[0].set_henry_model(HenryComponent<Var>::HENRY_ASPEN);
					componentsHenry[0].add_henry_parameters(std::vector<double>{3,-61.4347,1867.4,12.643,-0.027187,0});
					componentsHenry[0].add_henry_parameters(std::vector<double>{4,180.066,-6993.51,-26.3119,0.0150431,0});
					componentsHenry[0].set_solvent_mixing_rule(HenryComponent<Var>::MIX_ASPEN,std::vector<double>{117,55.9472});
					componentsHenry[0].set_heat_capacity_model(HenryComponent<Var>::CPIG_DIPPR107,std::vector<double>{27.617,9.56,2466,3.76,567.6});
				// CO2
					componentsHenry[1].set_henry_model(HenryComponent<Var>::HENRY_ASPEN);
					componentsHenry[1].add_henry_parameters(std::vector<double>{3,15.4699,-3426.7,1.5108,-0.025451,0});
					componentsHenry[1].add_henry_parameters(std::vector<double>{4,159.865,-8741.55,-21.669,0.00110259,0});
					componentsHenry[1].set_solvent_mixing_rule(HenryComponent<Var>::MIX_ASPEN,std::vector<double>{117,55.9472});
					componentsHenry[1].set_heat_capacity_model(HenryComponent<Var>::CPIG_DIPPR107,std::vector<double>{29.37,34.54,1428,26.4,588});
				// CO
					componentsHenry[2].set_henry_model(HenryComponent<Var>::HENRY_ASPEN);
					componentsHenry[2].add_henry_parameters(std::vector<double>{3,4.21187,1144.4,0,0,0});
					componentsHenry[2].add_henry_parameters(std::vector<double>{4,171.775,-8296.75,-23.3372,0,0});
					componentsHenry[2].set_solvent_mixing_rule(HenryComponent<Var>::MIX_ASPEN,std::vector<double>{117,55.9472});
					componentsHenry[2].set_heat_capacity_model(HenryComponent<Var>::CPIG_DIPPR107,std::vector<double>{29.108,8.773,3085.1,8.4553,1538.2});
			// Subcritical component property parameters
				// Extended Antoine
					componentsSubcritical[0].set_vapor_pressure_model(SubcriticalComponent<Var>::PVAP_XANTOINE,std::vector<double>{71.2051, -6904.5, 0.0, 0.0, -8.8622, 7.4664e-6, 2});
					componentsSubcritical[1].set_vapor_pressure_model(SubcriticalComponent<Var>::PVAP_XANTOINE,std::vector<double>{62.1361, -7258.2, 0.0, 0.0, -7.3037, 4.1653e-6, 2});
				// Enthalpy of vaporization
					componentsSubcritical[0].set_enthalpy_of_vaporization_model(SubcriticalComponent<Var>::DHVAP_DIPPR106,std::vector<double>{32615, -1.0407, 1.8695, -0.60801, 0});
					componentsSubcritical[1].set_enthalpy_of_vaporization_model(SubcriticalComponent<Var>::DHVAP_DIPPR106,std::vector<double>{56600, 0.61204, -0.6257, 0.3988, 0});
				// Heat capacity
					componentsSubcritical[0].set_heat_capacity_model(SubcriticalComponent<Var>::CPIG_DIPPR107,std::vector<double>{39.252,87.9,1916.5,53.654,896.7});
					componentsSubcritical[1].set_heat_capacity_model(SubcriticalComponent<Var>::CPIG_DIPPR107,std::vector<double>{33.363,26.79,2610.5,8.896,1169});


		// Resizing
			ps.resize(ncomp); x.resize(ncomp); y.resize(ncomp); henry.resize(ncomp); H2in.resize(ncomp); CO2in.resize(ncomp); Feed.resize(ncomp); Prod.resize(ncomp);
			Liq.resize(ncomp); Gas.resize(ncomp); Purge.resize(ncomp); Rec.resize(ncomp); TearGuess.resize(ncomp); TearCalc.resize(ncomp);
			ps2.resize(ncomp); x2.resize(ncomp); y2.resize(ncomp); henry2.resize(ncomp); Liq2.resize(ncomp); Gas2.resize(ncomp); Mix.resize(ncomp); ProdC.resize(ncomp); H2LP.resize(ncomp); CO2HP.resize(ncomp); RecHP.resize(ncomp);
			ps3.resize(ncomp); x3.resize(ncomp); y3.resize(ncomp); henry3.resize(ncomp);

	// Input:
		// H2 input
			H2in[0] = 0.75; // H2
			H2in[1] = 0.0; // CO2
			H2in[2] = 0.0; // CO
			H2in[3] = 0.0; // MeOH
			H2in[4] = 0.0; // H2O
			Ex_H2 = H2in[0] * e_H2;
			T_H2in = T_amb;
			p_H2in = 80;
			H_H2in = 0.;
			for(size_t i=0; i<ncompHenry; ++i) {
				Var hig_i(componentsHenry[i].calculate_ideal_gas_enthalpy_conv(T_H2in));
				H_H2in += H2in[i]*hig_i;
			}
			for(size_t i=0; i<ncompSubcritical; ++i) {
				Var hig_i(componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(T_H2in));
				H_H2in += H2in[i+ncompHenry]*hig_i;
			}
		// CO2 input
			CO2in[0] = 0.0; // H2
			CO2in[1] = 0.25;// CO2
			CO2in[2] = 0.0; // CO
			CO2in[3] = 0.0; // MeOH
			CO2in[4] = 0.0; // H2O
			T_CO2in = T_amb;
			p_CO2in = 22.5;

}


//////////////////////////////////////////////////////////////////////////
// Evaluate the model
maingo::EvaluationContainer Model::evaluate(const std::vector<Var> &optVars)
{

	// Rename  inputs
		sumTearGuess = 0.0;
		for (size_t i=0; i<ncomp; ++i) {
			TearGuess[i] = optVars[i];
			sumTearGuess += TearGuess[i];
		}
		rRWGS = optVars[ncomp];
		rMeOH = optVars[ncomp+1];
		for (size_t i=0; i<ncomp; ++i) {
			x[i] = optVars[ncomp+2+i];
		}
		for (size_t i=0; i<ncomp; ++i) {
			y[i] = optVars[2*ncomp+2+i];
		}
		sumGas=optVars[3*ncomp+2];
		sumLiq=optVars[3*ncomp+3];
		for (size_t i=0; i<ncomp; ++i) {
			x2[i] = optVars[3*ncomp+4+i];
		}
		for (size_t i=0; i<ncomp; ++i) {
			y2[i] = optVars[4*ncomp+4+i];
		}
		sumGas2=optVars[5*ncomp+4];
		sumLiq2=optVars[5*ncomp+5];
		H_TearGuess=optVars[5*ncomp+6];
		Tiso_CO2 = optVars[5*ncomp+7] + 273.15;
		Tiso_rec = optVars[5*ncomp+8] + 273.15;
		sumGas3=optVars[5*ncomp+9];
		sumLiq3=optVars[5*ncomp+10];
		for (size_t i=0; i<ncomp; ++i) {
			x3[i] = optVars[5*ncomp+11+i];
		}
		for (size_t i=0; i<ncomp; ++i) {
			y3[i] = optVars[6*ncomp+11+i];
		}
		// Degrees of freedom:
		p_Feed = optVars[7*ncomp+11];
		T_Prod = optVars[7*ncomp+12] + 273.15;
		p_Gas2 = optVars[7*ncomp+13]; 
		T_Gas2 = optVars[7*ncomp+14] + 273.15;
		// Fixed:
		T_Gas  = 70.002 + 273.15;

	// Model

		// H2 throttle
			H2LP = H2in;
			p_H2LP = p_Feed;
			H_H2LP = H_H2in;

		// CO2 input
			H_CO2in = 0.;
			for(size_t i=0; i<ncompHenry; ++i) {
				Var hig_i(componentsHenry[i].calculate_ideal_gas_enthalpy_conv(T_CO2in));
				H_CO2in += CO2in[i]*hig_i;
			}
			for(size_t i=0; i<ncompSubcritical; ++i) {
				Var hig_i(componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(T_CO2in));
				H_CO2in += CO2in[i+ncompHenry]*hig_i;
			}
			S_CO2in = 0.;
			for(size_t i=0; i<ncompHenry; ++i) {
				Var sig_i(componentsHenry[i].calculate_ideal_gas_entropy(T_CO2in,p_CO2in));
				S_CO2in += CO2in[i]*sig_i;
			}
			for(size_t i=0; i<ncompSubcritical; ++i) {
				Var sig_i(componentsSubcritical[i].calculate_ideal_gas_entropy(T_CO2in,p_CO2in));
				S_CO2in += CO2in[i+ncompHenry]*sig_i;
			}
			Ex_CO2 = CO2in[1] * e_CO2;

		// CO2 compressor
			CO2HP = CO2in;
			p_CO2HP = p_Feed;
			Siso_CO2 = 0.;
			for(size_t i=0; i<ncompHenry; ++i) {
				Var sig_i(componentsHenry[i].calculate_ideal_gas_entropy(Tiso_CO2,p_CO2HP));
				Siso_CO2 += CO2HP[i]*sig_i;
			}
			for(size_t i=0; i<ncompSubcritical; ++i) {
				Var sig_i(componentsSubcritical[i].calculate_ideal_gas_entropy(Tiso_CO2,p_CO2HP));
				Siso_CO2 += CO2HP[i+ncompHenry]*sig_i;
			}
			Hiso_CO2 = 0.;
			for(size_t i=0; i<ncompHenry; ++i) {
				Var hig_i(componentsHenry[i].calculate_ideal_gas_enthalpy_conv(Tiso_CO2));
				Hiso_CO2 += CO2HP[i]*hig_i;
			}
			for(size_t i=0; i<ncompSubcritical; ++i) {
				Var hig_i(componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(Tiso_CO2));
				Hiso_CO2 += CO2HP[i+ncompHenry]*hig_i;
			}
			H_CO2HP = H_CO2in*(1-1/eta_s) + Hiso_CO2/eta_s;
			Wc_CO2 = (Hiso_CO2-H_CO2in)/(eta_s*eta_m);


		// Tear "input":
			Gas = TearGuess;
			H_Gas = H_TearGuess;


		// Split:
			sumPurge = 0.0;
			for (size_t i=0; i<ncomp; ++i) {
				Purge[i] = Gas[i] * kPurge;
				sumPurge += Purge[i];
				Rec[i] = Gas[i] - Purge[i];
			}
			H_Purge = H_Gas * kPurge;
			H_Rec = H_Gas - H_Purge;
			T_Rec = T_Gas;
			p_Rec = 0.95*p_Feed;
			S_Rec = 0.;
			for(size_t i=0; i<ncompHenry; ++i) {
				Var sig_i(componentsHenry[i].calculate_ideal_gas_entropy(T_Rec,p_Rec));
				S_Rec += Rec[i]*sig_i;
			}
			for(size_t i=0; i<ncompSubcritical; ++i) {
				Var sig_i(componentsSubcritical[i].calculate_ideal_gas_entropy(T_Rec,p_Rec));
				S_Rec += Rec[i+ncompHenry]*sig_i;
			}

		// Recycle compressor
			RecHP = Rec;
			p_RecHP = p_Feed;
			Siso_Rec = 0.;
			for(size_t i=0; i<ncompHenry; ++i) {
				Var sig_i(componentsHenry[i].calculate_ideal_gas_entropy(Tiso_rec,p_RecHP));
				Siso_Rec += RecHP[i]*sig_i;
			}
			for(size_t i=0; i<ncompSubcritical; ++i) {
				Var sig_i(componentsSubcritical[i].calculate_ideal_gas_entropy(Tiso_rec,p_RecHP));
				Siso_Rec += RecHP[i+ncompHenry]*sig_i;
			}
			Hiso_Rec = 0.;
			for(size_t i=0; i<ncompHenry; ++i) {
				Var hig_i(componentsHenry[i].calculate_ideal_gas_enthalpy_conv(Tiso_rec));
				Hiso_Rec += RecHP[i]*hig_i;
			}
			for(size_t i=0; i<ncompSubcritical; ++i) {
				Var hig_i(componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(Tiso_rec));
				Hiso_Rec += RecHP[i+ncompHenry]*hig_i;
			}
			H_RecHP = H_Rec*(1-1/eta_s) + Hiso_Rec/eta_s;
			Wc_Rec = max(Var(0.),(Hiso_Rec-H_Rec)/(eta_s*eta_m));


		// Mixer:
			for (size_t i=0; i<ncomp; ++i) {
				Mix[i] = H2LP[i] + CO2HP[i] + RecHP[i];
			}
			H_Mix = H_H2LP + H_CO2HP + H_RecHP;


		// Pre-heater - cold side
			Feed = Mix;
			T_Feed = 273.15 + 170;
			H_Feed = 0.;
			for(size_t i=0; i<ncompHenry; ++i) {
				Var hig_i(componentsHenry[i].calculate_ideal_gas_enthalpy_conv(T_Feed));
				H_Feed += Feed[i]*hig_i;
			}
			for(size_t i=0; i<ncompSubcritical; ++i) {
				Var hig_i(componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(T_Feed));
				H_Feed += Feed[i+ncompHenry]*hig_i;
			}
			Q_Preheater = H_Feed - H_Mix;


		// Reactor:
			p_Prod = p_Feed * 0.95;
			KxRWGS = pow( 10.0 , -2073.0/T_Prod+2.029 );
			KxMeOH = pow( 10.0 , 5139.0/T_Prod-12.621 ) * sqr(p_Prod);
			Prod[0] = Feed[0] - 2*rMeOH - rRWGS;
			Prod[1] = Feed[1] - rRWGS;
			Prod[2] = Feed[2] - rMeOH + rRWGS;
			Prod[3] = Feed[3] + rMeOH;
			Prod[4] = Feed[4] + rRWGS;
			sumProd = 0.0;
			for (size_t i=0; i<ncomp; ++i) {
				sumProd += Prod[i];
			}
			H_Prod = 0.;
			for(size_t i=0; i<ncompHenry; ++i) {
				Var hig_i(componentsHenry[i].calculate_ideal_gas_enthalpy_conv(T_Prod));
				H_Prod += Prod[i]*hig_i;
			}
			for(size_t i=0; i<ncompSubcritical; ++i) {
				Var hig_i(componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(T_Prod));
				H_Prod += Prod[i+ncompHenry]*hig_i;
			}
			Q_Reactor = (H_Prod - H_Feed);
			Ex_Reactor = Q_Reactor * (1 - T_amb / T_MP);


		// Pre-heater - hot side
			ProdC = Prod;
			sumProdC = sumProd;
			p_ProdC = p_Prod;
			H_ProdC = H_Prod + (-Q_Preheater);


		// Cooler (LP steam)
			ProdCC = ProdC;
			sumProdCC = sumProdC;
			p_ProdCC = p_ProdC;
			T_ProdCC = 273.15 + 110;
			// "Dummy flash":
			// 1. Overall mass balance
				// in equality constraint
			// 2. Calculate phase equilibrium residual & enthalpies
				psi3=0, sumX3=0, sumY3=0;
				H_ProdCC = 0.;
				for (size_t i=0; i<ncompHenry; ++i) {
					  // Phase equilibrium
					henry3[i] = componentsHenry[i].calculate_henry_mixed(T_ProdCC,x3);
					sumX3 += x3[i];
					sumY3 += y3[i];
					psi3 += x3[i]-y3[i];
					  // Enthalpies
					Var hig_i(componentsHenry[i].calculate_ideal_gas_enthalpy_conv(T_ProdCC));
					Var deltahsol_i(componentsHenry[i].calculate_solution_enthalpy_mixed(T_ProdCC,x3));
					H_ProdCC += ProdCC[i]*hig_i;
					H_ProdCC += -sumLiq3*x3[i]*deltahsol_i;
				}
				for (unsigned i=0;i<ncompSubcritical;i++) {
					  // Phase equilibrium
					sumX3 += x3[i+ncompHenry];
					sumY3 += y3[i+ncompHenry];
					psi3 += x3[i+ncompHenry]-y3[i+ncompHenry];
					ps3[i+ncompHenry] = componentsSubcritical[i].calculate_vapor_pressure_conv(T_ProdCC);
					  // Enthalpies
					Var hig_i(componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(T_ProdCC));
					Var deltahv_i(componentsSubcritical[i].calculate_vaporization_enthalpy_conv(T_ProdCC));
					H_ProdCC += ProdCC[i+ncompHenry]*hig_i;
					H_ProdCC += -sumLiq3*x3[i+ncompHenry]*deltahv_i;
				}
			// 3. Energy balance:
			Q_LP = (H_ProdCC - H_ProdC);
			Ex_LP = Q_LP * (1 - T_amb / T_LP);

		// Flash:
			// 1. Overall mass balance
				// in equality constraint
			// 2. Calculate phase equilibrium residual & enthalpies
				psi=0, sumX=0, sumY=0;
				H_Gas=0, H_Liq=0;
				p_Gas = p_ProdCC;
				p_Liq = p_ProdCC;
				for (size_t i=0; i<ncompHenry; ++i) {
					  // Phase equilibrium
					henry[i] = componentsHenry[i].calculate_henry_mixed(T_Gas,x);
					sumX += x[i];
					sumY += y[i];
					psi += x[i]-y[i];
					Liq[i] = sumLiq*x[i];
					Gas[i] = sumGas*y[i];
					  // Enthalpies
					Var hig_i(componentsHenry[i].calculate_ideal_gas_enthalpy_conv(T_Gas));
					Var deltahsol_i(componentsHenry[i].calculate_solution_enthalpy_mixed(T_Gas,x));
					H_Gas += Gas[i]*hig_i;
					H_Liq += Liq[i]*(hig_i - deltahsol_i);
				}
				for (unsigned i=0;i<ncompSubcritical;i++) {
					  // Phase equilibrium
					sumX += x[i+ncompHenry];
					sumY += y[i+ncompHenry];
					psi += x[i+ncompHenry]-y[i+ncompHenry];
					Liq[i+ncompHenry] = sumLiq*x[i+ncompHenry];
					Gas[i+ncompHenry] = sumGas*y[i+ncompHenry];
					ps[i+ncompHenry] = componentsSubcritical[i].calculate_vapor_pressure_conv(T_Gas);
					  // Enthalpies
					Var hig_i(componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(T_Gas));
					Var deltahv_i(componentsSubcritical[i].calculate_vaporization_enthalpy_conv(T_Gas));
					H_Gas += Gas[i+ncompHenry]*hig_i;
					H_Liq += Liq[i+ncompHenry]*(hig_i - deltahv_i);
				}
			// 3. Energy balance:
				Q_Flash1 = H_Gas + H_Liq - H_ProdCC;
				Ex_Flash1 = 0.;	// Cooling with cooling water



		// Tear "output"
			TearCalc = Gas;
			H_TearCalc = H_Gas;


		// Flash 2:
			// 1. Overall mass balance
				// in equality constraint
			// 2. Calculate phase equilibrium residual & enthalpies
				psi2=0,sumX2=0, sumY2=0;
				H_Gas2=0, H_Liq2=0;
				p_Liq2 = p_Gas2;
				for (size_t i=0; i<ncompHenry; ++i) {
					  // Phase equilibrium
					henry2[i] = componentsHenry[i].calculate_henry_mixed(T_Gas2,x2);
					sumX2 += x2[i];
					sumY2 += y2[i];
					psi2 += x2[i]-y2[i];
					Liq2[i] = sumLiq2*x2[i];
					Gas2[i] = sumGas2*y2[i];
					  // Enthalpies
					Var hig_i(componentsHenry[i].calculate_ideal_gas_enthalpy_conv(T_Gas2));
					Var deltahsol_i(componentsHenry[i].calculate_solution_enthalpy_mixed(T_Gas2,x2));
					H_Gas2 += Gas2[i]*hig_i;
					H_Liq2 += Liq2[i]*(hig_i - deltahsol_i);
				}
				for (unsigned i=0;i<ncompSubcritical;i++) {
					  // Phase equilibrium
					sumX2 += x2[i+ncompHenry];
					sumY2 += y2[i+ncompHenry];
					psi2 += x2[i+ncompHenry]-y2[i+ncompHenry];
					Liq2[i+ncompHenry] = sumLiq2*x2[i+ncompHenry];
					Gas2[i+ncompHenry] = sumGas2*y2[i+ncompHenry];
					ps2[i+ncompHenry] = componentsSubcritical[i].calculate_vapor_pressure_conv(T_Gas2);
					  // Enthalpies
					Var hig_i(componentsSubcritical[i].calculate_ideal_gas_enthalpy_conv(T_Gas2));
					Var deltahv_i(componentsSubcritical[i].calculate_vaporization_enthalpy_conv(T_Gas2));
					H_Gas2 += Gas2[i+ncompHenry]*hig_i;
					H_Liq2 += Liq2[i+ncompHenry]*(hig_i - deltahv_i);
				}
			// 3. Energy balance:
				Q_Flash2 = H_Gas2 + H_Liq2 - H_Liq;
				Ex_Flash2 = Q_Flash2 * (1 - T_amb / T_LP);
				Ex_MeOH = Liq2[3] * e_MeOH;


	// Prepare output
		maingo::EvaluationContainer result;
		// Objective:
			result.objective = - (Ex_MeOH + -Ex_Reactor + ((-Ex_LP)-Ex_Flash2)) / pos(Ex_H2 + Ex_CO2 + Wc_CO2+ Wc_Rec);
		// Inequalities (<=0):
			result.ineq.push_back((0 - Q_Flash2)/1e2);
			for (size_t i=0; i<ncomp; ++i) {
				result.ineq.push_back((0 - Prod[i])/0.1);
			}


		// Equalities (=0):
			// Overall
					result.eqRelaxationOnly.push_back( (H_H2in+H_CO2in-H_Purge-H_Liq +Q_Reactor+Q_LP+Q_Flash1 + Wc_CO2*eta_m+Wc_Rec*eta_m)/1e5 );
					result.eqRelaxationOnly.push_back( ((H2in[0]+CO2in[1]-2*rMeOH)-(sumLiq+sumPurge))/1.0 );
					result.eqRelaxationOnly.push_back( ((H2in[0] + CO2in[0] - 2*rMeOH - rRWGS)-(Liq[0]+Purge[0]))/0.1 );
					result.eqRelaxationOnly.push_back( ((H2in[1] + CO2in[1]           - rRWGS)-(Liq[1]+Purge[1]))/0.1 );
					result.eqRelaxationOnly.push_back( ((H2in[2] + CO2in[2] -   rMeOH + rRWGS)-(Liq[2]+Purge[2]))/0.1 );
					result.eqRelaxationOnly.push_back( ((H2in[3] + CO2in[3] +   rMeOH        )-(Liq[3]+Purge[3]))/0.1 );
					result.eqRelaxationOnly.push_back( ((H2in[4] + CO2in[4]           + rRWGS)-(Liq[4]+Purge[4]))/0.1 );
			// Tear
				for (size_t i=0; i<ncomp; ++i) {
					result.eq.push_back((TearCalc[i] - TearGuess[i])/0.1);
				}
				result.eq.push_back((H_TearCalc - H_TearGuess)/1e5);


			// CO2 compressor
				result.eq.push_back( (S_CO2in - Siso_CO2) / 1e1 );
			// Recycle compressor
				result.eq.push_back( (S_Rec - Siso_Rec) / 1e1 );
			// Reactor
				result.eq.push_back( (KxRWGS*Prod[0]*Prod[1] - Prod[2]*Prod[4]) / 1e-2 );
				result.eq.push_back( (KxMeOH*sqr(Prod[0])*Prod[2] - sqr(sumProd)*Prod[3]) / 1e-1 );
			// "Dummy Flash" (LP steam exchanger)
				result.eq.push_back(((sumGas3+sumLiq3) - sumProdC)/1.0);
				for (size_t i=0; i<ncomp; ++i) {
					result.eq.push_back((ProdC[i]-(sumGas3*y3[i] + sumLiq3*x3[i]))/1.0);
				}
				for (size_t i=0; i<ncompHenry; ++i) {
					result.eq.push_back((p_ProdCC*y3[i]-henry3[i]*x3[i])/1.0);
				}
				for (size_t i=0; i<ncompSubcritical; ++i) {
					result.eq.push_back((p_ProdCC*y3[i+ncompHenry]-ps3[i+ncompHenry]*x3[i+ncompHenry])/1.0);
				}
				result.eq.push_back((psi3-0.0)/1.0);
				// r.o.
					result.eqRelaxationOnly.push_back( (sumX3-1.0)/1.0 );
					result.eqRelaxationOnly.push_back( (sumY3-1.0)/1.0 );
			// Flash
				result.eq.push_back(((sumGas+sumLiq) - sumProd)/1.0);
				for (size_t i=0; i<ncomp; ++i) {
					result.eq.push_back((ProdCC[i]-(TearGuess[i] + Liq[i]))/1.0);
				}
				for (size_t i=0; i<ncompHenry; ++i) {
					result.eq.push_back((p_Gas*y[i]-henry[i]*x[i])/1.0);
				}
				for (size_t i=0; i<ncompSubcritical; ++i) {
					result.eq.push_back((p_Gas*y[i+ncompHenry]-ps[i+ncompHenry]*x[i+ncompHenry])/1.0);
				}
				result.eq.push_back((psi-0.0)/1.0);
				// r.o.
					result.eqRelaxationOnly.push_back( (sumX-1.0)/1.0 );
					result.eqRelaxationOnly.push_back( (sumY-1.0)/1.0 );
			// Flash 2
				result.eq.push_back(((sumGas2+sumLiq2) - sumLiq)/1.0);
				for (size_t i=0; i<ncomp; ++i) {
					result.eq.push_back((Liq[i]-(Gas2[i] + Liq2[i]))/1.0);
				}
				for (size_t i=0; i<ncompHenry; ++i) {
					result.eq.push_back((p_Gas2*y2[i]-henry2[i]*x2[i])/1.0);
				}
				for (size_t i=0; i<ncompSubcritical; ++i) {
					result.eq.push_back((p_Gas2*y2[i+ncompHenry]-ps2[i+ncompHenry]*x2[i+ncompHenry])/1.0);
				}
				result.eq.push_back((psi2-0.0)/1.0);
				// r.o.
					result.eqRelaxationOnly.push_back( (sumX2-1.0)/1.0 );
					result.eqRelaxationOnly.push_back( (sumY2-1.0)/1.0 );

		return result;
}