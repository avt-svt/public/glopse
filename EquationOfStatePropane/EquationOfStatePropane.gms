* EOS model based on
*     E. W. Lemmon, M. O. McLinden, W. Wagner: "Thermodynamic Properties of
*         Propane. III. A Reference Equation of State for Temperatures from
*         the Melting Line to 650 K and Pressures up to 1000 MPa",
*         Journal of Chemical & Engineering Data 54(12), 3141-3180 (2009).
*         https://doi.org/10.1021/je900217v
* that was originally implemented for
*     S. Sass, A. Tsoukalas, I. H. Bell, D. Bongartz, J. Najman, A. Mitsos:
*         "Towards global parameter estimation exploiting reduced data
*         sets", Optimization Methods and Software 38(6), 1129–1141, (2023).
* E-mail: amitsos@alum.mit.edu
*
* Copyright (c) 2024 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 
*
* Optimization variables
*

*Continuous variables
Variables n10, t10, eta10, beta10, gamma10, epsilon10, objectiveVar;

*Binary variables
Binary Variables c10;

*Integer variables
Integer Variables d10, l10;

*Continuous variable bounds
n10.LO = -3.;
n10.UP = 2.;
t10.LO = 1.e-5;
t10.UP = 22.;
eta10.LO = 0.;
eta10.UP = 20.;
beta10.LO = 0.;
beta10.UP = 550.;
gamma10.LO = 0.;
gamma10.UP = 2.;
epsilon10.LO = 0.;
epsilon10.UP = 1.5;

*Integer variable bounds
d10.LO = 1;
d10.UP = 10;
l10.LO = 1;
l10.UP = 2;

*Use the following to start from solution of Lemmon et al. (2009)
*n10.l = -0.60405866;
*t10.l = 3.25;
*d10.l = 2;
*c10.l = 1;
*l10.l = 2;
*eta10.l = 0.;
*beta10.l = 0.;
*gamma10.l = 0.;
*epsilon10.l = 0.;


*
* Constants and data
*

*Constants
Scalar
    R 'universal gas constant based on Lemmon et al. (2009)' / 8.314472 /
    rhoCrit 'critical molar density' / 5000.000000000001 /
    Tcrit 'critical temperature' /369.89/
    ;
   
*Fixed parameters
Set
   idxParams 'index of parameters'   / 1*18  /
   idxFixed(idxParams) 'index of fixed parameters'   / 1*9, 11*18  /
   ;
Parameter
    n(idxFixed) / 1 = 0.042910051, 2 = 1.7313671, 3 = -2.4516524, 4 = 0.34157466, 5 = -0.46047898, 6 = -0.66847295, 7 = 0.20889705, 8 = 0.19421381, 9 = -0.22917851, 11 = 0.066680654, 12 = 0.017534618, 13 = 0.33874242, 14 = 0.22228777, 15 = -0.23219062, 16 = -0.09220694, 17 = -0.47575718, 18 = -0.017486824 /
    t(idxFixed) / 1 = 1, 2 = 0.33, 3 = 0.8, 4 = 0.43, 5 = 0.9, 6 = 2.46, 7 = 2.09, 8 = 0.88, 9 = 1.09, 11 = 4.62, 12 = 0.76, 13 = 2.5, 14 = 2.75, 15 = 3.05, 16 = 2.55, 17 = 8.4, 18 = 6.75 /
    d(idxFixed) / 1 = 4, 2 = 1, 3 = 1, 4 = 2, 5 = 2, 6 = 1, 7 = 3, 8 = 6, 9 = 6, 11 = 3, 12*14 = 1, 15 = 2, 16 = 2, 17 = 4, 18 = 1 /
    c(idxFixed) / 1*5 = 0, 6*9 = 1, 11 = 1, 12*18 = 0 /
    l(idxFixed) / 1*5 = 0, 6*9 = 1, 11 = 2, 12*18 = 0 /
    eta(idxFixed) / 1*9 = 0, 11 = 0, 12 = 0.963, 13 = 1.977, 14 = 1.917, 15 = 2.307, 16 = 2.546, 17 = 3.28, 18 = 14.6 /
    beta(idxFixed) / 1*9 = 0, 11 = 0, 12 = 2.33, 13 = 3.47, 14 = 3.15, 15 = 3.19, 16 = 0.92, 17 = 18.8, 18 = 547.8 /
    gamma(idxFixed) / 1*9 = 0, 11 = 0, 12 = 0.684, 13 = 0.829, 14 = 1.419, 15 = 0.817, 16 = 1.5, 17 = 1.426, 18 = 1.093 /
    epsilon(idxFixed) / 1*9 = 0, 11 = 0, 12 = 1.283, 13 = 0.6936, 14 = 0.788, 15 = 0.473, 16 = 0.8577, 17 = 0.271, 18 = 0.948/
    ;

*Dataset
Set
   idxData  'index of data point'
   ivar 'index of state variable';

*Pick the dataset you want to optimize for 
$call csv2gdx data/data_EOS262.csv id=meas autoCol=dataPoint autoRow=stateVar colCount=262 values=1..lastCol fieldSep=SemiColon output=data.gdx
*$call csv2gdx data/data_EOS2262.csv id=meas autoCol=dataPoint autoRow=stateVar colCount=2262 values=1..lastCol fieldSep=SemiColon output=data.gdx
*$call csv2gdx data/data_EOS262noisy.csv id=meas autoCol=dataPoint autoRow=stateVar colCount=262 values=1..lastCol fieldSep=SemiColon output=data.gdx
*$call csv2gdx data/data_EOS2262noisy.csv id=meas autoCol=dataPoint autoRow=stateVar colCount=2262 values=1..lastCol fieldSep=SemiColon output=data.gdx

$gdxIn data.gdx
$load ivar = Dim1
$load idxData  = Dim2

Parameter meas(ivar,idxData) 'measurements';
$load meas
$gdxIn
*display meas;


*
* Model equations
*
* stateVar1 = p, stateVar2 = tau, stateVar3 = delta

*Auxiliary variables
Variables
    auxVar(idxData,idxParams),
    dAlpha_term(idxData,idxParams), dAlphaRdDelta(idxData),
    pCalc(idxData), rho(idxData), Temp(idxData);

*Equation variables
Equations
    auxiliaryFunction(idxData,idxFixed)
    auxiliaryFunction10(idxData)
    auxiliaryFctTerm(idxData,idxFixed)
    auxiliaryFctTerm10(idxData)
    calculateAr01(idxData)
    calculateRho(idxData)
    calculateT(idxData)
    calculateP(idxData)
    objective;

*Auxiliary equations
auxiliaryFunction(idxData,idxFixed).. auxVar(idxData,idxFixed) =E= d(idxFixed) - 2. * sqr(meas('stateVar3',idxData)) * eta(idxFixed) - c(idxFixed) * (meas('stateVar3',idxData))**(l(idxFixed)) * l(idxFixed) + 2. * meas('stateVar3',idxData) * epsilon(idxFixed) * eta(idxFixed);
auxiliaryFunction10(idxData).. auxVar(idxData,'10') =E= d10 - 2. * sqr(meas('stateVar3',idxData)) * eta10 - c10 * (meas('stateVar3',idxData))**(l10) * l10 + 2. * meas('stateVar3',idxData) * epsilon10 * eta10;

auxiliaryFctTerm(idxData,idxFixed).. dAlpha_term(idxData,idxFixed) =E= (n(idxFixed) * (meas('stateVar3',idxData))**(d(idxFixed) ) * (meas('stateVar2',idxData))**(t(idxFixed)) * auxVar(idxData,idxFixed))*exp(-c(idxFixed) * (meas('stateVar3',idxData))**(l(idxFixed)) - eta(idxFixed) * sqr(meas('stateVar3',idxData) - epsilon(idxFixed)) - beta(idxFixed) * sqr(meas('stateVar2',idxData) - gamma(idxFixed)));
auxiliaryFctTerm10(idxData).. dAlpha_term(idxData,'10') =E= (n10 * (meas('stateVar3',idxData))**(d10) * (meas('stateVar2',idxData))**(t10) * auxVar(idxData,'10'))*exp(-c10 * (meas('stateVar3',idxData))**(l10) - eta10 * sqr(meas('stateVar3',idxData) - epsilon10) - beta10 * sqr(meas('stateVar2',idxData) - gamma10));
    
calculateAr01(idxData).. dAlphaRdDelta(idxData) =E= sum(idxParams,dAlpha_term(idxData,idxParams));

calculateRho(idxData).. rho(idxData) =E= meas('stateVar3',idxData)*rhoCrit;
calculateT(idxData).. Temp(idxData) =E= Tcrit/meas('stateVar2',idxData);
calculateP(idxData).. pCalc(idxData) =E= rho(idxData) * R * Temp(idxData) *( 1 + dAlphaRdDelta(idxData) );

*Objective function
*SSE:
objective.. objectiveVar =E= sum(idxData, sqr(meas('stateVar1',idxData) - pCalc(idxData))/meas('stateVar1',idxData));
*MSE:
*objective .. objectiveVar =E= sum(idxData, sqr(meas('stateVar1',idxData) - pCalc(idxData))/meas('stateVar1',idxData))/card(idxData);


*
* Optimization problem
*

*Model information and options
Model eos / all /;

*Optimality tolerances, time and solver
option OPTCA = 0.01;
option OPTCR = 0.01;
option RESLIM = 82800;
option MINLP = BARON;

*Solve statement
solve eos using MINLP minimizing objectiveVar;