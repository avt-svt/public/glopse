/**
 *
 * @brief EOS model based on
 *        E. W. Lemmon, M. O. McLinden, W. Wagner: "Thermodynamic Properties of
 *              Propane. III. A Reference Equation of State for Temperatures from
 *              the Melting Line to 650 K and Pressures up to 1000 MPa",
 *              Journal of Chemical & Engineering Data 54(12), 3141-3180 (2009).
 *              https://doi.org/10.1021/je900217v
 *        that was originally implemented for
 *        S. Sass, A. Tsoukalas, I. H. Bell, D. Bongartz, J. Najman, A. Mitsos:
 *              "Towards global parameter estimation exploiting reduced data
 *              sets", Optimization Methods and Software 38(6), 1129–1141, (2023).
 *              https://doi.org/10.1080/10556788.2023.2205645
 *        E-mail: amitsos@alum.mit.edu
 *
 * ==============================================================================
 * © 2024, Process Systems Engineering (AVT.SVT), RWTH Aachen University
 * ==============================================================================
 */


#pragma once

#include "MAiNGOmodel.h"


namespace userInput {
    // Enter the path to the directory containing the csv-file with the data
    std::string pathToCSVfile = "../GloPSE/EquationOfStatePropane/data/";
    
    // Pick the dataset you want to optimize for:
    std::string eosModel = "EOS262";
    //std::string eosModel = "EOS2262";
    //std::string eosModel = "EOS262noisy";
    //std::string eosModel = "EOS2262noisy";
}

///////////////
//// 1. Model-specific auxiliary functions etc.
///////////////

//////////////////////////////////////////////////////////////////////////
// struct for saving data points of full dataset
struct EosData {
    std::vector<double> p, tau, delta;
};


//////////////////////////////////////////////////////////////////////////
// struct for efficient handling of the parameters
struct EosParams {
    mc::FFVar n, t, d, c, l, eta, beta, gamma, epsilon;
};


//////////////////////////////////////////////////////////////////////////
// auxiliary function for calculating derivative of residual Helmholtz energy
template <typename U>
auto A_r_01(const U tau, const U delta, const std::vector<EosParams> &params)
{
    if (params.empty()) {
        throw std::runtime_error("Error in model: Empty parameter vector");
    }

    // Initialize first term of sum
    size_t i = 0;
    auto aux = params[i].d - 2. * sqr(delta) * params[i].eta - params[i].c * pow(delta, params[i].l) * params[i].l + 2. * delta * params[i].epsilon * params[i].eta;

    auto dAlphaRdDelta = expx_times_y(-params[i].c * pow(delta, params[i].l) - params[i].eta * sqr(delta - params[i].epsilon) - params[i].beta * sqr(tau - params[i].gamma),
                                      params[i].n * pow(delta, params[i].d - 1.) * pow(tau, params[i].t) * aux);
    
    // Remaining terms of sum
    for (size_t i = 1; i < params.size(); ++i) {
        aux = params[i].d - 2. * sqr(delta) * params[i].eta - params[i].c * pow(delta, params[i].l) * params[i].l + 2. * delta * params[i].epsilon * params[i].eta;

        dAlphaRdDelta += expx_times_y(-params[i].c * pow(delta, params[i].l) - params[i].eta * sqr(delta - params[i].epsilon) - params[i].beta * sqr(tau - params[i].gamma),
                                      params[i].n * pow(delta, params[i].d - 1.) * pow(tau, params[i].t) * aux);
    }

    return dAlphaRdDelta;
}


///////////////
//// 2. Data input
///////////////

namespace solnLemmon {
    std::vector<double> n =       {0.042910051, 1.7313671, -2.4516524, 0.34157466, -0.46047898, -0.66847295, 0.20889705, 0.19421381, -0.22917851, -0.60405866, 0.066680654, 0.017534618, 0.33874242, 0.22228777, -0.23219062, -0.09220694, -0.47575718, -0.017486824};
    std::vector<double> t =       {1, 0.33, 0.8, 0.43, 0.9, 2.46, 2.09, 0.88, 1.09, 3.25, 4.62, 0.76, 2.5, 2.75, 3.05, 2.55, 8.4, 6.75};
    std::vector<double> d =       {4, 1, 1, 2, 2, 1, 3, 6, 6, 2, 3, 1, 1, 1, 2, 2, 4, 1};
    std::vector<double> c =       {0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    std::vector<double> l =       {0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 0, 0, 0, 0, 0, 0, 0};
    std::vector<double> eta =     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.963, 1.977, 1.917, 2.307, 2.546, 3.28, 14.6};
    std::vector<double> beta =    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2.33, 3.47, 3.15, 3.19, 0.92, 18.8, 547.8};
    std::vector<double> gamma =   {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.684, 0.829, 1.419, 0.817, 1.5, 1.426, 1.093};
    std::vector<double> epsilon = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1.283, 0.6936, 0.788, 0.473, 0.8577, 0.271, 0.948};
}


//////////////////////////////////////////////////////////////////////////
// auxiliary function for reading data from csv-file
void read_csv_to_vector(const std::string name, std::vector<std::vector<double>> &data)
{
    // Open data file
    std::ifstream infile;
    infile.open(name);
    if (infile.fail()) {
        throw std::runtime_error("Error in model: Unable to open data file "+name);
    }
    else {
        std::cout << "Read data from " << name << std::endl;
    }

    std::string line, tmpCooStr;
    std::vector<double> tmpData;
    double tmpCoo;
    data.clear();
    // Read all lines
    while (std::getline(infile, line)) {
        // Split line into coordinates
        std::istringstream stream(line);
        tmpData.resize(0);
        // First entry
        stream >> tmpCoo;
        tmpData.push_back(tmpCoo);
        // Read all remaining entries
        while (std::getline(stream, tmpCooStr, ';')) {
            stream >> tmpCoo;
            tmpData.push_back(tmpCoo);
        }
        data.push_back(tmpData);
    }

    if (data.size() != 3) {
        throw std::runtime_error("Error in model: Unexpected number of variables in data file");
    }
}


///////////////
//// 3. MAiNGO model
///////////////

class Model: public maingo::MAiNGOmodel {

  public:

    Model();

    maingo::EvaluationContainer evaluate(const std::vector<Var> &optVars);
    std::vector<maingo::OptimizationVariable> get_variables();
    std::vector<double> get_initial_point();

  private:
    // Setup of Lemmon et al. (2009)
    const double _R = 8.314472;                     // Universal gas constant in J/(mol*K)
    const size_t _nTerms = 18;                      // Number of terms used in residual Helmholtz energy
    const double _rhomolarCrit = 5000.000000000001; // Critical density in mol/(m^3)
    const double _Tcrit = 369.89;                   // Critical temperature in K

    // Adaptions of Sass et al. (2023)
    size_t _idxUnknown = 10; // Index of unknown parameters; in 1 .. 18
    size_t _noOfDataPoints;  // Number of data points in full dataset
    EosData _eosData;        // Full dataset
};


//////////////////////////////////////////////////////////////////////////
// function for providing optimization variable data to the Branch-and-Bound solver
std::vector<maingo::OptimizationVariable>
Model::get_variables()
{
    std::string idxUnknown = std::to_string(_idxUnknown);

    std::vector<maingo::OptimizationVariable> variables;
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-3.,2.), "n" + idxUnknown));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1.e-5,22.), "t" + idxUnknown));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1,10), maingo::VT_INTEGER, "d" + idxUnknown));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1), maingo::VT_BINARY, "c" + idxUnknown));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1,2), maingo::VT_INTEGER, "l" + idxUnknown));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.,20.), "eta" + idxUnknown));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.,550.), "beta" + idxUnknown));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.,2.), "gamma" + idxUnknown));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.,1.5), "epsilon" + idxUnknown));
    
    return variables;
}


//////////////////////////////////////////////////////////////////////////
// function for providing initial point data to the Branch-and-Bound solver
std::vector<double>
Model::get_initial_point()
{

    std::vector<double> initialPoint;

    // Use the following to start the optimization from the optimal solution
    // of Lemmon et al. (2009)
    /*
    initialPoint.resize(9);
    size_t i = _idxUnknown - 1; // C++: Start counting from 0 rather than 1 
    initialPoint[0] = solnLemmon::n[i];
    initialPoint[0] = solnLemmon::t[i];
    initialPoint[0] = solnLemmon::d[i];
    initialPoint[0] = solnLemmon::c[i];
    initialPoint[0] = solnLemmon::l[i];
    initialPoint[0] = solnLemmon::eta[i];
    initialPoint[0] = solnLemmon::beta[i];
    initialPoint[0] = solnLemmon::gamma[i];
    initialPoint[0] = solnLemmon::epsilon[i];
    */
 
    return initialPoint;
}


//////////////////////////////////////////////////////////////////////////
// constructor for the model
Model::Model()
{
    _idxUnknown = _idxUnknown - 1; // C++: Start counting from 0 rather than 1
    
    // Catch measurement data from file
    std::string nameFile = userInput::pathToCSVfile + "data_" + userInput::eosModel + ".csv";
    std::vector<std::vector<double>> tmpData;
    tmpData.clear();
    read_csv_to_vector(nameFile, tmpData);

    // Plug data into struct
    _eosData.p     = tmpData[0];
    _eosData.tau   = tmpData[1];
    _eosData.delta = tmpData[2];

    // Catch size of full dataset
    _noOfDataPoints = tmpData[0].size();
}


//////////////////////////////////////////////////////////////////////////
// evaluate the model
maingo::EvaluationContainer
Model::evaluate(const std::vector<Var> &optVars)
{
    // Catch the optimization variables
    // The vector optVars is of the same size and sorted in the same order as the user-defined variables vector in function get_variables()
    std::vector<EosParams> params(_nTerms);

    unsigned int  iVar = 0;
    for (auto i=0; i< _nTerms; ++i) {
        if(i == _idxUnknown) {
            params[i].n = optVars[iVar++];
            params[i].t = optVars[iVar++];
            params[i].d = optVars[iVar++];
            params[i].c = optVars[iVar++];
            params[i].l = optVars[iVar++];
            params[i].eta = optVars[iVar++];
            params[i].beta = optVars[iVar++];
            params[i].gamma = optVars[iVar++];
            params[i].epsilon = optVars[iVar++];
        } else {
            params[i].n = solnLemmon::n[i];
            params[i].t = solnLemmon::t[i];
            params[i].d = solnLemmon::d[i];
            params[i].c = solnLemmon::c[i];
            params[i].l = solnLemmon::l[i];
            params[i].eta = solnLemmon::eta[i];
            params[i].beta = solnLemmon::beta[i];
            params[i].gamma = solnLemmon::gamma[i];
            params[i].epsilon = solnLemmon::epsilon[i];
        }
    }

    // Model
    maingo::EvaluationContainer result;

    Var se = 0.; // Summed squared prediction error (SSE)
    for (auto idx = 0; idx < _noOfDataPoints; idx++) {
        const double rho = _eosData.delta[idx]*_rhomolarCrit;
        const double T = _Tcrit/_eosData.tau[idx];
        Var pCalc = rho * _R * T * ( 1. + _eosData.delta[idx] * A_r_01(_eosData.tau[idx], _eosData.delta[idx], params) );

        Var sePerData = sqr((_eosData.p[idx] - pCalc) / _eosData.p[idx]);
        se += sePerData;

        // Objective for B&B algorithm with growing datasets
        result.objective_per_data.push_back(sePerData);
    }
    // Objective for standard B&B algorithm
    result.objective = se; // SSE
//    result.objective = se / _noOfDataPoints; // Mean squared error (MSE)

    return result;
}
