# EquationOfStatePropane

Parameter estimation problem for fitting an equation of state based on the Helmholtz energy for propane based on different synthetic measurement datasets.
In the C++ header files for MAiNGO, we provide the summed squared prediction error and the mean squared prediction error as the objective for the standard B&B algorithm as well as the squared prediction error for each data point as the objective per data used in the B&B algorithm with growing datasets, see [manual of MAiNGO](https://avt-svt.pages.rwth-aachen.de/public/maingo/special_uses.html#growing_datasets) and [Sass et al. (2024a)](https://doi.org/10.1016/j.ejor.2024.02.020).
In the GAMS file, we formulate all model equations as equalities in dependence of the data points to increase the readability of the model.
For switching between instances EOS262, EOS2262, EOS262noisy, and EOS2262noisy, please choose the respective string *eosModel*, see Line 30 of *EquationOfStatePropane.h*, or call the respective csv-file, see Line 94 of *EquationOfStatePropane.gms*.

Note that we provide the measurement data in a csv file to improve its usability.
Since we can only store 16 digits, there is a small deviation to the data used in [Sass et al. (2023)](https://doi.org/10.1080/10556788.2023.2205645), [Sass et al. (2024a)](https://doi.org/10.1016/j.ejor.2024.02.020), and Sass et al. (2024b).
Note further that the data is stored in rows to enable an efficient reading of the data into the MAiNGO model.
In fact, rows 1, 2, and 3 contain the measurements for pressure p in Pascal, scaled temperature &#964; (dimensionless), and scaled density &#948; (dimensionless), respectively.

The general model was originally proposed in the following publication:
 - Lemmon, E. W., McLinden, M. O., & Wagner, W. (2009). [Thermodynamic Properties of Propane. III. A Reference Equation of State for Temperatures from the Melting Line to 650 K and Pressures up to 1000 MPa](https://www.doi.org/10.1021/je900217v). *Journal of Chemical & Engineering Data*, 54 (12), 3141-3180.

The problems with noise-free measurement data were originally implemented for the following publication:
 - Sass, S., Tsoukalas, A., Bell, I. H., Bongartz, D., Najman, J., & Mitsos, A. (2023). [Towards global parameter estimation exploiting reduced data sets](https://doi.org/10.1080/10556788.2023.2205645). *Optimization Methods and Software*, 38(6), 1129–1141.

The noisy measurement data was originally generated for the following publication:
 - Sass, S., Mitsos, A., Nikolov, N. I., & Tsoukalas, A. (2024b).  Out-of-sample estimation for a branch-and-bound algorithm with growing datasets. Submitted.

<!-- Empty spaces in last column are inserted to increase column width -->
| Instance        | Problem Type | Number of Continuous Variables | Number of Integer Variables | Number of Binary Variables | Number of Equalities | Number of Inequalities | Best Known Objective Value* | Proven Objective Bound* | Description&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|
| ------------- |-------------| ------------| ------------| ------------| ------------| ------------| :------------: | :-----: | ------------- |
| EquationOfStatePropane_EOS2262 | MINLP | 6 | 2 | 1 | 0 | 0 |  &#949;** | 0 | Model EOS2262 of Sass et al. (2024a,b): Dataset contains 2262 data points without measurement noise generated with the model |
| EquationOfStatePropane_EOS262 | MINLP | 6 | 2 | 1 | 0 | 0 | &#949;** | 0 | Model EOS262 of Sass et al. (2024a,b): Reduced noise-free dataset to 262 data points |
| EquationOfStatePropane_EOS2262noisy | MINLP | 6 | 2 | 1 | 0 | 0 | 6.2123e7*** | 0.0105*** | Model EOS2262noisy of Sass et al. (2024b): Dataset contains 2262 data points with synthetic measurement noise generated with the model |
| EquationOfStatePropane_EOS262noisy | MINLP | 6 | 2 | 1 | 0 | 0 | 7.8811e5**** | 0.0032**** | Model EOS262noisy of Sass et al. (2024b): Reduced noisy dataset to 262 data points |

*The values correspond to using the summed squared error as the objective function.

**Since we use exact synthetic measurements, after convergence the global solution is smaller than the optimality tolerance &#949;.

***Calculated with the deterministic approach of the B&B algorithm with growing datasets applying augmentation rule CONST using the parameter values of Lemmon et al. (2009) as initial solution (CLP by J.J. Forrest et al. for lower bounding, CPU time limit of 23h, rest are default settings; Intel Xeon 8468 Sapphire processors with frequency 2.1GHz, 2.56GB RAM)

****Calculated with the deterministic approach of the B&B algorithm with growing datasets augmentation rule CONST and without resampling within a CPU time limit of 23h, see Sass et al. (2024b)