# GloPSE - *Glo*bal optimization problems from *P*rocess *S*ystems *E*ngineering

GloPSE is a library of test problems for global optimization of nonlinear programming (NLP) and mixed-integer nonlinear programming (MINLP) problems.
Most problems originate from design, operation, or parameter estimation problems in process systems engineering.
Most problems have originally been implemented for use with the global optimization solver [MAiNGO](https://git.rwth-aachen.de/avt.svt/public/MAiNGO.git).

This project is licensed under [Creative Commons Attribution 4.0 International license (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/).
If you use any of the models, please cite the publications indicated in the Readme.md files in the corresponding folder/s.

Most problems are currently available in three formats:
- as C++ header files for the C++ API of MAiNGO,
- as *.txt* files in the text-based input format for MAiNGO that is based on the [ALE library](https://git.rwth-aachen.de/avt.svt/public/libale),
- as GAMS input files.
The GAMS versions of the problems typically have one additional variable and equality compared to the MAiNGO versions because of the requirement of GAMS that the objective be a single variable.
Other differences between the GAMS and MAiNGO versions of the problems are described in the Readme.md files in the corresponding folders of this repository.

---------------------

Here is an overview of the available problem families, each of which can contain multiple problem instances:


| Family        | Description           | Problem Types* | Number of Instances  | 
| ------------- |-------------| :------------:| :-----:|
| BottomingCycle | Flowsheet optimization problems for the design of steam power cycles using ideal thermodynamics | NLP, DNLP | 9 |
| BottomingCycleIAPWS | Flowsheet optimization problems for the design of steam power cycles using the IAPWS-IF97 model | DNLP | 6 |
| DirectOxidationOME | Flowsheet optimization problem for production of OME via direct oxidation of methanol | DNLP | 1 |
| ElectrochemicalImpedanceSpectroscopy2CPE | Parameter estimation problem for fitting the electrical impedance of an electrochemical system with two constant phase elements | NLP | 1 |
| EquationOfStatePropane | Parameter estimation problem for fitting an equation of state for propane | MINLP | 4 |
| FlashNRTL | Optimization problems containing a single flash unit using the NRTL model for activity coefficients and excess enthalpy | NLP | 4 |
| GaussianMixtureModel1D | Parameter estimation problem for fitting a Gaussian mixture model based on one-dimensional data points | NLP | 2 |
| IndirectHardModeling | Parameter estimation problem for fitting the Raman intensity following the approach of indirect hard modeling | NLP | 2 |
| MetabolicPathways | Parameter estimation problem for fitting metabolic pathways | NLP-DO | 2 | 
| MethanolProduction | Flowsheet optimization problems for methanol production from hydrogen and carbon dioxide | DNLP | 3 |
| ReactionKineticsC6H7 | Parameter estimation problem for fitting the kinetic mechanism of a chemical reaction of cyclohexadienyl radicals | NLP-DO | 1 |
| TrainingArtificialNeuralNetworks | Parameter estimation problem for training an artificial neural network | NLP | 1 |
| TwoColumn | Flowsheet optimization problem for two distillation columns in series modeled with the Underwood shortcut | NLP | 1 |

*Problem types abbreviations:
- DNLP: nonsmooth nonlinear program
- MINLP: mixed-integer nonlinear program
- NLP: nonlinear program
- NLP-DO: dynamic optimization problem transformed into NLP by discretization