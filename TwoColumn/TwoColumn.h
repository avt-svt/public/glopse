/**
 *
 * @brief File implementing the optimization problem for the case study of two distillation
 *        columns for separating a mixtures of OMEs, which are modeled with the Underwood shortcut.
 *
 *        The relaxations of the thermodynamic models as well as the case study were originally published in:
 *        J. Najman, D. Bongartz, and A. Mitsos: "Relaxations of thermodynamic property
 *             and costing models in process engineering",
 *             Computers & Chemical Engineering 130 (2019), 106571.
 *             https://www.sciencedirect.com/science/article/abs/pii/S0098135419309494
 *        E-mail: amitsos@alum.mit.edu
 *
 * ==============================================================================
 * © 2020, Process Systems Engineering (AVT.SVT), RWTH Aachen University
 * ==============================================================================
 *
 * used in:
 *    D. Bongartz: "Deterministic Global Flowsheet Optimization for the Design of Energy Conversion Processes",
 *     	            PhD Thesis, RWTH Aachen University, 2020.
 *
 */

#pragma once

#include "MAiNGOmodel.h"
#include "Thermo/SubcriticalComponent.h"

using Var = mc::FFVar;


//////////////////////////////////////////////////////////////////////////
// Model class to be passed to MAiNGO
class Model: public maingo::MAiNGOmodel {

	public:
		Model();
		maingo::EvaluationContainer evaluate(const std::vector<Var> &optVars);
		std::vector<maingo::OptimizationVariable> get_variables();


	private:

		// Model parameters:
			double F,Tf,pf;
			std::vector<double> z,distSplit,distSplit2;
			size_t ncomp,heavyKey,lightKey,heavyKey2,lightKey2;
		// Declaration of model variables:
			std::vector< SubcriticalComponent<Var> > components;
			std::vector< Var > Top,Bot,Vap,dhvapTop,dhvapBot,psTop,psBot,higTop,higBot,Ktop,Kbot,avPs,alpha;
			Var hf,sumBot,sumTop,underwoodRhs,Theta,rRHS,rLHS,Rmin,R,Qcond,Qreb;
			Var Ttop,Tbot,Htop,Hbot,psiTop,psiBot,pColumn,Nmin,Nt,nRHS,nLHS,eta,alphaLightHeavy,N,H,fR;
			std::vector< Var > Top2,Bot2,Vap2,dhvapTop2,dhvapBot2,psTop2,psBot2,higTop2,higBot2,Ktop2,Kbot2,avPs2,alpha2;
			Var hf2,sumBot2,sumTop2,underwoodRhs2,Theta2,rRHS2,rLHS2,Rmin2,R2,Qcond2,Qreb2;
			Var Ttop2,Tbot2,Htop2,Hbot2,psiTop2,psiBot2,pColumn2,Nmin2,Nt2,nRHS2,nLHS2,eta2,alphaLightHeavy2,N2,H2,fR2;


};


//////////////////////////////////////////////////////////////////////////
// function for providing optimization variable data to the Branch-and-Bound solver
std::vector<maingo::OptimizationVariable> Model::get_variables() {

	std::vector<maingo::OptimizationVariable> variables;
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1+1e-8,100),"Theta"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-6,100),"Rmin"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(30,200),"Ttop"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(30,350),"Tbot"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1+1e-8,100),"Theta-2"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-6,100),"Rmin-2"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(30,200),"Ttop-2"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(30,350),"Tbot-2"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.1,10),"pColumn"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.1,10),"pColumn-2"));

	return variables;
}


//////////////////////////////////////////////////////////////////////////
// constructor for the model
Model::Model() {

	// Initialize data if necessary:
		// Pure component models:
			components.push_back(SubcriticalComponent<Var>("OME1",76.0953,480.6,-1,-1,-1,-1));
			components.push_back(SubcriticalComponent<Var>("OME2",106.12,552.2,-1,-1,-1,-1));
			components.push_back(SubcriticalComponent<Var>("OME3",136.147,603.4,-1,-1,-1,-1));
			components.push_back(SubcriticalComponent<Var>("OME4",166.173,646.9,-1,-1,-1,-1));
			components.push_back(SubcriticalComponent<Var>("OME5",196.199,683.7,-1,-1,-1,-1));
			components.push_back(SubcriticalComponent<Var>("OME6",226.225,714.8,-1,-1,-1,-1));
			components.push_back(SubcriticalComponent<Var>("OME7",256.251,743.0,-1,-1,-1,-1));
			components.push_back(SubcriticalComponent<Var>("OME8",286.277,769.2,-1,-1,-1,-1));
			components.push_back(SubcriticalComponent<Var>("OME9",316.303,794.6,-1,-1,-1,-1));
			components.push_back(SubcriticalComponent<Var>("OME10",346.329,819.9,-1,-1,-1,-1));
			ncomp = components.size();
			z.resize(ncomp); distSplit.resize(ncomp); distSplit2.resize(ncomp);
			Top.resize(ncomp); Bot.resize(ncomp); Vap.resize(ncomp); avPs.resize(ncomp); dhvapTop.resize(ncomp);
			dhvapBot.resize(ncomp); psTop.resize(ncomp); psBot.resize(ncomp); higTop.resize(ncomp); higBot.resize(ncomp); Ktop.resize(ncomp); Kbot.resize(ncomp); alpha.resize(ncomp);
			Top2.resize(ncomp); Bot2.resize(ncomp); Vap2.resize(ncomp); avPs2.resize(ncomp); dhvapTop2.resize(ncomp);
			dhvapBot2.resize(ncomp); psTop2.resize(ncomp); psBot2.resize(ncomp); higTop2.resize(ncomp); higBot2.resize(ncomp); Ktop2.resize(ncomp); Kbot2.resize(ncomp); alpha2.resize(ncomp);
		// Pure component property parameters
			// Extended Antoine [bar,K]
				components[0].set_vapor_pressure_model(SubcriticalComponent<Var>::PVAP_XANTOINE,std::vector<double>{80.1271,-6279.1,0,0,-10.631,9.7948e-6,2});
				components[1].set_vapor_pressure_model(SubcriticalComponent<Var>::PVAP_XANTOINE,std::vector<double>{68.104,-7223.44,0,0,-8.2522,0,1});
				components[2].set_vapor_pressure_model(SubcriticalComponent<Var>::PVAP_XANTOINE,std::vector<double>{63.682,-8042.31,0,0,-7.4100,0,1});
				components[3].set_vapor_pressure_model(SubcriticalComponent<Var>::PVAP_XANTOINE,std::vector<double>{81.214,-10017.28,0,0,-9.7511,0,1});
				components[4].set_vapor_pressure_model(SubcriticalComponent<Var>::PVAP_XANTOINE,std::vector<double>{86.939,-11323.17,0,0,-10.3994,0,1});
				components[5].set_vapor_pressure_model(SubcriticalComponent<Var>::PVAP_XANTOINE,std::vector<double>{93.494,-12720.0,0,0,-11.1491,0,1});
				components[6].set_vapor_pressure_model(SubcriticalComponent<Var>::PVAP_XANTOINE,std::vector<double>{99.812,-14090.9,0,0,-11.8697,0,1});
				components[7].set_vapor_pressure_model(SubcriticalComponent<Var>::PVAP_XANTOINE,std::vector<double>{106.13,-15462,0,0,-12.5903,0,1});
				components[8].set_vapor_pressure_model(SubcriticalComponent<Var>::PVAP_XANTOINE,std::vector<double>{112.448,-16833,0,0,-13.3109,0,1});
				components[9].set_vapor_pressure_model(SubcriticalComponent<Var>::PVAP_XANTOINE,std::vector<double>{118.766,-18204,0,0,-14.0315,0,1});
			// DIPPR106 [kJ/kmol,K]
				components[0].set_enthalpy_of_vaporization_model(SubcriticalComponent<Var>::DHVAP_DIPPR106,std::vector<double>{3.294e+04,-1.714,5.991,-5.656,1.775});
				components[1].set_enthalpy_of_vaporization_model(SubcriticalComponent<Var>::DHVAP_DIPPR106,std::vector<double>{2.672e+04,-3.999,11.56,-11.76,4.327});
				components[2].set_enthalpy_of_vaporization_model(SubcriticalComponent<Var>::DHVAP_DIPPR106,std::vector<double>{2.971e4,-4.13,11.68,-11.99,4.478});
				components[3].set_enthalpy_of_vaporization_model(SubcriticalComponent<Var>::DHVAP_DIPPR106,std::vector<double>{3.984e+04,-3.577,10.46,-10.61,3.866});
				components[4].set_enthalpy_of_vaporization_model(SubcriticalComponent<Var>::DHVAP_DIPPR106,std::vector<double>{4.153e+04,-4.053,11.69,-11.6,4.383});
				components[5].set_enthalpy_of_vaporization_model(SubcriticalComponent<Var>::DHVAP_DIPPR106,std::vector<double>{4.111e+04,-4.796,13.61,-13.92,5.19});
				components[6].set_enthalpy_of_vaporization_model(SubcriticalComponent<Var>::DHVAP_DIPPR106,std::vector<double>{4.479e+04,-4.894,13.87,-14.19,5.297});
				components[7].set_enthalpy_of_vaporization_model(SubcriticalComponent<Var>::DHVAP_DIPPR106,std::vector<double>{4.735e+04,-5.113,14.44,-14.78,5.534});
				components[8].set_enthalpy_of_vaporization_model(SubcriticalComponent<Var>::DHVAP_DIPPR106,std::vector<double>{5.03e+04,-5.253,14.8,-15.16,5.684});
				components[9].set_enthalpy_of_vaporization_model(SubcriticalComponent<Var>::DHVAP_DIPPR106,std::vector<double>{4.773e+04,-6.016,16.79,-17.23,6.51});
			// Heat capacity - DIPPR107 [kJ/kmolK,K]
				components[0].set_heat_capacity_model(PureComponent<Var>::CPIG_ASPEN,std::vector<double>{1.502629586446264e+02,-9.551611083636588e-01,3.953082258697942e-03,-5.548950420989062e-06,2.763999776250384e-09,0});
				components[1].set_heat_capacity_model(PureComponent<Var>::CPIG_ASPEN,std::vector<double>{1.605577593591216e+02,-1.040368265607772e+00,4.977756291459314e-03,-7.463794724839981e-06,3.894956309960266e-09,0});
				components[2].set_heat_capacity_model(PureComponent<Var>::CPIG_ASPEN,std::vector<double>{1.820024630414500e+02,-1.217257500254852e+00,6.140830001932475e-03,-9.380045555624142e-06,4.952338714432728e-09,0});
				components[3].set_heat_capacity_model(PureComponent<Var>::CPIG_ASPEN,std::vector<double>{2.024018497185518e+02,-1.384817107446859e+00,7.273167575905652e-03,-1.125234758345771e-05,5.986593726307056e-09,0});
				components[4].set_heat_capacity_model(PureComponent<Var>::CPIG_ASPEN,std::vector<double>{2.222406683049157e+02,-1.547249512609190e+00,8.388640664136242e-03,-1.310051805115538e-05,7.008127482440612e-09,0});
				components[5].set_heat_capacity_model(PureComponent<Var>::CPIG_ASPEN,std::vector<double>{2.417561328164309e+02,-1.706734961452716e+00,9.494268130216792e-03,-1.493441057041688e-05,8.022056461358733e-09,0});
				components[6].set_heat_capacity_model(PureComponent<Var>::CPIG_ASPEN,std::vector<double>{2.610551416985755e+02,-1.864236060379643e+00,1.059326043867153e-02,-1.675870852051629e-05,9.030887741249190e-09,0});
				components[7].set_heat_capacity_model(PureComponent<Var>::CPIG_ASPEN,std::vector<double>{2.801942177847529e+02,-2.020289688399828e+00,1.168746772371273e-02,-1.857615681951461e-05,1.003610601225607e-08,0});
				components[8].set_heat_capacity_model(PureComponent<Var>::CPIG_ASPEN,std::vector<double>{2.992379066665130e+02,-2.175454833995909e+00,1.277861528177447e-02,-2.038908040773843e-05,1.103888116400356e-08,0});
				components[9].set_heat_capacity_model(PureComponent<Var>::CPIG_ASPEN,std::vector<double>{3.181865705479742e+02,-2.329728014336780e+00,1.386701261943493e-02,-2.219823406830808e-05,1.203972372112378e-08,0});

	// Feed:
		F = 1; 			// [kmol/s]
		Tf = 343; 		// [K]
		pf = 1;			// [bar]
		z[0] = 0.273;	// OME1
		z[1] = 0.222;	// OME2
		z[2] = 0.165;	// OME3
		z[3] = 0.118;	// OME4
		z[4] = 0.081;	// OME5
		z[5] = 0.055;	// OME6
		z[6] = 0.037;	// OME7
		z[7] = 0.024;	// OME8
		z[8] = 0.015;	// OME9
		z[9] = 1. - z[0] - z[1] - z[2] - z[3] - z[4] - z[5] - z[6] - z[7] - z[8];	// OME10
		hf=0;		// kJ/kmol
		for (unsigned int i=0; i<ncomp; i++) {
			Var deltahvfi = components[i].calculate_vaporization_enthalpy(Tf);
			Var higfi = components[i].calculate_ideal_gas_enthalpy(Tf);
			hf += z[i]*(higfi - deltahvfi);
		}


	// Column specification - C1
		distSplit[0] = 1.; 		// OME1
		distSplit[1] = 0.995; 	// OME2
		distSplit[2] = 0.005;	// OME3
		distSplit[3] = 0.; 		// OME4
		distSplit[4] = 0.;  	// OME5
		distSplit[5] = 0.;	 	// OME6
		distSplit[6] = 0.;		// OME7
		distSplit[7] = 0.;		// OME8
		distSplit[8] = 0.;		// OME9
		distSplit[9] = 0.;		// OME10
		lightKey = 1;	// light key component in distillation: OME2
		heavyKey = 2; 	// heavy key component in distillation: OME3

	// Column specification - C2
		distSplit2[0] = 1.; 	// OME1
		distSplit2[1] = 1.; 	// OME2
		distSplit2[2] = 1.;		// OME3
		distSplit2[3] = 1.; 	// OME4
		distSplit2[4] = 0.995;  // OME5
		distSplit2[5] = 0.005; 	// OME6
		distSplit2[6] = 0.;		// OME7
		distSplit2[7] = 0.;		// OME8
		distSplit2[8] = 0.;		// OME9
		distSplit2[9] = 0.;		// OME10
		lightKey2 = 4;	// light key component in distillation: OME5
		heavyKey2 = 5; 	// heavy key component in distillation: OME6

}


//////////////////////////////////////////////////////////////////////////
// Evaluate the model
maingo::EvaluationContainer Model::evaluate(const std::vector<Var> &optVars)
{

	// Rename / prepare inputs
		Theta = optVars[0];
		Rmin = optVars[1];
		Ttop = optVars[2] + 273.15;
		Tbot = optVars[3] + 273.15;
		Theta2 = optVars[4];
		Rmin2 = optVars[5];
		Ttop2 = optVars[6] + 273.15;
		Tbot2 = optVars[7] + 273.15;
		pColumn = optVars[8];
		pColumn2 = optVars[9];

	// Model

		// Column C1
			// 1. Species balance
				sumTop = 0.; sumBot = 0.;
				for(size_t i=0; i<ncomp; ++i) {
					Top[i] = distSplit[i]*F*z[i];
					Bot[i] = (1-distSplit[i])*F*z[i];
					sumTop += Top[i];
					sumBot += Bot[i];
				}
			// // 2. Compute reflux ratio
				// vapor pressure: top, bottom, and average
					for (size_t i=0; i<ncomp; ++i) {
						psTop[i] = components[i].calculate_vapor_pressure_conv(Ttop);
						psBot[i] = components[i].calculate_vapor_pressure_conv(Tbot);
						avPs[i] = sqrt( psTop[i]*psBot[i] );
					}
					for (size_t i=0; i<ncomp; ++i) {
						if (i!=heavyKey) {
							alpha[i] = sqrt( psTop[i]*psBot[i] / (psTop[heavyKey]*psBot[heavyKey]));
						} else {
							alpha[i] = 1.0;
						}
					}
				// compute Underwood factor
					underwoodRhs = 0.;
					for (size_t i=0; i<ncomp; ++i) {
						Var tmp = alpha[i]*z[i];
						for (size_t j=0; j<ncomp; ++j) {
							if (j!=i) {
								tmp = tmp*(alpha[j]-Theta);
							}
						}
						underwoodRhs += tmp;
					}
				// compute reflux ratio
					rRHS = 0.;
					rLHS = (Rmin+1.)*sumTop;
					for (size_t i=0; i<ncomp; ++i) {
						rLHS = rLHS * (alpha[i]-Theta);
						Var tmp = alpha[i]*Top[i];
						for (size_t j=0; j<ncomp; ++j) {
							if (j!=i) {
								tmp = tmp*(alpha[j]-Theta);
							}
						}
						rRHS += tmp;
					}
					fR = 1.3;
					R = Rmin * fR;
			// 3. Compute condenser duty
				for (size_t i=0; i<ncomp; ++i) {
					Vap[i] = Top[i] * (1+R);
				}
				Htop = 0.; Qcond = 0.; psiTop = 0.;
				for (size_t i=0; i<ncomp; ++i) {
					Ktop[i] = psTop[i]/pColumn;
					psiTop += Top[i]*(1-Ktop[i]);
					higTop[i] = components[i].calculate_ideal_gas_enthalpy_conv(Ttop);
					dhvapTop[i] = components[i].calculate_vaporization_enthalpy_conv(Ttop);
					Htop += Top[i]*(higTop[i]-dhvapTop[i]);
					Qcond -= Vap[i]*dhvapTop[i];
				}
			// 4. Compute reboiler duty
				// get reboiler duty
					Hbot = 0.; psiBot = 0.;
					for (size_t i=0; i<ncomp; ++i) {
						Kbot[i] = psBot[i]/pColumn;
						psiBot += Bot[i]*(1-Kbot[i]);
						higBot[i] = components[i].calculate_ideal_gas_enthalpy_conv(Tbot);
						dhvapBot[i] = components[i].calculate_vaporization_enthalpy_conv(Tbot);
						Hbot += Bot[i]*(higBot[i]-dhvapBot[i]);
					}
					Qreb = -Qcond - F*hf + Hbot + Htop;


		// Column C2
			// 1. Species balance
				sumTop2 = 0.; sumBot2 = 0.;
				for(size_t i=0; i<ncomp; ++i) {
					Top2[i] = distSplit2[i]*Bot[i];
					Bot2[i] = (1-distSplit2[i])*Bot[i];
					sumTop2 += Top2[i];
					sumBot2 += Bot2[i];
				}
			// 2. Compute reflux ratio
				// vapor pressure: top, bottom, and average
					for (size_t i=0; i<ncomp; ++i) {
						psTop2[i] = components[i].calculate_vapor_pressure_conv(Ttop2);
						psBot2[i] = components[i].calculate_vapor_pressure_conv(Tbot2);
					}
					for (size_t i=0; i<ncomp; ++i) {
						if (i!=heavyKey2) {
							alpha2[i] = sqrt( psTop2[i]*psBot2[i] / (psTop2[heavyKey2]*psBot2[heavyKey2]));
						} else {
							alpha2[i] = 1.0;
						}
					}
				// compute Underwood factor
					underwoodRhs2 = 0.;
					for (size_t i=0; i<ncomp; ++i) {
						Var tmp = alpha2[i]*Bot[i];
						for (size_t j=0; j<ncomp; ++j) {
							if (j!=i) {
								tmp = tmp*(alpha2[j]-Theta2);
							}
						}
						underwoodRhs2 += tmp;
					}
				// compute reflux ratio
					rRHS2 = 0.;
					rLHS2 = (Rmin2+1.)*sumTop2;
					for (size_t i=0; i<ncomp; ++i) {
						rLHS2 = rLHS2 * (alpha2[i]-Theta2);
						Var tmp = alpha2[i]*Top2[i];
						for (size_t j=0; j<ncomp; ++j) {
							if (j!=i) {
								tmp = tmp*(alpha2[j]-Theta2);
							}
						}
						rRHS2 += tmp;
					}
					fR2 = 1.3;
					R2 = Rmin2 * fR2;
			// 3. Compute condenser duty
				for (size_t i=0; i<ncomp; ++i) {
					Vap2[i] = Top2[i] * (1+R2);
				}
				Htop2 = 0.; Qcond2 = 0.; psiTop2 = 0.;
				for (size_t i=0; i<ncomp; ++i) {
					Ktop2[i] = psTop2[i]/pColumn2;
					psiTop2 += Top2[i]*(1-Ktop2[i]);
					higTop2[i] = components[i].calculate_ideal_gas_enthalpy_conv(Ttop2);
					dhvapTop2[i] = components[i].calculate_vaporization_enthalpy_conv(Ttop2);
					Htop2 += Top2[i]*(higTop2[i]-dhvapTop2[i]);
					Qcond2 -= Vap2[i]*dhvapTop2[i];
				}
			// 4. Compute reboiler duty
				// get reboiler duty
					Hbot2 = 0.; psiBot2 = 0.;
					for (size_t i=0; i<ncomp; ++i) {
						Kbot2[i] = psBot2[i]/pColumn2;
						psiBot2 += Bot2[i]*(1-Kbot2[i]);
						higBot2[i] = components[i].calculate_ideal_gas_enthalpy_conv(Tbot2);
						dhvapBot2[i] = components[i].calculate_vaporization_enthalpy_conv(Tbot2);
						Hbot2 += Bot2[i]*(higBot2[i]-dhvapBot2[i]);
					}
					Qreb2 = -Qcond2 - Hbot + Hbot2 + Htop2;


	// Prepare output
		maingo::EvaluationContainer result;
		// Objective:
			result.objective = Qreb+Qreb2;
		// Inequalities (<=0):
			// C1
				result.ineq.push_back( (Theta - alpha[lightKey]) / 1e0 );
			// C2
				result.ineq.push_back( (Theta2 - alpha2[lightKey2]) / 1e0 );

		// Equalities (=0):
			// C1
				result.eq.push_back( (underwoodRhs) / 1e-4 );
				result.eq.push_back( (rRHS - rLHS) / 1e0 );
				result.eq.push_back( (psiBot - 0.) / 1e0 );
				result.eq.push_back( (psiTop - 0.) / 1e0 );
			// C2
				result.eq.push_back( (underwoodRhs2) / 1e-2 );
				result.eq.push_back( (rRHS2 - rLHS2) / 1e0 );
				result.eq.push_back( (psiTop2 - 0.) / 1e0 );
				result.eq.push_back( (psiBot2 - 0.) / 1e0 );

		return result;
}