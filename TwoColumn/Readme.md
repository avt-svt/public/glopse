# TwoColumn

Flowsheet optimization problem for minimizing the reboiler duty of two columns connected in series aiming at separating a 10-component mixture of oxymethylene dimethyl ethers (OME_n, with n=1,...,10).
The distillation columns are modeled via the Underwood shortcut.
Phase equilibrium is descibed using the extended Antoine equation for vapor pressures.
Enthalpies are computed using the Aspen polynomial for ideal gas heat capacity and the DIPPR 106 equation for enthalpy of vaporization.
The MAiNGO version of the problems (both for the C++ API and the text-based versions) use intrinsic functions and custom relaxations for the above-mentioned property models.

The problem as well as the custom relaxations for vapor pressure, ideal gas enthalpy, enthalpy of vaporization were original implemented for the following publication:
 - Najman, J., Bongartz, D., & Mitsos, A. (2019). [Relaxations of thermodynamic property and costing models in process engineering](https://www.sciencedirect.com/science/article/abs/pii/S0098135419309494). *Computers & Chemical Engineering*, 130, 106571.                        



| Instance        | Problem Type | Number of Continuous Variables | Number of Integer Variables | Number of Binary Variables | Number of Equalities | Number of Inequalities | Best Known Objective Value | Proven Objective Bound | Description           |
| ------------- |-------------| ------------| ------------| ------------| ------------| ------------| :------------: | :-----: | ----- |
| TwoColumn  | NLP | 10 | 0 | 0 | 8 | 2 | 49230.8 | 49230.6 | Corresponds to problem `2column' in Najman et al. (2019) |