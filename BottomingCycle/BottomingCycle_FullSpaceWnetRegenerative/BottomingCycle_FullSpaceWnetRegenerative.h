/**
 *
 * @brief Full-space formulation of Case Study II (Regenerative Rankine Cycle)
 *        that was originally published in
 *        D. Bongartz, A. Mitsos: "Deterministic global optimization of process
 *             flowsheets in a reduced space using McCormick relaxations",
 *             Journal of Global Optimization 69 (2017), 761-796.
 *             https://link.springer.com/article/10.1007/s10898-017-0547-4
 *        E-mail: amitsos@alum.mit.edu
 *
 * ==============================================================================
 * © 2020, Process Systems Engineering (AVT.SVT), RWTH Aachen University
 * ==============================================================================
 *
 * used in:
 *    D. Bongartz: "Deterministic Global Flowsheet Optimization for the Design of Energy Conversion Processes",
 *     	            PhD Thesis, RWTH Aachen University, 2020.
 *
 */

#pragma once

#include "MAiNGOmodel.h"
#include "../Thermo/IdealFluidProperties.h"


class Model: public maingo::MAiNGOmodel {

  public:

    Model();

    maingo::EvaluationContainer evaluate(const std::vector<Var>& optVars);
    std::vector<maingo::OptimizationVariable> get_variables();

  private:

    const double eta_p, eta_t, mcpG, TGin, TGout, Tmax, dTap, dTmin, xmin, Amin, Vmin;
    const double kEco, kEvap, kSH, kCond, k1A, k2A, k3A, p1A, p2A, p3A, FmA, B1A, B2A, k1B, k2B, k3B, FmB, B1B, B2B, Tcin, Tcout;
    const double InvGT, WorkGT, FuelHeat, FuelPrice, fPhi, fAnnu, Teq, Varcost;
	double c1A,c2A,c3A,p0;

    const thermo::IdealFluidProperties properties;
};


//////////////////////////////////////////////////////////////////////////
// function for providing optimization variable data to the Branch-and-Bound solver
std::vector<maingo::OptimizationVariable>
Model::get_variables()
{
	using maingo::OptimizationVariable;
	using maingo::Bounds;

    std::vector<OptimizationVariable> variables;
	variables.push_back(OptimizationVariable(Bounds(0.1*0.2,0.1*5),"p2"));
	variables.push_back(OptimizationVariable(Bounds(0.1*3,0.1*100),"p4"));
	variables.push_back(OptimizationVariable(Bounds(5,100),"mdot"));
	variables.push_back(OptimizationVariable(Bounds(2480,3750),"h7"));
	variables.push_back(OptimizationVariable(Bounds(0.01,0.2),"k"));
	variables.push_back(OptimizationVariable(Bounds(300,873),"T1sat"));
	variables.push_back(OptimizationVariable(Bounds(300,873),"T2sat"));
	variables.push_back(OptimizationVariable(Bounds(300,873),"T3sat"));
	variables.push_back(OptimizationVariable(Bounds(300,873),"T4sat"));
	variables.push_back(OptimizationVariable(Bounds(300,873),"T5sat"));
	variables.push_back(OptimizationVariable(Bounds(300,873),"T6sat"));
	variables.push_back(OptimizationVariable(Bounds(300,873),"T7sat"));
	variables.push_back(OptimizationVariable(Bounds(300,873),"T8sat"));
	variables.push_back(OptimizationVariable(Bounds(300,873),"T9sat"));
	variables.push_back(OptimizationVariable(Bounds(300,873),"T10sat"));
	variables.push_back(OptimizationVariable(Bounds(300,873),"T11sat"));
	variables.push_back(OptimizationVariable(Bounds(0.1*0.05,0.1*100),"p7"));
	variables.push_back(OptimizationVariable(Bounds(300,873),"T7"));
	variables.push_back(OptimizationVariable(Bounds(4.23,10),"s7"));
	variables.push_back(OptimizationVariable(Bounds(0.1*0.05,0.1*5),"p8"));
	variables.push_back(OptimizationVariable(Bounds(0.1*0.05,0.1*5),"p10"));
	variables.push_back(OptimizationVariable(Bounds(0,1500),"h8satliq"));
	variables.push_back(OptimizationVariable(Bounds(2480,3200),"h8satvap"));
	variables.push_back(OptimizationVariable(Bounds(0,1500),"h10satliq"));
	variables.push_back(OptimizationVariable(Bounds(2480,3200),"h10satvap"));
	variables.push_back(OptimizationVariable(Bounds(0,3),"s10satliq"));
	variables.push_back(OptimizationVariable(Bounds(4.23,10),"s10satvap"));
	variables.push_back(OptimizationVariable(Bounds(0.5,1),"x10"));
	variables.push_back(OptimizationVariable(Bounds(1240,3200),"h10"));
	variables.push_back(OptimizationVariable(Bounds(2.115,10),"s10"));
	variables.push_back(OptimizationVariable(Bounds(100,3750),"wt8"));
	variables.push_back(OptimizationVariable(Bounds(1240,3200),"h8"));
	variables.push_back(OptimizationVariable(Bounds(0.85,1),"x8"));
	variables.push_back(OptimizationVariable(Bounds(0.1*0.05,0.1*3),"p9"));
	variables.push_back(OptimizationVariable(Bounds(0.1*0.05,0.1*3),"p11"));
	variables.push_back(OptimizationVariable(Bounds(0,1500),"h9satliq"));
	variables.push_back(OptimizationVariable(Bounds(2480,3200),"h9satvap"));
	variables.push_back(OptimizationVariable(Bounds(0,1500),"h11satliq"));
	variables.push_back(OptimizationVariable(Bounds(2480,3200),"h11satvap"));
	variables.push_back(OptimizationVariable(Bounds(0,3),"s11satliq"));
	variables.push_back(OptimizationVariable(Bounds(4.23,10),"s11satvap"));
	variables.push_back(OptimizationVariable(Bounds(0.5,1),"x11"));
	variables.push_back(OptimizationVariable(Bounds(1240,3200),"h11"));
	variables.push_back(OptimizationVariable(Bounds(2.115,10),"s11"));
	variables.push_back(OptimizationVariable(Bounds(100,3750),"wt9"));
	variables.push_back(OptimizationVariable(Bounds(1240,3200),"h9"));
	variables.push_back(OptimizationVariable(Bounds(0.85,1),"x9"));
	variables.push_back(OptimizationVariable(Bounds(0,1500),"h1satliq"));
	variables.push_back(OptimizationVariable(Bounds(0,300),"h1"));
	variables.push_back(OptimizationVariable(Bounds(0,12.5),"wp2"));
	variables.push_back(OptimizationVariable(Bounds(0.25,325),"h2"));
	variables.push_back(OptimizationVariable(Bounds(0.1*0.05,0.1*100),"p3"));
	variables.push_back(OptimizationVariable(Bounds(0.25,1500),"h3"));
	variables.push_back(OptimizationVariable(Bounds(0,1500),"h3satliq"));
	variables.push_back(OptimizationVariable(Bounds(0,12.5),"wp4"));
	variables.push_back(OptimizationVariable(Bounds(0.25,1500),"h4"));
	variables.push_back(OptimizationVariable(Bounds(300,873),"T4"));
	variables.push_back(OptimizationVariable(Bounds(12400,375000),"Qzu"));
	variables.push_back(OptimizationVariable(Bounds(423,900),"TG4"));
	variables.push_back(OptimizationVariable(Bounds(0.1*0.05,0.1*100),"p5"));
	variables.push_back(OptimizationVariable(Bounds(300,873),"T5"));
	variables.push_back(OptimizationVariable(Bounds(0.25,1500),"h5"));
	variables.push_back(OptimizationVariable(Bounds(423,900),"TG3"));
	variables.push_back(OptimizationVariable(Bounds(0.1*0.05,0.1*100),"p6"));
	variables.push_back(OptimizationVariable(Bounds(2480,3200),"h6satvap"));
	variables.push_back(OptimizationVariable(Bounds(2480,3200),"h6"));
	variables.push_back(OptimizationVariable(Bounds(423,900),"TG2"));
	variables.push_back(OptimizationVariable(Bounds(2480,3200),"h7satvap"));
	variables.push_back(OptimizationVariable(Bounds(0,1250),"Work_Pump2"));
	variables.push_back(OptimizationVariable(Bounds(0,1250),"Work_Pump4"));
	variables.push_back(OptimizationVariable(Bounds(500,375000),"Work_Turb8"));
	variables.push_back(OptimizationVariable(Bounds(500,375000),"Work_Turb9"));
	variables.push_back(OptimizationVariable(Bounds(200,375000),"WorkNet"));

    return variables;
}


//////////////////////////////////////////////////////////////////////////
// constructor for the model
Model::Model():
    eta_p(0.8), eta_t(0.9), mcpG(200), TGin(900), TGout(448), Tmax(873.15), dTap(10), dTmin(15), xmin(0.85), Amin(10), Vmin(1),
    kEco(0.06), kEvap(0.06), kSH(0.03), kCond(0.35), k1A(4.3247), k2A(-0.303), k3A(0.1634),
    p1A(0.03881), p2A(-0.11272), p3A(0.08183), FmA(2.75), B1A(1.63), B2A(1.66), k1B(3.5565), k2B(0.3776), k3B(0.0905),
    FmB(1), B1B(1.49), B2B(1.52), Tcin(273.15+20), Tcout(273.15+25),
    InvGT(22.7176e6), WorkGT(69676), FuelHeat(182359), FuelPrice(14), fPhi(1.06), fAnnu(0.1875), Teq(4000), Varcost(4),
    properties("Water", 2.08, 4.18, 0.462, 0.001, 3.55959-1., 643.748, -198.043, 2480., 313.8336)
{
	// Convert coefficients for pressure factor from bar to MPa:
	c1A = p1A+p2A+p3A;
	c2A = p2A+2*p3A;
	c3A = p3A;
	p0 = pow(10.,properties.A - properties.B/(properties.T0+properties.C));
}


//////////////////////////////////////////////////////////////////////////
// evaluate the model
maingo::EvaluationContainer
Model::evaluate(const std::vector<Var>& optVars)
{
    // Rename inputs, these are the only real optimization variables
    Var p2=optVars[0];
	Var p4=optVars[1];
	Var mdot=optVars[2];
	Var h7=optVars[3];
	Var k=optVars[4];
	// Saturation temperatures
	Var T1sat = optVars[5];
	Var T2sat = optVars[6];
	Var T3sat = optVars[7];
	Var T4sat = optVars[8];
	Var T5sat = optVars[9];
	Var T6sat = optVars[10];
	Var T7sat = optVars[11];
	Var T8sat = optVars[12];
	Var T9sat = optVars[13];
	Var T10sat = optVars[14];
	Var T11sat = optVars[15];
	// Turbines:
	  // Inlet:
	Var p7 = optVars[16];
	Var T7 = optVars[17];
	Var s7 = optVars[18];
	  // Turbine 8 (bleed):
	Var p8 = optVars[19];
	Var p10 = optVars[20];
	Var h8satliq = optVars[21];
	Var h8satvap = optVars[22];
	Var h10satliq = optVars[23];
	Var h10satvap = optVars[24];
	Var s10satliq = optVars[25];
	Var s10satvap = optVars[26];
	Var x10 = optVars[27];
	Var h10 = optVars[28];
	Var s10 = optVars[29];
	Var wt8 = optVars[30];
	Var h8 = optVars[31];
	Var x8 = optVars[32];
	  // Turbine 9 (main):
	Var p9 = optVars[33];
	Var p11 = optVars[34];
	Var h9satliq = optVars[35];
	Var h9satvap = optVars[36];
	Var h11satliq = optVars[37];
	Var h11satvap = optVars[38];
	Var s11satliq = optVars[39];
	Var s11satvap = optVars[40];
	Var x11 = optVars[41];
	Var h11 = optVars[42];
	Var s11 = optVars[43];
	Var wt9 = optVars[44];
	Var h9 = optVars[45];
	Var x9 = optVars[46];
	// Feedwater line
	// Condensate pump
	Var h1satliq = optVars[47];
	Var h1 = optVars[48];
	Var wp2 = optVars[49];
	Var h2 = optVars[50];
	// Deaerator
	Var p3 = optVars[51];
	Var h3 = optVars[52];
	Var h3satliq = optVars[53];
	// Feedwater pump
	Var wp4 = optVars[54];
	Var h4 = optVars[55];
	Var T4 = optVars[56];
	// Boiler
	// Overall
	Var Qzu = optVars[57];
	Var TG4 = optVars[58];
	// Eco
	Var p5 = optVars[59];
	Var T5 = optVars[60];
	Var h5 = optVars[61];
	Var TG3 = optVars[62];
	// Evaporator
	Var p6 = optVars[63];
	Var h6satvap = optVars[64];
	Var h6 = optVars[65];
	Var TG2 = optVars[66];
	Var h7satvap = optVars[67];
	// Power
	Var Work_Pump2 = optVars[68];
	Var Work_Pump4 = optVars[69];
	Var Work_Turb8 = optVars[70];
	Var Work_Turb9 = optVars[71];
	Var WorkNet = optVars[72];



    // Model in a reduced-space formulation
    maingo::EvaluationContainer result;


	// Model equations
		Var p1    = 0.1*0.05;    // Fixed condenser pressure (converted to MPa)
		result.eq.push_back((T1sat - (properties.B/(properties.A-log(p1)/log(10.0))-properties.C))/100);
		result.eq.push_back((T2sat - (properties.B/(properties.A-log(p2)/log(10.0))-properties.C))/100);
		result.eq.push_back((T3sat - (properties.B/(properties.A-log(p3)/log(10.0))-properties.C))/100);
		result.eq.push_back((T4sat - (properties.B/(properties.A-log(p4)/log(10.0))-properties.C))/100);
		result.eq.push_back((T5sat - (properties.B/(properties.A-log(p5)/log(10.0))-properties.C))/100);
		result.eq.push_back((T6sat - (properties.B/(properties.A-log(p6)/log(10.0))-properties.C))/100);
		result.eq.push_back((T7sat - (properties.B/(properties.A-log(p7)/log(10.0))-properties.C))/100);
		result.eq.push_back((T8sat - (properties.B/(properties.A-log(p8)/log(10.0))-properties.C))/100);
		result.eq.push_back((T9sat - (properties.B/(properties.A-log(p9)/log(10.0))-properties.C))/100);
		result.eq.push_back((T10sat - (properties.B/(properties.A-log(p10)/log(10.0))-properties.C))/100);
		result.eq.push_back((T11sat - (properties.B/(properties.A-log(p11)/log(10.0))-properties.C))/100);
		// Model
		// Turbines:
		  // Turbine 8 (bleed):
		  // isentropic
		result.eq.push_back((p10 - (p2))/10);
		result.eq.push_back((h10satliq - (properties.cif*(T10sat-properties.T0) + properties.vif*1e3*(p10-p0)))/100);
		result.eq.push_back((h10satvap - (properties.deltaH + properties.cpig*(T10sat-properties.T0)))/1000);
		result.eq.push_back((s10satliq - (properties.cif*log(T10sat/properties.T0)))/1);
		result.eq.push_back((s10satvap - (properties.deltaH/properties.T0 + properties.cpig*log(T10sat/properties.T0) - properties.Rm*log(p10/p0)))/10);
		result.eq.push_back((s10 - (s7))/1);
		result.eq.push_back((s10 - (s10satliq+x10*(s10satvap-s10satliq)))/1);
		result.eq.push_back((h10 - (h10satliq + x10*(h10satvap-h10satliq)))/1000);
			//actual
		result.eq.push_back((p8 - (p2))/10);
		result.eq.push_back((h8satliq - (properties.cif*(T8sat-properties.T0) + properties.vif*1e3*(p8-p0)))/100);
		result.eq.push_back((h8satvap - (properties.deltaH + properties.cpig*(T8sat-properties.T0)))/1000);
		result.eq.push_back((h8 - (h7 - wt8))/1000);
		result.eq.push_back((h8 - (h8satliq+x8*(h8satvap-h8satliq)))/1000);
		result.eq.push_back((wt8 - (eta_t*(h7-h10)))/1000);
		result.eq.push_back((Work_Turb8 - (mdot*k*wt8))/10000);
		  // Turbine 9 (main):
		  // isentropic
		result.eq.push_back((p11 - (p1))/1);
		result.eq.push_back((h11satliq - (properties.cif*(T11sat-properties.T0) + properties.vif*1e3*(p11-p0)))/100);
		result.eq.push_back((h11satvap - (properties.deltaH + properties.cpig*(T11sat-properties.T0)))/1000);
		result.eq.push_back((s11satliq - (properties.cif*log(T11sat/properties.T0)))/1);
		result.eq.push_back((s11satvap - (properties.deltaH/properties.T0 + properties.cpig*log(T11sat/properties.T0) - properties.Rm*log(p11/p0)))/10);
		result.eq.push_back((s11 - (s7))/1);
		result.eq.push_back((s11 - (s11satliq+x11*(s11satvap-s11satliq)))/1);
		result.eq.push_back((h11 - (h11satliq + x11*(h11satvap-h11satliq)))/1000);
			// actual
		result.eq.push_back((p9 - (p1))/1);
		result.eq.push_back((h9satliq - (properties.cif*(T9sat-properties.T0) + properties.vif*1e3*(p9-p0)))/100);
		result.eq.push_back((h9satvap - (properties.deltaH + properties.cpig*(T9sat-properties.T0)))/1000);
		result.eq.push_back((h9 - (h7 - wt9))/1000);
		result.eq.push_back((h9 - (h9satliq+x9*(h9satvap-h9satliq)))/1000);
		result.eq.push_back((wt9 - (eta_t*(h7-h11)))/1000);
		result.eq.push_back((Work_Turb9 - (mdot*(1-k)*wt9))/10000);
		// Feedwater line
		// Condensate pump
		result.eq.push_back((h1satliq - (properties.cif*(T1sat-properties.T0) + properties.vif*1e3*(p1-p0)))/100);
		result.eq.push_back((h1 - (h1satliq))/100);
		result.eq.push_back((h2 - (h1 + wp2))/100);
		result.eq.push_back((wp2 - (properties.vif*(p2-p1)*1e3/eta_p))/10);
		result.eq.push_back((Work_Pump2 - (mdot*(1-k)*wp2))/100);
		// Deaerator
		result.eq.push_back((p3 - (p2))/10);
		result.eq.push_back((h3satliq - (properties.cif*(T3sat-properties.T0) + properties.vif*1e3*(p3-p0)))/100);
		result.eq.push_back((h3-(h3satliq))/100);
		result.eq.push_back((h3 - (k*h8 + (1-k)*h2))/100);
		// Feedwater pump
		result.eq.push_back((h4 - (properties.cif*(T4-properties.T0)+properties.vif*1e3*(p4-p0)))/100);
		result.eq.push_back((h4 - (h3 + wp4))/100);
		result.eq.push_back((wp4 - (properties.vif*(p4-p3)*1e3/eta_p))/10);
		result.eq.push_back((Work_Pump4 - (mdot*wp4))/100);
		// Boiler
		// Overall
		result.eq.push_back((Qzu - (mcpG*(TGin-TG4)))/10000);
		result.eq.push_back((mdot*(h7-h4) - (mcpG*(TGin-TG4)))/10000);
		// Superheater
		result.eq.push_back((p7 - (p6))/10);
		result.eq.push_back((h7satvap - (properties.deltaH + properties.cpig*(T7sat-properties.T0)))/1000);
		result.eq.push_back((h7 - (properties.deltaH+properties.cpig*(T7-properties.T0)))/1000);
		result.eq.push_back((s7 - (properties.deltaH/properties.T0 + properties.cpig*log(T7/properties.T0) - properties.Rm*log(p7/p0)))/1);
		result.eq.push_back((mdot*(h7-h6) - (mcpG*(TGin-TG2)))/10000);
		// Evaporator
		result.eq.push_back((p6 - (p5))/10);
		result.eq.push_back((h6satvap - (properties.deltaH + properties.cpig*(T6sat-properties.T0)))/1000);
		result.eq.push_back((h6 - (h6satvap))/1000);
		result.eq.push_back((mdot*(h6-h5) - (mcpG*(TG2-TG3)))/10000);
		// Eco
		result.eq.push_back((p5 - (p4))/10);
		result.eq.push_back((T5 - (T5sat-dTap))/100);
		result.eq.push_back((h5 - (properties.cif*(T5-properties.T0) + properties.vif*1e3*(p5-p0)))/1000);
		// Power
		result.eq.push_back((WorkNet - mdot*(k*wt8+(1-k)*(wt9-wp2)-wp4))/10000);

	// Inequalities
		result.ineq.push_back((h7satvap - h7) / 1e3,"hvap(p7)<=h7");
		result.ineq.push_back((p2 - p4) / 1e0, "p2<=p4");
		result.ineq.push_back((dTmin - (TG3 - T6sat)) / 1e1, "Pinch: Evaporator, cold end");
		result.ineq.push_back((dTmin - (TG4 - T4)) / 1e1, "Pinch: Economizer, cold end");

	// Objective:
		result.objective = -WorkNet;

    return result;
}