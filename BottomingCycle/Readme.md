# BottomingCycle

Flowsheet optimization problems for the design of bottoming cycles for combined cycle power plants using very simple thermodynamic models (ideal gas & ideal liquid with constant heat capacities).
These problems were originally implemented for the following publication:
 - Bongartz, D., & Mitsos, A. (2017). [Deterministic global optimization of process flowsheets in a reduced space using McCormick relaxations](https://link.springer.com/article/10.1007/s10898-017-0547-4). *Journal of Global Optimization*, 69(4), 761-796.

| Instance        | Problem Type | Number of Continuous Variables | Number of Integer Variables | Number of Binary Variables | Number of Equalities | Number of Inequalities | Best Known Objective Value* | Proven Objective Bound* | Description           |
| ------------- |-------------| ------------| ------------| ------------| ------------| ------------| :------------: | :-----: | ----- |
| BottomingCycle_ReducedSpaceWnetBasic**  | DNLP | 2 | 0 | 0 | 0 | 5 | -30039.1920 | -30039.1922 | Maximize net power output of a basic Rankine cycle using a reduced-space optimization formulation |
| BottomingCycle_ReducedSpaceWnetRegenerative**  | DNLP | 5 | 0 | 0 | 1 | 7 | -34367.5752 | -34367.5753 | Maximize net power output of a regenerative Rankine cycle using a reduced-space optimization formulation |
| BottomingCycle_ReducedSpaceWnetTwoPressure** | DNLP | 8 | 0 | 0 | 1 | 15 | -39349.735 | -39349.738 | Maximize net power output of a two-pressure cycle using a reduced-space optimization formulation |
| BottomingCycle_ReducedSpaceLCOEBasic**,*** |  DNLP | 2 | 0 | 0 | 0 | 9 | 50.187946 | 50.187941 | Minimize levelized cost of electricity of a basic Rankine cycle using a reduced-space optimization formulation |
| BottomingCycle_ReducedSpaceLCOERegenerative**,***   | DNLP | 5 | 0 | 0 | 1 | 12 | 48.819977 | 48.819972 |Minimize levelized cost of electricity of a regenerative Rankine cycle using a reduced-space optimization formulation |
| BottomingCycle_ReducedSpaceLCOETwoPressure**,***  | DNLP | 8 | 0 | 0 | 1 |29 | 49.51 |  46.85 | Minimize levelized cost of electricity of a two-pressure cycle using a reduced-space optimization formulation |
| BottomingCycle_FullSpaceWnetBasic | NLP | 44 | 0 | 0 | 42 | 2 | -390039.70 | -390039.71 | Maximize net power output of basic a Rankine cycle using a full-space optimization formulation |
| BottomingCycle_FullSpaceWnetRegenerative | NLP | 73 | 0 | 0 | 69 | 4 | -34367.4 | -34367.6 | Maximize net power output of a regenerative Rankine cycle using a full-space optimization formulation |
| BottomingCycle_FullSpaceLCOEBasic*** | NLP | 84 | 0 | 0 | 82 | 2 |  50.188 | 50.183 | Minimize levelized cost of electricity of basic a Rankine cycle using a full-space optimization formulation |

*The best known objective values and proven bounds refer to the MAiNGO version of the problems. The optimal objective of the GAMS version may differ slightly because of the approximation of the logarithmic mean temperature difference that is used in these models (see below).

**The reduced-space instances contain the binary function $`\max(x,y)`$ to avoid domain violations in the reduced space formulations (in conjunction with suitable constriants). Since these functions are not available in many deterministic global solvers, we also provide the GAMS files with suffix *_maxAsAbs*. In these files, the max function is reformulated as $`\max(x,y)=0.5 \cdot (x + y + |x-y|)`$.

***The instances minimizing LCOE contain the [logarithmic mean temperature difference](https://en.wikipedia.org/wiki/Logarithmic_mean_temperature_difference) $`\text{LMTD}(x,y):=\frac{x-y}{\ln(x/y)}`$ or its reciprocal $`\text{rLMTD}(x,y):=1/\text{LMTD}(x,y)`$. Since the formulas for computing $`\text{LMTD}(x,y)`$ or $`\text{rLMTD}(x,y)`$ result in a division by zero if $`x=y`$, they cannot be used directly in an optimization problem.
    In MAiNGO, $`\text{LMTD}(x,y)`$ and $`\text{rLMTD}(x,y)`$ are  implemented as intrinsic functions using the smooth piecewise defined versions and associated relaxations proposed by [Mistry & Misener](https://www.sciencedirect.com/science/article/pii/S0098135416302216) and analyzed by [Najman & Mitsos](https://www.sciencedirect.com/science/article/pii/B9780444634283502721). These versions are identical to the originnal ones if $`x\ne y`$ but set $`\text{LMTD}(x,x):=x`$ and $`\text{rLMTD}(x,x):=1/x`$, thus removing the singularities in the original formulas.
    In GAMS, [Chen's approximation](https://pascal-francis.inist.fr/vibad/index.php?action=getRecordDetail&idt=7451408) $`\text{LMTD}(x,y)\approx \left(x\cdot y\cdot \frac{x+y}{2}\right)^{1/3}`$ is used instead.

