/**
 *
 * @brief Reduced-space formulation of Case Study I (Basic Rankine Cycle)
 *        that was originally published in
 *        D. Bongartz, A. Mitsos: "Deterministic global optimization of process
 *             flowsheets in a reduced space using McCormick relaxations",
 *             Journal of Global Optimization 69 (2017), 761-796.
 *             https://link.springer.com/article/10.1007/s10898-017-0547-4
 *        E-mail: amitsos@alum.mit.edu
 *
 * ==============================================================================
 * © 2020, Process Systems Engineering (AVT.SVT), RWTH Aachen University
 * ==============================================================================
 *
 * used in:
 *    D. Bongartz: "Deterministic Global Flowsheet Optimization for the Design of Energy Conversion Processes",
 *     	            PhD Thesis, RWTH Aachen University, 2020.
 *
 */

#pragma once

#include "MAiNGOmodel.h"
#include "../Thermo/IdealFluidModel.h"
#include "../Thermo/IdealFluidProperties.h"



//////////////////////////////////////////////////////////////////////////
// specialization of the abstract MAiNGOmodel class to be handed to MAiNGO
class Model: public maingo::MAiNGOmodel {

  public:
    Model();

    maingo::EvaluationContainer evaluate(const std::vector<Var>& optVars);
    std::vector<maingo::OptimizationVariable> get_variables();

  private:
    const double eta_p, eta_t, mcpG, TGin, TGout, Tmax, dTap, dTmin, xmin, Amin, Vmin;
    const double kEco, kEvap, kSH, kCond, k1A, k2A, k3A, p1A, p2A, p3A, FmA, B1A, B2A, Tcin, Tcout;
    const double InvGT, WorkGT, FuelHeat, FuelPrice, fPhi, fAnnu, Teq, Varcost;
	double c1A,c2A,c3A;

    const thermo::IdealFluidModel thermoModel;
};


//////////////////////////////////////////////////////////////////////////
// function for providing optimization variable data to the Branch-and-Bound solver
std::vector<maingo::OptimizationVariable>
Model::get_variables()
{

    std::vector<maingo::OptimizationVariable> variables;
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.1*3, 0.1*100), "p2 [MPa]"   ));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(5, 100),         "mdot [kg/s]"));

    return variables;
}


//////////////////////////////////////////////////////////////////////////
// constructor for the model
Model::Model():
    eta_p(0.8), eta_t(0.9), mcpG(200), TGin(900), TGout(448), Tmax(873.15), dTap(10), dTmin(15), xmin(0.85), Amin(10), Vmin(1),
    kEco(0.06), kEvap(0.06), kSH(0.03), kCond(0.35), k1A(4.3247), k2A(-0.303), k3A(0.1634),
    p1A(0.03881), p2A(-0.11272), p3A(0.08183), FmA(2.75), B1A(1.63), B2A(1.66), Tcin(273.15+20), Tcout(273.15+25),
    InvGT(22.7176e6), WorkGT(69676), FuelHeat(182359), FuelPrice(14), fPhi(1.06), fAnnu(0.1875), Teq(4000), Varcost(4),
    thermoModel(thermo::IdealFluidProperties("Water", 2.08, 4.18, 0.462, 0.001, 3.55959-1., 643.748, -198.043, 2480., 313.8336))
{
	// Convert coefficients for pressure factor from bar to MPa:
	c1A = p1A+p2A+p3A;
	c2A = p2A+2*p3A;
	c3A = p3A;
}


//////////////////////////////////////////////////////////////////////////
// evaluate the model
maingo::EvaluationContainer
Model::evaluate(const std::vector<Var>& optVars)
{

    // Rename inputs, these are the only real optimization variables
    Var p2     = optVars[0];
    Var mdot   = optVars[1];

    // Model in a reduced-space formulation
    maingo::EvaluationContainer result;

	// Feedwater line
    // Condenser
    Var p1    = 0.1*0.2;    // Fixed condenser pressure (converted to MPa)
    Var T1    = thermoModel.get_Ts_p(p1);
    Var h1    = thermoModel.get_hliq_p(p1);
    Var s1    = thermoModel.get_sliq_p(p1);
    // Feedwater pump
    Var wpFeed       = 1e3 * 1e-3 * (p2 - p1) / eta_p;
    Var WorkPumpFeed = mdot * wpFeed;
    Var h2           = h1 + wpFeed;
    Var T2 = thermoModel.get_T_liq_ph(p2, h2);
    // Boiler
	// Overall balance
	Var Qzu = mcpG*(TGin-TGout);
	Var p5 = p2;
	Var h5 = h2 + Qzu/mdot;
	Var T5 = thermoModel.get_T_vap_ph(p5,h5);
    Var s5 = thermoModel.get_s_vap_ph(p5, h5);
    result.ineq.push_back( (thermoModel.get_hvap_p(p5) - h5) / 1e3,"hvap(p5)<=h5");
    result.ineq.push_back((T5 - Tmax) / 1e2, "T5<=Tmax");
    // Superheater
    Var p4  = p2;
    Var T4  = thermoModel.get_Ts_p(p4);
    Var h4  = thermoModel.get_hvap_p(p4);
    Var QSH = mdot * (h5 - h4);
    Var TG2 = TGin - QSH / mcpG;
    // Economizer & Evaporator
    Var p3    = p2;
    Var T3    = thermoModel.get_Ts_p(p3) - dTap;
    Var h3    = thermoModel.get_h_liq_pT(p3, T3);
    Var QEco  = mdot * (h3 - h2);
	Var TG3 = TGout + QEco / mcpG;
    Var QEvap = Qzu - QSH - QEco;
    result.ineq.push_back((dTmin - (TG3 - T4)) / 1e1, "Pinch: Evaporator, cold end");
    // Turbines:
    Var p6    = p1;
    Var s6iso = s5;
    Var h6iso        = thermoModel.get_h_twophase_ps(p6, s6iso);
    Var wtMain       = eta_t * (h5 - h6iso);
    Var WorkTurbMain = mdot * wtMain;
    Var h6           = h5 * (1. - eta_t) + eta_t * h6iso;
    Var T6           = thermoModel.get_Ts_p(p6);
    Var x6           = thermoModel.get_x_ph(p6, h6);
    Var h6liq        = thermoModel.get_hliq_p(p6);
    Var h6vap        = thermoModel.get_hvap_p(p6);
    result.ineq.push_back((h6-h6vap) / 1e3, "h6<=hvap(p6)");
    result.ineq.push_back((xmin * h6vap - h6 + h6liq * (1 - xmin)) / 1e1, "xmin<=x6");
	// Condenser
	Var QCond = mdot * (h6 - h1);
    // Overall
    Var WorkNet = mdot * (wtMain - wpFeed);
    Var eta     = WorkNet / Qzu;

    // Combined Cycle Power Plant
    Var WorkTotal = WorkNet + WorkGT;
    WorkTotal = max(WorkTotal, 1e-3);
    Var etaCC = WorkTotal / FuelHeat;

	// Objective:
	result.objective = -WorkNet;

    return result;
}