/**
 *
 * @brief Reduced-space formulation of Case Study II (Regenerative Rankine Cycle)
 *        that was originally published in
 *        D. Bongartz, A. Mitsos: "Deterministic global optimization of process
 *             flowsheets in a reduced space using McCormick relaxations",
 *             Journal of Global Optimization 69 (2017), 761-796.
 *             https://link.springer.com/article/10.1007/s10898-017-0547-4
 *        E-mail: amitsos@alum.mit.edu
 *
 * ==============================================================================
 * © 2020, Process Systems Engineering (AVT.SVT), RWTH Aachen University
 * ==============================================================================
 *
 * used in:
 *    D. Bongartz: "Deterministic Global Flowsheet Optimization for the Design of Energy Conversion Processes",
 *     	            PhD Thesis, RWTH Aachen University, 2020.
 *
 */

#pragma once

#include "MAiNGOmodel.h"
#include "../Thermo/IdealFluidModel.h"
#include "../Thermo/IdealFluidProperties.h"


class Model: public maingo::MAiNGOmodel {

  public:

    Model();

    maingo::EvaluationContainer evaluate(const std::vector<Var>& optVars);
    std::vector<maingo::OptimizationVariable> get_variables();

  private:

    const double eta_p, eta_t, mcpG, TGin, TGout, Tmax, dTap, dTmin, xmin, Amin, Vmin;
    const double kEco, kEvap, kSH, kCond, k1A, k2A, k3A, p1A, p2A, p3A, FmA, B1A, B2A, k1B, k2B, k3B, FmB, B1B, B2B, Tcin, Tcout;
    const double InvGT, WorkGT, FuelHeat, FuelPrice, fPhi, fAnnu, Teq, Varcost;
	double c1A,c2A,c3A;

    const thermo::IdealFluidModel thermoModel;
};


//////////////////////////////////////////////////////////////////////////
// function for providing optimization variable data to the Branch-and-Bound solver
std::vector<maingo::OptimizationVariable>
Model::get_variables()
{

    std::vector<maingo::OptimizationVariable> variables;
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.1*0.2, 0.1*5), "p2 [MPa]"   ));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.1*3, 0.1*100), "p4 [MPa]"   ));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(5, 100),         "mdot [kg/s]"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(2480, 3750),     "h7 [kJ/kg]"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.01, 0.2),      "k [-]"      ));

    return variables;
}


//////////////////////////////////////////////////////////////////////////
// constructor for the model
Model::Model():
    eta_p(0.8), eta_t(0.9), mcpG(200), TGin(900), TGout(448), Tmax(873.15), dTap(10), dTmin(15), xmin(0.85), Amin(10), Vmin(1),
    kEco(0.06), kEvap(0.06), kSH(0.03), kCond(0.35), k1A(4.3247), k2A(-0.303), k3A(0.1634),
    p1A(0.03881), p2A(-0.11272), p3A(0.08183), FmA(2.75), B1A(1.63), B2A(1.66), k1B(3.5565), k2B(0.3776), k3B(0.0905),
    FmB(1), B1B(1.49), B2B(1.52), Tcin(273.15+20), Tcout(273.15+25),
    InvGT(22.7176e6), WorkGT(69676), FuelHeat(182359), FuelPrice(14), fPhi(1.06), fAnnu(0.1875), Teq(4000), Varcost(4),
    thermoModel(thermo::IdealFluidProperties("Water", 2.08, 4.18, 0.462, 0.001, 3.55959-1., 643.748, -198.043, 2480., 313.8336))
{
	// Convert coefficients for pressure factor from bar to MPa:
	c1A = p1A+p2A+p3A;
	c2A = p2A+2*p3A;
	c3A = p3A;
}


//////////////////////////////////////////////////////////////////////////
// evaluate the model
maingo::EvaluationContainer
Model::evaluate(const std::vector<Var>& optVars)
{
    // Rename inputs, these are the only real optimization variables
    Var p2     = optVars[0];
    Var p4     = optVars[1];
    Var mdot   = optVars[2];
    Var h7     = optVars[3];
    Var k      = optVars[4];
    Var mBleed = mdot * k;
    Var mMain  = mdot * (1. - k);

    // Model in a reduced-space formulation
    maingo::EvaluationContainer result;
    // Turbines:
    // Inlet:
    Var p7 = p4;
    result.ineq.push_back( (thermoModel.get_hvap_p(p7) - h7) / 1e3,"hvap(p7)<=h7");
    Var T7 = thermoModel.get_T_vap_ph(p7, h7);
    result.ineq.push_back((T7 - Tmax) / 1e2, "T7<=Tmax");
    Var s7 = thermoModel.get_s_vap_ph(p7, h7);
    // Turbine 1 (bleed):
    Var p8    = p2;
    Var s8iso = s7;
    Var h8iso         = thermoModel.get_h_twophase_ps(p8, s8iso);
    Var wtBleed       = eta_t * (h7 - h8iso);
    Var WorkTurbBleed = mBleed * wtBleed;
    Var h8            = h7 * (1. - eta_t) + eta_t * h8iso;
    Var x8 = thermoModel.get_x_ph(p8, h8);
    Var T8 = thermoModel.get_Ts_p(p8);
    result.ineq.push_back((h8 - thermoModel.get_hvap_p(p8)) / 1e3, "x8<=1");
    // Turbine 2 (main):
    Var p1    = 0.1*0.05;    // Fixed condenser pressure (converted to MPa)
    Var p9    = p1;
    Var s9iso = s7;
    Var h9iso        = thermoModel.get_h_twophase_ps(p9, s9iso);
    Var wtMain       = eta_t * (h7 - h9iso);
    Var WorkTurbMain = mMain * wtMain;
    Var h9           = h7 * (1. - eta_t) + eta_t * h9iso;
    Var T9    = thermoModel.get_Ts_p(p9);
    Var x9    = thermoModel.get_x_ph(p9, h9);
    Var h9liq = thermoModel.get_hliq_p(p9);
    Var h9vap = thermoModel.get_hvap_p(p9);
    result.ineq.push_back((xmin * h9vap - h9 + h9liq * (1 - xmin)) / 1e1, "xmin<=x9");
    // Feedwater line
    // Condenser
    Var T1    = thermoModel.get_Ts_p(p1);
    Var h1    = thermoModel.get_hliq_p(p1);
    Var s1    = thermoModel.get_sliq_p(p1);
    Var QCond = mMain * (h9 - h1);
    // Condensate pump
    Var wpCond       = 1e3 * 1e-3 * (p2 - p1) / eta_p;	// Shortcut for ideal model
    Var WorkPumpCond = mMain * wpCond;
    Var h2           = h1 + wpCond;
    Var T2           = thermoModel.get_T_liq_ph(p2, h2);
    // Deaerator
    Var p3 = p2;
    Var h3 = k*h8 + (1-k)*h2;
    result.eq.push_back((h3 - thermoModel.get_hliq_p(p3)) / 1e2, "h3=hliq(p3)");
    Var s3 = thermoModel.get_sliq_p(p3);
    Var T3 = thermoModel.get_Ts_p(p3);
    // Feedwater pump
    result.ineq.push_back((p2 - p4) / 1e0, "p2<=p4");
    Var wpFeed       = 1e3 * 1e-3 * (p4 - p3) / eta_p;
    Var WorkPumpFeed = mdot * wpFeed;
    Var h4           = h3 + wpFeed;
    Var T4 = thermoModel.get_T_liq_ph(p4, h4);
    // Boiler
    // Superheater
    Var p6  = p4;
    Var T6  = thermoModel.get_Ts_p(p6);
    Var h6  = thermoModel.get_hvap_p(p6);
    Var QSH = mdot * (h7 - h6);
    Var TG2 = TGin - QSH / mcpG;
    // Evaporator
    Var p5    = p4;
    Var T5    = thermoModel.get_Ts_p(p5) - dTap;
    Var h5    = thermoModel.get_h_liq_pT(p5, T5);
    Var QEvap = mdot * (h6 - h5);
    Var TG3   = TGin - mdot * (h7 - h5) / mcpG;
    result.ineq.push_back((dTmin - (TG3 - T6)) / 1e1, "Pinch: Evaporator, cold end");
    // Eco
    Var QEco  = mdot * (h5 - h4);
    Var TGout = TGin - mdot * (h7 - h4) / mcpG;
    result.ineq.push_back((dTmin - (TGout - T4)) / 1e1, "Pinch: Economizer, cold end");
    // Overall
    Var WorkNet = mdot * ( k*(wtBleed) + (1.-k)*(wtMain - wpCond) - wpFeed);
    Var Qzu     = mdot * (h7 - h4);
    Var eta     = WorkNet / Qzu;

    // Combined Cycle Power Plant
    Var WorkTotal = WorkNet + WorkGT;
    WorkTotal = max(WorkTotal, 1e-3);
    Var etaCC = WorkTotal / FuelHeat;

	// Investment cost
	// Condenser
	Var dT1   = T9 - Tcout;
	Var dT2   = T1 - Tcin;
	Var ACond = rlmtd(dT1,dT2)*QCond / (kCond);
	result.ineq.push_back((Amin - ACond) / 1e1, "Amin<=ACond");
	ACond       = max(Amin, ACond);
	Var Cp0     = cost_function(ACond,1,k1A,k2A,k3A);
	Var Fp      = 1.0;
	Var InvCond = 1.18 * (B1A + B2A * FmA * Fp) * Cp0;
	// Pumps
	Var InvPumpCond = 3540. * pow(max(WorkPumpCond, 1e-3), 0.71);
	Var InvPumpFeed = 3540. * pow(max(WorkPumpFeed, 1e-3), 0.71);
	// Turbines incl. generator
	Var InvTurb = 6000. * pow(max(WorkTurbBleed + WorkTurbMain, 1e-3), 0.7) + 60. * pow(max(WorkTurbBleed + WorkTurbMain, 1e-3), 0.95);
	// Economizer
	dT1      = TGout - T4;
	dT2      = TG3 - T5;
	dT1      = max(dTmin, dT1);
	dT2      = max(dTmin, dT2);
	Var AEco = rlmtd(dT1,dT2)*QEco / (kEco);
	result.ineq.push_back((Amin - AEco) / 1e1, "Amin<=AEvo");
	AEco       = max(Amin, AEco);
	Cp0     = cost_function(AEco,1,k1A,k2A,k3A);
	Fp      = cost_function(p4, 1, c1A, c2A, c3A);
	Var InvEco = 1.18 * (B1A + B2A * FmA * Fp) * Cp0;
	// Evaporator
	dT1       = TG3 - T6;
	dT2       = TG2 - T6;
	dT1       = max(dTmin, dT1);
	dT2       = max(dTmin, dT2);
	Var AEvap = rlmtd(dT1,dT2)*QEvap / (kEvap);
	result.ineq.push_back((Amin - AEvap) / 1e1, "Amin<=AEvap");
	AEvap       = max(Amin, AEvap);
	Cp0         = cost_function(AEvap,1,k1A,k2A,k3A);
	Var InvEvap = 1.18 * (B1A + B2A * FmA * Fp) * Cp0;
	// Superheater
	dT1     = dT2;
	dT2     = TGin - T7;
	dT1     = max(dTmin, dT1);
	dT2     = max(dTmin, dT2);
	Var ASH = rlmtd(dT1,dT2)*QSH / (kSH);
	result.ineq.push_back((Amin - ASH) / 1e1, "Amin<=ASH");
	ASH       = max(Amin, ASH);
	Cp0         = cost_function(ASH,1,k1A,k2A,k3A);
	Var InvSH = 1.18 * (B1A + B2A * FmA * Fp) * Cp0;
	// Deaerator
	Var VDae = 1.5 * 600 * mdot * 0.001;
	result.ineq.push_back((Vmin - VDae) / 1e0, "Vmin");
	Var Cp0DAE = cost_function(VDae,1,k1B,k2B,k3B);
	Var FpDAE  = 1.25;
	Var InvDae = 1.18 * (B1B + B2B * FmB * FpDAE) * Cp0DAE;
	// Cycle
	Var Inv = InvCond + InvPumpCond + InvPumpFeed + InvEco + InvEvap + InvSH + InvTurb + InvDae;

	// Combined Cycle Plant
	Var InvTotal = Inv + InvGT;
	Var LCOE     = (InvTotal * fPhi * fAnnu * 1000 / Teq + FuelPrice * FuelHeat) / WorkTotal + Varcost;

	// Objective:
	result.objective = LCOE;

    return result;
}