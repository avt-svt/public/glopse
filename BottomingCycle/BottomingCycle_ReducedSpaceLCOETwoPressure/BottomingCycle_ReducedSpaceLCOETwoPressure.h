/**
 *
 * @brief Reduced-space formulation of Case Study III (Two-Pressure Cycle) for
 *        that was originally published in
 *        D. Bongartz, A. Mitsos: "Deterministic global optimization of process
 *             flowsheets in a reduced space using McCormick relaxations",
 *             Journal of Global Optimization 69 (2017), 761-796.
 *             https://link.springer.com/article/10.1007/s10898-017-0547-4
 *        E-mail: amitsos@alum.mit.edu
 *
 * ==============================================================================
 * © 2020, Process Systems Engineering (AVT.SVT), RWTH Aachen University
 * ==============================================================================
 *
 * used in:
 *    D. Bongartz: "Deterministic Global Flowsheet Optimization for the Design of Energy Conversion Processes",
 *     	            PhD Thesis, RWTH Aachen University, 2020.
 *
 */

#pragma once

#include "MAiNGOmodel.h"
#include "../Thermo/IdealFluidModel.h"
#include "../Thermo/IdealFluidProperties.h"



class Model: public maingo::MAiNGOmodel {

  public:

    Model();

    maingo::EvaluationContainer evaluate(const std::vector<Var>& optVars);
    std::vector<maingo::OptimizationVariable> get_variables();

  private:

    const double eta_p, eta_t, mcpG, TGin, TGout, Tmax, dTap, dTmin, xmin, Amin, Vmin;
    const double kEco, kEvap, kSH, kCond, k1A, k2A, k3A, p1A, p2A, p3A, FmA, B1A, B2A, k1B, k2B, k3B, FmB, B1B, B2B, Tcin, Tcout;
    const double InvGT, WorkGT, FuelHeat, FuelPrice, fPhi, fAnnu, Teq, Varcost;
	double c1A,c2A,c3A;

    const thermo::IdealFluidModel thermoModel;
};


//////////////////////////////////////////////////////////////////////////
// function for providing optimization variable data to the Branch-and-Bound solver
std::vector<maingo::OptimizationVariable>
Model::get_variables()
{

    std::vector<maingo::OptimizationVariable> variables;
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.1*0.2, 0.1*3),  "p2 [MPa]"   ));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.1*3, 0.1*15),   "p4 [MPa]"   ));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.1*10, 0.1*100), "p8 [MPa]"   ));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(5, 100),          "mdot [kg/s]"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(2480, 3750),      "h7 [kJ/kg]"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(2480, 3750),      "h11 [kJ/kg]"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.05, 0.5),       "kLP [-]"    ));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.01, 0.2),       "kBl [-]"    ));

    return variables;
}


//////////////////////////////////////////////////////////////////////////
// constructor for the model
Model::Model():
    eta_p(0.8), eta_t(0.9), mcpG(200), TGin(900), TGout(448), Tmax(873.15), dTap(10), dTmin(15), xmin(0.85), Amin(10), Vmin(1),
    kEco(0.06), kEvap(0.06), kSH(0.03), kCond(0.35), k1A(4.3247), k2A(-0.303), k3A(0.1634),
    p1A(0.03881), p2A(-0.11272), p3A(0.08183), FmA(2.75), B1A(1.63), B2A(1.66), k1B(3.5565), k2B(0.3776), k3B(0.0905),
    FmB(1), B1B(1.49), B2B(1.52), Tcin(273.15+20), Tcout(273.15+25),
    InvGT(22.7176e6), WorkGT(69676), FuelHeat(182359), FuelPrice(14), fPhi(1.06), fAnnu(0.1875), Teq(4000), Varcost(4),
    thermoModel(thermo::IdealFluidProperties("Water", 2.08, 4.18, 0.462, 0.001, 3.55959-1., 643.748, -198.043, 2480., 313.8336))
{
	// Convert coefficients for pressure factor from bar to MPa:
	c1A = p1A+p2A+p3A;
	c2A = p2A+2*p3A;
	c3A = p3A;
}


//////////////////////////////////////////////////////////////////////////
// evaluate the model
maingo::EvaluationContainer
Model::evaluate(const std::vector<Var>& optVars)
{
    // Rename inputs, these are the only real optimization variables
    Var p2     = optVars[0];
    Var p4     = optVars[1];
    Var p8     = optVars[2];
    Var mdot   = optVars[3];
    Var h7     = optVars[4];
    Var h11    = optVars[5];
    Var kLP    = optVars[6];
    Var kBl    = optVars[7];

    maingo::EvaluationContainer result;
    // Model in a reduced-space formulation
	Var mLP = mdot*kLP;
	Var mHP = mdot*(1.-kLP);
    Var mBleed = mdot * kBl;
    Var mMain  = mdot * (1. - kBl);
    // HP Turbine
		// HP superheater outlet
		Var p11 = p8;
		Var T11 = thermoModel.get_T_vap_ph(p11,h11);
		Var s11 = thermoModel.get_s_vap_ph(p11,h11);
		result.ineq.push_back((thermoModel.get_hvap_p(p11)-h11)/1e3,"hvap(p11)<=h11");
		result.ineq.push_back((T11-Tmax)/1e2,"T11<=Tmax");
		// Turbine
		Var p12 = p4;
		Var s12iso = s11;
		Var h12iso = thermoModel.get_h_vap_ps(p12,s12iso);
		result.ineq.push_back((thermoModel.get_hvap_p(p12)-h12iso)/1e0,"hvap(p12)<=h12iso");
		Var wtHP = eta_t*(h11-h12iso);
		Var WorkTurbHP = mHP*wtHP;
		Var h12 = h11*(1-eta_t) + eta_t*h12iso;
	// LP Turbine
		// LP superheater outlet
		Var p7 = p4;
		Var T7 = thermoModel.get_T_vap_ph(p7,h7);
		result.ineq.push_back((thermoModel.get_hvap_p(p7)-h7)/1e3,"hvap(p7)<=h7");
		result.ineq.push_back((T7-Tmax)/1e2,"T7<=Tmax");
		// Mixer
		Var p13 = p7;
		Var h13 = kLP*h7+(1.-kLP)*h12;
		Var s13 = thermoModel.get_s_vap_ph(p13,h13);
		Var T13 = thermoModel.get_T_vap_ph(p13,h13);
		// Bleed
		Var p14 = p2;
		Var s14iso = s13;
		Var h14iso = thermoModel.get_h_twophase_ps(p14,s14iso);
		Var wtBleed = eta_t*(h13-h14iso);
		Var WorkTurbBleed = mBleed*wtBleed;
		Var h14 = h13*(1-eta_t) + eta_t*h14iso;
		Var x14 = thermoModel.get_x_ph(p14,h14);
		Var T14 = thermoModel.get_Ts_p(p14);
		result.ineq.push_back((h14-thermoModel.get_hvap_p(p14))/1e3,"h14<=hvap(p14)");
		// Main
		Var p1 = 0.1*0.05;  // Convert to MPa
		Var p15 = p1;
		Var s15iso = s13;
		Var h15iso = thermoModel.get_h_twophase_ps(p15,s15iso);
		Var wtMain = eta_t*(h13-h15iso);
		Var WorkTurbMain = mMain*wtMain;
		Var h15 = h13*(1-eta_t) +  eta_t*h15iso;
		Var T15 = thermoModel.get_Ts_p(p15);
		Var x15 = thermoModel.get_x_ph(p15,h15);
		Var hliq15 = thermoModel.get_hliq_p(p15);
		Var hvap15 = thermoModel.get_hvap_p(p15);
		result.ineq.push_back((xmin*hvap15+hliq15*(1.-xmin)-h15)/1e2,"xmin<=x15");
	// Feedwater line
	// Condenser
		Var h1 = thermoModel.get_hliq_p(p1);
		Var T1 = thermoModel.get_Ts_p(p1);
		Var QCond = mMain*(h15-h1);
	// Condensate pump
		Var wpCond = 1e3 * 1e-3 * (p2 - p1) / eta_p;	// Shortcut for ideal model
		Var WorkPumpCond = mMain*wpCond;
		Var h2 = h1 + wpCond;
		Var T2 = thermoModel.get_T_liq_ph(p2,h2);
    // Deaerator
		Var p3 = p2;
		Var h3 = kBl*h14 + (1.-kBl)*h2;
		result.eq.push_back((h3 - thermoModel.get_hliq_p(p3)) / 1e2, "h3=hliq(p3)");
		Var s3 = thermoModel.get_sliq_p(p3);
		Var T3 = thermoModel.get_Ts_p(p3);
	// LP pump
		result.ineq.push_back((p3-p4)/1e0,"p3<=p4");
		Var wpLP = 1e3 * 1e-3 * (p4 - p3) / eta_p;	// Shortcut for ideal model
		Var WorkPumpLP = mdot*wpLP;
		Var h4 = h3 + wpLP;
		Var T4 = thermoModel.get_T_liq_ph(p4,h4);
	// LP Preheater (Part 1)
		Var p5 = p4;
		Var T5 = thermoModel.get_Ts_p(p5) - dTap;
		Var h5 = thermoModel.get_h_liq_pT(p5,T5);
	// HP Pump
		result.ineq.push_back((p5-p8)/1e0,"p5<=p8");
		Var wpHP = 1e3 * 1e-3 * (p8 - p5) / eta_p;	// Shortcut for ideal model
		Var WorkPumpHP = mHP*wpHP;
		Var h8 = h5 + wpHP;
		Var T8 = thermoModel.get_T_liq_ph(p8,h8);

	// HP Superheater
		Var p10 = p8;
		Var h10 = thermoModel.get_hvap_p(p10);
		Var T10 = thermoModel.get_Ts_p(p10);
		Var QHPSH = mHP*(h11-h10);
		Var TG2 = TGin - QHPSH/mcpG;
	// HP Evaporator
		Var p9 = p8;
		Var T9 = thermoModel.get_Ts_p(p9) - dTap;
		Var h9 = thermoModel.get_h_liq_pT(p9,T9);
		Var QHPEvap = mHP*(h10-h9);
		Var TG3 = TG2 - QHPEvap/mcpG;
		result.ineq.push_back((dTmin - (TG3-T10))/1e1,"TG3-T10>=dTmin");
	// LP Superheater
		result.ineq.push_back((dTmin - (TG3-T7))/1e1,"TG3-T7>=dTmin");
		Var p6 = p5;
		Var h6 = thermoModel.get_hvap_p(p6);
		Var T6 = thermoModel.get_Ts_p(p6);
		Var QLPSH = mLP*(h7-h6);
		Var TG4 = TG3 - QLPSH/mcpG;
	// HP Preheater
		result.ineq.push_back((dTmin - (TG4-T9))/1e1,"TG4-T9>=dTmin");
		Var QHPPre = mHP*(h9-h8);
		Var TG5 = TG4 - QHPPre/mcpG;
	// LP Evaporator
		Var QLPEvap = mLP*(h6-h5);
		Var TG6 = TG5 - QLPEvap/mcpG;
		result.ineq.push_back((dTmin - (TG6-T6))/1e1,"TG6-T6>=dTmin");
	// LP Preheater (Part 2)
		Var QLPPre = mdot*(h5-h4);
		Var TG7 = TG6 - QLPPre/mcpG;
		result.ineq.push_back((dTmin - (TG7-T4))/1e1,"TG7-T4>=dTmin");
	// Overall
		Var WorkNet = mdot * ((1-kLP)*(wtHP-wpHP) + kBl*wtBleed + (1-kBl)*(wtMain-wpCond) - wpLP);
		Var Qzu = QLPPre + QLPEvap + QLPSH + QHPPre + QHPEvap + QHPSH;
		Var eta = WorkNet/Qzu;

	// CCPP
		Var WorkTotal = WorkNet + WorkGT;
		result.ineq.push_back((WorkGT - WorkTotal)/1e5,"WorkGT<=WorkTotal");
		WorkTotal = max(WorkTotal,WorkGT);
		Var etaCC = WorkTotal / FuelHeat;

	// Investment cost
		// Condenser
			Var dT1 = T15-Tcout;
			Var dT2 = T1-Tcin;
			dT1 = max(5.,dT1);
			dT2 = max(5.,dT2);
			Var ACond = rlmtd(dT1,dT2)*QCond / (kCond);
			result.ineq.push_back((Amin - ACond) / 1e1, "Amin<=ACond");
			ACond       = max(Amin, ACond);
			Var Cp0     = cost_function(ACond,1,k1A,k2A,k3A);
			Var Fp = 1.0;
			Var InvCond = 1.18 * (B1A+B2A*FmA*Fp) * Cp0;
		// Pumps
			result.ineq.push_back((1e-3-WorkPumpCond)/1e3,"0<=WpCond" );
			result.ineq.push_back((1e-3-WorkPumpLP)/1e3,"0<=WorkPumpLP" );
			result.ineq.push_back((1e-3-WorkPumpHP)/1e3,"0<=WorkPumpHP" );
			Var InvPumpCond = 3540.*pow(max(WorkPumpCond,1e-3),0.71);
			Var InvPumpLP = 3540.*pow(max(WorkPumpLP,1e-3),0.71);
			Var InvPumpHP = 3540.*pow(max(WorkPumpHP,1e-3),0.71);
		// Turbines incl. generator
			result.ineq.push_back((1e-3-WorkTurbHP)/1e5,"0<=WtHP");
			result.ineq.push_back((1e-3-WorkTurbBleed)/1e5,"0<=WtBleed");
			result.ineq.push_back((1e-3-WorkTurbMain)/1e5,"0<=WtBleed");
			Var InvTurb = 6000.*pow(max(WorkTurbHP,1e-3),0.7) + 6000.*pow(max(WorkTurbBleed+WorkTurbMain,2e-3),0.7) + 60.*pow(max(WorkTurbBleed+WorkTurbMain+WorkTurbHP,3e-3),0.95);
		// LP Economizer
			dT1 = TG7 - T4;
			dT2 = TG6 - T5;
			dT1 = max(dTmin,dT1);
			dT2 = max(dTmin,dT2);
			Var AEcoLP = rlmtd(dT1,dT2)*QLPPre / (kEco);
			result.ineq.push_back((Amin - AEcoLP) / 1e1, "Amin<=AEcoLP");
			AEcoLP = max(Amin, AEcoLP);
			Cp0 = cost_function(AEcoLP,1,k1A,k2A,k3A);
			Fp = cost_function(p4,1,c1A,c2A,c3A);
			Var InvEcoLP = 1.18 * (B1A+B2A*FmA*Fp) * Cp0;
		// LP Evaporator
			dT1 = TG6 - T6;
			dT2 = TG5 - T6;
			dT1 = max(dTmin,dT1);
			dT2 = max(dTmin,dT2);
			Var AEvapLP = rlmtd(dT1,dT2)*QLPEvap / (kEvap);
			result.ineq.push_back((Amin - AEvapLP) / 1e1, "Amin<=AEvapLP");
			AEvapLP       = max(Amin, AEvapLP);
			Cp0         = cost_function(AEvapLP,1,k1A,k2A,k3A);
			Var InvEvapLP = 1.18 * (B1A+B2A*FmA*Fp) * Cp0;
		// LP Superheater
			dT1 = TG4 - T6;
			dT2 = TG3 - T7;
			dT1 = max(dTmin,dT1);
			dT2 = max(dTmin,dT2);
			Var ASHLP = rlmtd(dT1,dT2)*QLPSH / (kSH);
			result.ineq.push_back((Amin - ASHLP) / 1e1, "Amin<=ASHLP");
			ASHLP       = max(Amin, ASHLP);
			Cp0         = cost_function(ASHLP,1,k1A,k2A,k3A);
			Var InvSHLP = 1.18 * (B1A+B2A*FmA*Fp) * Cp0;
		// HP Economizer
			dT1 = TG5 - T8;
			dT2 = TG4 - T9;
			dT1 = max(dTmin,dT1);
			dT2 = max(dTmin,dT2);
			Var AEcoHP = rlmtd(dT1,dT2)*QHPPre / (kEco);
			result.ineq.push_back((Amin - AEcoHP) / 1e1, "Amin<=AEcoHP");
			AEcoHP       = max(Amin, AEcoHP);
			Cp0     = cost_function(AEcoHP,1,k1A,k2A,k3A);
			Fp = cost_function(p8,1,c1A,c2A,c3A);
			Var InvEcoHP = 1.18 * (B1A+B2A*FmA*Fp) * Cp0;
		// HP Evaporator
			dT1 = TG3 - T10;
			dT2 = TG2 - T10;
			dT1 = max(dTmin,dT1);
			dT2 = max(dTmin,dT2);
			Var AEvapHP = rlmtd(dT1,dT2)*QHPEvap / (kEvap);
			result.ineq.push_back((Amin - AEvapHP) / 1e1, "Amin<=AEvapHP");
			AEvapHP       = max(Amin, AEvapHP);
			Cp0         = cost_function(AEvapHP,1,k1A,k2A,k3A);
			Var InvEvapHP = 1.18 * (B1A+B2A*FmA*Fp) * Cp0;
		// HP Superheater
			dT1 = TG2 - T10;
			dT2 = TGin - T11;
			dT1 = max(dTmin,dT1);
			dT2 = max(dTmin,dT2);
			Var ASHHP = rlmtd(dT1,dT2)*QHPSH / (kSH);
			result.ineq.push_back((Amin - ASHHP) / 1e1, "Amin<=ASHHP");
			ASHHP       = max(Amin, ASHHP);
			Cp0         = cost_function(ASHHP,1,k1A,k2A,k3A);
			Var InvSHHP = 1.18 * (B1A+B2A*FmA*Fp) * Cp0;
		// Deaerator
			Var VDae = 1.5 * 600 * mdot * 0.001;
			result.ineq.push_back((Vmin-VDae)/1e0,"Vmin");
			VDae = max(VDae,Vmin);
			Var Cp0DAE = cost_function(VDae,1,k1B,k2B,k3B);
			Var FpDAE = 1.25;
			Var InvDae = 1.18 * (B1B+B2B*FmB*FpDAE) * Cp0DAE;
		// Cycle
			Var Inv = InvCond + InvPumpCond + InvPumpLP + InvPumpHP + InvEcoLP + InvEvapLP + InvSHLP + InvEcoHP + InvEvapHP + InvSHHP + InvTurb + InvDae;

	// Combined Cycle Plant
		Var InvTotal = Inv + InvGT;
		Var CAPEX = InvTotal*fPhi*fAnnu/(WorkTotal/1000*Teq);
		Var FuelCost = FuelPrice/etaCC;
		Var LCOE = (InvTotal*fPhi*fAnnu*1000/Teq + FuelHeat*FuelPrice)/WorkTotal + Varcost;

	// Objective:
		result.objective = LCOE;

	return result;
}