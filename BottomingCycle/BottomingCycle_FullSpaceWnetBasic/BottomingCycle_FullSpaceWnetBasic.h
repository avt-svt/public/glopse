/**
 *
 * @brief Full-space formulation of Case Study I (Basic Rankine Cycle)
 *        that was originally published in
 *        D. Bongartz, A. Mitsos: "Deterministic global optimization of process
 *             flowsheets in a reduced space using McCormick relaxations",
 *             Journal of Global Optimization 69 (2017), 761-796.
 *             https://link.springer.com/article/10.1007/s10898-017-0547-4
 *        E-mail: amitsos@alum.mit.edu
 *
 * ==============================================================================
 * © 2020, Process Systems Engineering (AVT.SVT), RWTH Aachen University
 * ==============================================================================
 *
 * used in:
 *    D. Bongartz: "Deterministic Global Flowsheet Optimization for the Design of Energy Conversion Processes",
 *     	            PhD Thesis, RWTH Aachen University, 2020.
 *
 */

#pragma once

#include "MAiNGOmodel.h"
#include "../Thermo/IdealFluidProperties.h"



class Model: public maingo::MAiNGOmodel {

  public:

    Model();

    maingo::EvaluationContainer evaluate(const std::vector<Var>& optVars);
    std::vector<maingo::OptimizationVariable> get_variables();

  private:

    const double eta_p, eta_t, mcpG, TGin, TGout, Tmax, dTap, dTmin, xmin, Amin, Vmin;
    const double kEco, kEvap, kSH, kCond, k1A, k2A, k3A, p1A, p2A, p3A, FmA, B1A, B2A, Tcin, Tcout;
    const double InvGT, WorkGT, FuelHeat, FuelPrice, fPhi, fAnnu, Teq, Varcost;
	double c1A,c2A,c3A,p0;

	const thermo::IdealFluidProperties properties;
};


//////////////////////////////////////////////////////////////////////////
// function for providing optimization variable data to the Branch-and-Bound solver
std::vector<maingo::OptimizationVariable>
Model::get_variables()
{

	using maingo::OptimizationVariable;
	using maingo::Bounds;

    std::vector<maingo::OptimizationVariable> variables;
	variables.push_back(OptimizationVariable(Bounds(0.1*3,0.1*100),"p2"));
	variables.push_back(OptimizationVariable(Bounds(5,100),"mdot"));
	variables.push_back(OptimizationVariable(Bounds(349,873),"T1sat"));
	variables.push_back(OptimizationVariable(Bounds(0,1500),"h1satliq"));
	variables.push_back(OptimizationVariable(Bounds(0,300),"h1"));
	variables.push_back(OptimizationVariable(Bounds(0.25,12.5),"wp"));
	variables.push_back(OptimizationVariable(Bounds(1.25,1250),"WorkPump"));
	variables.push_back(OptimizationVariable(Bounds(349,873),"T2sat"));
	variables.push_back(OptimizationVariable(Bounds(0.25,325),"h2"));
	variables.push_back(OptimizationVariable(Bounds(12400,375000),"Qzu"));
	variables.push_back(OptimizationVariable(Bounds(0.1*3,0.1*100),"p5"));
	variables.push_back(OptimizationVariable(Bounds(349,873),"T5sat"));
	variables.push_back(OptimizationVariable(Bounds(2480,3200),"h5satvap"));
	variables.push_back(OptimizationVariable(Bounds(2480,3750),"h5"));
	variables.push_back(OptimizationVariable(Bounds(349,873),"T5"));
	variables.push_back(OptimizationVariable(Bounds(4.23,10),"s5"));
	variables.push_back(OptimizationVariable(Bounds(0.1*3,0.1*100),"p4"));
	variables.push_back(OptimizationVariable(Bounds(349,873),"T4sat"));
	variables.push_back(OptimizationVariable(Bounds(2480,3200),"h4satvap"));
	variables.push_back(OptimizationVariable(Bounds(2480,3200),"h4"));
	variables.push_back(OptimizationVariable(Bounds(423,900),"TG2"));
	variables.push_back(OptimizationVariable(Bounds(0.1*3,0.1*100),"p3"));
	variables.push_back(OptimizationVariable(Bounds(349,873),"T3sat"));
	variables.push_back(OptimizationVariable(Bounds(349,873),"T3"));
	variables.push_back(OptimizationVariable(Bounds(0.25,1500),"h3"));
	variables.push_back(OptimizationVariable(Bounds(423,900),"TG3"));
	variables.push_back(OptimizationVariable(Bounds(0.1*0.2,0.1*3),"p7"));
	variables.push_back(OptimizationVariable(Bounds(349,873),"T7sat"));
	variables.push_back(OptimizationVariable(Bounds(0,1500),"h7satliq"));
	variables.push_back(OptimizationVariable(Bounds(2480,3200),"h7satvap"));
	variables.push_back(OptimizationVariable(Bounds(0,3),"s7satliq"));
	variables.push_back(OptimizationVariable(Bounds(4.23,10),"s7satvap"));
	variables.push_back(OptimizationVariable(Bounds(2.115,10),"s7"));
	variables.push_back(OptimizationVariable(Bounds(0.5,1),"x7"));
	variables.push_back(OptimizationVariable(Bounds(1240,3200),"h7"));
	variables.push_back(OptimizationVariable(Bounds(100,3750),"wt"));
	variables.push_back(OptimizationVariable(Bounds(500,375000),"WorkTurb"));
	variables.push_back(OptimizationVariable(Bounds(0.1*0.2,0.1*3),"p6"));
	variables.push_back(OptimizationVariable(Bounds(349,873),"T6sat"));
	variables.push_back(OptimizationVariable(Bounds(0,1500),"h6satliq"));
	variables.push_back(OptimizationVariable(Bounds(2480,3200),"h6satvap"));
	variables.push_back(OptimizationVariable(Bounds(1240,3200),"h6"));
	variables.push_back(OptimizationVariable(Bounds(0.85,1),"x6"));
	variables.push_back(OptimizationVariable(Bounds(200,375000),"WorkNet"));

    return variables;
}


//////////////////////////////////////////////////////////////////////////
// constructor for the model
Model::Model():
    eta_p(0.8), eta_t(0.9), mcpG(200), TGin(900), TGout(448), Tmax(873.15), dTap(10), dTmin(15), xmin(0.85), Amin(10), Vmin(1),
    kEco(0.06), kEvap(0.06), kSH(0.03), kCond(0.35), k1A(4.3247), k2A(-0.303), k3A(0.1634),
    p1A(0.03881), p2A(-0.11272), p3A(0.08183), FmA(2.75), B1A(1.63), B2A(1.66), Tcin(273.15+20), Tcout(273.15+25),
    InvGT(22.7176e6), WorkGT(69676), FuelHeat(182359), FuelPrice(14), fPhi(1.06), fAnnu(0.1875), Teq(4000), Varcost(4),
	properties("Water", 2.08, 4.18, 0.462, 0.001, 3.55959-1., 643.748, -198.043, 2480., 313.8336)
{
	// Convert coefficients for pressure factor from bar to MPa:
	c1A = p1A+p2A+p3A;
	c2A = p2A+2*p3A;
	c3A = p3A;
	p0 = pow(10.,properties.A - properties.B/(properties.T0+properties.C));
}


//////////////////////////////////////////////////////////////////////////
// evaluate the model
maingo::EvaluationContainer
Model::evaluate(const std::vector<Var>& optVars)
{

    // Rename inputs, these are the only real optimization variables
		Var p2=optVars[0];
		Var mdot=optVars[1];
		Var T1sat=optVars[2];
		Var h1satliq=optVars[3];
		Var h1=optVars[4];
		Var wp=optVars[5];
		Var WorkPump=optVars[6];
		Var T2sat=optVars[7];
		Var h2=optVars[8];
		Var Qzu=optVars[9];
		Var p5=optVars[10];
		Var T5sat=optVars[11];
		Var h5satvap=optVars[12];
		Var h5=optVars[13];
		Var T5=optVars[14];
		Var s5=optVars[15];
		Var p4=optVars[16];
		Var T4sat=optVars[17];
		Var h4satvap=optVars[18];
		Var h4=optVars[19];
		Var TG2=optVars[20];
		Var p3=optVars[21];
		Var T3sat=optVars[22];
		Var T3=optVars[23];
		Var h3=optVars[24];
		Var TG3=optVars[25];
		Var p7=optVars[26];
		Var T7sat=optVars[27];
		Var h7satliq=optVars[28];
		Var h7satvap=optVars[29];
		Var s7satliq=optVars[30];
		Var s7satvap=optVars[31];
		Var s7=optVars[32];
		Var x7=optVars[33];
		Var h7=optVars[34];
		Var wt=optVars[35];
		Var WorkTurb=optVars[36];
		Var p6=optVars[37];
		Var T6sat=optVars[38];
		Var h6satliq=optVars[39];
		Var h6satvap=optVars[40];
		Var h6=optVars[41];
		Var x6=optVars[42];
		Var WorkNet=optVars[43];


     maingo::EvaluationContainer result;


	// Model
		Var p1 = 0.1*0.2;    // Fixed condenser pressure (converted to MPa)
		result.eq.push_back((T1sat - (properties.B/(properties.A-log(p1)/log(10.0))-properties.C))/100);
		result.eq.push_back((T2sat - (properties.B/(properties.A-log(p2)/log(10.0))-properties.C))/100);
		result.eq.push_back((T3sat - (properties.B/(properties.A-log(p3)/log(10.0))-properties.C))/100);
		result.eq.push_back((T4sat - (properties.B/(properties.A-log(p4)/log(10.0))-properties.C))/100);
		result.eq.push_back((T5sat - (properties.B/(properties.A-log(p5)/log(10.0))-properties.C))/100);
		result.eq.push_back((T6sat - (properties.B/(properties.A-log(p6)/log(10.0))-properties.C))/100);
		result.eq.push_back((T7sat - (properties.B/(properties.A-log(p7)/log(10.0))-properties.C))/100);
		// Condenser outlet
		result.eq.push_back((h1satliq - (properties.cif*(T1sat-properties.T0) + properties.vif*1e3*(p1-p0)))/100);
		result.eq.push_back((h1 - (h1satliq))/100);
		// Feedwater Pump
		result.eq.push_back((WorkPump - (mdot*wp))/100);
		result.eq.push_back((wp - (properties.vif*(p2-p1)*1e3/eta_p))/10);
		result.eq.push_back((h2 - (h1 + wp))/100);
		// Boiler
		// Superheater
		// Overall balance
		result.eq.push_back((Qzu - (mcpG*(TGin-TGout)))/10000);
		result.eq.push_back((Qzu - (mdot*(h5-h2)))/10000);
		// Superheater
		result.eq.push_back((p5 - (p4))/10);
		result.eq.push_back((h5satvap - (properties.deltaH + properties.cpig*(T5sat-properties.T0)))/1000);
		result.eq.push_back((h5 - (properties.deltaH + properties.cpig*(T5-properties.T0)))/1000);
		result.eq.push_back((s5 - (properties.deltaH/properties.T0 + properties.cpig*log(T5/properties.T0) - properties.Rm*log(p5/p0)))/10);
		result.eq.push_back((mdot*(h5-h4) - (mcpG*(TGin-TG2)))/10000);
		// Evaporator
		result.eq.push_back((p4 - (p3))/10);
		result.eq.push_back((h4satvap - (properties.deltaH + properties.cpig*(T4sat-properties.T0)))/1000);
		result.eq.push_back((h4 - (h4satvap))/1000);
		result.eq.push_back((mdot*(h4-h3) - (mcpG*(TG2-TG3)))/10000);
		// Economizer
		result.eq.push_back((p3 - (p2))/10);
		result.eq.push_back((T3 - (T3sat-dTap))/100);
		result.eq.push_back((h3 - (properties.cif*(T3-properties.T0) + properties.vif*(p3-p0)*1e3))/100);
		// Turbine
		// isentrop
		result.eq.push_back((p7 - (p1))/1);
		result.eq.push_back((h7satliq - (properties.cif*(T7sat-properties.T0) + properties.vif*1e3*(p7-p0)))/100);
		result.eq.push_back((h7satvap - (properties.deltaH + properties.cpig*(T7sat-properties.T0)))/1000);
		result.eq.push_back((s7satliq - (properties.cif*log(T7sat/properties.T0)))/1);
		result.eq.push_back((s7satvap - (properties.deltaH/properties.T0 + properties.cpig*log(T7sat/properties.T0) - properties.Rm*log(p7/p0)))/10);
		result.eq.push_back((s7 - s5)/10);
		result.eq.push_back((s7 - (s7satliq+x7*(s7satvap-s7satliq)))/1);
		result.eq.push_back((h7 - (h7satliq+x7*(h7satvap-h7satliq)))/1000);
		// actual
		result.eq.push_back((p6 - (p1))/1);
		result.eq.push_back((h6satliq - (properties.cif*(T6sat-properties.T0) + properties.vif*1e3*(p6-p0)))/100);
		result.eq.push_back((h6satvap - (properties.deltaH + properties.cpig*(T6sat-properties.T0)))/1000);
		result.eq.push_back((h6 - (h6satliq+x6*(h6satvap-h6satliq)))/1000);
		result.eq.push_back((h6 - (h5-wt))/1000);
		result.eq.push_back((wt - (eta_t*(h5-h7)))/1000);
		result.eq.push_back((WorkTurb - (mdot*wt))/10000);
		// cycle
		result.eq.push_back((WorkNet - (WorkTurb-WorkPump))/100000);

	// Inequalities
    	result.ineq.push_back((h5satvap - h5) / 1e3,"hvap(p5)<=h5");
		result.ineq.push_back((dTmin - (TG3 - T4sat)) / 1e1, "Pinch: Evaporator, cold end");

	// Objective:
		result.objective = -WorkNet;
	
    return result;
}