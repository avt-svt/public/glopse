/**
 *
 * @brief Auxiliary file containing the definition of the IdealFluidProperties struct
 *        used in the combined cycle power plant case studies originally published in
 *        D. Bongartz, A. Mitsos: "Deterministic global optimization of process
 *             flowsheets in a reduced space using McCormick relaxations",
 *             Journal of Global Optimization 69 (2017), 761-796.
 *             https://link.springer.com/article/10.1007/s10898-017-0547-4
 *        E-mail: amitsos@alum.mit.edu
 *
 * ==============================================================================
 * © 2020, Process Systems Engineering (AVT.SVT), RWTH Aachen University
 * ==============================================================================
 *
 * used in:
 *    D. Bongartz: "Deterministic Global Flowsheet Optimization for the Design of Energy Conversion Processes",
 *     	            PhD Thesis, RWTH Aachen University, 2020.
 * 
 */

#pragma once

#include <string>


namespace thermo {


/**
* @class IdealFluidProperties
* @brief Struct storing all parameters needed to model an ideal fluid with constant heat capacities and using the Antoine equation for saturation pressure
*/
struct IdealFluidProperties {

    const std::string name; /*!< Name of the fluid */

    const double cpig;   /*!< Heat capacity ideal gas                   [kJ/kg*K] */
    const double cif;    /*!< Heat capacity ideal fluid                 [kJ/kg*K] */
    const double Rm;     /*!< Specific gas constant                     [kJ/kg*K] */
    const double vif;    /*!< Specific volume ideal fluid               [m^3/kg]  */
    const double A;      /*!< Parameter for Antoine equation (in MPa,K) [-]       */
    const double B;      /*!< Parameter for Antoine equation (in MPa,K) [K]       */
    const double C;      /*!< Parameter for Antoine equation (in MPa,K) [K]       */
    const double deltaH; /*!< Evaporation enthalpy                      [kJ/kg]   */
    const double T0;     /*!< Reference temperature for evaporation     [K]       */

    /**
        * @brief Constructor accepting parameter inputs
        */
    IdealFluidProperties(const std::string nameIn, const double cpigIn, const double cifIn, const double RmIn, const double vifIn, const double AIn,
                         const double BIn, const double CIn, const double deltaHIn, const double T0in):
        name(nameIn),
        cpig(cpigIn), cif(cifIn), Rm(RmIn), vif(vifIn), A(AIn), B(BIn), C(CIn), deltaH(deltaHIn), T0(T0in) {}
};


}    // end namespace thermo