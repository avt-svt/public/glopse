/**
 *
 * @brief EIS model proposed by
 *        J. R. Seidenberg, Process Systems Engineering (AVT.SVT), RWTH Aachen
 *              University (2024)
 *        using experimental data provided in
 *        N. Thissen, S. Khan, A. K. Mechler: "Electrochemical data on alkaline
 *              water electrolysis", Zenodo (2024).
 *              https://www.doi.org/10.5281/zenodo.11103701
 *        that was originally implemented for
 *        S. Sass, A. Mitsos, N. I. Nikolov, A. Tsoukalas: "Out-of-sample
 *              estimation for a branch-and-bound algorithm with growing datasets",
 *              Submitted 2024.
 *        E-mail: amitsos@alum.mit.edu
 *
 * ==============================================================================
 * © 2024, Process Systems Engineering (AVT.SVT), RWTH Aachen University
 * ==============================================================================
 */

#pragma once

#include "MAiNGOmodel.h"


namespace userInput {
    // Enter the path to the directory containing the csv-file with the data
    std::string pathToCSVfile = "../GloPSE/ElectrochemicalImpedanceSpectroscopy2CPE/";
}

///////////////
//// 1. Model-specific auxiliary functions etc.
///////////////

//////////////////////////////////////////////////////////////////////////
// struct for saving data points of full dataset
struct EisData {
    std::vector<double> ReZ, nImZ, freq;
};


///////////////
//// 2. Data input
///////////////

//////////////////////////////////////////////////////////////////////////
// auxiliary function for reading data from csv-file
void read_csv_to_vector(const std::string name, std::vector<std::vector<double>> &data)
{
    // Open data file
    std::ifstream infile;
    infile.open(name);
    if (infile.fail()) {
        throw std::runtime_error("Error in model: Unable to open data file "+name);
    }
    else {
        std::cout << "Read data from " << name << std::endl;
    }

    std::string line, tmpCooStr;
    std::vector<double> tmpData;
    double tmpCoo;
    data.clear();
    // Read all lines
    while (std::getline(infile, line)) {
        // Split line into coordinates
        std::istringstream stream(line);
        tmpData.resize(0);
        // First entry
        stream >> tmpCoo;
        tmpData.push_back(tmpCoo);
        // Read all remaining entries
        while (std::getline(stream, tmpCooStr, ';')) {
            stream >> tmpCoo;
            tmpData.push_back(tmpCoo);
        }
        data.push_back(tmpData);
    }

    if (data.size() != 3) {
        throw std::runtime_error("Error in model: Unexpected number of variables in data file " + name);
    }
}


///////////////
//// 3. MAiNGO model
///////////////

class Model: public maingo::MAiNGOmodel {

  public:

    Model();

    maingo::EvaluationContainer evaluate(const std::vector<Var> &optVars);
    std::vector<maingo::OptimizationVariable> get_variables();
    std::vector<double> get_initial_point();

  private:
    // Fixed parameters
    std::vector<double> _weights; // Weights of prediction error
    std::vector<double> _lnOmega; // Logarithm of angular frequency calculated from input frequency

    // Measurement data
    unsigned int _noOfDataPoints; // Size of full data set
    EisData _eisData;             // Full dataset
};


//////////////////////////////////////////////////////////////////////////
// function for provding optimization variable data to the Branch-and-Bound solver
std::vector<maingo::OptimizationVariable>
Model::get_variables()
{
    std::vector<maingo::OptimizationVariable> variables;

    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.1  , 0.15), maingo::VT_CONTINUOUS, "R0"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.001, 0.03), maingo::VT_CONTINUOUS, "R1"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.001, 0.03), maingo::VT_CONTINUOUS, "R2"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.001, 0.05), maingo::VT_CONTINUOUS, "Q1"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.1  , 10. ), maingo::VT_CONTINUOUS, "Q2"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.4, 0.999999), maingo::VT_CONTINUOUS, "alpha1"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.4, 0.999999), maingo::VT_CONTINUOUS, "alpha2"));

    return variables;
}


//////////////////////////////////////////////////////////////////////////
// function for provding initial point data to the Branch-and-Bound solver
std::vector<double>
Model::get_initial_point()
{

    std::vector<double> initialPoint;

    // Use the following to start the optimization from the global solution
    //initialPoint = {0.13686, 0.0081671, 0.015939, 0.012061, 1.92067, 0.9999986, 0.9999989};

    return initialPoint;
}


//////////////////////////////////////////////////////////////////////////
// constructor for the model
Model::Model()
{
    // Catch measurement data from file
    std::string nameFile = userInput::pathToCSVfile + "data_ThissenEtAl2024_reduced.csv";
    std::vector<std::vector<double>> tmpData;
    tmpData.clear();
    read_csv_to_vector(nameFile, tmpData);

    // Plug data into struct
    _eisData.freq = tmpData[0];
    _eisData.ReZ  = tmpData[1];
    _eisData.nImZ = tmpData[2];

    // Catch size of full dataset
    _noOfDataPoints = tmpData[0].size();
    
    // Calculate weights of the prediction errors
    _weights.resize(_noOfDataPoints, 1);
    for (auto i = 0; i < _noOfDataPoints; i++) {
        // (1/(0.01*norm(Zmeas)))^2 = 1e4/norm(Zmeas)
        _weights[i] = 1e4 / (sqr(_eisData.ReZ[i]) + sqr(_eisData.nImZ[i]));
    }

    // Calculate logarithm of angular frequency
    _lnOmega.resize(_noOfDataPoints, 1);
    for (auto i = 0; i < _noOfDataPoints; i++) {
        // omega = 2 * \pi * freq
        _lnOmega[i] = std::log(2 * M_PI * _eisData.freq[i]);
    }
}


//////////////////////////////////////////////////////////////////////////
// evaluate the model
maingo::EvaluationContainer
Model::evaluate(const std::vector<Var> &optVars)
{

    // Catch optimization variables
    // The vector optVars is of the same size and sorted in the same order as the user-defined variables vector in function get_variables()
    unsigned int iVar = 0;
    std::vector<Var> R, Q, alpha;

    Var R0 = optVars.at(iVar);
    iVar++;

    R.clear();
    R.push_back(optVars.at(iVar));
    iVar++;
    R.push_back(optVars.at(iVar));
    iVar++;

    Q.clear();
    Q.push_back(optVars.at(iVar));
    iVar++;
    Q.push_back(optVars.at(iVar));
    iVar++;
    
    alpha.clear();
    alpha.push_back(optVars.at(iVar));
    iVar++;
    alpha.push_back(optVars.at(iVar));

    // Model
    maingo::EvaluationContainer result;

    Var se = 0; // Summed squared prediction error (SSE)
    for (auto d = 0; d < _noOfDataPoints; d++) {
        Var nImPred = 0; // Negative imaginary part
        Var RePred = R0;
        for (auto i = 0; i < 2; i++) {
            Var x = expx_times_y(alpha[i] * _lnOmega[d], Q[i]);        
            Var denominator = sqr( R[i] * x * sin(M_PI_2 * alpha[i]) )
                            + sqr( R[i] * x * cos(M_PI_2 * alpha[i]) + 1);
            nImPred = nImPred +  sin(M_PI_2 * alpha[i]) * sqr(R[i]) * x / denominator;
            RePred  = RePred + (cos(M_PI_2 * alpha[i]) * sqr(R[i]) * x + R[i]) / denominator;
        }
        Var sePerData = sqr(nImPred - _eisData.nImZ[d])
                        + sqr( RePred - _eisData.ReZ[d]);
        sePerData = sePerData * _weights[d];
        se += sePerData;
        
        // Objective for B&B algorithm with growing datasets
        result.objective_per_data.push_back(sePerData);
    }
    // Objective for standard B&B algorithm
    result.objective = se; // SSE
//    result.objective = se / _noOfDataPoints; // Mean squared error (MSE)

    // Inequality for breaking the symmetry
    result.ineq.push_back(R[0]*Q[0] - Q[1] * R[1], "R_1 * Q_1 <= Q_2 * R_2");

    return result;
}