# ElectrochemicalImpedanceSpectroscopy2CPE

Parameter estimation problem for fitting the electrical impedance of an electrochemical system with two constant phase elements.
In the C++ header files for MAiNGO, we provide the summed squared prediction error and the mean squared prediction error as the objective for the standard B&B algorithm as well as the squared prediction error for each data point as the objective per data used in the B&B algorithm with growing datasets, see [manual of MAiNGO](https://avt-svt.pages.rwth-aachen.de/public/maingo/special_uses.html#growing_datasets)  and [Sass et al. (2024a)](https://doi.org/10.1016/j.ejor.2024.02.020).
In the GAMS file, we formulate all model equations as equalities in dependence of the data points to increase the readability of the model.

The data is stored in rows to enable an efficient reading of the data into the MAiNGO model.
In fact, rows 1, 2, and 3 contain the measurements for input frequency f in Hertz, the real part of electrical impedance Z in Ohm, and the _negative_ imaginary part of electrical impedance Z in Ohm, respectively.

The implementation is based on model equations given by
 - J. R. Seidenberg, Process Systems Engineering (AVT.SVT), RWTH Aachen University (2024)
 
for measurement data published in
 - Thissen, N., Khan, S., & Mechler, A. K (2024). [Electrochemical data on alkaline water electrolysis](https://www.doi.org/10.5281/zenodo.11103701). *Zenodo*

based on the experimental setup described in
 - Thissen, N., Hoffmann, J., Tigges, S., Vogel, D. A. M., Thoede, J. J., Khan, S., Schmitt, N., Heumann, S., Etzold, B. J. M., & Mechler, A. K. (2024). [Industrially Relevant Conditions in Lab-Scale Analysis for Alkaline Water Electrolysis](https://www.doi.org/10.1002/celc.202300432). *ChemElectroChem*, 11 (1), e202300432.

The problem was originally implemented for the following publication:
 - Sass, S., Mitsos, A., Nikolov, N. I., & Tsoukalas, A. (2024b).  Out-of-sample estimation for a branch-and-bound algorithm with growing datasets. Submitted.
 
| Instance        | Problem Type | Number of Continuous Variables | Number of Integer Variables | Number of Binary Variables | Number of Equalities | Number of Inequalities | Best Known Objective Value* | Proven Objective Bound* | Description           |
| ------------- |-------------| ------------| ------------| ------------| ------------| ------------| :------------: | :-----: | ----- |
| EIS2CPE | NLP | 7 | 0 | 0 | 0 | 1 |  10.8450 | 9.7605 | Model EIS of Sass et al. (2024b) |

*The values correspond to using the summed squared error as the objective function.