* EIS model proposed by
*     J. R. Seidenberg, Process Systems Engineering (AVT.SVT), RWTH Aachen
*          University (2024)
* using experimental data provided in
*     N. Thissen, S. Khan, A. K. Mechler: "Electrochemical data on alkaline
*          water electrolysis", Zenodo (2024).
*          https://www.doi.org/10.5281/zenodo.11103701
* that was originally implemented for
*     S. Sass, A. Mitsos, N. I. Nikolov, A. Tsoukalas: "Out-of-sample
*          estimation for a branch-and-bound algorithm with growing datasets",
*          Submitted 2024.
* E-mail: amitsos@alum.mit.edu
*
* Copyright (c) 2024 Process Systems Engineering (AVT.SVT), RWTH Aachen University

*
* Optimization variables
*

*Continuous variables
Set
   idxPE 'index of phase element'   / p1*p2  /
   ;
Variables R0, R(idxPE), Q(idxPE), alpha(idxPE), objectiveVar;

*Continuous variable bounds
R0.LO = 0.1;
R0.UP = 0.15;
R.LO(idxPE) = 0.001;
R.UP(idxPE) = 0.03;
Q.LO('p1') = 0.001;
Q.UP('p1') = 0.05;
Q.LO('p2') = 0.1;
Q.UP('p2') = 10.;
alpha.LO(idxPE) = 0.4;
alpha.UP(idxPE) = 0.999999;

*Use the following to start from the global optimum
*R0.L = 0.13686;
*R.L('p1') = 0.0081671;
*R.L('p2') = 0.015939;
*Q.L('p1') = 0.012061;
*Q.L('p2') = 1.92067;
*alpha.L('p1') = 0.9999986;
*alpha.L('p2') = 0.9999989;


*
* Constants and data
*

*Dataset
Set
   idxData  'index of data point'
   ivar 'index of state variable';

*Pick the dataset you want to optimize for 
$call csv2gdx data_ThissenEtAl2024_reduced.csv id=meas autoCol=dataPoint autoRow=stateVar colCount=26 values=1..lastCol fieldSep=SemiColon output=data.gdx

$gdxIn data.gdx
$load ivar = Dim1
$load idxData  = Dim2

Parameter meas(ivar,idxData) 'measurements';
$load meas
$gdxIn
*display meas;

*
* Model equations
*
* stateVar1 = freq, stateVar2 = Re(Z), stateVar3 = -Im(Z)

*Auxiliary variables
Variables
    weights(idxData), lnOmega(idxData),
    auxVar(idxPE,idxData), denominator(idxPE,idxData),
    ReZCalc(idxData), nImZCalc(idxData);

*Dummy values for denominators to avoid division by zero
weights.L(idxData) = 1;
denominator.L(idxPE,idxData) = 1;

*Equation variables
Equations
    calculateWeights(idxData)
    calculateLnOmega(idxData)
    calculateAuxVar(idxPE,idxData)
    calculateDenominator(idxPE,idxData)
    calculateReZ(idxData)
    calculateNImZ(idxData)
    objective
    symmetryBreak;

*Auxiliary equations
calculateWeights(idxData).. weights(idxData) =E= 1e4 / (sqr(meas('stateVar2',idxData)) + sqr(meas('stateVar3',idxData)));
calculateLnOmega(idxData).. lnOmega(idxData) =E= log(2 * pi * meas('stateVar1',idxData));

calculateAuxVar(idxPE,idxData).. auxVar(idxPE,idxData) =E= exp(alpha(idxPE) * lnOmega(idxData)) * Q(idxPE);
calculateDenominator(idxPE,idxData).. denominator(idxPE,idxData) =E= sqr( R(idxPE) * auxVar(idxPE,idxData) * sin(pi/2 * alpha(idxPE)) ) + sqr( R(idxPE) * auxVar(idxPE,idxData) * cos( pi/2 * alpha(idxPE)) + 1);

calculateNImZ(idxData).. nImZCalc(idxData) =E= sum(idxPE, (sin(pi/2 * alpha(idxPE)) * sqr(R(idxPE)) * auxVar(idxPE,idxData)) / denominator(idxPE,idxData));
calculateReZ(idxData)..  ReZCalc(idxData)  =E= sum(idxPE, (cos(pi/2 * alpha(idxPE)) * sqr(R(idxPE)) * auxVar(idxPE,idxData) + R(idxPE)) / denominator(idxPE,idxData));

*Objective function
*SSE:
objective.. objectiveVar =E= sum(idxData, (sqr(meas('stateVar2',idxData) - ReZCalc(idxData)) + sqr(meas('stateVar3',idxData) - nImZCalc(idxData))) / weights(idxData));
*MSE:
*objective .. objectiveVar =E= sum(idxData, (sqr(meas('stateVar2',idxData) - ReZCalc(idxData)) + sqr(meas('stateVar3',idxData) - nImZCalc(idxData))) / weights(idxData))/card(idxData);

*Inequality for breaking the symmetry
symmetryBreak .. R('p1')*Q('p1') =L= Q('p2') * R('p2') ;


*
* Optimization problem
*

*Model information and options
Model eis / all /;

*Optimality tolerances, time and solver
option OPTCA = 0.01;
option OPTCR = 0.01;
option RESLIM = 82800;
*option NLP = BARON;

*Solve statement
solve eis using NLP minimizing objectiveVar;