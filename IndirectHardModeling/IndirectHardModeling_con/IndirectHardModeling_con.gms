* IHMcon model based on
*     A. Echtermeyer, C. Marks, A. Mitsos, J. Viell: "Inline Raman Spectroscopy
*          and Indirect Hard Modeling for Concentration Monitoring of
*          Dissociated Acid Species", Applied spectroscopy 75(5), 506-519 (2021).
*          https://www.doi.org/10.1177/0003702820973275
* that was originally implemented for
*     S. Sass, A. Mitsos, N. I. Nikolov, A. Tsoukalas: "Out-of-sample
*          estimation for a branch-and-bound algorithm with growing datasets",
*          Submitted 2024.
* E-mail: amitsos@alum.mit.edu
*
* Copyright (c) 2024 Process Systems Engineering (AVT.SVT), RWTH Aachen University

*
* Optimization variables
*

*Continuous variables
Set
   idxPeak 'index of peaks'   / c1p1*c1p3, c2p1*c2p7 /
   ;
Variables alphaTilde(idxPeak), gamma(idxPeak), objectiveVar;

*Continuous variable bounds
alphaTilde.LO(idxPeak) = 0.;
alphaTilde.UP(idxPeak) = 1e4;
gamma.LO(idxPeak) = 1.;
gamma.UP(idxPeak) = 100.;

*Use the following to start from the solution of Echtermeyer et al. (2021)
*alphaTilde.L('c1p1') = 5903.88803699102;
*alphaTilde.L('c1p2') = 920.092825695185;
*alphaTilde.L('c1p3') = 1817.52265906915;
*gamma.L('c1p1') = 58.8131012990515;
*gamma.L('c1p2') = 53.2035144224224;
*gamma.L('c1p3') = 75.0237619804884;

*alphaTilde.L('c2p1') = 5525.16909663814;
*alphaTilde.L('c2p2') = 2476.20696934017;
*alphaTilde.L('c2p3') = 2166.94091643271;
*alphaTilde.L('c2p4') = 2665.29534242889;
*alphaTilde.L('c2p5') = 336.667639059137;
*alphaTilde.L('c2p6') = 2654.02152796236;
*alphaTilde.L('c2p7') = 407.980637488699;
*gamma.L('c2p1') = 22.8382667478905;
*gamma.L('c2p2') = 31.0208210691256;
*gamma.L('c2p3') = 5.79409595326439;
*gamma.L('c2p4') = 25.5932285227208;
*gamma.L('c2p5') = 5.66959510619154;
*gamma.L('c2p6') = 27.3887920071993;
*gamma.L('c2p7') = 17.6357030236057;

*Dummy values for denominators to avoid division by zero
*Note: dummy values are not required when using start solution given above
alphaTilde.L(idxPeak) = 1;
gamma.L(idxPeak) = 1;


*
* Constants and data
*

*Fixed parameters
Scalar
   intercept 'intercept of baseline' / 6391.28009065046 /
   slope 'slope of baseline' / -1.34286050618806 /
   ;

Parameter
   delta(idxPeak) 'peak position' / c1p1	= 1642.77239001424, c1p2 = 1510.25892421134, c1p3 = 1354.37757219951, c2p1 = 1722.15765977765, c2p2 = 1703.85923731233, c2p3 = 1401.02576508565, c2p4 = 1391.45416156505, c2p5 = 1350.17908876739, c2p6 = 1217.49982696117, c2p7 = 1060.89917366566/
   beta(idxPeak)  'Gaussian-Lorentzian-ratio' / c1p1 = 0.292491843755009, c1p2 = 0.000109912475525492, c1p3 = 0.000100000000002083, c2p1 = 0.999899999938264, c2p2 = 0.999899977375973, c2p3 = 0.999855314666393, c2p4 = 0.000100014417215933, c2p5 = 0.999899999993971, c2p6 = 0.702665809164258, c2p7 = 0.99989999999989 /
   areaSimplified(idxPeak) 'alphaTilde*gamma' / c1p1 = 347225.965177811, c1p2 = 48952.1719218411, c1p3 = 136357.387368148, c2p1 = 126185.285656223, c2p2 = 76813.9733260233, c2p3 = 12555.4635948658, c2p4 = 68213.512779326, c2p5 = 1908.76919882274, c2p6 = 72690.4436119902, c2p7 = 7195.02536213202 /
   ;

*Dataset
Set
   idxData  'index of data point'
   ivar 'index of state variable';

*Pick the dataset you want to optimize for 
$call csv2gdx ../data/data_EchtermeyerEtAl_titrationStep1_reduced.csv id=meas autoCol=stateVar autoRow=dataPoint colCount=2 values=1..lastCol fieldSep=SemiColon output=data.gdx

$gdxIn data.gdx
$load idxData  = Dim1
$load ivar = Dim2

Parameter meas(idxData,ivar) 'measurements';
$load meas
$gdxIn
*display meas;


*
* Model equations
*
* stateVar1 = ny, stateVar2 = A

*Auxiliary variables
Variables
    spectrumShifted(idxData),
    sqrdFraction(idxData,idxPeak), intensityPeak(idxData,idxPeak),
    spectrumAll(idxData);

*Equation variables
Equations
    calculateShiftedSpectrum(idxData)
    calculateSqrdFraction(idxData,idxPeak)
    calculateIntensityPeak(idxData,idxPeak)
    calculateSpectrumAll(idxData)
    objective
    equalityArea(idxPeak);

*Auxiliary equations
calculateShiftedSpectrum(idxData).. spectrumShifted(idxData) =E= meas(idxData,'stateVar2') - intercept - meas(idxData,'stateVar1')*slope;

calculateSqrdFraction(idxData,idxPeak).. sqrdFraction(idxData,idxPeak) =E= sqr( ( meas(idxData,'stateVar1') - delta(idxPeak) ) / gamma(idxPeak) );
calculateIntensityPeak(idxData,idxPeak).. intensityPeak(idxData,idxPeak) =E= alphaTilde(idxPeak) * ( beta(idxPeak)  * exp(-log(2) * sqrdFraction(idxData,idxPeak) ) + (1 - beta(idxPeak)) / (sqrdFraction(idxData,idxPeak) + 1.) );

calculateSpectrumAll(idxData).. spectrumAll(idxData) =E= sum(idxPeak, intensityPeak(idxData,idxPeak));

*Objective function
*SSE:
objective.. objectiveVar =E= sum(idxData, sqr(meas(idxData, 'stateVar2') - spectrumAll(idxData)));
*MSE:
*objective .. objectiveVar =E= sum(idxData, sqr(meas(idxData, 'stateVar2') - spectrumAll(idxData)))/card(idxData);

*Equality of area below peaks
equalityArea(idxPeak).. alphaTilde(idxPeak)*gamma(idxPeak) =E= areaSimplified(idxPeak);


*
* Optimization problem
*

*Model information and options
Model ihmCon / all /;

*Optimality tolerances, time and solver
option OPTCA = 0.01;
option OPTCR = 0.01;
option RESLIM = 82800;
option NLP = BARON;

*Solve statement
solve ihmCon using NLP minimizing objectiveVar;