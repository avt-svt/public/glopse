/**
 *
 * @brief IHMcon model based on
 *        A. Echtermeyer, C. Marks, A. Mitsos, J. Viell: "Inline Raman Spectroscopy
 *              and Indirect Hard Modeling for Concentration Monitoring of
 *              Dissociated Acid Species", Applied spectroscopy 75(5), 506-519 (2021).
 *              https://www.doi.org/10.1177/0003702820973275
 *        that was originally implemented for
 *        S. Sass, A. Mitsos, N. I. Nikolov, A. Tsoukalas: "Out-of-sample
 *              estimation for a branch-and-bound algorithm with growing datasets",
 *              Submitted.
 *        E-mail: amitsos@alum.mit.edu
 *
 * ==============================================================================
 * © 2024, Process Systems Engineering (AVT.SVT), RWTH Aachen University
 * ==============================================================================
 */

#pragma once

#include "MAiNGOmodel.h"


namespace userInput {
    // Enter the path to the directory containing the csv-file with the data
    std::string pathToCSVfile = "../GloPSE/IndirectHardModeling/data/";

    // Name of file containing measurement data ("data file")
    std::string fileData = "data_EchtermeyerEtAl_titrationStep1_reduced.csv";
    // Name of file containing reference solution (determining number of peaks and fixed peak parameters)
    std::string fileSoln = "solution_EchtermeyerEtAl_reduced.csv";
}

///////////////
//// 1. Model-specific auxiliary functions etc.: None
///////////////


///////////////
//// 2. Data input
///////////////


//////////////////////////////////////////////////////////////////////////
// auxiliary function for reading data from csv-filevoid read_csv_to_vector(const std::string name, std::vector<std::vector<double>>& data)
void read_csv_to_vector(const std::string name, std::vector<std::vector<double>>& data)
{
    // Open data file
    std::ifstream infile;
    infile.open(name);
    if (infile.fail()) {
        throw std::runtime_error("Error in model: Unable to open data file "+name);
    }
    else {
        std::cout << "Read data from " << name << std::endl;
    }

    std::string line, tmpCooStr;
    std::vector<double> tmpData;
    double tmpCoo;
    data.clear();
    // Read all lines
    while(std::getline(infile,line)){
        // Split line into coordinates
        std::istringstream stream(line);
        tmpData.resize(0);
        // First entry
        stream >> tmpCoo;
        tmpData.push_back(tmpCoo);
        // Read all remaining entries
        while(std::getline(stream, tmpCooStr, ';')){
            stream >> tmpCoo;
            tmpData.push_back(tmpCoo);
        }
        data.push_back(tmpData);
    }
}


///////////////
//// 3. MAiNGO model
///////////////

class Model: public maingo::MAiNGOmodel {

  public:

    Model();

    maingo::EvaluationContainer evaluate(const std::vector<Var> &optVars);
    std::vector<maingo::OptimizationVariable> get_variables();
    std::vector<double> get_initial_point();

  private:
    // Size of problem
    unsigned int _noOfComponents;                     // Number of components in mixture
    std::vector<unsigned int> _noOfPeaksPerComponent; // Number of peaks for each component

    // Fixed parameters
    double _slope, _intercept;
    std::vector<double> _spectrumShifted;             // Spectrum shifted by constant baseline
    std::vector<std::vector<double>> _beta, _delta, _areaSimplified;
    std::vector<double> _initialGuess;                // Initial solution provided via csv-file

    // Measurement data
    unsigned int _noOfDataPoints;                     // Number of data points in full dataset
    std::vector<std::vector<double>> _dataPoint;      // Full dataset
};


//////////////////////////////////////////////////////////////////////////
// function for providing optimization variable data to the Branch-and-Bound solver
std::vector<maingo::OptimizationVariable>
Model::get_variables()
{

    std::vector<maingo::OptimizationVariable> variables;

    for (auto k=0; k<_noOfComponents; k++){
        std::string nameComponent = std::to_string(k);
        for (auto i=0; i<_noOfPeaksPerComponent[k]; i++){
            std::string namePeak = std::to_string(i);
            // Modified peak maximum alphaTilde = w_k * alpha_k,i
            variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0., 1.e4), maingo::VT_CONTINUOUS, "alphaTilde_" + nameComponent + "," + namePeak));
            // Half width of half maximum (HWHM) gamma_k,i
            variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1., 100.), maingo::VT_CONTINUOUS, "gamma_" + nameComponent + "," + namePeak));
        }
    }

    return variables;
}


//////////////////////////////////////////////////////////////////////////
// function for providing initial point data to the Branch-and-Bound solver
std::vector<double>
Model::get_initial_point()
{
    std::vector<double> initialPoint;
    
    // Use the following to start the optimization from the optimal solution
    // of Echtermeyer et al. (2021)
    //initialPoint = _initialGuess;

    return initialPoint;
}


//////////////////////////////////////////////////////////////////////////
// constructor for the model
// reading in measurement data and initial solution
Model::Model()
{
    // Catch measurement data from file
    std::string fileData = userInput::pathToCSVfile +  userInput::fileData;
    read_csv_to_vector(fileData, _dataPoint);

    // Catch size of full dataset
    _noOfDataPoints = _dataPoint.size();

    // Catch reference solution
    std::vector<std::vector<double>> dataIn;
    std::string fileInit = userInput::pathToCSVfile + userInput::fileSoln;
    read_csv_to_vector(fileInit, dataIn);

    // Catch number of peaks
    _noOfComponents = dataIn.size() - 1; // Subtract baseline data
    _noOfPeaksPerComponent.resize(_noOfComponents, 0);
    for (auto k = 0; k < _noOfComponents; k++) {
        _noOfPeaksPerComponent[k] = (dataIn[k + 1].size() - 1) / 4; // Subtract weight w_k, each peak is determined by 4 parameters
    }

    // Fix baseline
    _intercept = dataIn[0][0];
    _slope     = dataIn[0][1];

    // Component-specific parameters
    _initialGuess.clear();
    _beta.resize(_noOfComponents);
    _delta.resize(_noOfComponents);
    _areaSimplified.resize(_noOfComponents);
    for (auto k = 0; k < _noOfComponents; k++) {
        for (auto i = 0; i < _noOfPeaksPerComponent[k]; i++) {
            // dataIn[k + 1][0]: weight w_k
            _delta[k].push_back(dataIn[k + 1][1 + 4 * i]);        // Position delta_k,i
            // dataIn[k + 1][1 + 4 * i + 1]: Peak maximum alpha_k,i
            _initialGuess.push_back(dataIn[k + 1][1 + 4 * i + 2]);// Half width at half maximum gamma_k,i
            _beta[k].push_back(dataIn[k + 1][1 + 4 * i + 3]);     // Gaussian-Lorentzian-ratio beta_k,i
            
            // Modified peak maximum alphaTilde = w_k * alpha_{k,i}
            _initialGuess.push_back(dataIn[k + 1][0] * dataIn[k + 1][1 + 4 * i + 1]);
            
            // Peak area for constant beta: alpha*gamma*constant
            // We substitute alpha with alphaTilde
            _areaSimplified[k].push_back(dataIn[k + 1][0] * dataIn[k + 1][1 + 4 * i + 1] * dataIn[k + 1][1 + 4 * i + 2]);
        }
    }

    // Shift measured spectrum by baseline
    _spectrumShifted.resize(_noOfDataPoints);
    for (auto d = 0; d < _noOfDataPoints; d++) {
        _spectrumShifted[d] = _dataPoint[d][1] - _intercept - _dataPoint[d][0] * _slope;
    }
}


//////////////////////////////////////////////////////////////////////////
// evaluate the model
maingo::EvaluationContainer
Model::evaluate(const std::vector<Var> &optVars)
{
    // Catch optimization variables
    // The vector optVars is of the same size and sorted in the same order as the user-defined variables vector in function get_variables()
    std::vector<std::vector<Var>> alphaTilde, gamma;
    alphaTilde.resize(_noOfComponents);
    gamma.resize(_noOfComponents);

    unsigned int  iVar = 0;
    for (auto k=0; k<_noOfComponents; k++){
        alphaTilde[k].resize(_noOfPeaksPerComponent[k]);
        gamma[k].resize(_noOfPeaksPerComponent[k]);
        for (auto i=0; i<_noOfPeaksPerComponent[k]; i++){
            alphaTilde[k][i] = optVars.at(iVar);
            iVar++;
            gamma[k][i] = optVars.at(iVar);
            iVar++;
        }
    }

    // Model
    maingo::EvaluationContainer result;
    
    std::vector<Var> spectrumComp;
    Var spectrumAll, intensityPeak, sqrdFraction;

    Var se = 0.; // Summed squared prediction error (SSE)
    spectrumComp.resize(_noOfComponents);
    for (auto d = 0; d < _noOfDataPoints; d++) {
        spectrumAll = 0;
        for (auto k = 0; k < _noOfComponents; k++) {
            // Spectrum of component
            spectrumComp[k] = 0;
            for (auto i = 0; i < _noOfPeaksPerComponent[k]; i++) {
                // Peaks
                sqrdFraction = sqr( (_dataPoint[d][0] - _delta[k][i]) / gamma[k][i] );
                intensityPeak = alphaTilde[k][i] * (       _beta[k][i]  * exp(-log(2) * sqrdFraction)
                                                    + (1 - _beta[k][i]) / (sqrdFraction + 1.)  );

                // Sum of peaks
                spectrumComp[k] = spectrumComp[k] + intensityPeak;
            }

            // Spectrum of mixture
            spectrumAll = spectrumAll + spectrumComp[k];
        }
        Var sePerData = sqr(_spectrumShifted[d] - spectrumAll);
        se += sePerData;
        
        // Objective for B&B algorithm with growing datasets
        result.objective_per_data.push_back(sePerData);
    }
    // Objective for standard B&B algorithm
    result.objective = se; // SSE
//    result.objective = se / _noOfDataPoints; // Mean squared error (MSE)
    
    // Equality of area below peaks
    for (auto k = 0; k < _noOfComponents; k++) {
        for (auto i = 0; i < _noOfPeaksPerComponent[k]; i++) {
            result.eq.push_back(alphaTilde[k][i] * gamma[k][i] - _areaSimplified[k][i]);
        }
    }

    return result;
}