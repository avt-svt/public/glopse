# IndirectHardModeling

Parameter estimation problem for fitting spectral data obtained by Raman spectroscopy to Pseudo-Voigt functions following the approach of indirect hard modeling, see [Alsmeyer et al. (2004)](https://www.doi.org/10.1366/0003702041655368).
In the C++ header files for MAiNGO, we provide the summed squared prediction error and the mean squared prediction error as the objective for the standard B&B algorithm as well as the squared prediction error for each data point as the objective per data used in the B&B algorithm with growing datasets, see [manual of MAiNGO](https://avt-svt.pages.rwth-aachen.de/public/maingo/special_uses.html#growing_datasets) and [Sass et al. (2024a)](https://doi.org/10.1016/j.ejor.2024.02.020).
In the GAMS files, we formulate all model equations as equalities in dependence of the data points to increase the readability of the model.

The measurement data are stored in columns with the first column being the Raman shift &#957; in 1/cm and the second column being the Raman intensity (dimensionless).
In a second csv file, we store the reference solution.
The first row contains the intercept and slope of the baseline.
Each of the subsequent rows contain the parameters of one component: the weight of the component-specific spectrum followed by the parameters of the peaks, i.e., tuples of the values for peak position &#948;, peak maximum &#945;, half width at half maximum &#947; , and Gaussian-Lorentzian-ratio &#946;.

The data as well as the reference solution were originally published in the following publications:
 - Echtermeyer, A., Marks, C., Mitsos, A., & Viell, J. (2021). [Inline Raman Spectroscopy and Indirect Hard Modeling for Concentration Monitoring of Dissociated Acid Species](https://www.doi.org/10.1177/0003702820973275). *Applied spectroscopy*, 75 (5), 506-519.
 - Echtermeyer, A. W. W., Marks, C., Mitsos, A., & Viell, J. (2024). [Dataset to ``Inline Raman Spectroscopy and Indirect Hard Modeling for Concentration Monitoring of Dissociated Acid Species''](https://www.doi.org/10.18154/RWTH-2024-01177). RWTH Aachen University.

The problems were originally implemented for the following publication:
 - Sass, S., Mitsos, A., Nikolov, N. I., & Tsoukalas, A. (2024b).  Out-of-sample estimation for a branch-and-bound algorithm with growing datasets. Submitted.
 
<!-- Empty spaces in last column are inserted to increase column width -->
| Instance        | Problem Type | Number of Continuous Variables | Number of Integer Variables | Number of Binary Variables | Number of Equalities | Number of Inequalities | Best Known Objective Value* | Proven Objective Bound* | Description&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|
| ------------- |-------------| ------------| ------------| ------------| ------------| ------------| :------------: | :-----: | ------------- |
| IndirectHardModeling_con | NLP | 20 | 0 | 0 | 10 | 0 |  1.433988e+07** | 2.619593e+05** | Model IHMcon of Sass et al. (2024b): Constraint fixing the peaks' area |
| IndirectHardModeling_unc | NLP | 10 | 0 | 0 | 0 | 0 |  1.41336e+07*** | 1.27477e+06*** | Model IHMunc of Sass et al. (2024b): Using constant area to reduce number of optimization variables |

*The values correspond to using the summed squared error as the objective function.

**Calculated with the standard B&B algorithm using the summed squared error as objective within a CPU time limit of 23h, see Sass et al. (2024b)

***Calculated with the SSE heuristic of the B&B algorithm with growing datasets without resampling and augmentation rule SCALING within a CPU time limit of 23h, see Sass et al. (2024b)