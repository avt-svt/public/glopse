* GMMcon model that was originally implemented for
*     S. Sass, A. Mitsos, N. I. Nikolov, A. Tsoukalas: "Out-of-sample
*          estimation for a branch-and-bound algorithm with growing datasets",
*          Submitted 2024.
* using the waiting time of dataset 'faithful' of
*     R Core Team: "R: A Language and Environment for Statistical Computing",
*          R Foundation for Statistical Computing, Vienna, Austria (2004).
*          https://www.R-project.org
* originally proposed by
*     W. Haerdle: "Smoothing techniques: With implementation in S", Springer
*          series in statistics, Springer, New York (1991).
*          https://www.doi.org/10.1007/978-1-4612-4432-5
*     A. Azzalini, A. W. Bowman: "A Look at Some Data on the Old Faithful
*          Geyser", Applied Statistics 39(3), 357-365 (1990).
*          https://www.doi.org/10.2307/2347385
*
* E-mail: amitsos@alum.mit.edu
*
* Copyright (c) 2024 Process Systems Engineering (AVT.SVT), RWTH Aachen University

*
* Optimization variables
*

*Continuous variables
Set
   idxCluster 'index of cluster'   / c1*c2  /
   ;
Variables phi(idxCluster), mean(idxCluster), stdDev(idxCluster), objectiveVar;

*Continuous variable bounds
phi.LO(idxCluster) = 0.1;
phi.UP(idxCluster) = 0.9;
mean.LO(idxCluster) = 40.;
mean.UP(idxCluster) = 100.;
stdDev.LO(idxCluster) = 5.;
stdDev.UP(idxCluster) = 300.;

*Use the following to start from the solution of Benaglia et al. (2009), doi: 10.18637/jss.v032.i06
*phi.L('c1') = 0.63915;
*mean.L('c1') = 80.1;
*stdDev.L('c1') = 5.9;
*phi.L('c2') = 0.36085;
*mean.L('c2') = 54.6;
*stdDev.L('c2') = 5.9;


*
* Constants and data
*

*Dataset
Set
   idxData  'index of data point'
   ivar 'index of state variable';

*Pick the dataset you want to optimize for 
$call csv2gdx ../data_oldFaithful_waitingTime.csv id=meas autoCol=dataPoint autoRow=stateVar colCount=272 values=1..lastCol fieldSep=SemiColon output=data.gdx

$gdxIn data.gdx
$load ivar = Dim1
$load idxData  = Dim2

Parameter meas(ivar,idxData) 'measurements';
$load meas
$gdxIn
*display meas;

*
* Model equations
*
* stateVar1 = waiting time

*Auxiliary variables
Variables
    standardizedData(idxCluster,idxData),
    normal(idxCluster,idxData), nlogNormal(idxCluster,idxData),
    gamma(idxCluster,idxData), nlogLikelihood(idxCluster,idxData),
    gammaSum(idxData), QCalc(idxData);

*Dummy values for denominators to avoid division by zero
gammaSum.L(idxData) = 1;

*Equation variables
Equations
    standardizeData(idxCluster,idxData)
    calculateNormal(idxCluster,idxData)
    calculateNLogNormal(idxCluster,idxData)
    calculateBayesian(idxCluster,idxData)
    sumGammaUp(idxData)
    calculateNLogLikelihood(idxCluster,idxData)
    calculateQCalc(idxData)
    objective
    sumMixingCoefficients
    symmetryBreak;

*Auxiliary equations
standardizeData(idxCluster,idxData).. standardizedData(idxCluster,idxData) =E= (meas('stateVar1',idxData) - mean(idxCluster))/stdDev(idxCluster);

calculateNormal(idxCluster,idxData).. normal(idxCluster,idxData) =E= 1/( sqrt(2*pi) * stdDev(idxCluster) )*exp( - sqr( standardizedData(idxCluster,idxData) ) / 2 );
calculateNLogNormal(idxCluster,idxData).. nlogNormal(idxCluster,idxData) =E= log( 2*pi ) / 2 + log( stdDev(idxCluster) ) + sqr( standardizedData(idxCluster,idxData) ) / 2;

sumGammaUp(idxData).. gammaSum(idxData) =E= sum(idxCluster, phi(idxCluster) * normal(idxCluster,idxData) );
calculateBayesian(idxCluster,idxData).. gamma(idxCluster,idxData) =E= phi(idxCluster) * normal(idxCluster,idxData) / gammaSum(idxData);

calculateNLogLikelihood(idxCluster,idxData).. nlogLikelihood(idxCluster,idxData) =E= - log( phi(idxCluster) ) + nlogNormal(idxCluster,idxData);
calculateQCalc(idxData).. QCalc(idxData) =E= sum(idxCluster, gamma(idxCluster,idxData) * nlogLikelihood(idxCluster,idxData) );
    
*Objective function
*SSE:
objective.. objectiveVar =E= sum(idxData, QCalc(idxData) );
*Analogue to MSE:
*objective .. objectiveVar =E= sum(idxData, QCalc(idxData) )/card(idxData);

*Equality: Mixing coefficients sum up to 1
sumMixingCoefficients.. 1 =E= sum(idxCluster, phi(idxCluster) );

*Inequality for breaking the symmetry
symmetryBreak .. mean('c2') =L= mean('c1');


*
* Optimization problem
*

*Model information and options
Model gmmCon / all /;

*Optimality tolerances, time and solver
option OPTCA = 0.01;
option OPTCR = 0.01;
option RESLIM = 82800;
option NLP = BARON;

*Solve statement
solve gmmCon using NLP minimizing objectiveVar;