# GaussianMixtureModel1D

Parameter estimation problem for fitting a Gaussian mixture model with _K_ = 2 clusters based on one-dimensional data points.
In the C++ header files for MAiNGO, we provide the summed squared prediction error and the mean squared prediction error as the objective for the standard B&B algorithm as well as the squared prediction error for each data point as the objective per data used in the B&B algorithm with growing datasets, see [manual of MAiNGO](https://avt-svt.pages.rwth-aachen.de/public/maingo/special_uses.html#growing_datasets) and [Sass et al. (2024a)](https://doi.org/10.1016/j.ejor.2024.02.020).
For changing the number of clusters, please adapt parameter \_K, see Line 94 of *GaussianMixtureModel1D_con.h* and *GaussianMixtureModel1D_ineq.h*.
In the GAMS files, we formulate all model equations as equalities in dependence of the data points to increase the readability of the model.

The data is stored in rows to enable an efficient reading of the data into the MAiNGO model.
The dataset contains the waiting times of geyser _Old_ _faithful_ in Yellowstone National Park, Wyoming, USA which was originally proposed by
 - Härdle, W. (1991). [Smoothing techniques: With implementation in S](https://www.doi.org/10.1007/978-1-4612-4432-5). *Springer series in statistics*, Springer, New York.
 - Azzalini, A., Bowman, A. W. (1990). [A Look at Some Data on the Old Faithful Geyser](https://www.doi.org/10.2307/2347385). *Applied Statistics*, 39 (3), 357–365.
 
We use a version equal to the built-in dataset *faithful* within software R
- R Core Team (2004). [R: A Language and Environment for Statistical
                  Computing](https://www.R-project.org). *R Foundation for Statistical Computing}*, Vienna, Austria

which is also accessible in various other online resources like git repositories or electronic supplementary information.
Note that [Azzalini and Bowman (1990)](https://www.doi.org/10.2307/2347385) propose an extended version which is included in R package *MASS* supporting [Venables and Ripley (2002)](https://www.doi.org/10.1007/978-0-387-21706-2).

The model is based on the established maximum likelihood problem.
The problem was originally implemented for the following publication:
 - Sass, S., Mitsos, A., Nikolov, N. I., & Tsoukalas, A. (2024b).  Out-of-sample estimation for a branch-and-bound algorithm with growing datasets. Submitted.
 
| Instance        | Problem Type | Number of Continuous Variables | Number of Integer Variables | Number of Binary Variables | Number of Equalities | Number of Inequalities | Best Known Objective Value* | Proven Objective Bound* | Description           |
| ------------- |-------------| ------------| ------------| ------------| ------------| ------------| :------------: | :-----: | ----- |
| GaussianMixtureModel1D_con | NLP | 6** | 0 | 0 | 1 | 1** |  1079.3520*** | 135.9964*** | Model GMMcon of Sass et al. (2024b) |
| GaussianMixtureModel1D_ineq | NLP | 5**** | 0 | 0 | 0 | 1*** |  1044.1270*** | 154.2456*** | Model GMMineq of Sass et al. (2024b) |

*The values correspond to using the summed squared error as the objective function.

**When using _K_ clusters, we have 3&#183;_K_ continuous variables and _K_-1 inequality constraints

***Calculated with the standard B&B algorithm with the summed squared error as the objective function within a CPU time limit of 23h, see Sass et al. (2024b)

****When using _K_ > 2 clusters, we have 3&#183;_K_ - 1  continuous variables and _K_ inequality constraints