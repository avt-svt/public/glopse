/**
 *
 * @brief GMMineq model that was originally implemented for
 *        S. Sass, A. Mitsos, N. I. Nikolov, A. Tsoukalas: "Out-of-sample
 *              estimation for a branch-and-bound algorithm with growing datasets",
 *              Submitted 2024.
 *        using the waiting time of dataset 'faithful' of
 *        R Core Team: "R: A Language and Environment for Statistical Computing",
 *              R Foundation for Statistical Computing, Vienna, Austria (2004).
 *              https://www.R-project.org
 *        originally proposed by
 *        W. Härdle: "Smoothing techniques: With implementation in S", Springer
 *              series in statistics, Springer, New York (1991).
 *              https://www.doi.org/10.1007/978-1-4612-4432-5
 *        A. Azzalini, A. W. Bowman: "A Look at Some Data on the Old Faithful
 *              Geyser", Applied Statistics 39(3), 357–365 (1990).
 *              https://www.doi.org/10.2307/2347385
 *
 *        E-mail: amitsos@alum.mit.edu
 *
 * ==============================================================================
 * © 2024, Process Systems Engineering (AVT.SVT), RWTH Aachen University
 * ==============================================================================
 */

#pragma once

#include "MAiNGOmodel.h"


namespace userInput {
    // Enter the path to the directory containing the csv-file with the data
    std::string pathToCSVfile = "../GloPSE/GaussianMixtureModel1D/";
}

///////////////
//// 1. Model-specific auxiliary functions etc.: None
///////////////


///////////////
//// 2. Data input
///////////////

//////////////////////////////////////////////////////////////////////////
// auxiliary function for reading data from csv-file
void read_csv_to_vector(const std::string name, std::vector<double> &data)
{
    // Open data file
    std::ifstream infile;
    infile.open(name);
    if (infile.fail()) {
        throw std::runtime_error("Error in model: Unable to open data file "+name);
    }
    else {
        std::cout << "Read data from " << name << std::endl;
    }

    std::string line, tmpCooStr;
    double tmpCoo;
    data.clear();
    // Read (the only) line
    std::getline(infile, line);
    // Split line into coordinates
    std::istringstream stream(line);
    // First entry
    stream >> tmpCoo;
    data.push_back(tmpCoo);
    // Read all remaining entries
    while (std::getline(stream, tmpCooStr, ';')) {
        stream >> tmpCoo;
        data.push_back(tmpCoo);
    }
}


///////////////
//// 3. MAiNGO model
///////////////

class Model: public maingo::MAiNGOmodel {

  public:

    Model();

    maingo::EvaluationContainer evaluate(const std::vector<Var> &optVars);
    std::vector<maingo::OptimizationVariable> get_variables();
    std::vector<double> get_initial_point();

  private:
    // Fixed parameters
    const unsigned int _noOfClusters = 2;

    // Measurement data
    unsigned int _noOfDataPoints;   // Number of data points in full dataset
    std::vector<double> _dataPoint; // Full dataset
};


//////////////////////////////////////////////////////////////////////////
// function for providing optimization variable data to the Branch-and-Bound solver
std::vector<maingo::OptimizationVariable>
Model::get_variables()
{

    std::vector<maingo::OptimizationVariable> variables;

    // Mixing coefficients
    for (auto k = 0; k < _noOfClusters-1; k++) {
        variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.1, 0.9), maingo::VT_CONTINUOUS, "phi_" + std::to_string(k)));
    }

    // Gaussian distribution
    for (auto k = 0; k < _noOfClusters; k++) {
        // Mean
        variables.push_back(maingo::OptimizationVariable(maingo::Bounds(40., 100.), maingo::VT_CONTINUOUS, "mu_" + std::to_string(k)));
        // Standard deviation
        variables.push_back(maingo::OptimizationVariable(maingo::Bounds(5., 300.), maingo::VT_CONTINUOUS, "stdDev_" + std::to_string(k)));
    }

    return variables;
}


//////////////////////////////////////////////////////////////////////////
// function for providing initial point data to the Branch-and-Bound solver
std::vector<double>
Model::get_initial_point()
{
    std::vector<double> initialPoint;

    // Use the following to start the optimization from the optimal solution
    // of Benaglia et al. (2009), doi: 10.18637/jss.v032.i06
    /*
    std::vector<double> solnLiterature = {0.63915, 80.1, 5.9, 54.6, 5.9};
    initialPoint = solnLiterature;
    */

    return initialPoint;
}


//////////////////////////////////////////////////////////////////////////
// constructor for the model
Model::Model()
{
    // Catch measurement data from file
    std::string fileData = userInput::pathToCSVfile +  "data_oldFaithful_waitingTime.csv";
    read_csv_to_vector(fileData, _dataPoint);

    // Catch size of full dataset
    _noOfDataPoints = _dataPoint.size();
}


//////////////////////////////////////////////////////////////////////////
// evaluate the model
maingo::EvaluationContainer
Model::evaluate(const std::vector<Var> &optVars)
{

    // Catch optimization variables
    // The vector optVars is of the same size and sorted in the same order as the user-defined variables vector in function get_variables()
    unsigned int iVar = 0;

    std::vector<Var> phi;
    phi.resize(_noOfClusters);
    Var sumPhi = 0;
    for (auto k = 0; k < _noOfClusters -1; k++) {
        phi[k] = optVars.at(iVar);
        iVar++;
        sumPhi = sumPhi + phi[k];
    }

    std::vector<Var> mean;
    std::vector<Var> stdDev;
    mean.resize(_noOfClusters);
    stdDev.resize(_noOfClusters);
    for (auto k = 0; k < _noOfClusters; k++) {
        mean[k] = optVars.at(iVar);
        iVar++;
        stdDev[k] = optVars.at(iVar);
        iVar++;
    }

    // Model
    maingo::EvaluationContainer result;

    std::vector<std::vector<Var>> Normal, logNormal;
    Var dataTrans;
    std::vector<std::vector<Var>> gamma, l;
    Var ExpQd, gamma_sum;
 
    // Last mixing coefficient is fixed: all coefficients sum up to 1
    phi[_noOfClusters-1] = 1. - sumPhi;

    // (log-)Density funtion of normal distribution
    Normal.resize(_noOfDataPoints);
    logNormal.resize(_noOfDataPoints);
    for (auto d = 0; d < _noOfDataPoints; d++) {
        Normal[d].resize(_noOfClusters);
        logNormal[d].resize(_noOfClusters);
        for (auto k = 0; k < _noOfClusters; k++) {
            // Transform into standard normal distribution to use specific relaxations
            dataTrans = (_dataPoint[d] - mean[k]) / stdDev[k];
            Normal[d][k] = gaussian_probability_density_function(dataTrans) / stdDev[k];

            // - log(N_d,k)
            logNormal[d][k] = log(2 * M_PI) / 2 + log(stdDev[k]) + sqr(dataTrans) / 2;
        }
    }

    // Calculate Qd
    Var summedExpQ = 0; // Sum of Qd (objective function)
    gamma.resize(_noOfDataPoints);
    l.resize(_noOfDataPoints);
    for(auto d = 0; d < _noOfDataPoints; d++){
        gamma[d].resize(_noOfClusters);
        l[d].resize(_noOfClusters);

        // Denominator of Bayesian term
        gamma_sum = 0;
        for (auto j = 0; j < _noOfClusters; j++) {
            gamma_sum += phi[j] * Normal[d][j];
        }

        // Summing up expected value for each cluster
        ExpQd = 0;
        for(auto k = 0; k < _noOfClusters; k++){
            // Bayesian term
            gamma[d][k] = phi[k] * Normal[d][k] / gamma_sum;

            // negative log-Likelihood
            l[d][k] = logNormal[d][k] - log(phi[k]);

            // Summing up expectation value Qd
            ExpQd = ExpQd + gamma[d][k] * l[d][k];
        }
        summedExpQ += ExpQd;

        // Objective for B&B algorithm with growing datasets
        result.objective_per_data.push_back(ExpQd);
    }
    // Objective for standard B&B algorithm
    result.objective = summedExpQ;
//    result.objective = summedExpQ / _noOfDataPoints; // Analogue to Mean squared error (MSE)

    // Inequality for bounding last mixing coefficient
    if(_noOfClusters > 2){
        // For up to 2 clusters, this is guaranteed by the bounds on optimization variable phi 
        result.ineq.push_back(-phi[_noOfClusters-1], "0<= phi[last]");
    }

    // Inequality for breaking the symmetry
    for(auto k=1; k<_noOfClusters; k++){
        result.ineq.push_back(mean[k] - mean[k-1], "mean[" + std::to_string(k) + "] <= mean[" + std::to_string(k-1) + "]");
    }

    return result;
}
