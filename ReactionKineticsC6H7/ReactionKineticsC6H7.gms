* Kinetics model based on
*     J. W. Taylor, G. Ehlker, H.-H. Carstensen, L. Ruslen, R. W. Field,
*          W. H. Green: "Direct Measurement of the Fast, Reversible Addition
*          of Oxygen to Cyclohexadienyl Radicals in Nonpolar  Solvents", The
*          Journal of Physical Chemistry A 108(35), 7193-7203 (2004).
*          https://www.doi.org/10.1021/jp0379547
*     A. B. Singer, J. W. Taylor, P. I. Barton, W. H. Green: "Global Dynamic
*          Optimization for Parameter Estimation In Chemical Kinetics", The
*          Journal of Physical Chemistry A 110(3), 971-976 (2006).
*          https://www.doi.org/10.1021/jp0548873
*     A. Mitsos, B. Chachuat, P. I. Barton: "McCormick-Based Relaxations of
*          Algorithms", SIAM Journal on Optimization 20(2), 573-601 (2009).
*          https://www.doi.org/10.1137/080717341
* that was originally implemented for
*     S. Sass, A. Mitsos, N. I. Nikolov, A. Tsoukalas: "Out-of-sample
*          estimation for a branch-and-bound algorithm with growing datasets",
*          Submitted 2024.
* E-mail: amitsos@alum.mit.edu
*
* Copyright (c) 2024 Process Systems Engineering (AVT.SVT), RWTH Aachen University

*
* Optimization variables
*

*Continuous variables
Variables k1, k2f, k3f, k4, k5, objectiveVar;

*Continuous variable bounds
k1.LO = 10.;
k1.UP = 1200.;
k2f.LO = 10.;
k2f.UP = 1200.;
k3f.LO = 10.;
k3f.UP = 1200.;
k4.LO = 0.001;
k4.UP = 40.;
k5.LO = 0.001;
k5.UP = 40.;

*Use the following to start from the optimal solution of Mitsos et al. (2009) and Singer et al. (2006)
*k1.L = 53.;
*k2f.L = 828.;
*k3f.L = 394.;
*k4.L = 13.5;
*k5.L = 0.0012;


*
* Constants and data
*

*Constants
Scalar
    xO2 'Fixed parameter' / 0.002 /
    T 'Temperature in Kelvin' / 273.0 /
    K2inv 'Reciprocal of fixed parameter K2 (dummy initialization)' / 1 /
    K3inv 'Reciprocal of fixed parameter K3 (dummy initialization)' / 1 /
    dt 'step width of explicit Euler scheme' / 0.01 /
    ;
K2inv = 1./(46*exp(6500/T-18));
K3inv = 0.5*K2inv;

*Time discretization
Set
    idxTime 'time index in Euler loop' / t0 * t446 /
    idxTime0(idxTime) 'initial time point';
    idxTime0(idxTime) = yes$(ord(idxTime) = 1);

*Dataset
Set
   idxData(idxTime)  'time index of data point (t1 ... t446)'
   ivar 'index of state variable';

*Pick the dataset you want to optimize for 
$call csv2gdx data_MitsosEtAl2009.csv id=meas autoCol=t autoRow=output colCount=447 values=2..lastCol fieldSep=SemiColon output=data.gdx

$gdxIn data.gdx
$load ivar  = Dim1
$load idxData = Dim2

Parameter meas(ivar,idxData) 'measurements';
$load meas
$gdxIn
*display meas;

*
* Model equations
*

*Differential states and auxiliary variables
Set xName 'label of differential state'   / A, B, D, Y, Z /;
Variables
    x(xName,idxTime), ddtx(xName,idxTime), yCalc(idxTime);

*Initial values
x.fx('A',idxTime0) = 0.;
x.fx('B',idxTime0) = 0.;
x.fx('D',idxTime0) = 0.;
x.fx('Y',idxTime0) = 0.4;
x.fx('Z',idxTime0) = 140.;

*Equation variables
Equations
    rhsXA(idxTime)
    rhsXB(idxTime)
    rhsXD(idxTime)
    rhsXY(idxTime)
    rhsXZ(idxTime)
    ode(xName,idxTime)
    calculateY(idxTime)
    objective;

*Auxiliary equations
rhsXA(idxTime).. ddtx('A',idxTime) =E= k1 * x('Z',idxTime) * x('Y',idxTime) - xO2 * (k2f + k3f) * x('A',idxTime) + k2f*K2inv * x('D',idxTime)
                                             + k3f*K3inv * x('B',idxTime) - k5 * x('A',idxTime);
rhsXB(idxTime).. ddtx('B',idxTime) =E= k3f*xO2* x('A',idxTime) - (k3f*K3inv + k4) * x('B',idxTime);
rhsXD(idxTime).. ddtx('D',idxTime) =E= k2f*xO2* x('A',idxTime) - k2f*K2inv * x('D',idxTime);
rhsXY(idxTime).. ddtx('Y',idxTime) =E= -k1*1e-6 * x('Z',idxTime) * x('Y',idxTime);
rhsXZ(idxTime).. ddtx('Z',idxTime) =E= -k1 * x('Z',idxTime) * x('Y',idxTime);

ode(xName,idxTime+1).. x(xName, idxTime+1) =E= x(xName,idxTime) + dt*ddtx(xName,idxTime);

calculateY(idxTime).. yCalc(idxTime) =E= x('A',idxTime) + 2/21 * x('B',idxTime) + 2/21 * x('D',idxTime);

*Objective function
*SSE:
objective .. objectiveVar =E= sum(idxData, sqr(yCalc(idxData)-meas('output1',idxData)) );
*MSE
*objective .. objectiveVar =E= sum(idxData, sqr(yCalc(idxData)-meas('output1',idxData)) )/card(idxData);


*
* Optimization problem
*

*Model information and options
Model kinetics / all /;

*Optimality tolerances, time and solver
option OPTCA = 0.01;
option OPTCR = 0.01;
option RESLIM = 82800;
option NLP = BARON;

*Solve statement
solve kinetics using NLP minimizing objectiveVar;