# ReactionKineticsC6H7

Parameter estimation problem for fitting the kinetic mechanism of a chemical reaction of cyclohexadienyl radicals C<sub>6</sub>H<sub>7</sub>.
Note that we transformed the dynamic optimization problem discussed by [Taylor et al. (2004)](https://www.doi.org/10.1021/jp0379547) and [Singer et al. (2006)](https://www.doi.org/10.1021/jp0548873) into an algebraic optimization problem by applying the explicit Euler scheme, compare also [Mitsos et al. (2009)](https://www.doi.org/10.1137/080717341). 
In the C++ header files for MAiNGO, we provide the summed squared prediction error and the mean squared prediction error as the objective for the standard B&B algorithm as well as the squared prediction error for each data point as the objective per data used in the B&B algorithm with growing datasets, see [manual of MAiNGO](https://avt-svt.pages.rwth-aachen.de/public/maingo/special_uses.html#growing_datasets) and [Sass et al. (2024a)](https://doi.org/10.1016/j.ejor.2024.02.020).
In the GAMS file, we formulate all model equations as equalities in dependence of the data points to increase the readability of the model.

The data given by the optical density of the output variable is stored in a row to enable an efficient reading of the data into the MAiNGO model.
The columns represent time points 0, 0.01, 0.02, ..., 4.46 &#956;s.

The general model was originally proposed in the following publications:
 - Taylor, J. W., Ehlker, G., Carstensen, H.-H., Ruslen, L., Field, R. W., & Green, W. H. (2004). [Direct Measurement of the Fast, Reversible Addition of Oxygen to Cyclohexadienyl Radicals in Nonpolar Solvents](https://www.doi.org/10.1021/jp0379547). *The Journal of Physical Chemistry A*, 108(35), 7193-7203.
 - Singer, A. B., Taylor, J. W., Barton, P. I., & Green, W. H. (2006). [Global Dynamic Optimization for Parameter Estimation In Chemical  Kinetics](https://www.doi.org/10.1021/jp0548873). *The Journal of Physical Chemistry A*, 110(3), 971-976.
 - Mitsos, A., Chachuat, B., & Barton, P. I. (2009). [McCormick-Based Relaxations of Algorithms](https://www.doi.org/10.1137/080717341). *SIAM Journal on Optimization*, 20(2), 573-601.

The problem was originally implemented for the proposal of DFG grant MI 1851/10-1 ``Parameter estimation with (almost) deterministic global optimization'' with the help of Jaromi&#322; Najman and for the following publication:
 - Sass, S., Mitsos, A., Nikolov, N. I., & Tsoukalas, A. (2024b).  Out-of-sample estimation for a branch-and-bound algorithm with growing datasets. Submitted.
 
| Instance        | Problem Type | Number of Continuous Variables | Number of Integer Variables | Number of Binary Variables | Number of Equalities | Number of Inequalities | Best Known Objective Value* | Proven Objective Bound* | Description           |
| ------------- |-------------| ------------| ------------| ------------| ------------| ------------| :------------: | :-----: | ----- |
| ReactionKineticsC6H7_kinetics | NLP-DO | 5 | 0 | 0 | 0 | 0 |  495.68** | 1.1843** | Model kinetics of Sass et al. (2024b) |

*The values correspond to using the summed squared error as the objective function.

**Calculated with the standard B&B algorithm with the mean squared error as the objective function within a CPU time limit of 23h, see Sass et al. (2024b)