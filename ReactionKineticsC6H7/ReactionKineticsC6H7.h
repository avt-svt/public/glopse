/**
 *
 * @brief Kinetics model based on
 *        J. W. Taylor, G. Ehlker, H.-H. Carstensen, L. Ruslen, R. W. Field,
 *              W. H. Green: "Direct Measurement of the Fast, Reversible Addition
 *              of Oxygen to Cyclohexadienyl Radicals in Nonpolar  Solvents", The
 *              Journal of Physical Chemistry A 108(35), 7193-7203 (2004).
 *              https://www.doi.org/10.1021/jp0379547
 *        A. B. Singer, J. W. Taylor, P. I. Barton, W. H. Green: "Global Dynamic
 *              Optimization for Parameter Estimation In Chemical Kinetics", The
 *              Journal of Physical Chemistry A 110(3), 971-976 (2006).
 *              https://www.doi.org/10.1021/jp0548873
 *        A. Mitsos, B. Chachuat, P. I. Barton: "McCormick-Based Relaxations of
 *              Algorithms", SIAM Journal on Optimization 20(2), 573-601 (2009).
 *              https://www.doi.org/10.1137/080717341
 *        that was originally implemented with the help of Jaromil Najman for
 *        S. Sass, A. Mitsos, N. I. Nikolov, A. Tsoukalas: "Out-of-sample
 *              estimation for a branch-and-bound algorithm with growing datasets",
 *              Submitted 2024.
 *        E-mail: amitsos@alum.mit.edu
 *
 * ==============================================================================
 * © 2024, Process Systems Engineering (AVT.SVT), RWTH Aachen University
 * ==============================================================================
 */

#pragma once

#include "MAiNGOmodel.h"


namespace userInput {
    // Enter the path to the directory containing the csv-file with the data
    std::string pathToCSVfile = "../GloPSE/ReactionKineticsC6H7/";
}

///////////////
//// 1. Model-specific auxiliary functions etc.: None
///////////////


///////////////
//// 2. Data input
///////////////

//////////////////////////////////////////////////////////////////////////
// auxiliary function for reading data from csv-file
void read_csv_to_vector(const std::string name, std::vector<double> &data)
{
    // Open data file
    std::ifstream infile;
    infile.open(name);
    if (infile.fail()) {
        throw std::runtime_error("Error in model: Unable to open data file "+name);
    }
    else {
        std::cout << "Read data from " << name << std::endl;
    }

    std::string line, tmpCooStr;
    double tmpCoo;
    data.clear();
    // Read (the only) line
    std::getline(infile, line);
    // Split line into coordinates
    std::istringstream stream(line);
    // First entry
    stream >> tmpCoo;
    data.push_back(tmpCoo);
    // Read all remaining entries
    while (std::getline(stream, tmpCooStr, ';')) {
        stream >> tmpCoo;
        data.push_back(tmpCoo);
    }
}


///////////////
//// 3. MAiNGO model
///////////////

class Model: public maingo::MAiNGOmodel {

  public:

    Model();

    maingo::EvaluationContainer evaluate(const std::vector<Var> &optVars);
    std::vector<maingo::OptimizationVariable> get_variables();
    std::vector<double> get_initial_point();

  private:
    // Time discretization
    const double _dt_data = 0.01;  // Time discretization of data
    const double _dt_euler = 0.01; // Time discretization of Euler loop: necessarily _dt_euler <= _dt_data
    const double _tf = 4.46;       // Final time

    // Fixed parameters
    const double xA0, xB0, xD0, xY0, xZ0; // Initial values at t=0
    const double T, K2inv, K3inv, xO2;    // Known parameters
    
    // Measurement data
    unsigned int _noOfDataPoints;   // Number of data points in full dataset
    std::vector<double> _dataPoint; // Full dataset
};


//////////////////////////////////////////////////////////////////////////
// function for providing optimization variable data to the Branch-and-Bound solver
std::vector<maingo::OptimizationVariable>
Model::get_variables()
{

    std::vector<maingo::OptimizationVariable> variables;
    
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(10., 1200.), maingo::VT_CONTINUOUS, "k1"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(10., 1200.), maingo::VT_CONTINUOUS, "k2f"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(10., 1200.), maingo::VT_CONTINUOUS, "k3f"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.001, 40.), maingo::VT_CONTINUOUS, "k4"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.001, 40.), maingo::VT_CONTINUOUS, "k5"));

    return variables;
}


//////////////////////////////////////////////////////////////////////////
// function for providing initial point data to the Branch-and-Bound solver
std::vector<double>
Model::get_initial_point()
{
    std::vector<double> initialPoint;

    // Use the following to start the optimization from the optimal solution
    // of Mitsos et al. (2009) and Singer et al. (2006)
    /*
    std::vector<double> solnLiterature = {53., 828., 394., 13.5, 0.0012};
    initialPoint = solnLiterature;
    */

    return initialPoint;
}


//////////////////////////////////////////////////////////////////////////
// constructor for the model
Model::Model():
    xA0(0.0),xB0(0.0),xD0(0.0),xY0(0.4),xZ0(140.0),xO2(0.002),T(273.0),K2inv(1./(46*exp(6500/T-18))),K3inv(0.5*K2inv)
{
    // Catch measurement data from file
    std::string fileData = userInput::pathToCSVfile +  "data_MitsosEtAl2009.csv";
    read_csv_to_vector(fileData, _dataPoint);

    // Catch size of full dataset
    _noOfDataPoints = _dataPoint.size();
}


//////////////////////////////////////////////////////////////////////////
// evaluate the model
maingo::EvaluationContainer
Model::evaluate(const std::vector<Var> &optVars)
{
    // Catch optimization variables
    // The vector optVars is of the same size and sorted in the same order as the user-defined variables vector in function get_variables()
    unsigned int iVar = 0;
    Var k1 = optVars.at(iVar);
    iVar++;
    Var k2f = optVars.at(iVar);
    iVar++;
    Var k3f = optVars.at(iVar);
    iVar++;
    Var k4 = optVars.at(iVar);
    iVar++;
    Var k5 = optVars.at(iVar);

    // Model
    maingo::EvaluationContainer result;

    Var k1s = k1 * 1e-6;

    // Set up time discretization
    const size_t nt = std::trunc(_tf / _dt_euler);
    std::vector<Var> y(nt); // Output without fixed initial date at t=0

    // Initial values
    Var xA=xA0;
    Var xB=xB0;
    Var xD=xD0;
    Var xY=xY0;
    Var xZ=xZ0;

    // Euler loop
    for (size_t it=0;it<nt;it++) {
        Var xA_new = xA*(1 - _dt_euler * xO2 * ( k2f + k3f ) - _dt_euler * k5 * xA)
                           + _dt_euler * k1 * xZ * xY
                           + _dt_euler * k2f * K2inv * xD
                           + _dt_euler * k3f * K3inv * xB;
        Var xB_new = xB*(1 - _dt_euler * ( k3f*K3inv+k4 )) + _dt_euler * k3f * xO2 * xA;
        Var xD_new = xD*(1 - _dt_euler * k2f * K2inv) + _dt_euler * k2f * xO2 * xA;
        Var xY_new = xY*(1 - _dt_euler * k1s * xZ);
        Var xZ_new = xZ*(1 - _dt_euler * k1 * xY);

        xA = xA_new;
        xB = xB_new;
        xY = xY_new;
        xD = xD_new;
        xZ = xZ_new;
        
        y[it]= xA + 2./21. * xB + 2./21. * xD;
    }

    // Compare to measurements
    Var se = 0; // Summed squared prediction error (SSE)
    const double noOfInnerSteps = std::trunc(_dt_data / _dt_euler);
    for (size_t t = 1; t < _noOfDataPoints; t++) {
        // Recall index shift: y[0] = y(t=t_1) since we do not fit initial value
        Var sePerData = sqr(y[(t-1)*noOfInnerSteps] - _dataPoint[t]);
        se += sePerData;
        
        // Objective for B&B algorithm with growing datasets
        result.objective_per_data.push_back(sePerData);
    }
    // Objective for standard B&B algorithm
    result.objective = se; // SSE
//    result.objective = se / _noOfDataPoints; // Mean squared error (MSE)

    return result;
}