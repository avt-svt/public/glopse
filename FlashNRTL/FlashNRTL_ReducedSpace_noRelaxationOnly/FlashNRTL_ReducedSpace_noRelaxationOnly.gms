* ------------------------------------------------------------------------------------------------------------------- *
*                                                                                                          /)_        *
*                                                                                                         //\  `.     *
*                                                                                                  ____,.//, \   \    *
*                           This file was generated by MAiNGO v0.3.0                           _.-'         `.`.  \   *
*                                                                                            ,'               : `..\  *
*                                                                                           :         ___      :      *
* Copyright (c) 2019 Process Systems Engineering (AVT.SVT), RWTH Aachen University         :       .'     `.    :     *
*                                                                                         :         `.    /     ;     *
* MAiNGO and the accompanying materials are made available under the                     :           /   /     ;      *
* terms of the Eclipse Public License 2.0 which is available at                         :        __.'   /     :       *
* http://www.eclipse.org/legal/epl-2.0.                                                 ;      /       /     :        *
*                                                                                       ;      `------'     /         *
* SPDX-License-Identifier: EPL-2.0                                                      :                  :          *
* Authors: Dominik Bongartz, Jaromil Najman, Susanne Sass, Alexander Mitsos             \                 /           *
*                                                                                        `.             .`            *
* Please provide all feedback and bugs to the developers.                                  '-._     _.-'              *
* E-mail: MAiNGO@avt.rwth-aachen.de                                                            `'''`                  *
* ------------------------------------------------------------------------------------------------------------------- *

*Continuous variables
variables T, p, V_F, x_0, x_1, x_2, y_0, y_1, y_2, objectiveVar;

*Continuous variable bounds
T.LO = 200;
T.UP = 298;
p.LO = 1;
p.UP = 15;
V_F.LO = 0;
V_F.UP = 1;
x_0.LO = 1e-06;
x_0.UP = 1;
x_1.LO = 1e-06;
x_1.UP = 1;
x_2.LO = 1e-06;
x_2.UP = 1;
y_0.LO = 1e-06;
y_0.UP = 1;
y_1.LO = 1e-06;
y_1.UP = 1;
y_2.LO = 1e-06;
y_2.UP = 1;

*Equation variables
equations objective,
ineq1,

eq1,
eq2,
eq3,
eq4,
eq5,
eq6,
eq7;

*Objective function
objective .. objectiveVar =E= -((V_F-1)*((1*x_0/(1*x_0+1*x_1*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T))+1*x_2*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T)))*((((2.7322-617.269/T+0*log(T)+0*T)*1*x_1*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T))/(1*x_1*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T))+1*x_0+1*x_2*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T)))+(0-18.9372/T+0*log(T)+0*T)*1*x_2*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T))/(1*x_2*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T))+1*x_0+1*x_1*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T))))*0.3-((2.7322-617.269/T+0*log(T)+0*T)*0.3-1))*x_1*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T))*(0+617.269/power(T,2)+0/T)+(((2.7322-617.269/T+0*log(T)+0*T)*1*x_1*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T))/(1*x_1*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T))+1*x_0+1*x_2*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T)))+(0-18.9372/T+0*log(T)+0*T)*1*x_2*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T))/(1*x_2*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T))+1*x_0+1*x_1*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T))))*0.2951-((0-18.9372/T+0*log(T)+0*T)*0.2951-1))*x_2*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T))*(0+18.9372/power(T,2)+0/T))-1*x_1/(1*x_1+1*x_0*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))+1*x_2*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T))))*((((0.6929999999999999-(172.987/T)-(0*log(T))-(0*T))*1*x_0*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))/(1*x_0*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))+1*x_1+1*x_2*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T))))+(0.223052-(611.456/T)-(0*log(T))-(0*T))*1*x_2*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T)))/(1*x_2*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T)))+1*x_0*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))+1*x_1))*0.3-((0.6929999999999999-(172.987/T)-(0*log(T))-(0*T))*0.3+1))*x_0*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))*(0-172.987/power(T,2)+0/T)+(((0.6929999999999999-(172.987/T)-(0*log(T))-(0*T))*1*x_0*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))/(1*x_0*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))+1*x_1+1*x_2*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T))))+(0.223052-(611.456/T)-(0*log(T))-(0*T))*1*x_2*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T)))/(1*x_2*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T)))+1*x_0*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))+1*x_1))*0.362916-((0.223052-(611.456/T)-(0*log(T))-(0*T))*0.362916+1))*x_2*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T)))*(0-611.456/power(T,2)+0/T))+1*x_2/(1*x_2+1*x_0*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))+1*x_1*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T)))*((((0+653.006/T+0*log(T)+0*T)*1*x_0*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))/(1*x_0*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))+1*x_1*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T))+1*x_2)+(3.59543-550.5/T+0*log(T)+0*T)*1*x_1*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T))/(1*x_1*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T))+1*x_0*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))+1*x_2))*0.2951-((0+653.006/T+0*log(T)+0*T)*0.2951-1))*x_0*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))*(0-653.006/power(T,2)+0/T)+(((0+653.006/T+0*log(T)+0*T)*1*x_0*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))/(1*x_0*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))+1*x_1*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T))+1*x_2)+(3.59543-550.5/T+0*log(T)+0*T)*1*x_1*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T))/(1*x_1*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T))+1*x_0*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))+1*x_2))*0.362916-((3.59543-550.5/T+0*log(T)+0*T)*0.362916-1))*x_1*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T))*(0+550.5/power(T,2)+0/T)))*sqr(T)*8.3144598-(x_0*(39.252*(T-(298.15))+168460.35*(1/(1-2/(exp(2*1916.5/T)+1))-1/(1-2/(exp(2*1916.5/(298.15))+1)))-48111.54180000001*(1-2/(exp(2*896.7/T)+1)-(1-2/(exp(2*896.7/(298.15))+1)))-200930-32615*exp(-(1.0407-(1.8695*T/512.5)+0.6080100000000001*power(T/512.5,2)-(0*power(T/512.5,3)))*log(1-T/512.5)))+x_1*(33.363*(T-(298.15))+69935.295*(1/(1-2/(exp(2*2610.5/T)+1))-1/(1-2/(exp(2*2610.5/(298.15))+1)))-10399.424*(1-2/(exp(2*1169/T)+1)-(1-2/(exp(2*1169/(298.15))+1)))-241818-56600*exp((0.61204-0.6257*T/647.1+0.3988*power(T/647.1,2)+0*power(T/647.1,3))*log(1-T/647.1)))+x_2*(57.431*(T-(298.15))+84620.32193999999*(1/(1-2/(exp(2*895.51/T)+1))-1/(1-2/(exp(2*895.51/(298.15))+1)))-160541.381*(1-2/(exp(2*2467.4/T)+1)-(1-2/(exp(2*2467.4/(298.15))+1)))-184100-26377*exp(-(0.072806-(0.5432399999999999*T/400.1)+0.13977*power(T/400.1,2)-(0*power(T/400.1,3)))*log(1-T/400.1)))))+V_F*(y_0*(39.252*(T-(298.15))+168460.35*(1/(1-2/(exp(2*1916.5/T)+1))-1/(1-2/(exp(2*1916.5/(298.15))+1)))-48111.54180000001*(1-2/(exp(2*896.7/T)+1)-(1-2/(exp(2*896.7/(298.15))+1)))-200930)+y_1*(33.363*(T-(298.15))+69935.295*(1/(1-2/(exp(2*2610.5/T)+1))-1/(1-2/(exp(2*2610.5/(298.15))+1)))-10399.424*(1-2/(exp(2*1169/T)+1)-(1-2/(exp(2*1169/(298.15))+1)))-241818)+y_2*(57.431*(T-(298.15))+84620.32193999999*(1/(1-2/(exp(2*895.51/T)+1))-1/(1-2/(exp(2*895.51/(298.15))+1)))-160541.381*(1-2/(exp(2*2467.4/T)+1)-(1-2/(exp(2*2467.4/(298.15))+1)))-184100))+237934.288431935);

*Inequalities
ineq1 .. -(y_2-0.99) =L= 0;

*Equalities
eq1 .. x_0*(V_F-1)-(V_F*y_0)+0.1668 =E= 0;
eq2 .. p*y_0-x_0*exp(-(1*x_0/(1*x_0+1*x_1*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T))+1*x_2*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T)))*((2.7322-617.269/T+0*log(T)+0*T)*1*x_1*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T))/(1*x_1*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T))+1*x_0+1*x_2*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T)))+(0-18.9372/T+0*log(T)+0*T)*1*x_2*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T))/(1*x_2*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T))+1*x_0+1*x_1*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T))))+1*x_1/(1*x_1+1*x_0*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))+1*x_2*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T))))*((0.6929999999999999-(172.987/T)-(0*log(T))-(0*T))*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))-(1*x_0*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))/(1*x_0*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))+1*x_1+1*x_2*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T))))*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T))*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))+1*x_2*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T)))/(1*x_2*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T)))+1*x_0*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))+1*x_1)*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))*(0.223052-(611.456/T)-(0*log(T))-(0*T))))-(1*x_2/(1*x_2+1*x_0*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))+1*x_1*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T)))*((0+653.006/T+0*log(T)+0*T)*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))-(1*x_0*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))/(1*x_0*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))+1*x_1*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T))+1*x_2)*(0+653.006/T+0*log(T)+0*T)*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))+1*x_1*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T))/(1*x_1*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T))+1*x_0*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))+1*x_2)*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))*(3.59543-550.5/T+0*log(T)+0*T))))-((x_1*(2.7322-617.269/T+0*log(T)+0*T)*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T))+x_2*(0-18.9372/T+0*log(T)+0*T)*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T)))/(x_2*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T))+x_0+x_1*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T))))))*exp(71.2051-6904.5/(T+0)+T*0-8.8622*log(T)+7.4664e-06*exp(2*log(T))) =E= 0;
eq3 .. x_1*(V_F-1)-(V_F*y_1)+0.4170999999999999 =E= 0;
eq4 .. p*y_1-x_1*exp(1*x_0/(1*x_0+1*x_1*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T))+1*x_2*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T)))*((2.7322-617.269/T+0*log(T)+0*T)*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T))-(1*x_1*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T))/(1*x_1*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T))+1*x_0+1*x_2*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T)))*(2.7322-617.269/T+0*log(T)+0*T)*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T))+1*x_2*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T))/(1*x_2*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T))+1*x_0+1*x_1*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T)))*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T))*(0-18.9372/T+0*log(T)+0*T)))+1*x_1/(1*x_1+1*x_0*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))+1*x_2*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T))))*((0.6929999999999999-(172.987/T)-(0*log(T))-(0*T))*1*x_0*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))/(1*x_0*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))+1*x_1+1*x_2*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T))))+(0.223052-(611.456/T)-(0*log(T))-(0*T))*1*x_2*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T)))/(1*x_2*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T)))+1*x_0*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))+1*x_1))+1*x_2/(1*x_2+1*x_0*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))+1*x_1*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T)))*((3.59543-550.5/T+0*log(T)+0*T)*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T))-(1*x_0*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))/(1*x_0*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))+1*x_1*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T))+1*x_2)*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T))*(0+653.006/T+0*log(T)+0*T)+1*x_1*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T))/(1*x_1*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T))+1*x_0*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))+1*x_2)*(3.59543-550.5/T+0*log(T)+0*T)*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T))))-(x_0*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T))*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))+x_2*(0.223052-(611.456/T)-(0*log(T))-(0*T))*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T))))/(x_2*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T)))+x_1+x_0*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))))*exp(62.1361-7258.2/(T+0)+T*0-7.3037*log(T)+4.1653e-06*exp(2*log(T))) =E= 0;
eq5 .. x_2*(V_F-1)-(V_F*y_2)+0.4161 =E= 0;
eq6 .. p*y_2-x_2*exp(1*x_0/(1*x_0+1*x_1*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T))+1*x_2*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T)))*((0-18.9372/T+0*log(T)+0*T)*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T))-(1*x_1*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T))/(1*x_1*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T))+1*x_0+1*x_2*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T)))*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T))*(2.7322-617.269/T+0*log(T)+0*T)+1*x_2*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T))/(1*x_2*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T))+1*x_0+1*x_1*exp(-0.3*(2.7322-617.269/T+0*log(T)+0*T)))*(0-18.9372/T+0*log(T)+0*T)*exp(-0.2951*(0-18.9372/T+0*log(T)+0*T))))-1*x_1/(1*x_1+1*x_0*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))+1*x_2*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T))))*((0.223052-(611.456/T)-(0*log(T))-(0*T))*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T)))-(1*x_0*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))/(1*x_0*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))+1*x_1+1*x_2*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T))))*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T)))*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T))+1*x_2*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T)))/(1*x_2*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T)))+1*x_0*exp(0.3*(0.6929999999999999-(172.987/T)-(0*log(T))-(0*T)))+1*x_1)*(0.223052-(611.456/T)-(0*log(T))-(0*T))*exp(0.362916*(0.223052-(611.456/T)-(0*log(T))-(0*T)))))-1*x_2/(1*x_2+1*x_0*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))+1*x_1*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T)))*((0+653.006/T+0*log(T)+0*T)*1*x_0*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))/(1*x_0*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))+1*x_1*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T))+1*x_2)+(3.59543-550.5/T+0*log(T)+0*T)*1*x_1*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T))/(1*x_1*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T))+1*x_0*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))+1*x_2))+(x_0*(0+653.006/T+0*log(T)+0*T)*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))+x_1*(3.59543-550.5/T+0*log(T)+0*T)*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T)))/(x_2+x_0*exp(-0.2951*(0+653.006/T+0*log(T)+0*T))+x_1*exp(-0.362916*(3.59543-550.5/T+0*log(T)+0*T))))*exp(51.72-4020/(T+0)+T*0-6.546*log(T)+9.439999999999999e-06*exp(2*log(T))) =E= 0;
eq7 .. x_0-y_0+x_1-y_1+x_2-y_2 =E= 0;

*Model information and options
model m / all /;

*Optional option file
m.optfile = 1;

*Optimality tolerances, time and solver
option OPTCA = 1e-09;
option OPTCR = 0.001;
option RESLIM = 300;
option NLP = SCIP;

*Solve statement
solve m using NLP minimizing objectiveVar;
