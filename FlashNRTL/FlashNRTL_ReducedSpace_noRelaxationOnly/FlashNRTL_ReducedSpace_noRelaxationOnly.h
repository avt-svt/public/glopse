/**
 *
 * @brief File implementing the reduced-space optimization problem for the case study
 *        of a flash with a mixture of MeOH, DME, and H2O modeled with the NRTL model
 *        (without relaxation-only constraints).
 *
 *        The flash models were originally published in:
 *        D. Bongartz, A. Mitsos: "Deterministic global flowsheet optimization:
 *             Between equation-oriented and sequential-modular methods",
 *             AIChE Journal 65 (2019), 1022-1034.
 *             https://aiche.onlinelibrary.wiley.com/doi/full/10.1002/aic.16507
 *        E-mail: amitsos@alum.mit.edu
 *
 *        The relaxations of the thermodynamic models were originally published in:
 *        J. Najman, D. Bongartz, and A. Mitsos: "Relaxations of thermodynamic property
 *             and costing models in process engineering",
 *             Computers & Chemical Engineering 130 (2019), 106571.
 *             https://www.sciencedirect.com/science/article/abs/pii/S0098135419309494
 *        E-mail: amitsos@alum.mit.edu
 *
 * ==============================================================================
 * © 2020, Process Systems Engineering (AVT.SVT), RWTH Aachen University
 * ==============================================================================
 *
 * used in:
 *    D. Bongartz: "Deterministic Global Flowsheet Optimization for the Design of Energy Conversion Processes",
 *     	            PhD Thesis, RWTH Aachen University, 2020.
 *
 */

#pragma once

#include "../Thermo/SubcriticalComponent.h"
#include "../Thermo/NRTL.h"

#include "MAiNGOmodel.h"

#include <string>


using Var = mc::FFVar;


//////////////////////////////////////////////////////////////////////////
// Model class to be passed to MAiNGO
class Model: public maingo::MAiNGOmodel {

	public:

		Model();
		maingo::EvaluationContainer evaluate(const std::vector<Var> &optVars);
		std::vector<maingo::OptimizationVariable> get_variables();


	private:

		// Constant model parameters:
			double F,Tf,pf,R;
			std::vector<double> z;
			unsigned int ncomp;
		// Declaration of model variables:
			std::vector<SubcriticalComponent <Var> > components;
			std::vector<Var> ps,hig,deltahv,x,y,gamma;
			Var psi,hl,hv,Q,V,L,VbF,T,p,hf,hE;
			NRTL NRTLmodel;
			NRTLpars theNRTLpars;


};


//////////////////////////////////////////////////////////////////////////
// function for providing optimization variable data to the Branch-and-Bound solver
std::vector<maingo::OptimizationVariable> Model::get_variables() {

	std::vector<maingo::OptimizationVariable> variables;
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(200,298),"T"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1,15),"p"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1),"V/F"));
	for (unsigned i=0; i<3; ++i) {
		variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-6,1),"x-"+std::to_string(i)));
	}
	for (unsigned i=0; i<3; ++i) {
		variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-6,1),"y-"+std::to_string(i)));
	}

	return variables;
}



//////////////////////////////////////////////////////////////////////////
// constructor for the model
Model::Model():
	R(8.3144598)
{

	// Initialize data:
		// Pure component models:
			components.push_back(SubcriticalComponent<Var>("MeOH",32.04,512.5,80.84,117,-200.93,0));
			components.push_back(SubcriticalComponent<Var>("Water",18.0154,647.1,220.64,56,-241.818,0));
			components.push_back(SubcriticalComponent<Var>("DME",46.07,400.1,54.0,164,-184.1,0));
		// Pure component property parameters
			// Extended Antoine
				components[0].set_vapor_pressure_model(SubcriticalComponent<Var>::PVAP_XANTOINE,std::vector<double>{71.2051, -6904.5, 0.0, 0.0, -8.8622, 7.4664e-6, 2});
				components[1].set_vapor_pressure_model(SubcriticalComponent<Var>::PVAP_XANTOINE,std::vector<double>{62.1361, -7258.2, 0.0, 0.0, -7.3037, 4.1653e-6, 2});
				components[2].set_vapor_pressure_model(SubcriticalComponent<Var>::PVAP_XANTOINE,std::vector<double>{51.72, -4020, 0.0, 0.0, -6.546, 9.44e-6, 2});
			// Enthalpy of vaporization
				components[0].set_enthalpy_of_vaporization_model(SubcriticalComponent<Var>::DHVAP_DIPPR106,std::vector<double>{32615, -1.0407, 1.8695, -0.60801, 0});
				components[1].set_enthalpy_of_vaporization_model(SubcriticalComponent<Var>::DHVAP_DIPPR106,std::vector<double>{56600, 0.61204, -0.6257, 0.3988, 0});
				components[2].set_enthalpy_of_vaporization_model(SubcriticalComponent<Var>::DHVAP_DIPPR106,std::vector<double>{26377, -0.072806, 0.54324, -0.13977, 0});
			// Heat capacity
				components[0].set_heat_capacity_model(SubcriticalComponent<Var>::CPIG_DIPPR107,std::vector<double>{39.252,87.9,1916.5,53.654,896.7});
				components[1].set_heat_capacity_model(SubcriticalComponent<Var>::CPIG_DIPPR107,std::vector<double>{33.363,26.79,2610.5,8.896,1169});
				components[2].set_heat_capacity_model(SubcriticalComponent<Var>::CPIG_DIPPR107,std::vector<double>{57.431,94.494,895.51,65.065,2467.4});
				ncomp = components.size();
			ps.resize(ncomp); hig.resize(ncomp); deltahv.resize(ncomp); x.resize(ncomp); y.resize(ncomp); z.resize(ncomp); gamma.resize(ncomp);
		// NRTL Parameters; 0: MeOH, 1: H2O, 2: DME
			std::vector< std::vector<double> > tmpMatrix(ncomp, std::vector<double>(ncomp, 0.0));
			theNRTLpars.a = tmpMatrix; theNRTLpars.b = tmpMatrix; theNRTLpars.c = tmpMatrix; theNRTLpars.d = tmpMatrix; theNRTLpars.e = tmpMatrix; theNRTLpars.f = tmpMatrix;
			theNRTLpars.a[0][1] = -0.693;
			theNRTLpars.a[1][0] = 2.7322;
			theNRTLpars.a[0][2] = 0.0;
			theNRTLpars.a[2][0] = 0.0;
			theNRTLpars.a[1][2] = 3.59543;
			theNRTLpars.a[2][1] = -0.223052;

			theNRTLpars.b[0][1] = 172.987;
			theNRTLpars.b[1][0] = -617.269;
			theNRTLpars.b[0][2] = 653.006;
			theNRTLpars.b[2][0] = -18.9372;
			theNRTLpars.b[1][2] = -550.5;
			theNRTLpars.b[2][1] = 611.456;

			theNRTLpars.c[0][1] = 0.3;
			theNRTLpars.c[1][0] = 0.3;
			theNRTLpars.c[0][2] = 0.2951;
			theNRTLpars.c[2][0] = 0.2951;
			theNRTLpars.c[1][2] = 0.362916;
			theNRTLpars.c[2][1] = 0.362916;
			NRTLmodel.setPars<Var>(theNRTLpars);

	// Feed:
		F = 1; 				// [kmol/s]
		Tf = 72+273.15; 	// [K]
		pf = 15;			// [bar]
		z[0] = 0.1668;		// MeOH
		z[2] = 0.4161;		// DME
		z[1] = 1 - z[0] - z[2];	// H2O
		hf=0.;
		for (unsigned int i=0; i<ncomp; i++) {
			Var higfi = components[i].calculate_ideal_gas_enthalpy_conv(Tf);
			Var deltahvfi = components[i].calculate_vaporization_enthalpy_conv(Tf);
			hf += z[i]*(higfi - deltahvfi);
		}
		hf += NRTLmodel.calculateHE<double>(Tf,z);

}


//////////////////////////////////////////////////////////////////////////
// Evaluate the model
maingo::EvaluationContainer Model::evaluate(const std::vector<Var> &optVars)
{

	// Rename / prepare inputs
		T=optVars[0];
		p=optVars[1];
		VbF=optVars[2];
		for (size_t i=0; i<ncomp; ++i) {
			x[i] = optVars[3+i];
		}
		for (size_t i=0; i<ncomp; ++i) {
			y[i] = optVars[3+ncomp+i];
		}

	// Model
		maingo::EvaluationContainer result;
		// FLASH:
			// 0. NRTL:
				gamma = NRTLmodel.calculateGamma<Var>(T,x);
				hE = NRTLmodel.calculateHE<Var>(T,x);
			// 1. Overall mass balance
				V = F*VbF;
				L = F-V;
			// 2. Calculate phase equilibrium residual & enthalpies
				psi=0;
				Var hlTmp=0,hvTmp=0;
				Var sumX(0), sumY(0);
				for (unsigned i=0;i<ncomp;i++) {
					// Phase equilibrium
					ps[i] = components[i].calculate_vapor_pressure_conv(T);
					hig[i] = components[i].calculate_ideal_gas_enthalpy_conv(T);
					deltahv[i] = components[i].calculate_vaporization_enthalpy_conv(T);
					psi += x[i] - y[i];
					sumX += x[i];
					sumY += y[i];
					  // Enthalpies
					hvTmp += y[i]*hig[i];
					hlTmp += x[i]*(hig[i] - deltahv[i]);
				}
			// 3. Energy balance:
				hlTmp += hE;
				Q = V*hvTmp + L*hlTmp - F*hf;

		// Objective:
			result.objective =  -Q;
		// Inequalities (<=0):
			result.ineq.push_back((0.99 - y[2])/1.0);
		// Equalities (=0):
			for (unsigned i=0;i<ncomp;i++) {
				result.eq.push_back( ((F*z[i]) - (V*y[i] + L*x[i]))/1.0 );
				result.eq.push_back( (p*y[i] - gamma[i]*ps[i]*x[i])/1.0 );
			}
			result.eq.push_back( psi/1.0 );

		return result;
}