/**
 *
 * @brief File implementing the full-space optimization problem for the case study
 *        of a flash with a mixture of MeOH, DME, and H2O modeled with the NRTL model.
 *        Contains relaxation-only constraints for the redundant summations of liquid and gas mole fractions.
 *
 *        The flash models were originally published in:
 *        D. Bongartz, A. Mitsos: "Deterministic global flowsheet optimization:
 *             Between equation-oriented and sequential-modular methods",
 *             AIChE Journal 65 (2019), 1022-1034.
 *             https://aiche.onlinelibrary.wiley.com/doi/full/10.1002/aic.16507
 *        E-mail: amitsos@alum.mit.edu
 *
 *        The relaxations of the thermodynamic models were originally published in:
 *        J. Najman, D. Bongartz, and A. Mitsos: "Relaxations of thermodynamic property
 *             and costing models in process engineering",
 *             Computers & Chemical Engineering 130 (2019), 106571.
 *             https://www.sciencedirect.com/science/article/abs/pii/S0098135419309494
 *        E-mail: amitsos@alum.mit.edu
 *
 * ==============================================================================
 * © 2020, Process Systems Engineering (AVT.SVT), RWTH Aachen University
 * ==============================================================================
 *
 * used in:
 *    D. Bongartz: "Deterministic Global Flowsheet Optimization for the Design of Energy Conversion Processes",
 *     	            PhD Thesis, RWTH Aachen University, 2020.
 *
 */

#pragma once

#include "../Thermo/SubcriticalComponent.h"
#include "../Thermo/NRTL.h"

#include "MAiNGOmodel.h"

#include <string>


using Var = mc::FFVar;


//////////////////////////////////////////////////////////////////////////
// Model class to be passed to MAiNGO
class Model: public maingo::MAiNGOmodel {

	public:

		Model();
		maingo::EvaluationContainer evaluate(const std::vector<Var> &optVars);
		std::vector<maingo::OptimizationVariable> get_variables();


	private:

		// Constant model parameters:
			double F,Tf,pf,R;
			std::vector<double> z;
			unsigned int ncomp;
		// Declaration of model variables:
			std::vector<SubcriticalComponent <Var> > components;
			std::vector<Var> ps,hig,deltahv,x,y,gamma;
			Var psi,hl,hv,Q,V,L,VbF,T,p,hf,hE;
			std::vector< std::vector<Var> > tau,G,dTaudT,dGdT;
			NRTL NRTLmodel;
			NRTLpars theNRTLpars;


};


//////////////////////////////////////////////////////////////////////////
// function for providing optimization variable data to the Branch-and-Bound solver
std::vector<maingo::OptimizationVariable> Model::get_variables() {

	std::vector<maingo::OptimizationVariable> variables;
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(200,298),"T"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1,15),"p"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1),"V"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1),"L"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-6e5,5e5),"hL"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-5e5,5e5),"hV"));
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-1e6,1e6),"Q"));
	for (size_t i=0; i<3; ++i) {
		variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-6,1),"x-"+std::to_string(i)));
	}
	for (size_t i=0; i<3; ++i) {
		variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-6,1),"y-"+std::to_string(i)));
	}
	for (size_t i=0; i<3; ++i) {
		variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,53),"ps-"+std::to_string(i)));
	}
	for (size_t i=0; i<3; ++i) {
		variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-5e5,5e5),"hig-"+std::to_string(i)));
	}
	for (size_t i=0; i<3; ++i) {
		variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0,1e5),"dHv-"+std::to_string(i)));
	}
	variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-1e4,1e4),"hE"));
	for (size_t i=0; i<3; ++i) {
		variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-3,1e3),"gamma-"+std::to_string(i)));
	}
	for (size_t i=0; i<3; ++i) {
		for (size_t j=0; j<3; ++j) {
			if (i!=j) {
				variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-1e2,1e2),"tau-ij"));
				variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1e-3,1e3),"G-ij"));
				variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-1,1),"dTaudT-ij"));
				variables.push_back(maingo::OptimizationVariable(maingo::Bounds(-1,1),"dGdT-ij"));
			}
		}
	}
	return variables;
}



//////////////////////////////////////////////////////////////////////////
// constructor for the model
Model::Model():
	R(8.3144598)
{


	// Initialize data if necessary:
		// Pure component models:
			components.push_back(SubcriticalComponent<Var>("MeOH",32.04,512.5,80.84,117,-200.93,0));
			components.push_back(SubcriticalComponent<Var>("Water",18.0154,647.1,220.64,56,-241.818,0));
			components.push_back(SubcriticalComponent<Var>("DME",46.07,400.1,54.0,164,-184.1,0));
		// Pure component property parameters
			// Extended Antoine
				components[0].set_vapor_pressure_model(SubcriticalComponent<Var>::PVAP_XANTOINE,std::vector<double>{71.2051, -6904.5, 0.0, 0.0, -8.8622, 7.4664e-6, 2});
				components[1].set_vapor_pressure_model(SubcriticalComponent<Var>::PVAP_XANTOINE,std::vector<double>{62.1361, -7258.2, 0.0, 0.0, -7.3037, 4.1653e-6, 2});
				components[2].set_vapor_pressure_model(SubcriticalComponent<Var>::PVAP_XANTOINE,std::vector<double>{51.72, -4020, 0.0, 0.0, -6.546, 9.44e-6, 2});
			// Enthalpy of vaporization
				components[0].set_enthalpy_of_vaporization_model(SubcriticalComponent<Var>::DHVAP_DIPPR106,std::vector<double>{32615, -1.0407, 1.8695, -0.60801, 0});
				components[1].set_enthalpy_of_vaporization_model(SubcriticalComponent<Var>::DHVAP_DIPPR106,std::vector<double>{56600, 0.61204, -0.6257, 0.3988, 0});
				components[2].set_enthalpy_of_vaporization_model(SubcriticalComponent<Var>::DHVAP_DIPPR106,std::vector<double>{26377, -0.072806, 0.54324, -0.13977, 0});
			// Heat capacity
				components[0].set_heat_capacity_model(SubcriticalComponent<Var>::CPIG_DIPPR107,std::vector<double>{39.252,87.9,1916.5,53.654,896.7});
				components[1].set_heat_capacity_model(SubcriticalComponent<Var>::CPIG_DIPPR107,std::vector<double>{33.363,26.79,2610.5,8.896,1169});
				components[2].set_heat_capacity_model(SubcriticalComponent<Var>::CPIG_DIPPR107,std::vector<double>{57.431,94.494,895.51,65.065,2467.4});
				ncomp = components.size();
			ps.resize(ncomp); hig.resize(ncomp); deltahv.resize(ncomp); x.resize(ncomp); y.resize(ncomp); z.resize(ncomp); gamma.resize(ncomp);
			std::vector< std::vector<Var> > tmpM1(ncomp, std::vector<Var>(ncomp, 0.0));
			tau = tmpM1; dTaudT = tmpM1; dGdT = tmpM1;
			std::vector< std::vector<Var> > tmpM2(ncomp, std::vector<Var>(ncomp, 1.0));
			G = tmpM2;
		// NRTL Parameters; 0: MeOH, 1: H2O, 2: DME
			std::vector< std::vector<double> > tmpMatrix(ncomp, std::vector<double>(ncomp, 0.0));
			theNRTLpars.a = tmpMatrix; theNRTLpars.b = tmpMatrix; theNRTLpars.c = tmpMatrix; theNRTLpars.d = tmpMatrix; theNRTLpars.e = tmpMatrix; theNRTLpars.f = tmpMatrix;
			theNRTLpars.a[0][1] = -0.693;
			theNRTLpars.a[1][0] = 2.7322;
			theNRTLpars.a[0][2] = 0.0;
			theNRTLpars.a[2][0] = 0.0;
			theNRTLpars.a[1][2] = 3.59543;
			theNRTLpars.a[2][1] = -0.223052;

			theNRTLpars.b[0][1] = 172.987;
			theNRTLpars.b[1][0] = -617.269;
			theNRTLpars.b[0][2] = 653.006;
			theNRTLpars.b[2][0] = -18.9372;
			theNRTLpars.b[1][2] = -550.5;
			theNRTLpars.b[2][1] = 611.456;

			theNRTLpars.c[0][1] = 0.3;
			theNRTLpars.c[1][0] = 0.3;
			theNRTLpars.c[0][2] = 0.2951;
			theNRTLpars.c[2][0] = 0.2951;
			theNRTLpars.c[1][2] = 0.362916;
			theNRTLpars.c[2][1] = 0.362916;
			NRTLmodel.setPars<Var>(theNRTLpars);

	// Feed:
		F = 1; 				// [kmol/s]
		Tf = 72+273.15; 	// [K]
		pf = 15;			// [bar]
		z[0] = 0.1668;		// MeOH
		z[2] = 0.4161;		// DME
		z[1] = 1 - z[0] - z[2];	// H2O
		hf=0.;
		for (unsigned int i=0; i<ncomp; i++) {
			Var higfi = components[i].calculate_ideal_gas_enthalpy_conv(Tf);
			Var deltahvfi = components[i].calculate_vaporization_enthalpy_conv(Tf);
			hf += z[i]*(higfi - deltahvfi);
		}
		hf += NRTLmodel.calculateHE<double>(Tf,z);

}


//////////////////////////////////////////////////////////////////////////
// Evaluate the model
maingo::EvaluationContainer Model::evaluate(const std::vector<Var> &optVars)
{

	// Rename / prepare inputs
		T=optVars[0];
		p=optVars[1];
		V=optVars[2];
		L=optVars[3];
		hl=optVars[4];
		hv=optVars[5];
		Q=optVars[6];
		for (size_t i=0; i<ncomp; ++i) {
			x[i] = optVars[7+i];
		}
		for (size_t i=0; i<ncomp; ++i) {
			y[i] = optVars[7+ncomp+i];
		}
		for (size_t i=0; i<ncomp; ++i) {
			ps[i] = optVars[7+2*ncomp+i];
		}
		for (size_t i=0; i<ncomp; ++i) {
			hig[i] = optVars[7+3*ncomp+i];
		}
		for (size_t i=0; i<ncomp; ++i) {
			deltahv[i] = optVars[7+4*ncomp+i];
		}
		hE=optVars[7+5*ncomp];
		for (size_t i=0; i<ncomp; ++i) {
			gamma[i] = optVars[8+5*ncomp+i];
		}
		int cnt = 0;
		for (size_t i=0; i<ncomp; ++i) {
			for (size_t j=0; j<ncomp; ++j) {
				if (i!=j) {
					tau[i][j] = optVars[8+6*ncomp+cnt*4];
					G[i][j] = optVars[9+6*ncomp+cnt*4];
					dTaudT[i][j] = optVars[10+6*ncomp+cnt*4];
					dGdT[i][j] = optVars[11+6*ncomp+cnt*4];
					++cnt;
				}
			}
		}


	// Model
		maingo::EvaluationContainer result;
		// FLASH:
			// 0. NRTL:
				//Preliminaries:
					for (size_t i=0; i<ncomp; ++i) {
						for (size_t j=0; j<ncomp; ++j) {
							if (i!=j) {
								result.eq.push_back( (tau[i][j] - nrtl_tau(T,theNRTLpars.a[i][j],theNRTLpars.b[i][j],0.,0.))/1 );
								result.eq.push_back( (G[i][j] - nrtl_G(T,theNRTLpars.a[i][j],theNRTLpars.b[i][j],0.,0.,theNRTLpars.c[i][j]))/1 );
								result.eq.push_back( (dTaudT[i][j] - nrtl_dtau(T,theNRTLpars.b[i][j],0.,0.))/1 );
								result.eq.push_back( (dGdT[i][j] - (-theNRTLpars.c[i][j]*nrtl_Gdtau(T,theNRTLpars.a[i][j],theNRTLpars.b[i][j],0.,0.,theNRTLpars.c[i][j])))/1 );
							}
						}
					}
				// Gamma
					std::vector<double> coeff(x.size()+1,1.0);
					for (size_t k=0;k<ncomp;k++) {
						Var sumA(0), sumB(0), sumC(0);
						std::vector<Var> denomA, denomB;
						for (size_t i=0; i<ncomp; ++i) {
							sumA += nrtl_Gtau(T,theNRTLpars.a[i][k],theNRTLpars.b[i][k],0.,0.,theNRTLpars.c[i][k])*x[i];
							sumB += G[i][k]*x[i];
							Var sumC1(0), sumC2(0), sumC3(0);

							denomA.clear();
							denomA.push_back(x[i]);	// actually, G[i][i]*x[i], but G[i][i]=1
							for (unsigned j=0;j<ncomp;j++) {
								sumC1 += G[j][i]*x[j];
								if(i!=j){
									denomA.push_back(G[j][i]*x[j]);
								}
								sumC2 += nrtl_Gtau(T,theNRTLpars.a[j][i],theNRTLpars.b[j][i],0.,0.,theNRTLpars.c[j][i])*x[j];
								denomB.clear();
								denomB.push_back(G[j][i]*x[j]);
								for (unsigned l=0;l<ncomp;l++) {
									if(l!=j){
										denomB.push_back(G[l][i]*x[l]);
									}
								}
								if (k==j) {
									sumC3 += nrtl_Gtau(T,theNRTLpars.a[k][i],theNRTLpars.b[k][i],0.,0.,theNRTLpars.c[k][i])*sum_div(denomB,coeff);
								} else {
									sumC3 += G[k][i]*tau[j][i]*sum_div(denomB,coeff);
								}
							}
							sumC += sum_div(denomA,coeff)* (nrtl_Gtau(T,theNRTLpars.a[k][i],theNRTLpars.b[k][i],0.,0.,theNRTLpars.c[k][i]) - sumC3);
						}
						result.eq.push_back( ( gamma[k] - exp( bounding_func(sumA/sumB + sumC,std::log(1/1000.),std::log(1000.)) ) )/1 );
					}
				// HE
					Var tmpHE(0);
					for (unsigned i=0;i<x.size();i++) {
						// left factor
						std::vector<Var> denomA;
						std::vector<double> coeff(x.size()+1,1.);
						denomA.push_back(G[i][i]*x[i]);
						for (unsigned j=0;j<x.size();j++) {
							if(j!=i){
								denomA.push_back(G[j][i]*x[j]);
							}
						}
						Var leftFactor = sum_div(denomA,coeff);
						// right factor
						Var div=0.0;
						Var rightFactor = 0.;
						for (unsigned l=0;l<x.size();l++) {
							std::vector<Var> denom;
							denom.push_back(G[l][i]*x[l]);
							for(unsigned int k = 0; k< x.size();k++){
								if(k!=l){
									denom.push_back(G[k][i]*x[k]);
								}
							}
							div += tau[l][i]*sum_div(denom,coeff);
						}
						for (unsigned j=0;j<x.size();j++) {
							if(j!=i){
								rightFactor += nrtl_Gdtau(T,theNRTLpars.a[j][i],theNRTLpars.b[j][i],0.,0.,theNRTLpars.c[j][i])*x[j]*(1 - theNRTLpars.c[j][i]*tau[j][i] + theNRTLpars.c[j][i]*div);
							}
						}
						tmpHE += leftFactor*rightFactor;
					}
					result.eq.push_back( ( hE - tmpHE*(-R*pow(T,2)) )/1 );

			// 1. Overall mass balance
				result.eq.push_back( (F - (L+V))/1.0 );
			// 2. Calculate phase equilibrium residual & enthalpies
				psi=0;
				Var hlTmp=0,hvTmp=0;
				Var sumX(0), sumY(0);
				for (unsigned i=0;i<ncomp;i++) {
					  // Phase equilibrium
					result.eq.push_back( (ps[i] - components[i].calculate_vapor_pressure_conv(T))/10 );
					result.eq.push_back( (hig[i] - components[i].calculate_ideal_gas_enthalpy_conv(T))/1e4 );
					result.eq.push_back( (deltahv[i] - components[i].calculate_vaporization_enthalpy_conv(T))/1e4 );
					result.eq.push_back( ((F*z[i]) - (V*y[i] + L*x[i]))/1.0 );
					result.eq.push_back( (p*y[i] - gamma[i]*ps[i]*x[i])/1.0 );
					psi += x[i] - y[i];
					sumX += x[i];
					sumY += y[i];
					  // Enthalpies
					hvTmp += y[i]*hig[i];
					hlTmp += x[i]*(hig[i] - deltahv[i]);
				}
				result.eq.push_back( psi/1.0 );
			// 3. Energy balance:
				hlTmp += hE;
				result.eq.push_back( (hl - hlTmp)/1e4 );
				result.eq.push_back( (hv - hvTmp)/1e4 );
				result.eq.push_back( (Q - (V*hv + L*hl - F*hf) )/1 );

		// Objective:
			result.objective =  -Q;
		// Inequalities (<=0):
			result.ineq.push_back((0.99 - y[2])/1.0);
		// Relaxation-only equalities (=0):
			result.eqRelaxationOnly.push_back( (1.0-sumX)/1.0 );
			result.eqRelaxationOnly.push_back( (1.0-sumY)/1.0 );

		return result;
}