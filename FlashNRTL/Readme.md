# FlashNRTL

Problems for testing the use of flash models containins the NRTL model. The problem aims at minimizing the cooling duty when partially condensing a mixture of water, methanol, and dimethyl ether subject to a purity constraint for the latter.
The phase equilibrium is descibed using the extended Antoine equation for vapor pressures and the NRTL model for the activity coefficients.
Enthalpies are computed using the DIPPR 107 equation for ideal gas heat capacity, the DIPPR 106 equation for enthalpy of vaporization, and the NRTL model for excess enthalpy.
The MAiNGO version of the problems (both for the C++ API and the text-based versions) use intrinsic functions and custom relaxations for the above-mentioned property models.

The problems and flash models were originally implemented for the following publication:
 - Bongartz, D., & Mitsos, A. (2019). [Deterministic global flowsheet optimization: Between equation-oriented and sequential-modular methods](https://aiche.onlinelibrary.wiley.com/doi/full/10.1002/aic.16507). *AIChE Journal*, 65(3), 1022-1034.

The custom relaxations for vapor pressure, ideal gas enthalpy, enthalpy of vaporization, and the NRTL model were originally implemented for the following publication:
 - Najman, J., Bongartz, D., & Mitsos, A. (2019). [Relaxations of thermodynamic property and costing models in process engineering](https://www.sciencedirect.com/science/article/abs/pii/S0098135419309494). *Computers & Chemical Engineering*, 130, 106571.                        



| Instance        | Problem Type | Number of Continuous Variables | Number of Integer Variables | Number of Binary Variables | Number of Equalities | Number of Inequalities | Number of Relaxation-Only Constraints* | Best Known Objective Value | Proven Objective Bound | Description           |
| ------------- |-------------| ------------| ------------| ------------| ------------| ------------| ------------| :------------: | :-----: | ----- |
| FlashNRTL_ReducedSpace_withRelaxationOnly  | NLP | 9 | 0 | 0 | 7 | 1 | 2 | 1064.417 | 1064.416 | Reduced-space formulation, corresponds to version (Flash) in Bongartz & Mitsos (2019) |
| FlashNRTL_ReducedSpace_noRelaxationOnly  | NLP | 9 | 0 | 0 | 7 | 1 | 0 | 1064.417 | 1064.416 | Reduced-space formulation, corresponds to version (Flash) in Bongartz & Mitsos (2019) |
| FlashNRTL_FullSpace_withRelaxationOnly  | NLP | 50 | 0 | 0 | 48 | 1 | 2 | 1065.251 | 1065.249 | Full-space formulation |
| FlashNRTL_FullSpace_noRelaxationOnly  | NLP | 50 | 0 | 0 | 48 | 1 | 0 | 1065.251 | 1065.249 | Full-space formulation |

*Some of the problems contain the redundant summation equations for the liquid and vapor phase mole fractions. In the MAiNGO version of the problems, these are implemented as relaxation-only constraints as proposed by [Sahinidis & Tawarmalani](https://link.springer.com/article/10.1007/s10898-004-2705-8). In the GAMS version of the problems, they are implemented as regular constraints because GAMS itself does not allow labelling constraints as relaxation-only. However, certain solvers may be able to label them as relaxation-only via their settings file (e.g., for BARON in baron.opt).
