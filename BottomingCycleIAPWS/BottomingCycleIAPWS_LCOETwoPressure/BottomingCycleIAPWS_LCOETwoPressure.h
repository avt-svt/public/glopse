/**
 *
 * @brief Problem definition for two-pressure bottoming cycle case study with IAPWS-IF97.
 *
 *        The process model was originally published in:
 *        D. Bongartz, A. Mitsos: "Deterministic global optimization of process
 *             flowsheets in a reduced space using McCormick relaxations",
 *             Journal of Global Optimization 69 (2017), 761-796.
 *             https://link.springer.com/article/10.1007/s10898-017-0547-4
 *        E-mail: amitsos@alum.mit.edu
 *
 *        The relaxations of the IAPWS-IF97 were originally published in:
 *        D. Bongartz, J. Najman, and A. Mitsos: "Deterministic global optimization
 *             of steam cycles using the IAPWS‑IF97 model",
 *             Optimization and Engineering (2020), in press.
 *             https://doi.org/10.1007/s11081-020-09502-1
 *        E-mail: amitsos@alum.mit.edu
 *
 * ==============================================================================
 * © 2020, Process Systems Engineering (AVT.SVT), RWTH Aachen University
 * ==============================================================================
 *
 * used in:
 *    D. Bongartz: "Deterministic Global Flowsheet Optimization for the Design of Energy Conversion Processes",
 *     	            PhD Thesis, RWTH Aachen University, 2020.
 *
 */

#pragma once

#include "MAiNGOmodel.h"
#include "../Thermo/IapwsIf97Model.h"

#include <memory>


class Model: public maingo::MAiNGOmodel {

    public:

        Model();
        maingo::EvaluationContainer evaluate(const std::vector<Var> &optVars);
        std::vector<maingo::OptimizationVariable> get_variables();

    private:

        std::unique_ptr<maingoflow::thermo::ThermoModel> thermoModel;

        const double eta_p, eta_t, mcpG, TGin, Tmax, dTap, dTmin, xmin, Amin, Vmin;
        const double kEco, kEvap, kSH, kCond, k1A, k2A, k3A, p1A, p2A, p3A, FmA, B1A, B2A, k1B, k2B, k3B, FmB, B1B, B2B, Tcin, Tcout;
        const double InvGT, WorkGT, FuelHeat, FuelPrice, fPhi, fAnnu, Teq, Varcost;
		double c1A,c2A,c3A;

};


//////////////////////////////////////////////////////////////////////////
// function for providing optimization variable data to the Branch-and-Bound solver
std::vector<maingo::OptimizationVariable> Model::get_variables() {

    std::vector<maingo::OptimizationVariable> variables;
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.02,0.3),  "p2   [MPa]"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.3,1.5),   "p4   [MPa]"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(1.0,10.0),  "p8   [MPa]"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds((1-0.5)*5,(1-0.05)*100), "mdotHP [kg/s]"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.01*5,0.2*100), "mdotBleed [kg/s]"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds((1-0.2)*5,(1-0.01)*100), "mdotMain [kg/s]"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(300,873), "T7   [K]"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(300,873), "T11  [K]"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(300,873), "T12iso  [K]"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(300,873), "T13  [K]"));

    return variables;
}


//////////////////////////////////////////////////////////////////////////
// constructor
Model::Model(): 	eta_p(0.8), eta_t(0.9), mcpG(200), TGin(900), Tmax(873.15), dTap(10), dTmin(15), xmin(0.85), Amin(10), Vmin(1),
                    kEco(0.06), kEvap(0.06), kSH(0.03), kCond(0.35), k1A(4.3247), k2A(-0.303), k3A(0.1634), p1A(0.03881), p2A(-0.11272), p3A(0.08183),
				    FmA(2.75), B1A(1.63), B2A(1.66), k1B(3.5565), k2B(0.3776), k3B(0.0905), FmB(1), B1B(1.49), B2B(1.52), Tcin(273.15+20), Tcout(273.15+25),
                    InvGT(22.7176e6), WorkGT(69676), FuelHeat(182359), FuelPrice(14), fPhi(1.06), fAnnu(0.1875), Teq(4000), Varcost(4)
{

    thermoModel = std::unique_ptr<maingoflow::thermo::ThermoModel>(new maingoflow::thermo::IapwsIf97Model());
	// Convert coefficients for pressure factor from bar to MPa:
	c1A = p1A+p2A+p3A;
	c2A = p2A+2*p3A;
	c3A = p3A;
}


//////////////////////////////////////////////////////////////////////////
// evaluate the model
maingo::EvaluationContainer Model::evaluate(const std::vector<Var> &optVars)
{

    // Rename inputs
        Var p2=optVars[0];
        Var p4=optVars[1];
        Var p8=optVars[2];
        Var mHP=optVars[3];
        Var mBleed=optVars[4];
        Var mMain=optVars[5];
        Var T7=optVars[6];
        Var T11=optVars[7];
        Var T12iso=optVars[8];
        Var T13=optVars[9];
        Var mdot = mMain + mBleed;
        Var mLP = mdot - mHP;

    // Model
        maingo::EvaluationContainer result;
		// Ensure compatibility with original model:
			result.ineq.push_back((mLP - 0.5*mdot)/10.);
			result.ineq.push_back((0.05*mdot - mLP)/1.);
			result.ineq.push_back((mBleed - 0.2*mdot)/1.);
			result.ineq.push_back((0.01*mdot - mBleed)/0.1);
        // HP Turbine
            // HP superheater outlet
            Var p11 = p8;
            result.ineq.push_back((thermoModel->get_Ts_p(p11)-T11)/1e2,"Ts(p11)<=T11");
            Var h11 = thermoModel->get_h_vap_pT(p11,T11);
            Var s11 = thermoModel->get_s_vap_pT(p11,T11);
            // Turbine
            Var p12 = p4;
            Var s12iso = s11;
            result.ineq.push_back((thermoModel->get_Ts_p(p12)-T12iso)/1e2,"Ts(p12)<=T12iso");
            result.eq.push_back((thermoModel->get_s_vap_pT(p12,T12iso)-s12iso)/1e0,"s(p12,T12iso)=s12iso");
            Var h12iso = thermoModel->get_h_vap_pT(p12,T12iso);
            Var wtHP = eta_t*(h11-h12iso);
            Var WorkTurbHP = mHP*wtHP;
            Var h12 = h11*(1-eta_t) + eta_t*h12iso;
        // LP Turbine
            // LP superheater outlet
            result.ineq.push_back((0.05*5 - mLP)/1e1,"mLP>=mmin");
            result.ineq.push_back((mLP - 0.5*100)/1e1,"mLP<=mmax");
            Var p7 = p4;
            result.ineq.push_back((thermoModel->get_Ts_p(p7)-T7)/1e2,"Ts(p7)<=T7");
            Var h7 = thermoModel->get_h_vap_pT(p7,T7);
            // Mixer
            Var p13 = p7;
            result.ineq.push_back((thermoModel->get_Ts_p(p13)-T13)/1e2,"Ts(p13)<=T13");
            Var h13 = thermoModel->get_h_vap_pT(p13,T13);
            Var s13 = thermoModel->get_s_vap_pT(p13,T13);
            result.eq.push_back((mLP*(h7-h13) + mHP*(h12-h13))/1e3,"Mixer, energy balance");
            // Bleed
            Var p14 = p2;
            Var s14iso = s13;
            result.ineq.push_back((thermoModel->get_sliq_p(p14)-s14iso)/1e0,"sliq(p14)<=s14iso");
            result.ineq.push_back((s14iso-thermoModel->get_svap_p(p14))/1e0,"s14iso<=svap(p14)");
            s14iso = min(max(s14iso,thermoModel->get_smin_twophase()),thermoModel->get_smax_twophase());
            Var h14iso = thermoModel->get_h_twophase_ps(p14,s14iso);
            Var wtBleed = eta_t*(h13-h14iso);
            Var WorkTurbBleed = mBleed*wtBleed;
            Var h14 = h13*(1-eta_t) + eta_t*h14iso;
            result.ineq.push_back((thermoModel->get_hliq_p(p14)-h14)/1e3,"hliq(p14)<=h14");
            result.ineq.push_back((h14-thermoModel->get_hvap_p(p14))/1e3,"h14<=hvap(p14)");
            h14 = min(h14,thermoModel->get_hmax_twophase());
            Var x14 = thermoModel->get_x_ph(p14,h14);
            Var T14 = thermoModel->get_Ts_p(p14);
            // Main
            Var p1 = 0.1*0.05;  // Convert to MPa
            Var p15 = p1;
            Var s15iso = s13;
            result.ineq.push_back((thermoModel->get_sliq_p(p15)-s15iso)/1e0,"sliq(p15)<=s15iso");
            result.ineq.push_back((s15iso-thermoModel->get_svap_p(p15))/1e0,"s15iso<=svap(p15)");
            s15iso = min(max(s15iso,thermoModel->get_smin_twophase()),thermoModel->get_smax_twophase());
            Var h15iso = thermoModel->get_h_twophase_ps(p15,s15iso);
            Var wtMain = eta_t*(h13-h15iso);
            Var WorkTurbMain = mMain*wtMain;
            Var h15 = h13*(1-eta_t) +  eta_t*h15iso;
            result.ineq.push_back((thermoModel->get_hliq_p(p15)-h15)/1e0,"hliq(p15)<=h15");
            result.ineq.push_back((h15-thermoModel->get_hvap_p(p15))/1e0,"h15<=hvap(p15)");
            h15 = min(max(h15,thermoModel->get_hmin_twophase()),thermoModel->get_hmax_twophase());
            Var T15 = thermoModel->get_Ts_p(p15);
            Var x15 = thermoModel->get_x_ph(p15,h15);
            Var hliq15 = thermoModel->get_hliq_p(p15);
            Var hvap15 = thermoModel->get_hvap_p(p15);
            result.ineq.push_back((xmin*hvap15+hliq15*(1.-xmin)-h15)/1e2,"xmin<=x15");
        // Feedwater line
        // Condenser
            Var h1 = thermoModel->get_hliq_p(p1);
            Var s1 = thermoModel->get_sliq_p(p1);
            Var T1 = thermoModel->get_Ts_p(p1);
            Var QCond = mMain*(h15-h1);
        // Condensate pump
            Var s2iso = s1;
            Var h2iso = thermoModel->get_h_liq_ps(p2,s2iso);
            Var wpCond = (h2iso-h1)/eta_p;
            Var WorkPumpCond = mMain*wpCond;
            Var h2 = h1*(1.-1./eta_p) + h2iso/eta_p;
            Var T2 = thermoModel->get_T_liq_ph(p2,h2);
        // Deaerator
            Var p3 = p2;
            Var h3 = thermoModel->get_hliq_p(p3);
            result.eq.push_back((mMain*(h3-h2)+mBleed*(h3-h14))/1e3,"Deaerator energy balance");
            Var s3 = thermoModel->get_sliq_p(p3);
            Var T3 = thermoModel->get_Ts_p(p3);
        // LP pump
            result.ineq.push_back((p3-p4)/1e0,"p3<=p4");
            Var s4iso = s3;
            s4iso = min(max(s4iso,thermoModel->get_smin_liq()),thermoModel->get_smax_liq());
            Var h4iso = thermoModel->get_h_liq_ps(p4,s4iso);
            Var wpLP = (h4iso-h3)/eta_p;
            Var WorkPumpLP = mdot*wpLP;
            Var h4 = h3*(1.-1./eta_p) + h4iso/eta_p;
            result.ineq.push_back((thermoModel->get_hmin_liq()-h4)/1e2,"hmin<=h4");
            result.ineq.push_back((h4-thermoModel->get_hliq_p(p4))/1e2,"h4<=hliq(p4)");
            h4 = min(max(h4,thermoModel->get_hmin_liq()),thermoModel->get_hmax_liq());
            Var T4 = thermoModel->get_T_liq_ph(p4,h4);
        // LP Preheater
            Var p5 = p4;
            Var T5 = thermoModel->get_Ts_p(p5) - dTap;
            T5 = max(min(T5,thermoModel->get_Tmax_liq()),thermoModel->get_Tmin_liq());
            Var h5 = thermoModel->get_h_liq_pT(p5,T5);
            Var s5 = thermoModel->get_s_liq_pT(p5,T5);
            Var QLPPre = mdot*(h5-h4);
        // HP Pump
            result.ineq.push_back((p5-p8)/1e0,"p5<=p8");
            Var s8iso = s5;
            s8iso = min(max(s8iso,thermoModel->get_smin_liq()),thermoModel->get_smax_liq());
            Var h8iso = thermoModel->get_h_liq_ps(p8,s8iso);
            Var wpHP = (h8iso-h5)/eta_p;
            Var WorkPumpHP = mHP*wpHP;
            Var h8 = h5*(1.-1./eta_p) + h8iso/eta_p;
            result.ineq.push_back((thermoModel->get_hmin_liq()-h8)/1e2,"hmin<=h8");
            result.ineq.push_back((h8-thermoModel->get_hliq_p(p8))/1e2,"h8<=hliq(p8)");
            h8 = min(max(h8,thermoModel->get_hmin_liq()),thermoModel->get_hmax_liq());
            Var T8 = thermoModel->get_T_liq_ph(p8,h8);
        // HP Preheater
            Var p9 = p8;
            Var T9 = thermoModel->get_Ts_p(p9) - dTap;
            T9 = max(min(T9,thermoModel->get_Tmax_liq()),thermoModel->get_Tmin_liq());
            Var h9 = thermoModel->get_h_liq_pT(p9,T9);
            Var QHPPre = mHP*(h9-h8);
        // HP Evaporator
            Var p10 = p8;
            Var h10 = thermoModel->get_hvap_p(p10);
            Var T10 = thermoModel->get_Ts_p(p10);
            Var QHPEvap = mHP*(h10-h9);
        // HP Superheater
            Var QHPSH = mHP*(h11-h10);
        // LP Evaporator
            Var p6 = p5;
            Var h6 = thermoModel->get_hvap_p(p6);
            Var T6 = thermoModel->get_Ts_p(p6);
            Var QLPEvap = mLP*(h6-h5);
        // LP Superheater
            Var QLPSH = mLP*(h7-h6);
        // Gas side
            Var TG2 = TGin - QHPSH/mcpG;
            Var TG3 = TGin - mHP*(h11-h9)/mcpG;
            result.ineq.push_back((dTmin - (TG3-T10))/1e1,"TG3-T10>=dTmin");
            result.ineq.push_back((dTmin - (TG3-T7))/1e1,"TG3-T7>=dTmin");
            Var TG4 = TGin - (mHP*(h11-h9-h7+h6)+mdot*(h7-h6))/mcpG;
            result.ineq.push_back((dTmin - (TG4-T9))/1e1,"TG4-T9>=dTmin");
            Var TG5 = TGin - (mHP*(h11-h8-h7+h6)+mdot*(h7-h6))/mcpG;
            Var TG6 = TGin - (mHP*(h11-h8-h7+h5)+mdot*(h7-h5))/mcpG;
            result.ineq.push_back((dTmin - (TG6-T6))/1e1,"TG6-T6>=dTmin");
            Var TG7 = TGin - (mHP*(h11-h8-h7+h5)+mdot*(h7-h4))/mcpG;
            result.ineq.push_back((dTmin - (TG7-T4))/1e1,"TG7-T4>=dTmin");
        // Overall
            Var WorkNet = mHP*(wtHP-wpHP) + mBleed*(wtBleed-wpLP) + mMain*(wtMain-wpCond-wpLP);
            Var Qzu = QLPPre + QLPEvap + QLPSH + QHPPre + QHPEvap + QHPSH;
            Var eta = WorkNet/Qzu;
        // CCPP
            Var WorkTotal = WorkNet + WorkGT;
            result.ineq.push_back((WorkGT - WorkTotal)/1e5,"WorkGT<=WorkTotal");
            WorkTotal = max(WorkTotal,WorkGT);
            Var etaCC = WorkTotal / FuelHeat;

        // Investment cost
            // Condenser
                Var dT1 = T15-Tcout;
                Var dT2 = T1-Tcin;
                dT1 = max(5.,dT1);
                dT2 = max(5.,dT2);
				result.output.emplace_back("T15",T15);
				result.output.emplace_back("T1",T1);
				result.output.emplace_back("Tcin",Tcin);
				result.output.emplace_back("Tcout",Tcout);
				result.output.emplace_back("dT1",dT1);
				result.output.emplace_back("dT2",dT2);
                Var rLMTD = rlmtd(dT1,dT2);
                Var ACond = rLMTD*QCond/kCond;
                result.ineq.push_back((Amin-ACond)/1e1,"Amin<=ACond");
                ACond = max(Amin,ACond);
                Var Cp0 = cost_function(ACond,1,k1A,k2A,k3A); // pow(10,k1A+k2A*log(ACond)/log(10.0)+k3A*pow(log(ACond)/log(10.0),2));
                Var Fp = 1.0;
                Var InvCond = 1.18 * (B1A+B2A*FmA*Fp) * Cp0;
            // Pumps
                result.ineq.push_back((1e-3-WorkPumpCond)/1e3,"0<=WpCond" );
                result.ineq.push_back((1e-3-WorkPumpLP)/1e3,"0<=WorkPumpLP" );
                result.ineq.push_back((1e-3-WorkPumpHP)/1e3,"0<=WorkPumpHP" );
                Var InvPumpCond = 3540.*pow(max(WorkPumpCond,1e-3),0.71);
                Var InvPumpLP = 3540.*pow(max(WorkPumpLP,1e-3),0.71);
                Var InvPumpHP = 3540.*pow(max(WorkPumpHP,1e-3),0.71);
            // Turbines incl. generator
                result.ineq.push_back((1e-3-WorkTurbHP)/1e5,"0<=WtHP");
                result.ineq.push_back((1e-3-WorkTurbBleed)/1e5,"0<=WtBleed");
                result.ineq.push_back((1e-3-WorkTurbMain)/1e5,"0<=WtBleed");
                Var InvTurb = 6000.*pow(max(WorkTurbHP,1e-3),0.7) + 6000.*pow(max(WorkTurbBleed+WorkTurbMain,2e-3),0.7) + 60.*pow(max(WorkTurbBleed+WorkTurbMain+WorkTurbHP,3e-3),0.95);
            // LP Economizer
                dT1 = TG7 - T4;
                dT2 = TG6 - T5;
                dT1 = max(dTmin,dT1);
                dT2 = max(dTmin,dT2);
                rLMTD = rlmtd(dT1,dT2);
                Var AEcoLP = rLMTD*QLPPre / kEco;
                result.ineq.push_back((Amin-AEcoLP)/1e1,"Amin<=AEcoLP");
                AEcoLP = max(Amin,AEcoLP);
                Cp0 = cost_function(AEcoLP,1,k1A,k2A,k3A);
                Fp = cost_function(p4,1,c1A,c2A,c3A);
                Var InvEcoLP = 1.18 * (B1A+B2A*FmA*Fp) * Cp0;
            // LP Evaporator
                dT1 = TG6 - T6;
                dT2 = TG5 - T6;
                dT1 = max(dTmin,dT1);
                dT2 = max(dTmin,dT2);
                rLMTD = rlmtd(dT1,dT2);
                Var AEvapLP = rLMTD*QLPEvap / kEvap;
                result.ineq.push_back((Amin-AEvapLP)/1e1,"Amin<=AEvapLP");
                AEvapLP = max(Amin,AEvapLP);
                Cp0 = cost_function(AEvapLP,1,k1A,k2A,k3A);
                Var InvEvapLP = 1.18 * (B1A+B2A*FmA*Fp) * Cp0;
            // LP Superheater
                dT1 = TG4 - T6;
                dT2 = TG3 - T7;
                dT1 = max(dTmin,dT1);
                dT2 = max(dTmin,dT2);
                rLMTD = rlmtd(dT1,dT2);
                Var ASHLP = rLMTD*QLPSH / kSH;
                result.ineq.push_back((Amin-ASHLP)/1e1,"Amin<=ASHLP");
                ASHLP = max(Amin,ASHLP);
                Cp0 = cost_function(ASHLP,1,k1A,k2A,k3A);
                Var InvSHLP = 1.18 * (B1A+B2A*FmA*Fp) * Cp0;
            // HP Economizer
                dT1 = TG5 - T8;
                dT2 = TG4 - T9;
                dT1 = max(dTmin,dT1);
                dT2 = max(dTmin,dT2);
                rLMTD = rlmtd(dT1,dT2);
                Var AEcoHP = rLMTD*QHPPre / kEco;
                result.ineq.push_back((Amin-AEcoHP)/1e1,"Amin<=AEcoHP");
                AEcoHP = max(Amin,AEcoHP);
                Cp0 = cost_function(AEcoHP,1,k1A,k2A,k3A);
                Fp = cost_function(p8,1,c1A,c2A,c3A);
                Var InvEcoHP = 1.18 * (B1A+B2A*FmA*Fp) * Cp0;
            // HP Evaporator
                dT1 = TG3 - T10;
                dT2 = TG2 - T10;
                dT1 = max(dTmin,dT1);
                dT2 = max(dTmin,dT2);
                rLMTD = rlmtd(dT1,dT2);
                Var AEvapHP = rLMTD*QHPEvap / kEvap;
                result.ineq.push_back((Amin-AEvapHP)/1e1,"Amin<=AEvapHP");
                AEvapHP = max(Amin,AEvapHP);
                Cp0 = cost_function(AEvapHP,1,k1A,k2A,k3A);
                Var InvEvapHP = 1.18 * (B1A+B2A*FmA*Fp) * Cp0;
            // HP Superheater
                dT1 = TG2 - T10;
                dT2 = TGin - T11;
                dT1 = max(dTmin,dT1);
                dT2 = max(dTmin,dT2);
                rLMTD = rlmtd(dT1,dT2);
                Var ASHHP = rLMTD*QHPSH / kSH;
                result.ineq.push_back((Amin-ASHHP)/1e1,"Amin<=ASHHP");
                ASHHP = max(Amin,ASHHP);
                Cp0 = cost_function(ASHHP,1,k1A,k2A,k3A);
                Var InvSHHP = 1.18 * (B1A+B2A*FmA*Fp) * Cp0;
            // Deaerator
                Var VDae = 1.5 * 600 * mdot * 0.001;
                result.ineq.push_back((Vmin-VDae)/1e0,"Vmin");
                VDae = max(VDae,Vmin);
                Var Cp0DAE = cost_function(VDae,1,k1B,k2B,k3B);
                Var FpDAE = 1.25;
                Var InvDae = 1.18 * (B1B+B2B*FmB*FpDAE) * Cp0DAE;
            // Cycle
                Var Inv = InvCond + InvPumpCond + InvPumpLP + InvPumpHP + InvEcoLP + InvEvapLP + InvSHLP + InvEcoHP + InvEvapHP + InvSHHP + InvTurb + InvDae;

        // Combined Cycle Plant
            Var InvTotal = Inv + InvGT;
            Var CAPEX = InvTotal*fPhi*fAnnu/(WorkTotal/1000*Teq);
            Var FuelCost = FuelPrice/etaCC;
            Var LCOE = (InvTotal*fPhi*fAnnu*1000/Teq + FuelHeat*FuelPrice)/WorkTotal + Varcost;

        // Objective:
            result.objective = LCOE;



    return result;

}