/**
 * @file ThermoModel.h
 *
 * @brief File containing definition of the abstract base class ThermoModel that provides an
 *        interface for unit operations to execute thermodynamic calculations
 *        for solving the combined cycle power plant case studies with the IAPWS-IF97.
 *
 *        The process models were originally published in:
 *        D. Bongartz, A. Mitsos: "Deterministic global optimization of process
 *             flowsheets in a reduced space using McCormick relaxations",
 *             Journal of Global Optimization 69 (2017), 761-796.
 *             https://link.springer.com/article/10.1007/s10898-017-0547-4
 *        E-mail: amitsos@alum.mit.edu
 *
 *        The relaxations of the IAPWS-IF97 were originally published in:
 *        D. Bongartz, J. Najman, and A. Mitsos: "Deterministic global optimization
 *             of steam cycles using the IAPWS‑IF97 model",
 *             Optimization and Engineering (2020), in press.
 *             https://doi.org/10.1007/s11081-020-09502-1
 *        E-mail: amitsos@alum.mit.edu
 *
 * ==============================================================================
 * © 2020, Process Systems Engineering (AVT.SVT), RWTH Aachen University
 * ==============================================================================
 *
 * used in:
 *    D. Bongartz: "Deterministic Global Flowsheet Optimization for the Design of Energy Conversion Processes",
 *     	            PhD Thesis, RWTH Aachen University, 2020.
 *
 */

#pragma once

#include "ffunc.hpp"

#include <string>


namespace maingoflow {


namespace thermo {


class ThermoModel {

public:

    using Var = mc::FFVar;

    virtual ~ThermoModel() {}

    virtual Var get_h_liq_pT(const Var &p, const Var &T) const = 0;
    virtual Var get_T_liq_ph(const Var &p, const Var &h) const = 0;
    virtual Var get_s_liq_pT(const Var &p, const Var &T) const = 0;
    virtual Var get_T_liq_ps(const Var &p, const Var &s) const = 0;
    virtual Var get_h_liq_ps(const Var &p, const Var &s) const = 0;
    virtual Var get_s_liq_ph(const Var &p, const Var &h) const = 0;
    virtual double get_hmin_liq() const = 0;
    virtual double get_hmax_liq() const = 0;
    virtual double get_smin_liq() const = 0;
    virtual double get_smax_liq() const = 0;
    virtual double get_pmin_liq() const = 0;
    virtual double get_pmax_liq() const = 0;
    virtual double get_Tmin_liq() const = 0;
    virtual double get_Tmax_liq() const = 0;
    virtual Var get_h_vap_pT(const Var &p, const Var &T) const = 0;
    virtual Var get_T_vap_ph(const Var &p, const Var &h) const = 0;
    virtual Var get_s_vap_pT(const Var &p, const Var &T) const = 0;
    virtual Var get_T_vap_ps(const Var &p, const Var &s) const = 0;
    virtual Var get_h_vap_ps(const Var &p, const Var &s) const = 0;
    virtual Var get_s_vap_ph(const Var &p, const Var &h) const = 0;
    virtual double get_hmin_vap() const = 0;
    virtual double get_hmax_vap() const = 0;
    virtual double get_smin_vap() const = 0;
    virtual double get_smax_vap() const = 0;
    virtual double get_pmin_vap() const = 0;
    virtual double get_pmax_vap() const = 0;
    virtual double get_Tmin_vap() const = 0;
    virtual double get_Tmax_vap() const = 0;
    virtual Var get_h_px(const Var &p, const Var &x) const = 0;
    virtual Var get_h_Tx(const Var &T, const Var &x) const = 0;
    virtual Var get_s_px(const Var &p, const Var &x) const = 0;
    virtual Var get_s_Tx(const Var &T, const Var &x) const = 0;
    virtual Var get_x_ph(const Var &p, const Var &h) const = 0;
    virtual Var get_x_ps(const Var &p, const Var &s) const = 0;
    virtual Var get_h_twophase_ps(const Var &p, const Var &s) const = 0;
    virtual Var get_s_twophase_ph(const Var &p, const Var &h) const = 0;
    virtual Var get_Ts_p(const Var &p) const = 0;
    virtual Var get_ps_T(const Var &T) const = 0;
    virtual Var get_hliq_p(const Var&p) const = 0;
    virtual Var get_hvap_p(const Var&p) const = 0;
    virtual Var get_hliq_T(const Var&T) const = 0;
    virtual Var get_hvap_T(const Var&T) const = 0;
    virtual Var get_sliq_p(const Var&p) const = 0;
    virtual Var get_svap_p(const Var&p) const = 0;
    virtual Var get_sliq_T(const Var&T) const = 0;
    virtual Var get_svap_T(const Var&T) const = 0;
    virtual double get_hmin_twophase() const = 0;
    virtual double get_hmax_twophase() const = 0;
    virtual double get_smin_twophase() const = 0;
    virtual double get_smax_twophase() const = 0;
    virtual double get_pmin_twophase() const = 0;
    virtual double get_pmax_twophase() const = 0;
    virtual double get_Tmin_twophase() const = 0;
    virtual double get_Tmax_twophase() const = 0;

};


} // end namespace thermo


} // end namespace maingoflow