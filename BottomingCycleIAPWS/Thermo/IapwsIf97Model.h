/**
 * @file IapwsIf97Model.h
 *
 * @brief File containing definition of the class IapwsIf97Model, which specializes the
 *        abstract base class ThermoModel for the case of the IAPWS-IF97 model for the
 *        properties of water for solving the combined cycle power plant case studies.
 *
 *        The process models were originally published in:
 *        D. Bongartz, A. Mitsos: "Deterministic global optimization of process
 *             flowsheets in a reduced space using McCormick relaxations",
 *             Journal of Global Optimization 69 (2017), 761-796.
 *             https://link.springer.com/article/10.1007/s10898-017-0547-4
 *        E-mail: amitsos@alum.mit.edu
 *
 *        The relaxations of the IAPWS-IF97 were originally published in:
 *        D. Bongartz, J. Najman, and A. Mitsos: "Deterministic global optimization
 *             of steam cycles using the IAPWS‑IF97 model",
 *             Optimization and Engineering (2020), in press.
 *             https://doi.org/10.1007/s11081-020-09502-1
 *        E-mail: amitsos@alum.mit.edu
 *
 * ==============================================================================
 * © 2020, Process Systems Engineering (AVT.SVT), RWTH Aachen University
 * ==============================================================================
 *
 * used in:
 *    D. Bongartz: "Deterministic Global Flowsheet Optimization for the Design of Energy Conversion Processes",
 *     	            PhD Thesis, RWTH Aachen University, 2020.
 *
 */

#pragma once

#include "ThermoModel.h"
#include "IAPWS/iapws.h"


namespace maingoflow {


namespace thermo {


class IapwsIf97Model: public ThermoModel {

public:

    Var get_h_liq_pT(const Var &p, const Var &T) const { return iapws(p,T,11); }
    Var get_s_liq_pT(const Var &p, const Var &T) const { return iapws(p,T,12); }
    Var get_T_liq_ph(const Var &p, const Var &h) const { return iapws(p,h,13); }
    Var get_T_liq_ps(const Var &p, const Var &s) const { return iapws(p,s,14); }
    Var get_h_liq_ps(const Var &p, const Var &s) const { return iapws(p,s,15); }
    Var get_s_liq_ph(const Var &p, const Var &h) const { return iapws(p,h,16); }
    double get_hmin_liq() const { return iapws_if97::region1::data::hmin; }
    double get_hmax_liq() const { return iapws_if97::region1::data::hmax; }
    double get_smin_liq() const { return iapws_if97::region1::data::smin; }
    double get_smax_liq() const { return iapws_if97::region1::data::smax; }
    double get_pmin_liq() const { return iapws_if97::region1::data::pmin; }
    double get_pmax_liq() const { return iapws_if97::region1::data::pmax; }
    double get_Tmin_liq() const { return iapws_if97::region1::data::Tmin; }
    double get_Tmax_liq() const { return iapws_if97::region1::data::Tmax; }
    Var get_h_vap_pT(const Var &p, const Var &T) const { return iapws(p,T,21); }
    Var get_s_vap_pT(const Var &p, const Var &T) const { return iapws(p,T,22); }
    Var get_T_vap_ph(const Var &p, const Var &h) const { throw std::runtime_error("Error in IAPWSIf97Model: Function currently not allowed"); }
    Var get_T_vap_ps(const Var &p, const Var &s) const { throw std::runtime_error("Error in IAPWSIf97Model: Function currently not allowed"); }
    Var get_h_vap_ps(const Var &p, const Var &s) const { throw std::runtime_error("Error in IAPWSIf97Model: Function currently not allowed"); }
    Var get_s_vap_ph(const Var &p, const Var &h) const { throw std::runtime_error("Error in IAPWSIf97Model: Function currently not allowed"); }
    double get_hmin_vap() const { return iapws_if97::region2::data::hmin; }
    double get_hmax_vap() const { return iapws_if97::region2::data::hmax; }
    double get_smin_vap() const { return iapws_if97::region2::data::smin; }
    double get_smax_vap() const { return iapws_if97::region2::data::smax; }
    double get_pmin_vap() const { return iapws_if97::region2::data::pmin; }
    double get_pmax_vap() const { return iapws_if97::region2::data::pmax; }
    double get_Tmin_vap() const { return iapws_if97::region2::data::Tmin; }
    double get_Tmax_vap() const { return iapws_if97::region2::data::Tmax; }
    Var get_ps_T(const Var &T) const { return iapws(T,41); }
    Var get_Ts_p(const Var &p) const { return iapws(p,42); }
    Var get_hliq_p(const Var&p) const { return iapws(p,411); }
    Var get_hliq_T(const Var&T) const { return iapws(T,412); }
    Var get_hvap_p(const Var&p) const { return iapws(p,413); }
    Var get_hvap_T(const Var&T) const { return iapws(T,414); }
    Var get_sliq_p(const Var&p) const { return iapws(p,415); }
    Var get_sliq_T(const Var&T) const { return iapws(T,416); }
    Var get_svap_p(const Var&p) const { return iapws(p,417); }
    Var get_svap_T(const Var&T) const { return iapws(T,418); }
    Var get_h_px(const Var &p, const Var &x) const { return iapws(p,x,43); }
    Var get_h_Tx(const Var &T, const Var &x) const { return iapws(T,x,44); }
    Var get_s_px(const Var &p, const Var &x) const { return iapws(p,x,45); }
    Var get_s_Tx(const Var &T, const Var &x) const { return iapws(T,x,46); }
    Var get_x_ph(const Var &p, const Var &h) const { return iapws(p,h,47); }
    Var get_x_ps(const Var &p, const Var &s) const { return iapws(p,s,48); }
    Var get_h_twophase_ps(const Var &p, const Var &s) const { return iapws(p,s,49); }
    Var get_s_twophase_ph(const Var &p, const Var &h) const { return iapws(p,h,410); }
    double get_hmin_twophase() const { return iapws_if97::region4::data::hmin12; }
    double get_hmax_twophase() const { return iapws_if97::region4::data::hmax12; }
    double get_smin_twophase() const { return iapws_if97::region4::data::smin12; }
    double get_smax_twophase() const { return iapws_if97::region4::data::smax12; }
    double get_pmin_twophase() const { return iapws_if97::region4::data::pmin; }
    double get_pmax_twophase() const { return iapws_if97::region4::data::pmax12; }
    double get_Tmin_twophase() const { return iapws_if97::region4::data::Tmin; }
    double get_Tmax_twophase() const { return iapws_if97::region4::data::Tmax12; }

};


} // end namespace thermo


} // end namespace maingoflow