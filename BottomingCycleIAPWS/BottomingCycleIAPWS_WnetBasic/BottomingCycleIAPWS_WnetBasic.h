/**
 *
 * @brief Problem definition for simple bottoming cycle case study with the IAPWS-IF97.
 *
 *        The process model was originally published in:
 *        D. Bongartz, A. Mitsos: "Deterministic global optimization of process
 *             flowsheets in a reduced space using McCormick relaxations",
 *             Journal of Global Optimization 69 (2017), 761-796.
 *             https://link.springer.com/article/10.1007/s10898-017-0547-4
 *        E-mail: amitsos@alum.mit.edu
 *
 *        The relaxations of the IAPWS-IF97 were originally published in:
 *        D. Bongartz, J. Najman, and A. Mitsos: "Deterministic global optimization
 *             of steam cycles using the IAPWS‑IF97 model",
 *             Optimization and Engineering (2020), in press.
 *             https://doi.org/10.1007/s11081-020-09502-1
 *        E-mail: amitsos@alum.mit.edu
 *
 * ==============================================================================
 * © 2020, Process Systems Engineering (AVT.SVT), RWTH Aachen University
 * ==============================================================================
 *
 * used in:
 *    D. Bongartz: "Deterministic Global Flowsheet Optimization for the Design of Energy Conversion Processes",
 *     	            PhD Thesis, RWTH Aachen University, 2020.
 *
 */

#pragma once

#include "MAiNGOmodel.h"
#include "../Thermo/IapwsIf97Model.h"

#include <memory>


class Model: public maingo::MAiNGOmodel {

    public:

        Model();
        maingo::EvaluationContainer evaluate(const std::vector<Var> &optVars);
        std::vector<maingo::OptimizationVariable> get_variables();

    private:

        std::unique_ptr<maingoflow::thermo::ThermoModel> thermoModel;

        const double eta_s, eta_t, mcpG, TGin, TGout, Tmax, dTap, dTmin, xmin, Amin;
        const double kEco, kEvap, kSH, kCond, k1A, k2A, k3A, p1A, p2A, p3A, FmA, B1A, B2A, Tcin, Tcout;
        const double InvGT, WorkGT, FuelHeat, FuelPrice, fPhi, fAnnu, Teq, Varcost;
		double c1A,c2A,c3A;

};


//////////////////////////////////////////////////////////////////////////
// constructor
Model::Model(): 	eta_s(0.8), eta_t(0.9), mcpG(200), TGin(900), TGout(448), Tmax(873.15), dTap(10), dTmin(15), xmin(0.85), Amin(10),
					kEco(0.06), kEvap(0.06), kSH(0.03), kCond(0.35), k1A(4.3247), k2A(-0.303), k3A(0.1634),
					p1A(0.03881), p2A(-0.11272), p3A(0.08183), FmA(2.75), B1A(1.63), B2A(1.66), Tcin(298), Tcout(303),
					InvGT(22.7176e6), WorkGT(69676), FuelHeat(182359), FuelPrice(14), fPhi(1.06), fAnnu(0.1875), Teq(4000), Varcost(4)
{
    thermoModel = std::unique_ptr<maingoflow::thermo::ThermoModel>(new maingoflow::thermo::IapwsIf97Model());
	// Convert coefficients for pressure factor from bar to MPa:
	c1A = p1A+p2A+p3A;
	c2A = p2A+2*p3A;
	c3A = p3A;
}


//////////////////////////////////////////////////////////////////////////
// function for providing optimization variable data to the Branch-and-Bound solver
std::vector<maingo::OptimizationVariable> Model::get_variables() {

    std::vector<maingo::OptimizationVariable> variables;
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.3,10),"p2 [MPa]"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(5,100),"mdot [kg/s]"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(300,873),"T5 [K]"));

    return variables;
}


//////////////////////////////////////////////////////////////////////////
// evaluate the model
maingo::EvaluationContainer Model::evaluate(const std::vector<Var> &optVars)
{

    // Rename inputs
    Var p2 = optVars[0];
    Var mdot = optVars[1];
    Var T5 = optVars[2];

    // Model
        maingo::EvaluationContainer result;
        // Condenser outlet
            Var p1 = 0.1*0.2;   // Convert to MPa...
            Var T1 = thermoModel->get_Ts_p(p1);      // r4, Ts(p))
            Var h1 = thermoModel->get_hliq_p(p1);    // r4-1/2, hliq(p)
            Var s1 = thermoModel->get_sliq_p(p1);    // r4-1/2, sliq(p)
        // Feedwater Pump
            Var h2iso = thermoModel->get_h_liq_ps(p2,s1);    // r1, h(p,s)
            Var wp = (h2iso-h1)/eta_s;
            Var h2 = h1*(1.-1./eta_s)+h2iso/eta_s;
            Var T2 = thermoModel->get_T_liq_ph(p2,h2);       // r1, T(p,h);
            Var WorkPump = mdot*wp;
        // Boiler
        // Overall balance
            Var Qzu = mcpG*(TGin-TGout);
            Var p5 = p2;
            result.ineq.push_back((thermoModel->get_Ts_p(p5)-T5)/100,"Ts(p5)<=T5");    // point 5 needs to stay within region 2 (above saturation line)
            Var h5 = thermoModel->get_h_vap_pT(p5,T5);
            Var s5 = thermoModel->get_s_vap_pT(p5,T5);
            result.eq.push_back((mdot*(h5-h2) - Qzu)/1e4,"Boiler energy balance");
        // Superheater
            Var p4 = p2;
            Var T4 = thermoModel->get_Ts_p(p4);          // r4, Ts(p)
            Var h4 = thermoModel->get_hvap_p(p4);        // r4-1/2, hvap(p)
            Var TG2 = TGin - mdot*(h5-h4)/mcpG;
            Var QSH = mdot*(h2-h4)+Qzu;
        // Evaporator
            Var p3 = p2;
            Var T3 = thermoModel->get_Ts_p(p3)-dTap;     // r4, Ts(p)
            T3 = min(max(T3,thermoModel->get_Tmin_liq()),thermoModel->get_Tmax_liq());
            Var h3 = thermoModel->get_h_liq_pT(p3,T3);   // r1, h(p,T)
            Var TG3 = TGout + mdot*(h3-h2)/mcpG;
            result.ineq.push_back((T4+dTmin - TG3)/10,"dTmin<=TG3-T4");                     // minimum temperature difference
            Var QEco = mdot * (h3-h2);
            Var QEvap = mdot * (h4-h3);
        // Turbine
            // assuming the outlet to be in region 4-1/2
            Var p6 = p1;
            Var s6iso = s5;
            Var T6iso = thermoModel->get_Ts_p(p6);       // r4, Ts(p)
            result.ineq.push_back((s6iso-thermoModel->get_svap_p(p6))/1e0,"s6iso<=svap(p6)");        // point 6iso needs to stay within region 4-1/2
            result.ineq.push_back((thermoModel->get_sliq_p(p6)-s6iso)/1e0,"sliq(p6)<=s6iso");        // point 6iso needs to stay within region 4-1/2
            s6iso = min(max(s6iso,thermoModel->get_smin_twophase()),thermoModel->get_smax_twophase());
            Var x6iso = thermoModel->get_x_ps(p6,s6iso);          // r4-1/2, x(p,s)
            Var h6iso = thermoModel->get_h_twophase_ps(p6,s6iso); // r4-1/2, h(p,s)
            Var h6liq = thermoModel->get_hliq_p(p6);
            Var h6vap = thermoModel->get_hvap_p(p6);
            // actual
            Var wt=eta_t*(h5-h6iso);
            Var WorkTurb = eta_t*(mdot*(h2-h6iso) + Qzu); // mdot*wt;
            Var h6 = h5*(1-eta_t) + eta_t*h6iso; // h5 - wt;
            result.ineq.push_back((h6-thermoModel->get_hvap_p(p6))/100,"h6<=hvap(p6)");              // point 6 needs to stay within region 4-1/2
            result.ineq.push_back((thermoModel->get_hliq_p(p6)-h6)/100,"hliq(p6)<=h6");          // point 6 needs to stay within region 4-1/2
            h6 = min(max(h6,thermoModel->get_hmin_twophase()),thermoModel->get_hmax_twophase());
            Var T6 = thermoModel->get_Ts_p(p6);      // r4, Ts(p)
            Var x6 = thermoModel->get_x_ph(p6,h6);   // r4-1/2, x(p,h)
            result.ineq.push_back((xmin*h6vap+(1-xmin)*h6liq-h6)/100,"xmin<=x6");
        // Overall
            Var WorkNet = mdot*(eta_t*(h1*(1.-1./eta_s)-h6iso)+(eta_t-1)*h2iso/eta_s+h1/eta_s) + eta_t*Qzu; // WorkTurb-WorkPump;
            Var eta=WorkNet/Qzu;
        // Condenser
            Var QCond = mdot * (h6 - h1);
        // Combined Cycle Plant
            Var WorkTotal = WorkNet + WorkGT;
            result.ineq.push_back((WorkGT - WorkTotal)/1e5,"WorkGT<=WorkTotal");
            WorkTotal = max(WorkTotal,WorkGT);
            Var etaCC = WorkTotal / FuelHeat;

        // Objective:
            result.objective = -WorkNet;

    return result;

}