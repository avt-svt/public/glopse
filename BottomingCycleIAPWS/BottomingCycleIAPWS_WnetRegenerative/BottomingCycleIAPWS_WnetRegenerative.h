/**
 *
 * @brief Problem definition for regenerative bottoming cycle case study with the IAPWS-IF97.
 *
 *        The process model was originally published in:
 *        D. Bongartz, A. Mitsos: "Deterministic global optimization of process
 *             flowsheets in a reduced space using McCormick relaxations",
 *             Journal of Global Optimization 69 (2017), 761-796.
 *             https://link.springer.com/article/10.1007/s10898-017-0547-4
 *        E-mail: amitsos@alum.mit.edu
 *
 *        The relaxations of the IAPWS-IF97 were originally published in:
 *        D. Bongartz, J. Najman, and A. Mitsos: "Deterministic global optimization
 *             of steam cycles using the IAPWS‑IF97 model",
 *             Optimization and Engineering (2020), in press.
 *             https://doi.org/10.1007/s11081-020-09502-1
 *        E-mail: amitsos@alum.mit.edu
 *
 * ==============================================================================
 * © 2020, Process Systems Engineering (AVT.SVT), RWTH Aachen University
 * ==============================================================================
 *
 * used in:
 *    D. Bongartz: "Deterministic Global Flowsheet Optimization for the Design of Energy Conversion Processes",
 *     	            PhD Thesis, RWTH Aachen University, 2020.
 *
 */

#pragma once

#include "MAiNGOmodel.h"
#include "../Thermo/IapwsIf97Model.h"

#include <memory>


class Model: public maingo::MAiNGOmodel {

    public:

        Model();
        maingo::EvaluationContainer evaluate(const std::vector<Var> &optVars);
        std::vector<maingo::OptimizationVariable> get_variables();

    private:

        std::unique_ptr<maingoflow::thermo::ThermoModel> thermoModel;

        const double eta_p, eta_t, mcpG, TGin, Tmax, dTap, dTmin, xmin, Amin, Vmin;
        const double kEco, kEvap, kSH, kCond, k1A, k2A, k3A, p1A, p2A, p3A, FmA, B1A, B2A, k1B, k2B, k3B, FmB, B1B, B2B, Tcin, Tcout;
        const double InvGT, WorkGT, FuelHeat, FuelPrice, fPhi, fAnnu, Teq, Varcost;
		double c1A,c2A,c3A;

};


//////////////////////////////////////////////////////////////////////////
// function for providing optimization variable data to the Branch-and-Bound solver
std::vector<maingo::OptimizationVariable> Model::get_variables() {

    std::vector<maingo::OptimizationVariable> variables;
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.02,0.5),"p2 [MPa]"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.3,10),"p4 [MPa]"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds((1-0.2)*5,(1-0.01)*100),"mMain [kg/s]"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.01*5,0.2*100),"mBleed [-]"));
    variables.push_back(maingo::OptimizationVariable(maingo::Bounds(300,873),"T7 [K]"));

    return variables;
}


//////////////////////////////////////////////////////////////////////////
// constructor
Model::Model():     eta_p(0.8), eta_t(0.9), mcpG(200), TGin(900), Tmax(873.15), dTap(10), dTmin(15), xmin(0.85), Amin(10), Vmin(1),
					kEco(0.06), kEvap(0.06), kSH(0.03), kCond(0.35), k1A(4.3247), k2A(-0.303), k3A(0.1634),
					p1A(0.03881), p2A(-0.11272), p3A(0.08183), FmA(2.75), B1A(1.63), B2A(1.66), k1B(3.5565), k2B(0.3776), k3B(0.0905),
					FmB(1), B1B(1.49), B2B(1.52), Tcin(273.15+20), Tcout(273.15+25),
					InvGT(22.7176e6), WorkGT(69676), FuelHeat(182359), FuelPrice(14), fPhi(1.06), fAnnu(0.1875), Teq(4000), Varcost(4)
{
	thermoModel = std::unique_ptr<maingoflow::thermo::ThermoModel>(new maingoflow::thermo::IapwsIf97Model());
	// Convert coefficients for pressure factor from bar to MPa:
	c1A = p1A+p2A+p3A;
	c2A = p2A+2*p3A;
	c3A = p3A;
}


//////////////////////////////////////////////////////////////////////////
// evaluate the model
maingo::EvaluationContainer Model::evaluate(const std::vector<Var> &optVars)
{

    // Rename inputs
        Var p2 = optVars[0];
        Var p4 = optVars[1];
        Var mMain = optVars[2];
        Var mBleed = optVars[3];
        Var T7 = optVars[4];
        Var mdot = mMain + mBleed;

    // Model
        maingo::EvaluationContainer result;
		// Ensure compatibility with original model:
			result.ineq.push_back((mBleed - 0.2*mdot)/1.);
			result.ineq.push_back((0.01*mdot - mBleed)/0.1);
        // Turbines:
        // Inlet:
            Var p7 = p4;
            result.ineq.push_back((thermoModel->get_Ts_p(p7)-T7)/1e2,"Ts(p7)<=T7");
            Var h7 = thermoModel->get_h_vap_pT(p7,T7);
            Var s7 = thermoModel->get_s_vap_pT(p7,T7);
        // Turbine 1 (bleed):
            Var p8 = p2;
            Var s8iso = s7;
            result.ineq.push_back((thermoModel->get_sliq_p(p8)-s8iso)/1e0,"sliq(p8)<=s8iso");
            result.ineq.push_back((s8iso-thermoModel->get_svap_p(p8))/1e0,"s8iso<=svap(p8)");
            s8iso = min(max(s8iso,thermoModel->get_smin_twophase()),thermoModel->get_smax_twophase());
            Var h8iso = thermoModel->get_h_twophase_ps(p8,s8iso);
            Var wtBleed = eta_t*(h7-h8iso);
            Var WorkTurbBleed = mBleed*wtBleed;
            Var h8 = h7*(1.-eta_t) + eta_t*h8iso;
            result.ineq.push_back((thermoModel->get_hliq_p(p8)-h8)/1e0,"hliq(p8)<=h8");
            result.ineq.push_back((h8-thermoModel->get_hvap_p(p8))/1e0,"h8<=hvap(p8)");
            h8 = min(h8,thermoModel->get_hmax_twophase());
            Var x8 = thermoModel->get_x_ph(p8,h8);
            Var T8 = thermoModel->get_Ts_p(p8);
        // Turbine 2 (main):
            Var p1 = 0.1*0.05;  // Convert to MPa
            Var p9 = p1;
            Var s9iso = s7;
            result.ineq.push_back((thermoModel->get_sliq_p(p9)-s9iso)/1e0,"sliq(p9)<=s9iso");
            result.ineq.push_back((s9iso-thermoModel->get_svap_p(p9))/1e0,"s9iso<=svap(p9)");
            s9iso = min(max(s9iso,thermoModel->get_smin_twophase()),thermoModel->get_smax_twophase());
            Var h9iso = thermoModel->get_h_twophase_ps(p9,s9iso);
            Var wtMain = eta_t*(h7-h9iso);
            Var WorkTurbMain = mMain*wtMain;
            Var h9 = h7*(1.-eta_t) + eta_t*h9iso;
            result.ineq.push_back((thermoModel->get_hliq_p(p9)-h9)/1e0,"hliq(p9)<=h9");
            result.ineq.push_back((h9-thermoModel->get_hvap_p(p9))/1e0,"h9<=hvap(p9)");
            h9 = min(max(h9,thermoModel->get_hmin_twophase()),thermoModel->get_hmax_twophase());
            Var T9 = thermoModel->get_Ts_p(p9);
            Var x9 = thermoModel->get_x_ph(p9,h9);
            Var h9liq = thermoModel->get_hliq_p(p9);
            Var h9vap = thermoModel->get_hvap_p(p9);
            result.ineq.push_back((xmin*h9vap-h9+h9liq*(1-xmin))/1e1,"xmin<=x9");
        // Feedwater line
        // Condenser
            Var T1 = thermoModel->get_Ts_p(p1);
            Var h1 = thermoModel->get_hliq_p(p1);
            Var s1 = thermoModel->get_sliq_p(p1);
            Var QCond = mMain*(h9-h1);
        // Condensate pump
            Var s2iso = s1;
            Var h2iso = thermoModel->get_h_liq_ps(p2,s2iso);
            Var wpCond = (h2iso-h1)/eta_p;
            Var WorkPumpCond = mMain*wpCond;
            Var h2 = h1*(1.-1./eta_p) + h2iso/eta_p;
            Var T2 = thermoModel->get_T_liq_ph(p2,h2);
        // Deaerator
            Var p3 = p2;
            Var h3 = thermoModel->get_hliq_p(p3);
            result.eq.push_back((mBleed*(h3-h8)+mMain*(h3-h2))/1e3,"Deaerator energy balance");
            Var s3 = thermoModel->get_sliq_p(p3);
            Var T3 = thermoModel->get_Ts_p(p3);
        // Feedwater pump
            result.ineq.push_back((p2-p4)/1e0,"p2<=p4");
            Var s4iso = s3;
            s4iso = min(max(s4iso,thermoModel->get_smin_liq()),thermoModel->get_smax_liq());
            Var h4iso = thermoModel->get_h_liq_ps(p4,s4iso);
            Var wpFeed = (h4iso-h3)/eta_p;
            Var WorkPumpFeed = mdot*wpFeed;
            Var h4 = h3*(1.-1./eta_p) + h4iso/eta_p;
            result.ineq.push_back((thermoModel->get_hmin_liq()-h4)/1e2,"hmin<=h4");
            result.ineq.push_back((h4-thermoModel->get_hliq_p(p4))/1e2,"h4<=hliq(p4)");
            h4 = min(max(h4,thermoModel->get_hmin_liq()),thermoModel->get_hmax_liq());
            Var T4 = thermoModel->get_T_liq_ph(p4,h4);
        // Boiler
        // Superheater
            Var p6 = p4;
            Var T6 = thermoModel->get_Ts_p(p6);
            Var h6 = thermoModel->get_hvap_p(p6);
            Var QSH = mdot*(h7-h6);
            Var TG2 = TGin - QSH/mcpG;
        // Evaporator
            Var p5 = p4;
            Var T5 = thermoModel->get_Ts_p(p5) - dTap;
            T5 = max(min(T5,thermoModel->get_Tmax_liq()),thermoModel->get_Tmin_liq());
            Var h5 = thermoModel->get_h_liq_pT(p5,T5);
            Var QEvap = mdot*(h6-h5);
            Var TG3 = TGin - mdot*(h7-h5)/mcpG;
            result.ineq.push_back((dTmin-(TG3-T6))/1e1,"Pinch: Evaporator, cold end");
        // Eco
            Var QEco = mdot*(h5-h4);
            Var TGout = TGin - mdot*(h7-h4)/mcpG;
            result.ineq.push_back((dTmin-(TGout-T4))/1e1,"Pinch: Economizer, cold end");
        // Overall
            Var WorkNet = mBleed*(wtBleed-wpFeed) + mMain*(wtMain-wpCond-wpFeed);
            Var Qzu = mdot*(h7-h4);
            Var eta = WorkNet/Qzu;
        // Combined Cycle Power Plant
            Var WorkTotal = WorkNet + WorkGT;
            result.ineq.push_back((WorkGT - WorkTotal)/1e5,"WorkGT<=WorkTotal");
            WorkTotal = max(WorkTotal,WorkGT);
            Var etaCC = WorkTotal / FuelHeat;

       // Objective:
            result.objective = -WorkNet;

    return result;
}