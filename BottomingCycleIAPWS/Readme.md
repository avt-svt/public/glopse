# BottomingCycleIAPWS

Flowsheet optimization problems for the design of bottoming cycles for combined cycle power plants using the [IAPWS-IF97 model](https://asmedigitalcollection.asme.org/gasturbinespower/article/122/1/150/461340/The-IAPWS-Industrial-Formulation-1997-for-the) for the properties of water and steam.
These problems are only available for the C++-API of MAiNGO. They use custom relaxations of the functions from the IAPWS-IF97.

The problems were originally implemented for the following publication:
 - Bongartz, D., & Mitsos, A. (2017). [Deterministic global optimization of process flowsheets in a reduced space using McCormick relaxations](https://link.springer.com/article/10.1007/s10898-017-0547-4). *Journal of Global Optimization*, 69(4), 761-796.

The custom relaxations were developed for the following publication:
 - Bongartz, D., Najman, J., & Mitsos, A. (2020). [Deterministic global optimization of steam cycles using the IAPWS‑IF97 model](https://link.springer.com/article/10.1007/s11081-020-09502-1), *Optimization & Engineering*, https://doi.org/10.1007/s11081-020-09502-1


| Instance        | Problem Type | Number of Continuous Variables | Number of Integer Variables | Number of Binary Variables | Number of Equalities | Number of Inequalities | Best Known Objective Value | Proven Objective Bound | Description           |
| ------------- |-------------| ------------| ------------| ------------| ------------| ------------| :------------: | :-----: | ----- |
| BottomingCycleIAPWS_WnetBasic  | DNLP | 3 | 0 | 0 | 1 | 8 | -31735.05 | -31735.06 | Maximize net power output of a basic Rankine cycle using a reduced-space formulation |
| BottomingCycleIAPWS_LCOEBasic  | DNLP | 3 | 0 | 0 | 1 | 14 | 50.7656 | 50.7654 | Minimize levelized cost of electricity of a basic Rankine cycle using a reduced-space formulation |
| BottomingCycleIAPWS_WnetRegenerative  | DNLP | 5 | 0 | 0 | 1 | 18 | -35705.05 | -35705.10 | Maximize net power output of a regenerative Rankine cycle using a reduced-space formulation |
| BottomingCycleIAPWS_LCOERegenerative  | DNLP | 5 | 0 | 0 | 1 | 27 | 51.2 | 51.0 | Minimize levelized cost of electricity of a regenerative Rankine cycle using a reduced-space formulation |
| BottomingCycleIAPWS_WnetTwoPressure  | DNLP | 10 | 0 | 0 | 1 | 31 | -39750 | -40149 | Maximize net power output of a two-pressure cycle using a reduced-space formulation |
| BottomingCycleIAPWS_LCOETwoPressure  | DNLP | 10 | 0 | 0 | 3 | 45 | 52.0 | 50.1 | Minimize levelized cost of electricity of a two-pressure cycle using a reduced-space formulation |