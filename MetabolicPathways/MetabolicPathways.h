/**
 *
 * @brief TSP model based on
 *        C. G. Moles, P. Mendes, J. R. Banga: "Parameter estimation in biochemical
 *              pathways: a comparison of global optimization methods", Genome
 *              research 13(11), 2467-2474 (2003).
 *              https://www.doi.org/10.1101/gr.1262503 
 *        A. F. Villaverde, F. Fröhlich, D. Weindl, J. Hasenauer, J. R. Banga:
 *              "Benchmarking optimization methods for parameter estimation in
 *              large kinetic models", Bioinformatics 35(5), 830-838 (2019). 
 *              https://www.doi.org/10.1093/bioinformatics/bty736
 *        that was originally implemented for
 *        S. Sass, A. Mitsos, D. Bongartz, I. H. Bell, N. I. Nikolov, A. Tsoukalas:
 *              A branch-and-bound algorithm with growing datasets for large-scale
 *              parameter estimation", European Journal of Operational Research 316(1),
 *              36-45 (2024).
 *              https://www.doi.org/10.1016/j.ejor.2024.02.020
 *        E-mail: amitsos@alum.mit.edu
 *
 * ==============================================================================
 * © 2024, Process Systems Engineering (AVT.SVT), RWTH Aachen University
 * ==============================================================================
 */

#pragma once

#include "MAiNGOmodel.h"


namespace userInput {
    // Enter the path to the directory containing the csv-file with the data
    std::string pathToCSVfile = "../GloPSE/MetabolicPathways/data/";

    // Pick the dataset you want to optimize for:
    std::string tspModel = "TSPexact";
    //std::string tspModel = "TSPnoisy";
}

///////////////
//// 1. Model-specific auxiliary functions etc.
///////////////

//////////////////////////////////////////////////////////////////////////
// Determining inputs
namespace tsp {
    // We use the first pair of inputs from Moles et al. (2003)
    const double S = 0.1;
    const double P = 0.05;
}


///////////////
//// 2. Data input
///////////////

//////////////////////////////////////////////////////////////////////////
// auxiliary function for reading data from csv-file
void read_csv_to_vector(const std::string name, std::vector<std::vector<double>>& data)
{
    // Open data file
    std::ifstream infile;
    infile.open(name);
    if (infile.fail()) {
        throw std::runtime_error("Error in model: Unable to open data file "+name);
    }
    else {
        std::cout << "Read data from " << name << std::endl;
    }

    std::string line, tmpCooStr;
    std::vector<double> tmpData;
    double tmpCoo;
    data.clear();
    // Forget about first line: header
    std::getline(infile, line);
    // Read all lines
    while(std::getline(infile,line)){
        // Split line into coordinates
        std::istringstream stream(line);
        tmpData.resize(0);
        // Read entries
        while(std::getline(stream, tmpCooStr, ';')){
            stream >> tmpCoo;
            tmpData.push_back(tmpCoo);
        }
        if (tmpData.size() != 8) {
            throw std::runtime_error("Error in model: Unexpected number of states in data file "+name);
        }
        else {
            data.push_back(tmpData);
        }
    }
}


///////////////
//// 3. MAiNGO model
///////////////

class Model : public maingo::MAiNGOmodel {

public:

    Model();

    maingo::EvaluationContainer evaluate(const std::vector<Var>& optVars);
    std::vector<maingo::OptimizationVariable> get_variables();
    std::vector<double> get_initial_point();

private:
    // Time discretization
    const double _dt_data  = 6.; // Time discretization of data
    const double _dt_euler = 1.; // Time discretization of Euler loop: necessarily _dt_euler <= _dt_data
    const double _tf = 120.;     // Final time

    // Measurement data
    unsigned int _noOfDataPoints;                // Number of data points in full dataset
    std::vector<std::vector<double>> _dataPoint; // Full dataset
};


//////////////////////////////////////////////////////////////////////////
// function for providing optimization variable data to the Branch-and-Bound solver
std::vector<maingo::OptimizationVariable>
Model::get_variables()
{

    std::vector<maingo::OptimizationVariable> variables;

    for (auto i = 1; i <= 3; i++) {
        variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.05, 100.), maingo::VT_CONTINUOUS, "Ki" + std::to_string(i)));
        variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.05, 100.), maingo::VT_CONTINUOUS, "Ka" + std::to_string(i)));
        variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.05, 100.), maingo::VT_CONTINUOUS, "K" + std::to_string(i + 3)));
        variables.push_back(maingo::OptimizationVariable(maingo::Bounds(0.05, 100.), maingo::VT_CONTINUOUS, "V" + std::to_string(i)));
    }

    return variables;
}


//////////////////////////////////////////////////////////////////////////
// function for providing initial point data to the Branch-and-Bound solver
std::vector<double>
Model::get_initial_point()
{
    std::vector<double> initialPoint;
    
    // Use the following to start the optimization from the nominal solution
    // of Moles et al. (2003)
    //initialPoint.resize(12,1.);

    return initialPoint;
}


//////////////////////////////////////////////////////////////////////////
// constructor for the model
Model::Model()
{
    // Catch measurement data from file
    std::string fileData = userInput::pathToCSVfile +  "data_" + userInput::tspModel + "_exp1.csv";
    read_csv_to_vector(fileData, _dataPoint);

    // Catch size of full dataset
    _noOfDataPoints = _dataPoint.size();

    if (_noOfDataPoints != (std::trunc(_tf / _dt_data) + 1) ) {
        throw std::runtime_error("Error in model: Unexpected number of data points in data file " +fileData);
    }
}


//////////////////////////////////////////////////////////////////////////
// evaluate the model
maingo::EvaluationContainer
Model::evaluate(const std::vector<Var>& optVars)
{
    // Catch the optimization variables
    // The vector optVars is of the same size and sorted in the same order as the user-defined variables vector in function get_variables()
    std::vector<Var> Ki, Ka, K, V;

    unsigned int iVar = 0;
    for (auto i = 1; i <= 3; i++) {
        Ki.push_back(optVars.at(iVar));
        iVar++;
        Ka.push_back(optVars.at(iVar));
        iVar++;
        K.push_back(optVars.at(iVar)); // Shift naming from K4..K6 to K[0] ... K[2]
        iVar++;
        V.push_back(optVars.at(iVar));
        iVar++;
    }

    // Model
    maingo::EvaluationContainer result;

    std::vector<Var> G, E, M, vGsyn, vEsyn, vMfrac;
    G.resize(3);
    E.resize(3);
    M.resize(4);
    vGsyn.resize(3);
    vEsyn.resize(3);
    vMfrac.resize(3);

    // Fix parameters to values of Moles et al. (2003)
    std::vector<double> kcat, KmInv, k;
    kcat.resize(3, 1.);
    KmInv.resize(6, 1.);

    k.resize(6, 1.);
    for (auto i = 3; i < 6; i++) {
        k[i] = 0.1;
        V.push_back(0.1);
    }
    // Implicitly used in model equations: ni == na == 2

    // Initial values
    for (auto i = 0; i < 3; i++) {
        G[i] = _dataPoint[0][i];
        E[i] = _dataPoint[0][i + 3];
    }
    // Extend M with inputs to M = [S, M1, M2, P]
    M[0] = tsp::S;
    M[1] = _dataPoint[0][6];
    M[2] = _dataPoint[0][7];
    M[3] = tsp::P;

    // Nested Euler loop
    Var se = 0; // Summed squared prediction error (SSE)
    unsigned int noOfInnerSteps = std::trunc(_dt_data / _dt_euler);
   // Outer Euler loop: Time discretization of data points
    for (auto idxTimeData = 1; idxTimeData < _noOfDataPoints; idxTimeData++) {
        // Inner Euler loop: Intermediate steps
        for (auto idxTime = 0; idxTime < noOfInnerSteps; idxTime++) {
            // Auxiliary equations
            for (auto i = 0; i < 3; i++) {
                vGsyn[i] = V[i] / (1 + sqr(M[3] / Ki[i]) + sqr(Ka[i] / M[i]));

                vEsyn[i] = V[i + 3] * G[i] / (K[i] + G[i]);

                vMfrac[i] = kcat[i] * KmInv[2 * i] * E[i] /
                        (1 + M[i] * KmInv[2 * i] + M[i + 1] * KmInv[2 * i + 1]);
            }

            // Values of differential states for next time step t_i+1
            for (auto i = 0; i < 3; i++) {
                // mRNA synthesis and degradation
                G[i] = pos(_dt_euler * vGsyn[i] + (1 - _dt_euler * k[i]) * G[i]);
                // Protein synthesis and degradation
                E[i] = pos(_dt_euler * vEsyn[i] + (1 - _dt_euler * k[i + 3]) * E[i]);
            }

            for (auto i = 1; i < 3; i++) {
                M[i] = pos((1 - _dt_euler * vMfrac[i - 1] - _dt_euler * vMfrac[i]) * M[i]
                        + _dt_euler * vMfrac[i - 1] * M[i - 1]
                        + _dt_euler * vMfrac[i] * M[i + 1]);
            }
            // Recall that M[0] and M[3] are constant inputs
        }
            
        // Order within _dataPoint: G1, G2, G3, E1, E2, E3, M1, M2

        // Compare to measurements
        Var sePerData = sqr(M[1] - _dataPoint[idxTimeData][6]) + sqr(M[2] - _dataPoint[idxTimeData][7]);
        for (auto i = 0; i < 3; i++) {
            sePerData = sePerData + sqr(G[i] - _dataPoint[idxTimeData][i])
                                  + sqr(E[i] - _dataPoint[idxTimeData][i + 3]);
        }
        se += sePerData;
            
        // Objective for B&B algorithm with growing datasets
        result.objective_per_data.push_back(sePerData);
    }
    // Objective for standard B&B algorithm
    result.objective = se; // SSE
//    result.objective = se / _noOfDataPoints; // Mean squared error (MSE)

    return result;
}