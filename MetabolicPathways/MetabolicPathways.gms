* TSP model based on
*     C. G. Moles, P. Mendes, J. R. Banga: "Parameter estimation in biochemical
*          pathways: a comparison of global optimization methods", Genome
*          research 13(11), 2467-2474 (2003).
*          https://www.doi.org/10.1101/gr.1262503 
*     A. F. Villaverde, F. Froehlich, D. Weindl, J. Hasenauer, J. R. Banga:
*          "Benchmarking optimization methods for parameter estimation in
*          large kinetic models", Bioinformatics 35(5), 830-838 (2019). 
*          https://www.doi.org/10.1093/bioinformatics/bty736
* that was originally implemented for
*     S. Sass, A. Mitsos, D. Bongartz, I. H. Bell, N. I. Nikolov, A. Tsoukalas:
*           A branch-and-bound algorithm with growing datasets for large-scale
*           parameter estimation", European Journal of Operational Research 316(1),
*           36-45 (2024).
*           https://www.doi.org/10.1016/j.ejor.2024.02.020
* E-mail: amitsos@alum.mit.edu
*
* Copyright (c) 2024 Process Systems Engineering (AVT.SVT), RWTH Aachen University

*
* Optimization variables
*

*Continuous variables
variables Ki1, Ki2, Ki3, Ka1, Ka2, Ka3, K4, K5, K6, V1, V2, V3, objectiveVar;

*Continuous variable bounds
Ki1.LO = 0.05;
Ki1.UP = 100.0;
Ki2.LO = 0.05;
Ki2.UP = 100.0;
Ki3.LO = 0.05;
Ki3.UP = 100.0;
Ka1.LO = 0.05;
Ka1.UP = 100.0;
Ka2.LO = 0.05;
Ka2.UP = 100.0;
Ka3.LO = 0.05;
Ka3.UP = 100.0;
K4.LO = 0.05;
K4.UP = 100.0;
K5.LO = 0.05;
K5.UP = 100.0;
K6.LO = 0.05;
K6.UP = 100.0;
V1.LO = 0.05;
V1.UP = 100.0;
V2.LO = 0.05;
V2.UP = 100.0;
V3.LO = 0.05;
V3.UP = 100.0;

*Use the following to start from the nominal solution of Moles et al. (2003)
*Ki1.L = 1.0;
*Ki2.L = 1.0;
*Ki3.L = 1.0;
*Ka1.L = 1.0;
*Ka2.L = 1.0;
*Ka3.L = 1.0;
*K4.L = 1.0;
*K5.L = 1.0;
*K6.L = 1.0;
*V1.L = 1.0;
*V2.L = 1.0;
*V3.L = 1.0;

*Dummy values for denominators to avoid division by zero
*Not required when using initial solution above
K4.L = 1;
K5.L = 1;
K6.L = 1;


*
* Constants and data
*

*Constants
Scalar
   S 'input S' / 0.1 /
   P 'input P' / 0.05 /
   dt 'step width of explicit Euler scheme' /1.0/
   ;
   
*Fixed parameters
Set i4 'index of fixed parameter'   / 4 * 6  /;
Set i6 'index of fixed parameter'   / 1 * 6  /;
Parameter
   k(i6) / 1*3 1.0, 4*6 0.1/
   V(i4) / 4*6 0.1 /;

*Time discretization
Set
    idxTime 'time index in Euler loop' / t0 * t120 /
    idxTime0(idxTime) 'initial time point';
    idxTime0(idxTime) = yes$(ord(idxTime) = 1);

*Dataset
Set
   idxData(idxTime)  'time index of data point'
   ivar 'index of state variable';

*Pick the dataset you want to optimize for 
$call csv2gdx data/data_TSPexact_exp1.csv id=meas fieldSep=SemiColon index=1 values=2..lastCol useHeader=y output=data.gdx
*$call csv2gdx data/data_TSPnoisy_exp1.csv id=meas fieldSep=SemiColon index=1 values=2..lastCol useHeader=y output=data.gdx

$gdxIn data.gdx
$load idxData  = Dim1
$load ivar = Dim2

Parameter meas(idxData,ivar) 'measurements';
$load meas
$gdxIn
display meas;

*
* Model equations
*

*Differential states and auxiliary variables
Set i3 'index of state'   / 1 * 3 /;
Set i2 'index of state'   / 1 * 2 /;

Variables E(idxTime,i3), vEsyn(idxTime,i3), G(idxTime,i3), vGsyn(idxTime,i3), M(idxTime,i2), ddtM(idxTime,i2);

*Dummy values for denominators to avoid division by zero
M.L(idxTime,'1') = meas('t0','M1');
M.L(idxTime,'2') = meas('t0','M2');

*Initial values
G.fx(idxTime0,'1') = meas('t0','G1');
G.fx(idxTime0,'2') = meas('t0','G2');
G.fx(idxTime0,'3') = meas('t0','G3');
E.fx(idxTime0,'1') = meas('t0','E1');
E.fx(idxTime0,'2') = meas('t0','E2');
E.fx(idxTime0,'3') = meas('t0','E3');
M.fx(idxTime0,'1') = meas('t0','M1');
M.fx(idxTime0,'2') = meas('t0','M2');

*Equation variables
Equations
    synthesisG1(idxTime)
    synthesisG2(idxTime)
    synthesisG3(idxTime)
    synthesisE1(idxTime)
    synthesisE2(idxTime)
    synthesisE3(idxTime)
    derivativeM1(idxTime)
    derivativeM2(idxTime)
    updatemRNA1(idxTime)
    updatemRNA2(idxTime)
    updatemRNA3(idxTime)
    odeE1(idxTime)
    odeE2(idxTime)
    odeE3(idxTime)
    odeM(idxTime,i2)
    objective;

*Auxiliary equations
synthesisG1(idxTime) .. vGsyn(idxTime,'1') =E= V1 / (1 + sqr(P / Ki1) + sqr(Ka1 / S));
synthesisG2(idxTime) .. vGsyn(idxTime,'2') =E= V2 / (1 + sqr(P / Ki2) + sqr(Ka2 / M(idxTime,'1')));
synthesisG3(idxTime) .. vGsyn(idxTime,'3') =E= V3 / (1 + sqr(P / Ki3) + sqr(Ka3 / M(idxTime,'2')));

synthesisE1(idxTime) .. vEsyn(idxTime,'1') =E= V('4') * G(idxTime,'1') / (K4.L + G(idxTime,'1'));
synthesisE2(idxTime) .. vEsyn(idxTime,'2') =E= V('5') * G(idxTime,'2') / (K5.L + G(idxTime,'2'));
synthesisE3(idxTime) .. vEsyn(idxTime,'3') =E= V('6') * G(idxTime,'3') / (K6.L + G(idxTime,'3'));

derivativeM1(idxTime) .. ddtM(idxTime, '1') =E= 
            E(idxTime,'1') * (S - M(idxTime,'1')) / (1 + S + M(idxTime,'1'))
            - E(idxTime,'2') * (M(idxTime,'1') - M(idxTime,'2')) / (1 + M(idxTime,'1') + M(idxTime,'2'));
derivativeM2(idxTime) .. ddtM(idxTime, '2') =E= 
            E(idxTime,'2') * (M(idxTime,'1') - M(idxTime,'2')) / (1 + M(idxTime,'1') + M(idxTime,'2'))
            - E(idxTime,'3') * (M(idxTime,'2') - P) / (1 + M(idxTime,'2') + P);

updatemRNA1(idxTime+1) .. G(idxTime+1,'1') =E= dt * vGsyn(idxTime,'1') + (1 - dt * k('1')) * G(idxTime,'1');
updatemRNA2(idxTime+1) .. G(idxTime+1,'2') =E= dt * vGsyn(idxTime,'2') + (1 - dt * k('2')) * G(idxTime,'2');
updatemRNA3(idxTime+1) .. G(idxTime+1,'3') =E= dt * vGsyn(idxTime,'3') + (1 - dt * k('3')) * G(idxTime,'3');

odeE1(idxTime+1) .. E(idxTime+1,'1') =E= dt * vEsyn(idxTime,'1') + (1 - dt * k('4')) * E(idxTime,'1');
odeE2(idxTime+1) .. E(idxTime+1,'2') =E= dt * vEsyn(idxTime,'2') + (1 - dt * k('5')) * E(idxTime,'2');
odeE3(idxTime+1) .. E(idxTime+1,'3') =E= dt * vEsyn(idxTime,'3') + (1 - dt * k('6')) * E(idxTime,'3');

odeM(idxTime+1,i2) .. M(idxTime+1,i2) =E= M(idxTime,i2) + dt*ddtM(idxTime,i2);

*Objective function
*SSE:
objective .. objectiveVar =E= sum(idxData$ (ord(idxData)>1),
      sqr(M(idxData,'1')-meas(idxData,'M1'))
    + sqr(M(idxData,'2')-meas(idxData,'M2'))
    + sqr(G(idxData,'1')-meas(idxData,'G1'))
    + sqr(G(idxData,'2')-meas(idxData,'G2'))
    + sqr(G(idxData,'3')-meas(idxData,'G3'))
    + sqr(E(idxData,'1')-meas(idxData,'E1'))
    + sqr(E(idxData,'2')-meas(idxData,'E2'))
    + sqr(E(idxData,'3')-meas(idxData,'E3')) );
*MSE
*objective .. objectiveVar =E= sum(idxData$ (ord(idxData)>1),
*      sqr(M(idxData,'1')-meas(idxData,'M1'))
*    + sqr(M(idxData,'2')-meas(idxData,'M2'))
*    + sqr(G(idxData,'1')-meas(idxData,'G1'))
*    + sqr(G(idxData,'2')-meas(idxData,'G2'))
*    + sqr(G(idxData,'3')-meas(idxData,'G3'))
*    + sqr(E(idxData,'1')-meas(idxData,'E1'))
*    + sqr(E(idxData,'2')-meas(idxData,'E2'))
*    + sqr(E(idxData,'3')-meas(idxData,'E3')) )/card(idxData);


*
* Optimization problem
*

*Model information and options
Model tsp / all /;

*Optimality tolerances, time and solver
option OPTCA = 0.01;
option OPTCR = 0.01;
option RESLIM = 82800;
option NLP = BARON;

*Solve statement
solve tsp using NLP minimizing objectiveVar;