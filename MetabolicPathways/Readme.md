# MetabolicPathways

Parameter estimation problem for fitting metabolic pathways, namely model TSP of benchmark library [BioPreDyn-bench](https://www.doi.org/10.1186/s12918-015-0144-4).
Note that we transformed the dynamic optimization problem _TSP_ of [Villaverde et al. (2019)](https://www.doi.org/10.1093/bioinformatics/bty736) into an algebraic optimization problem by applying the explicit Euler scheme. 
In the C++ header files for MAiNGO, we provide the summed squared prediction error and the mean squared prediction error as the objective for the standard B&B algorithm as well as the squared prediction error for each data point as the objective per data used in the B&B algorithm with growing datasets, see [manual of MAiNGO](https://avt-svt.pages.rwth-aachen.de/public/maingo/special_uses.html#growing_datasets) and [Sass et al. (2024a)](https://doi.org/10.1016/j.ejor.2024.02.020).
In the GAMS file, we formulate all model equations as equalities in dependence of the data points to increase the readability of the model.
For switching between instances TSPexact and TSPnoisy, please choose the respective string *tspModel*, see Line 32 of *MetabolicPathways.h*, or call the respective csv-file, see Line 95 of *MetabolicPathways.gms*.

The data of the differential states, i.e., concentrations G<sub>1</sub>, G<sub>2</sub>, G<sub>3</sub>, E<sub>1</sub>, E<sub>2</sub>, E<sub>3</sub>, M<sub>1</sub>, and M<sub>2</sub>, is stored in subsequent columns.
The rows represent time points 0, 6, 12, ..., 120.

The general model was originally proposed in the following publication:
 - Moles, C. G., Mendes, P., Banga, J. R. (2003). [Parameter estimation in biochemical pathways: a comparison of global optimization methods](https://www.doi.org/10.1101/gr.1262503). *Genome research*, 13 (11), 2467-2474.
 - Villaverde, A. F., Fröhlich, F., Weindl, D., Hasenauer, J., Banga, J. R. (2019). [Benchmarking optimization methods for parameter estimation in large kinetic models](https://www.doi.org/10.1093/bioinformatics/bty736). *Bioinformatics*, 35 (5), 830-838.

The problem with noise-free measurement data was originally implemented for the following publication:
 - Sass, S., Mitsos, A., Bongartz, D., Bell, I. H., Nikolov, N. I., & Tsoukalas, A. (2024a). [A branch-and-bound algorithm with growing datasets for large-scale parameter estimation](https://doi.org/10.1016/j.ejor.2024.02.020). *European Journal of Operational Research*, 316 (1), 36-45.
 
 The noisy measurement data was originally generated for the following publication:
 - Sass, S., Mitsos, A., Nikolov, N. I., & Tsoukalas, A. (2024b).  Out-of-sample estimation for a branch-and-bound algorithm with growing datasets. Submitted.
 
<!-- Empty spaces in last column are inserted to increase column width -->
| Instance        | Problem Type | Number of Continuous Variables | Number of Integer Variables | Number of Binary Variables | Number of Equalities | Number of Inequalities | Best Known Objective Value* | Proven Objective Bound* | Description&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|
| ------------- |-------------| ------------| ------------| ------------| ------------| ------------| :------------: | :-----: | ------------- |
| MetabolicPathways_TSPexact | NLP-DO | 12 | 0 | 0 | 0 | 0 |  &#949;** | 0 | Model TSP of Sass et al. (2024a/b): Dataset contains synthetic exact measurement data generated with the model for one pair of input values |
| MetabolicPathways_TSPnoisy | NLP-DO | 12 | 0 | 0 | 0 | 0 | 1.8817E-02*** | 4.2550E-04*** | Model TSPnoisy of Sass et al. (2024b): Dataset contains synthetic noisy measurement data generated with the model for one pair of input values  |

*The values correspond to using the summed squared error as the objective function.

**Since we use exact synthetic measurements, after convergence the global solution is smaller than the optimality tolerance &#949;.

***Calculated with the SSE heuristic of the B&B algorithm with growing datasets applying augmentation rule SCALING using the parameter values of Moles et al. (2003) as initial solution (CLP by J.J. Forrest et al. for lower bounding, CPU time limit of 23h, rest are default settings; Intel Xeon Platinum 8160 processors ``SkyLake'' with frequency 2.1GHz, 3.75GB RAM)